<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'name'=>'Lafarge',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'api\modules\v1\controllers',
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ],
    ],
    'components' => [
        'request' => [
            /* 'csrfParam' => '_csrf-api', */
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'identityClass' => 'api\modules\v1\models\User',
            'enableSession' => false,
            'loginUrl' => null,
            /* 'enableAutoLogin' => true, */
            /* 'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true], */
        ],
        /* 'session' => [ */
        /*     // this is the name of the session cookie used for login on the frontend */
        /*     'name' => 'advanced-frontend', */
        /* ], */
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        /* 'errorHandler' => [ */
        /*     'errorAction' => 'site/error', */
        /* ], */
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['country'],
                ]
            ],
        ],
    ],
    'params' => $params,
];
