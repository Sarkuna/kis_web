<?php
namespace api\modules\v1\controllers;

use yii\rest\Controller;
use Yii;
use yii\web\Response;
use common\models\VIPCustomerAddress;


class AddressBookController extends Controller
{
    /* public $modelClass = '\common\models\UserDriver'; */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            /* pass access-token as a parameter */
            'class' => yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                \yii\filters\auth\QueryParamAuth::className(),
                \yii\filters\auth\HttpBearerAuth::className(), 
            ]
        ];
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    

    /**
    * @api {get} /index Address Book
    * @apiGroup Address Book
    * @apiSuccess {Object[]} address-book Address Book
    * @apiParam {Integer} userID User ID
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *       "id": 23, "default_address": 0, "name": "Shihan Shan", "address": "123455 Sermban Sermban", "stateid": 10, "statename": "Negeri Sembilan"
    *       ...
    *       },
    *   ]
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "empty"
    *   }
    */
    public function actionIndex()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $user_id = Yii::$app->user->id;
        $address_lists = \common\models\VIPCustomerAddress::find()
            //->select('vip_customer_address_id,firstname,lastname,company,date_of_receipt')
            ->where([
                'userID' => [$user_id],
                'clientID' => [$clientID]
        ])->orderBy(['default_ID'=>SORT_DESC])->all();
        
        if (count($address_lists) == 0) {
            return ['message' => 'empty'];
        }else {
            $mylist = array();
            foreach ($address_lists as $myaddress) {
                if($myaddress->default_ID == 1) {
                    $defaultadd = 1;
                }else {
                    $defaultadd = 0;
                }
                $name = $myaddress->firstname.' '.$myaddress->lastname;
                $address =  $myaddress->address_1 . ' ' . $myaddress->address_2 . ' ' . $myaddress->city;
                $postcode = $myaddress->states->state_name . ' ' . $myaddress->postcode;
                $stateid = $myaddress->state;
                $statename = $myaddress->states->state_name;
                
                $mylist[] = ['id' => $myaddress->vip_customer_address_id, 'default_address' => $defaultadd, 'name' => $name, 'address' => $address, 'postcode' => $postcode, 'country' => $myaddress->country->name, 'stateid' => $stateid, 'statename' => $statename];
            }
            return [
                'address_lists' => $mylist,
            ];
        }
        
    }
    
    /**
    * @api {post} /address-book/new-address Add New
    * @apiGroup Address Book
    * @apiParam {String} company Company
    * @apiParam {String} first_name First Name (*)
    * @apiParam {String} last_name Last Name (*)
    * @apiParam {String} address_1 Address 1 (*)
    * @apiParam {String} address_2 Address 2 (*)
    * @apiParam {String} postcode Postcode (*)
    * @apiParam {String} city City (*)
    * @apiParam {Integer} region Region / State (*)
    * @apiParam {Integer} country Country (*)  
    * @apiSuccess {Boolean} message create successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": true
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionNewAddress()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $company = Yii::$app->request->post('company');
        $first_name = Yii::$app->request->post('first_name');
        $last_name = Yii::$app->request->post('last_name');
        
        $address_1 = Yii::$app->request->post('address_1');
        $address_2 = Yii::$app->request->post('address_2');
        $postcode = Yii::$app->request->post('postcode');
        $city = Yii::$app->request->post('city');
        $region = Yii::$app->request->post('region');
        $country = Yii::$app->request->post('country');
        
        $newaddress = new \common\models\VIPCustomerAddress();
        $newaddress->userID = Yii::$app->user->id;
        $newaddress->clientID = $clientID;
        $newaddress->company = $company;
        $newaddress->firstname = $first_name;
        $newaddress->lastname = $last_name;
        
        $newaddress->address_1 = $address_1;
        $newaddress->address_2 = $address_2;
        $newaddress->city = $city;
        $newaddress->state = $region;
        $newaddress->postcode = $postcode;
        $newaddress->country_id = $country;
        
        if($newaddress->save()){
            return ['message' => true];
        }
        
        return ['message' => false];
        
    }
    
    /**
    * @api {post} /address-book/edit-address Edit
    * @apiGroup Address Book
    * @apiParam {Integer} id ID (*)  
    * @apiParam {String} company Company
    * @apiParam {String} first_name First Name (*)
    * @apiParam {String} last_name Last Name (*)
    * @apiParam {String} address_1 Address 1 (*)
    * @apiParam {String} address_2 Address 2 (*)
    * @apiParam {String} postcode Postcode (*)
    * @apiParam {String} city City (*)
    * @apiParam {Integer} region Region / State FK (*)
    * @apiParam {Integer} country Country FK (*)  
    * @apiSuccess {Boolean} message create successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": true
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionEditAddress()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $id = Yii::$app->request->post('id');
        $company = Yii::$app->request->post('company');
        $first_name = Yii::$app->request->post('first_name');
        $last_name = Yii::$app->request->post('last_name');
        
        $address_1 = Yii::$app->request->post('address_1');
        $address_2 = Yii::$app->request->post('address_2');
        $postcode = Yii::$app->request->post('postcode');
        $city = Yii::$app->request->post('city');
        $region = Yii::$app->request->post('region');
        $country = Yii::$app->request->post('country');
        
        $newaddress = \common\models\VIPCustomerAddress::find()
                ->where([
                    'vip_customer_address_id' => $id,
            ])->one();
        $newaddress->userID = Yii::$app->user->id;
        $newaddress->clientID = $clientID;
        $newaddress->company = $company;
        $newaddress->firstname = $first_name;
        $newaddress->lastname = $last_name;
        
        $newaddress->address_1 = $address_1;
        $newaddress->address_2 = $address_2;
        $newaddress->city = $city;
        $newaddress->state = $region;
        $newaddress->postcode = $postcode;
        $newaddress->country_id = $country;
        
        if($newaddress->save()){
            return ['message' => true];
        }
        
        return ['message' => false];
        
    }
    
    /**
    * @api {post} /address-book/view-address View Address
    * @apiGroup Address Book
    * @apiParam {String} company Company
    * @apiParam {String} first_name First Name (*)
    * @apiParam {String} last_name Last Name (*)
    * @apiParam {String} address_1 Address 1 (*)
    * @apiParam {String} address_2 Address 2 (*)
    * @apiParam {String} postcode Postcode (*)
    * @apiParam {String} city City (*)
    * @apiParam {Integer} region Region / State FK (*)
    * @apiParam {Integer} country Country FK (*)  
    * @apiSuccess {Boolean} message create successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": true
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionViewAddress($id)
    {
        $clientID = Yii::$app->user->identity->client_id;
        $user_id = Yii::$app->user->id;
        $myaddress = \common\models\VIPCustomerAddress::find()
            //->select('vip_customer_address_id,firstname,lastname,company,date_of_receipt')
            ->where([
                'vip_customer_address_id' => $id,
                'userID' => [$user_id],
                'clientID' => [$clientID]
        ])->one();
        
        if (count($myaddress) == 0) {
            return ['message' => 'empty'];
        }else {
            $mylist = array();
            $mylist[] = [
                'id' => $myaddress->vip_customer_address_id,
                'company' => $myaddress->company,
                'firsr_name' => $myaddress->firstname,
                'last_name' => $myaddress->lastname, 
                'address_1' => $myaddress->address_1, 
                'address_2' => $myaddress->address_2,
                'city' => $myaddress->city,
                'postcode' => $myaddress->postcode,
                'state' => $myaddress->state,
                'country' => $myaddress->country_id
            ];
            return [
                'address_lists' => $mylist,
            ];
        }
        
    }
    
    /**
    * @api {get} /address-book/default-address Default Address
    * @apiGroup Address Book
    * @apiParam {Integer} id ID  PK(*)  
    * @apiSuccess {Boolean} message create successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": true
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionDefaultAddress($id)
    {
        $clientID = Yii::$app->user->identity->client_id;
        //$id = Yii::$app->request->post('id');
        VIPCustomerAddress::updateAll(['default_ID' => 0], ['userID' => Yii::$app->user->id]);
        $newaddress = \common\models\VIPCustomerAddress::find()
                ->where([
                    'vip_customer_address_id' => $id,
            ])->one();
        $newaddress->default_ID = 1;
        
        if($newaddress->save()){
            return ['message' => true];
        }
        
        return ['message' => false];
        
    }
    
}