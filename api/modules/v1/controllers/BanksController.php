<?php
namespace api\modules\v1\controllers;

use yii\rest\Controller;
use Yii;
use yii\web\Response;


class BanksController extends Controller
{
    /* public $modelClass = '\common\models\UserDriver'; */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            /* pass access-token as a parameter */
            'class' => yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                \yii\filters\auth\QueryParamAuth::className(),
                \yii\filters\auth\HttpBearerAuth::className(), 
            ]
        ];
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    

    /**
    * @api {get} /index Banks
    * @apiGroup Banks
    * @apiSuccess {Object[]} banks Banks
    * @apiParam {Integer} userID User ID
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *           "id": 5,
    *           "bank_name": "EON BANK",
    *           "account_name": "Shihan",
    *           "account_number": "1234535435",,
    *       ...
    *       },
    *   ]
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "empty"
    *   }
    */
    public function actionIndex()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $user_id = Yii::$app->user->id;
        $bank_lists = \common\models\VIPCustomerBank::find()
            //->select('vip_customer_address_id,firstname,lastname,company,date_of_receipt')
            ->where([
                'userID' => [$user_id],
                'clientID' => [$clientID]
        ])->all();
        
        if (count($bank_lists) == 0) {
            return ['message' => 'empty'];
        }else {
            $mylist = array();
            foreach ($bank_lists as $mybanks) {
                $mylist[] = ['id' => $mybanks->vip_customer_bank_id, 'bank_name' => $mybanks->bank->bank_name, 'account_name' => $mybanks->account_name, 'account_number' => $mybanks->account_number, 'nric_passport' => $mybanks->nric_passport, 'verify' => $mybanks->statusapi];
            }
            return [
                'bank_lists' => $mylist,
            ];
        }
        
    }
    
    /**
    * @api {post} /banks/new-bank Add New
    * @apiGroup Banks
    * @apiParam {Integer} bank_name_id Bank Name FK (*)
    * @apiParam {String} account_name Account Name (*)
    * @apiParam {String} account_number Account Name (*)
    * @apiParam {String} nric_passport NRIC/Passport (*)
    * @apiSuccess {Boolean} message create successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": true
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionNewBank()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $bank_name_id = Yii::$app->request->post('bank_name_id');
        $account_name = Yii::$app->request->post('account_name');
        $account_number = Yii::$app->request->post('account_number');        
        $nric_passport = Yii::$app->request->post('nric_passport');
        
        
        $newbank = new \common\models\VIPCustomerBank();
        $newbank->userID = Yii::$app->user->id;
        $newbank->clientID = $clientID;
        $newbank->bank_name_id = $bank_name_id;
        $newbank->account_name = $account_name;
        $newbank->account_number = $account_number;        
        $newbank->nric_passport = $nric_passport;
        
        if($newbank->save()){
            return ['message' => true];
        }
        
        return ['message' => false];
        
    }
    
    /**
    * @api {post} /banks/edit-address Edit
    * @apiGroup Banks
    * @apiParam {Integer} id ID PK (*)  
    * @apiParam {Integer} bank_name_id Bank Name FK (*)
    * @apiParam {String} account_name Account Name (*)
    * @apiParam {String} account_number Account Name (*)
    * @apiParam {String} nric_passport NRIC/Passport (*) 
    * @apiSuccess {Boolean} message create successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": true
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionEditBank()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $id = Yii::$app->request->post('id');
        $bank_name_id = Yii::$app->request->post('bank_name_id');
        $account_name = Yii::$app->request->post('account_name');
        $account_number = Yii::$app->request->post('account_number');        
        $nric_passport = Yii::$app->request->post('nric_passport');
        
        $editbank = \common\models\VIPCustomerBank::find()->where(['vip_customer_bank_id' => $id])->one();
        $editbank->userID = Yii::$app->user->id;
        $editbank->clientID = $clientID;
        $editbank->bank_name_id = $bank_name_id;
        $editbank->account_name = $account_name;
        $editbank->account_number = $account_number;        
        $editbank->nric_passport = $nric_passport;
        
        if($editbank->save()){
            return ['message' => true];
        }
        
        return ['message' => false];
        
    }
    
    /**
    * @api {get} /banks/view-bank View All Banks
    * @apiGroup Banks
    * @apiSuccess {Object[]} banks Banks
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *           "bank_info":{
    *               "id": 6,
    *               "bank_name ID": 5,
    *               "account_name": "Mohamed Abu",
    *               "account_number": "587485855582",
    *               "nric_passport": "25857458",
    *               "bank_status": "P"'
    *           }
    *       }
    *   ]
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionViewBank($id)
    {
        $clientID = Yii::$app->user->identity->client_id;
        $user_id = Yii::$app->user->id;
        $mybank = \common\models\VIPCustomerBank::find()
            //->select('vip_customer_address_id,firstname,lastname,company,date_of_receipt')
            ->where([
                'vip_customer_bank_id' => $id,
                'userID' => [$user_id],
                'clientID' => [$clientID]
        ])->one();
        
        if (count($mybank) == 0) {
            return ['message' => false];
        }else {
            $mylist = array();
            $mylist[] = [
                'id' => $mybank->vip_customer_bank_id,
                'bank_name_id' => $mybank->bank_name_id,
                'account_name' => $mybank->account_name,
                'account_number' => $mybank->account_number, 
                'nric_passport' => $mybank->nric_passport, 
                'bank_status' => $mybank->bank_status
            ];
            return [
                'bank_info' => $mylist,
            ];
        }
        
    }
}