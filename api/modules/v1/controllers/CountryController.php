<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;

/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class CountryController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Country';


    /**
    * @api {get} country/country Country
    * @apiGroup Country
    * @apiSuccess {Object[]} country Country
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "country_id": 129,
    *       "name": "Malaysia",
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 404 Not Found
    *   {
    *       "name": "Not Found",
    *       "message": "Object not foud",
    *       "code": 0,
    *       "status": 404,
    *       "type": "yii\\web\\NotFoundHttpException"
    *   }
    */
    public function actionCountry()
    {
        $country = \common\models\VipCountry::find()
            ->select(['country_id','name'])
            //->where(['country_id' => '129'])
            ->orderBy([
                'name' => SORT_ASC,
            ])->all();
        return $country;
    }
    
    /**
    * @api {get} country/regionlists Region Lists
    * @apiGroup Country
    * @apiSuccess {Object[]} country Region Lists
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "zone_id": 1,
    *       "name": "Peninsular",
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 404 Not Found
    *   {
    *       "name": "Not Found",
    *       "message": "Object not foud",
    *       "code": 0,
    *       "status": 404,
    *       "type": "yii\\web\\NotFoundHttpException"
    *   }
    */
    public function actionRegionlists()
    {
        $regionlists = \common\models\VIPZone::find()
            ->select(['zone_id','name'])
            ->where(['country_id' => '129'])
            ->orderBy([
                'name' => SORT_ASC,
            ])->all();
        return $regionlists;
    }
    
    /**
    * @api {get} country/region Region Lists
    * @apiGroup Country
    * @apiSuccess {Object[]} country Region Lists
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "zone_id": 1,
    *       "name": "Peninsular",
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 404 Not Found
    *   {
    *       "name": "Not Found",
    *       "message": "Object not foud",
    *       "code": 0,
    *       "status": 404,
    *       "type": "yii\\web\\NotFoundHttpException"
    *   }
    */
    public function actionRegion()
    {
        $cid = 129;
        $regionlists = \common\models\VIPStates::find()
        ->select(['states_id','state_name'])        
        ->where(['country_id' => $cid])
                ->orderBy([
            'state_name' => SORT_ASC,
        ])->all();
        return $regionlists;
    }
    
    /**
    * @api {get} country/bank Bank Lists
    * @apiGroup Country 
    * @apiSuccess {Object[]} country Bank Lists
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [{
    *       "id": 1,
    *       "bank_name": "Affin Bank Berhad",
    *   }]
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 404 Not Found
    *   {
    *       "name": "Not Found",
    *       "message": "Object not foud",
    *       "code": 0,
    *       "status": 404,
    *       "type": "yii\\web\\NotFoundHttpException"
    *   }
    */
    public function actionBank()
    {
        $banks = \common\models\Banks::find()->select(['id','bank_name'])->orderBy(['bank_name' => SORT_ASC,])->all();
        return $banks;
    }
    
    
    
}