<?php
namespace api\modules\v1\controllers;

use yii\rest\Controller;
use Yii;

class ListsController extends Controller
{
    /* public $modelClass = '\common\models\User'; */
    public $modelClass = '\common\models\Salutation';
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['post'],
            ],
        ];
        return $behaviors;
    }


    /**
    * @api {get} lists/salutation Salutation
    * @apiGroup Lists
    * @apiSuccess {Object[]} salutation Salutation
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "salutation_id": 1,
    *       "name": "Mr",
    *       "position": 1,
    *       "status": "A",
    *       ...
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 404 Not Found
    *   {
    *       "name": "Not Found",
    *       "message": "Object not foud: salutation_id",
    *       "code": 0,
    *       "status": 404,
    *       "type": "yii\\web\\NotFoundHttpException"
    *   }
    */
    public function actionSalutation()
    {
        $salutation = \common\models\Salutation::find()
            ->where([
                'status' => 'A',
        ])->orderBy('position')->all();
        return $salutation;
    }
    
    /**
    * @api {get} lists/states States
    * @apiGroup Lists
    * @apiSuccess {Object[]} states Malaysia States Only
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "zone_id": 1971,
    *       "country_id": 129,
    *       "name": "Johor",
    *       "code": "MY-01",
    *       "status": 1
    *       ...
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 404 Not Found
    *   {
    *       "name": "Not Found",
    *       "message": "Object not foud",
    *       "code": 0,
    *       "status": 404,
    *       "type": "yii\\web\\NotFoundHttpException"
    *   }
    */
    public function actionStates()
    {
        $statesall = \common\models\VIPZone::find()
            ->where([
                'country_id' => 129,
                'status' => 0,
        ])->orderBy('code')->all();
        return $statesall;
    }
    
    /**
    * @api {get} /lists/race Race List
    * @apiGroup Lists
    * @apiSuccess {Object[]} race Race
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "races_id": 1,
    *       "name": "Malay",
    *       "sort_order": 1,
    *   }  
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "Empty"
    *   }
    */
    public function actionRace()
    {
        $raceslists = \common\models\Races::find()->orderBy([
            'sort_order' => SORT_ASC,
        ])->all();
        

        return $raceslists;
    }
}
