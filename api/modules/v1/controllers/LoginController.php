<?php
namespace api\modules\v1\controllers;

use yii\rest\Controller;
use Yii;

use api\modules\v1\models\PasswordResetRequestForm;
use api\modules\v1\components\Helpers;

class LoginController extends Controller
{
    /* public $modelClass = '\common\models\User'; */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['post'],
            ],
        ];
        return $behaviors;
    }

    /**
    * @api {post} /login Login
    * @apiGroup Login
    * @apiParam {String} email  Email
    * @apiParam {String} password Password
    * @apiParam {Integer} clientID Client ID for Lafarge 12 Kansai 10
    * @apiSuccess {Object[]} userinfo User Information
    * @apiSuccess {String} access_token Access Token
    * @apiSuccess {String} status Status A=Verified Account N=Not Verified
    * @apiSuccess {Integer} type Type 1=Distributors, 2=Dealer, 5=Indirect
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "userinfo": {
    *           ""vip_customer_id": 649,
    *           "userID": 656,
    *           "userID": 12,
    *           "full_name": "Shihan"
    *           "mobile_no": "01115550000",
    *           "nric_passport_no": "856985744",
    *           "CIDB_registration_number": "A 8585",
    *           "CIDB_Grade": "G",
    *           "company_name": "API Company",
    *           "...": "..."
    *       },
    *       "access_token": "ozW2gO3fnYpJujFWo5DhMmm8nMsqtQTR",
    *       "status" : "N"
    *       "Type": 5
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionIndex()
    {
        $accessToken = null;
        
        $request = Yii::$app->request;
        $clientID = $request->post('clientID');
        $email = $request->post('email');
        
        if (strpos($email, '@') !== false) {
            $user = \common\models\User::findOne(['email' => $email, 'status'=>['A','N'], 'client_id' => $clientID,'user_type' => 'D']);
        } else {
            //Otherwise we search using the username
            $user = \common\models\User::findOne(['username' => $email, 'status'=>['A','N'], 'client_id' => $clientID, 'user_type' => 'D']);
        }
        
        $password = $request->post('password');
        
        if($user != null && $user->validatePassword($password))
        {
            $user->access_token = Yii::$app->security->generateRandomString();
            date_default_timezone_set("Asia/Kuala_Lumpur");
            $user->date_last_login = date('Y-m-d H:i:s');
            $user->save(false);
            $accessToken = $user->access_token;
            
            $profile = \common\models\VIPCustomer::find()
                    ->where(['userID' => $user->id])->one();
            Helpers::addtolog('success', 'Login success', $user->id, $clientID, null);
            return [
                    "userinfo" => $profile,
                    "access_token" => $user->access_token,
                    "status" => $user->status,
                    "Type" => $user->type
                ];
        }
        return [ 'message' => false ];
    }
    
    /**
    * @api {post} /login/signup Register
    * @apiGroup Login
    * @apiParam {Integer} clientID Client ID (FK) 
    * @apiParam {Integer} salutation Salutation ID (FK)
    * @apiParam {String} full_name Full Name
    * @apiParam {String} company_name Company Name
    * @apiParam {String} email Email 
    * @apiParam {String} mobile_no Mobile No eg: +60XXXXXXXX
    * @apiParam {String} password Password
    * @apiSuccess {Boolean} message True or False
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": true      
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */

    public function actionSignup()
    {
        $clientID = Yii::$app->request->post('clientID');
        $salutation = Yii::$app->request->post('salutation');
        $full_name = Yii::$app->request->post('full_name');
        $company_name = Yii::$app->request->post('company_name');                      
        $mobile_no = Yii::$app->request->post('mobile_no');
        $email = Yii::$app->request->post('email');
        $password = Yii::$app->request->post('password');
        $account_type = 5;
        
        
        if(!empty($clientID) && !empty($salutation) && !empty($full_name) && !empty($company_name) && !empty($mobile_no) && !empty($email) && !empty($password) && !empty($account_type)) {
            if(!empty($mobile_no)){
                $mobilecheck = \common\models\VIPCustomer::find()->where(['mobile_no'=>$mobile_no, 'clientID' => $clientID])->count();
                if($mobilecheck > 0){
                    return ['message' => 'Mobile '.$mobile_no.' has already been taken'];
                }            
            }

            if(!empty($email)){
                $emailcheck = \common\models\User::find()->where(['email'=>$email, 'client_id' => $clientID])->count();
                if($emailcheck > 0){
                    return ['message' => 'Email '.$email.' has already been taken'];
                }            
            }

                $mob = preg_replace('/(?<=\d)\s+(?=\d)/', '', $mobile_no);
                $user = new \common\models\User();
                $user->client_id = $clientID;
                $user->username = str_replace('+6', '', trim($mob));
                $user->email = trim($email);
                $user->email_verification = 'N';
                $user->status = 'P';
                $user->approved = 'Y';
                $user->newsletter = 'D';
                $user->user_type = 'D';
                $user->type = $account_type;
                $random_str = md5(uniqid(rand()));
                $activationKey = substr($random_str, 0, 8);
                $user->activation_key = $activationKey;

                $user->setPassword($password);
                $user->generateAuthKey();
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $user->save(false)) {
                        if ($flag) {
                            //$userprofile = new UserProfile();
                            $model = new \common\models\VIPCustomer();
                            $model->userID = $user->id;
                            $model->clientID=$clientID;
                            $model->clients_ref_no = date('dmyHis');
                            $model->dealer_ref_no = $clientID.'-'.date('dmyHis');
                            $model->salutation_id = $salutation;
                            $model->full_name = $full_name;                        
                            //$model->company_name = $company_name;                        
                            //$model->nric_passport_no = $nric;                        
                            $model->mobile_no = $mobile_no;
                            /*$model->address_1 = $address_1;
                            $model->city = $city;
                            $model->postcode = $postcode;
                            $model->state = $state;
                            $model->end_user_level_id = $yearly_purchase_volume;
                            $model->CIDB_registration_number = $CIDB_Registration;
                            $model->CIDB_Grade = $CIDB_Grade;*/
                            $model->created_by = $user->id;

                            //$userprofile->user_id = $userprofile->salutation_id;
                            //$userprofile->user_id = $userprofile->salutation_id;

                            if (($flag = $model->save(false)) === false) {      
                                $transaction->rollBack();
                                        //break;
                            }
                            $authassignment = new \common\models\AuthAssignment();
                            $authassignment->item_name = 'Dealer';
                            $authassignment->user_id = $user->id;;
                            $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                            $authassignment->save(false);
                            //AuthAssignment
                            
                            $companyInformation = new \common\models\CompanyInformation();
                            $companyInformation->user_id = $user->id;
                            $companyInformation->company_name = $company_name;
                            $companyInformation->save(false);

                        }
                    }

                if ($flag) {
                    $transaction->commit();
                    $data = \yii\helpers\Json::encode(array(
                        'verification_email_key' => $user->activation_key,
                    ));
                    Helpers::sendEmailSignup($user->id,$data,$clientID);
                    return ['message' => true];
                    //return $user;
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                return $e->getMessage();
                //return ['message' => FALSE];
            }
        } else {
            return ['message' => 'Validation of required field that may be empty or null'];
        }
        
        
    }
    
   

    /**
    * @api {post} /login/mobile-check Check mobile before login
    * @apiGroup Login
    * @apiParam {String} mobile Mobile
    * @apiSuccess {Boolean} message A message of whether mobile exists
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "yes" Allready taken
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionMobileCheck()
    {
        $mobile = Yii::$app->request->post('mobile');
        $flag = \common\models\VIPCustomer::find()
            ->where(['mobile_no' => $mobile])
            ->exists();
        if ($flag) {
            return ['message' => 'Yes'];
        }
        return ['message' => 'No'];
    }
    
    /**
    * @api {post} /login/forgot-password Forgot Password (send OTP)
    * @apiGroup Login
    * @apiParam {String} mobile Mobile (+601118889597)
    * @apiSuccess {Boolean} message A message return value
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "yes" OTP code send to given mobile number
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "No" No mean no record found our DB OR "SMS API Error code" can ref this URL http://isms.com.my/sms_api.php#SendSMS_Response
    *   }
    */
    public function actionForgotPassword(){
        $mobile_no = Yii::$app->request->post('mobile_no');
        $flag = \common\models\VIPCustomer::find()
            ->where(['mobile_no' => trim($mobile_no)])
            ->one();
        //if()

        if(count($flag) == 1) {
            $OTP = mt_rand(100000, 999999);
            $destination = trim($mobile_no);
            $message = 'Dear Lafarge member, OTP for change of password is '.$OTP.'. Do not share the OTP with anyone,';
            $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
            $message = urlencode($message);

            $username = urlencode("lafage");
            $password = urlencode("lafage2018");
            $sender_id = urlencode("66300");
            $type = urlencode("1");

            $fp = "https://www.isms.com.my/isms_send_all.php?un=$username&pwd=$password&dstno=$destination&msg=$message&type=$type&sendid=$sender_id&agreedterm=YES";
            $handle = @fopen($fp, "r");
            if ($handle) {
                while (!feof($handle)) {
                    $buffer = fgets($handle, 10000);
                    $st = $buffer;
                }
            }
            if($st == '2000 = SUCCESS' || $st == ''){
                $gsms = new \common\models\SMSOTP();
                $gsms->userID = $flag->userID;
                $gsms->mobile = trim($mobile_no);
                $gsms->otp = $OTP;
                $gsms->save(false);
                return ['message' => 'Yes'];
            }else {
                return ['message' => $st];
            }
        }else {
            return ['message' => 'No'];
        }
    }
    
    /**
    * @api {post} /login/forgot-password-email Forgot password Email
    * @apiGroup Login
    * @apiParam {String} email Email
    * @apiParam {Integer} clientID Client ID for Lafarge 12 Kansai 10 
    * @apiSuccess {Boolean} message Password reset process successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": true
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionForgotPasswordEmail()
    {
        $clientID = Yii::$app->request->post('clientID');
        $email = Yii::$app->request->post('email');
        
        $model = new PasswordResetRequestForm();
        
        $model->setAttributes(Yii::$app->request->post(), false);
        if ($model->validate() && $model->sendEmail()) {
            return ['message' => true];
        }
        return ['message' => false];
    }
    
    /**
    * @api {post} /login/new-password New Password 
    * @apiGroup Login
    * @apiParam {String} otp OTP 6 Digit Code From SMS (654123)
    * @apiParam {String} new_password New Paasword
    * @apiParam {String} retype_password Retype Password
    * @apiSuccess {Boolean} message A message return value
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "yes" Save Sccuess
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Code Invalid" No code found
    *   }
    */
    public function actionNewPassword(){
        $otp = Yii::$app->request->post('otp');
        $new_password = Yii::$app->request->post('new_password');
        $retype_password = Yii::$app->request->post('retype_password');
        $flag = \common\models\SMSOTP::find()
            ->where(['otp' => trim($otp)])
            ->one();
        //if()

        if(count($flag) == 1) {
            if($new_password == $retype_password) {
                $user = \common\models\User::find()
                ->where(['id' => $flag->userID])
                ->one();

                $user->setPassword($retype_password);
                $user->generateAuthKey();
                //$user->save(false);
                if($user->save(false)){
                    //$this->findModel($id)->delete();
                    $flag = \common\models\SMSOTP::findOne($flag->id);
                    $flag->delete();
                    return ['message' => 'Yes'];
                }else {
                    return ['message' => 'Password not update'];
                }
                
            }else {
               return ['message' => 'password not match']; 
            }
        }else {
            return ['message' => 'Code Invalid'];
        }
    }
}
