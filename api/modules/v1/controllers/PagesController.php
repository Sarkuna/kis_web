<?php
namespace api\modules\v1\controllers;

use yii\rest\Controller;
use Yii;
use yii\web\Response;

/* Listing of nature based on condo_id, might be used as some endpoint */
class PagesController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            /* pass access-token as a parameter */
            'class' => yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                \yii\filters\auth\QueryParamAuth::className(),
                \yii\filters\auth\HttpBearerAuth::className(), 
            ]
        ];
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    /**
    * @api {get} /pages/menu Menu Pages
    * @apiGroup Pages
    * @apiParam {icon_font} dev URL = http://kansai.businessboosters.com.my/ Pro URL = http://dealer.mykawankansai.com/ 
    * @apiSuccess {Object[]} pages Page
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *           "id": 168,
    *           "title": "About Us",
    *           "link": "about-us-2",
    *           "target": "_self",
    *           "parent": "0",
    *           "sort": 0,
    *           "created_at": "2020-04-21 16:46:16",
    *           "updated_at": null,
    *           "class": "",
    *           "icon_font": "",
    *           "menu_id": 6,
    *           "related_id": 73,
    *           "type": "page",
    *           "depth": 0,
    *       }
    *   ]
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "empty"
    *   }
    */    
    public function actionMenu()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $lang = Yii::$app->request->get('lang');

        $pages = \common\models\Menus::find()
                ->where(['clientID' => $clientID])
                ->andWhere(['like', 'display_location', 'app-menu'])
                ->one();
        
        if (count($pages) == 0) {
            return ['message' => 'empty'];
        }
        return $pages->menuitems;
    }
    
    /**
    * @api {get} /pages/view-page View Page
    * @apiGroup Pages
    * @apiParam {Integer} related_id This related_id to view the each page (FK)
    * @apiSuccess {Object[]} pages Page
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *           "page_title": "Product Info",
    *           "page_description": "<p>Sample</p>\r\n",
    *       }
    *   ]
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "empty"
    *   }
    */ 
    public function actionViewPage($related_id)
    {
        //$clientID = Yii::$app->user->identity->client_id;
        //$lang = Yii::$app->request->get('lang');
        $pages = \common\models\Pages::find()
            ->select('page_title, page_description')    
            ->where([
                'page_id' => $related_id,
                'status' => 'publish'
            ])
            ->asArray()    
            ->one();
        if (count($pages) == 0) {
            return ['message' => 'empty'];
        }
        return $pages;
    }
}
