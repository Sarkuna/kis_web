<?php
namespace api\modules\v1\controllers;

use yii\rest\Controller;
use Yii;
use yii\web\Response;
use common\models\VIPCustomerAccount2;
use api\modules\v1\components\Helpers;

class ProfileController extends Controller
{
    /* public $modelClass = '\common\models\UserDriver'; */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            /* pass access-token as a parameter */
            'class' => yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                \yii\filters\auth\QueryParamAuth::className(),
                \yii\filters\auth\HttpBearerAuth::className(), 
            ]
        ];
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    /* public function actions() */
    /* { */
    /*     /1* Unset rest controller index default behavior *1/ */
    /*     $actions = parent::actions(); */
    /*     unset($actions['index']); */
    /*     unset($actions['create']); */
    /*     unset($actions['update']); */

    /*     return $actions; */
    /* } */

    /**
    * @api {get} /profile Profile
    * @apiGroup Profile
    * @apiSuccess {Object[]} profile Profile
    * @apiParam {Integer} userID User ID
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *           "full_name": "Mrs. Shihan Hameed",
    *           "email": "shihanagni@gmail.com",
    *           "date_of_birth": "13-04-1985",
    *           "gender": "Male",
    *           "nric_passport_no": "A856985744",
    *           "mobile_no": "+601118889569",
    *           "telephone_no": "+601118889597",
    *           "race": "Indian",
    *           "address": "52 JLN Seremban ",
    *           "city": "Seremban",
    *           "postcode": "408450",
    *           "nationality_id": "Malaysia",
    *           "country_id": "Malaysia"
    *       },
    *   ]
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "empty"
    *   }
    */
    public function actionIndex()
    {
        $user_id = Yii::$app->user->id;
        $profile = \common\models\VIPCustomer::find()
            //->select('salutation_id, full_name, date_of_Birth, gender, nric_passport_no, mobile_no, telephone_no, Race, Region, nationality_id, country_id')     
            ->where([
                'userID' => [$user_id],
        ])->one();
        $gender = '';
        if($profile->gender == 'M') {
            $gender = 'Male';
        }else if($profile->gender == 'F') {
            $gender = 'Female';
        }
        
        if (count($profile) == 0) {
            return ['message' => 'empty'];
        }
        
        if(!empty($profile->Race)) {
            $race = $profile->races->name;
        }else {
            $race = 'N/A';
        }
        if(!empty($profile->country_id)) {
            $country = $profile->country->name;
            //$nationality = $profile->nationality->name;
        }else {
            $country = 'N/A';
        }
        
        if(!empty($profile->nationality_id)) {
            $nationality = $profile->nationality->name;
            //$nationality = $profile->nationality->name;
        }else {
            $nationality = 'N/A';
        }
        
        return [
            "salutation" => $profile->salutations->name,
            "full_name" => $profile->full_name,
            "email" => $profile->user->email,
            "date_of_birth" => date('d-m-Y', strtotime($profile->date_of_Birth)),
            "gender" => $gender,
            "nric_passport_no" => $profile->nric_passport_no,
            "mobile_no" => $profile->mobile_no, 
            "telephone_no" => $profile->telephone_no, 
            "race" => $race,
            "address" => $profile->address_1.' '.$profile->address_2,
            "city" => $profile->city,
            "postcode" => $profile->postcode,
            "nationality_id" => $nationality, 
            "country_id" => $country
        ];
    }
    
    /**
    * @api {get} /profile/edit Profile Edit
    * @apiGroup Profile
    * @apiSuccess {Object[]} profile Profile
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *           "userinfo":{
    *               "vip_customer_id": 738,
    *               "userID": 910,
    *               "clientID": 10,
    *               "assign_dealer_id": null,
    *               "salutation_id": 1,
    *               "clients_ref_no": "96857425",
    *               "dealer_ref_no": "10-010319233510",
    *               "full_name": "Shihan Shan",
    *               "gender": "M",
    *               ........ 
    *          }
    *       },
    *   ]
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "empty"
    *   }
    */
    public function actionEdit()
    {
        $user_id = Yii::$app->user->id;
        $profile = \common\models\VIPCustomer::find()->where(['userID' => [$user_id]])->one();
        
        return [
            "userinfo" => $profile,
        ];
    }
    
    /**
    * @api {get} /profile/total-available-point Acc 1 Available Point 
    * @apiGroup Profile
    * @apiParam {Integer} userID User ID 
    * @apiSuccess {Object[]} total-available-point TotalAvailablePoint
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Available Balance",
    *       "account1point": 9856
    *   }   
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Available Balance",
    *       "account1point": 0
    *   }
    */
    public function actionTotalAvailablePoint()
    {
        return Helpers::AvailablePointAcc1();
    }       
    
    
    /**
    * @api {get} /profile/point-expiring Acc 1 Point Expiring
    * @apiGroup Profile
    * @apiParam {Integer} userID User ID 
    * @apiSuccess {Object[]} point-expiring Point Expiring    
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Expiring on 31-12-2020",
    *       "total_expiry_account_one": 1000,
    *   } 
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Expiring on 31-12-2020",
    *       "total_expiry_account_one": 0,
    *   }
    */
    public function actionPointExpiring()
    {
        $exppoint1 = Helpers::GoingtoExpireAcc1();

        return [
            'message' => 'Expiring on 31-12-'. date('Y'),
            'total_expiry_account_one' => $exppoint1,
        ];
    }
    

    /**
    * @api {get} /profile/account-two Acc 2 Available Point 
    * @apiGroup Profile
    * @apiParam {Integer} userID User ID 
    * @apiSuccess {Object[]} account-two Account Two
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Available Balance",
    *       "account2point": 9856
    *   }   
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Available Balance",
    *       "account2point": 0
    *   }
    */
    public function actionAccountTwo()
    {
        return Helpers::AvailablePointAcc2();
    }
    
    /**
    * @api {get} /profile/point-expiring-account-two Acc 2 Point Expiring 
    * @apiGroup Profile
    * @apiParam {Integer} userID User ID 
    * @apiSuccess {Object[]} point-expiring-account-two Point Expiring Account Two   
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Expiring on 31-12-2020",
    *       "total_expiry_account_two": 100,
    *   } 
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Expiring on 31-12-2020",
    *       "total_expiry_account_two": 0,
    *   }
    */
    
    public function actionPointExpiringAccountTwo()
    {
        $exppoint = Helpers::GoingtoExpireAcc2();

        return [
            'message' => 'Expiring on 31-12-'. date('Y'),
            'total_expiry_account_two' => $exppoint,
        ];
    }
    
    /**
    * @api {get} /profile/announcement Announcement
    * @apiGroup Profile
    * @apiParam {String} lang Language Code (en,ch,bm)
    * @apiSuccess {Object[]} announcement Announcement    
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *           "message": "Sample Message\r\n",
    *       },
    *   ]
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "empty"
    *   }
    */
    public function actionAnnouncement()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $status = Yii::$app->user->identity->status;
        if($status == 'A') {
            $type = 'A';
        }else {
            $type = 'N';
        }
        $lang = Yii::$app->request->get('lang');
        
        $announcement = \common\models\VIPAnnouncement::find()
            ->where([
                'clientID' => [$clientID],
                'type' => $type,
                'lang' => [$lang],
                'status' => 'E'
        ])->one();
        if (count($announcement) == 0) {
            return ['message' => 'empty'];
        }else {
            return ['message' => $announcement->message];
        }
        //return $announcement->message;
    }
    
    /**
    * @api {put} /profile/update Update
    * @apiGroup Profile
    * @apiParam {Integer} vip_customer_id VIP Customer ID (PK)
    * @apiParam {Integer} salutation Salutation ID (FK)
    * @apiParam {String} full_name Full Name    
    * @apiParam {String} nric NRIC
    * @apiParam {String} date_of_birth Date of Birth
    * @apiParam {String} mobile_no Mobile No
    * @apiParam {String} telephone_no Telephone No
    * @apiParam {String} Race Race
    * @apiParam {String} gender Gender (M or F) 
    * @apiParam {Integer} country_id Country ID  (FK)
    * @apiParam {Integer} Region Region  (FK)
    * @apiParam {Integer} nationality_id Nationality ID  (FK)
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Success"
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Fail"
    *   }
    */
    public function actionUpdate()
    {
        $vip_customer_id = Yii::$app->request->post('vip_customer_id');
        $model = \api\modules\v1\models\VIPCustomer::findOne($vip_customer_id);
        $dob = date('Y-m-d', strtotime(Yii::$app->request->post('date_of_birth')));
        
        $model->salutation_id = Yii::$app->request->post('salutation');
        $model->full_name = Yii::$app->request->post('full_name');
        $model->nric_passport_no = Yii::$app->request->post('nric');        
        $model->gender = Yii::$app->request->post('gender');
        $model->mobile_no = Yii::$app->request->post('mobile_no');
        $model->telephone_no = Yii::$app->request->post('telephone_no');
        $model->Race = Yii::$app->request->post('Race');
        $model->date_of_Birth = $dob;
        $model->country_id = Yii::$app->request->post('country_id');
        $model->Region = Yii::$app->request->post('Region');
        $model->nationality_id = Yii::$app->request->post('nationality_id');

        if($model->save()) {
            //return $model;
            return ['message' => 'Success'];
        } else {
            return ['message' => 'Fail'];
        }
    }
    
    /**
    * @api {post} /profile/change-password Change Password
    * @apiGroup Profile
    * @apiParam {String} new_password New Paasword
    * @apiParam {String} retype_password Retype Password
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "yes" Save Sccuess
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "password not match"
    *   }
    */
    public function actionChangePassword(){
        $new_password = Yii::$app->request->post('new_password');
        $retype_password = Yii::$app->request->post('retype_password');

        if ($new_password == $retype_password) {
            $user = \common\models\User::find()
                    ->where(['id' => Yii::$app->user->id])
                    ->one();

            $user->setPassword($retype_password);
            $user->generateAuthKey();
            //$user->save(false);
            if ($user->save(false)) {
                return ['message' => 'Yes'];
            } else {
                return ['message' => 'Password not update'];
            }
        } else {
            return ['message' => 'password not match'];
        }
    }
    
    /**
    * @api {get} /profile/company Company
    * @apiGroup Profile
    * @apiSuccess {Object[]} company Company
    * @apiParam {Integer} userID User ID
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *           "id": 25
    *           "company_name": "ABC Sdn Bhd",
    *           "contact_person": "Shihan",
    *           "contact_no": "+601118889597",
    *           "mailing_address": "52 JLN Seremban ",
    *           "tel": "+60378889597",
    *           "fax": "+60378889597",
    *           "registration_type": "SSM",
    *           "registration_number": "23455"
    *       },
    *   ]
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "empty"
    *   }
    */
    public function actionCompany()
    {
        $user_id = Yii::$app->user->id;
        $company = \common\models\CompanyInformation::find()
            //->select('salutation_id, full_name, date_of_Birth, gender, nric_passport_no, mobile_no, telephone_no, Race, Region, nationality_id, country_id')     
            ->where([
                'user_id' => [$user_id],
        ])->one();
        
        if(!empty($company->company_reg_type)){
            $rtype = $company->registration->name;
        }else {
            $rtype = 'N/A';
        }
        
        return [
            "id" => $company->id,
            "company_name" => $company->company_name,
            "contact_person" => $company->contact_person,
            "contact_no" => $company->contact_no,
            "mailing_address" => $company->mailing_address,
            "tel" => $company->tel,
            "fax" => $company->fax,
            "registration_type" => $rtype, 
            "registration_number" => $company->company_registration_number
        ];
    }
    
    /**
    * @api {put} /profile/update-company Update Company
    * @apiGroup Profile
    * @apiParam {Integer} id  ID (PK)
    * @apiParam {String} company_name Company Name  
    * @apiParam {String} contact_person Contact Person
    * @apiParam {String} contact_no Contact No
    * @apiParam {String} mailing_address Mailing Address
    * @apiParam {String} tel Tel
    * @apiParam {String} fax Fax
    * @apiParam {Integer} registration_type Registration Type (FK)
    * @apiParam {String} registration_number Registration Number
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Success"
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Fail"
    *   }
    */
    public function actionUpdateCompany()
    {
        $id = Yii::$app->request->post('id');
        
        $model = \common\models\CompanyInformation::find()->where(['id' => [$id]])->one();
        $model->company_name = Yii::$app->request->post('company_name');
        $model->contact_person = Yii::$app->request->post('contact_person');
        $model->contact_no = Yii::$app->request->post('contact_no');        
        $model->mailing_address = Yii::$app->request->post('mailing_address');
        $model->tel = Yii::$app->request->post('tel');
        $model->fax = Yii::$app->request->post('fax');
        $model->company_reg_type = Yii::$app->request->post('registration_type');
        $model->company_registration_number = Yii::$app->request->post('registration_number');

        if($model->save()) {
            //return $model;
            return ['message' => 'Success'];
        } else {
            //print_r($model->getErrors());
            return ['message' => 'Fail'];
        }
    }
    
}