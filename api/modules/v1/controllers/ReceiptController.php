<?php
namespace api\modules\v1\controllers;

use yii\rest\Controller;
use Yii;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;
use api\modules\v1\models\VIPReceipt;

class ReceiptController extends Controller
{
    /* public $modelClass = '\common\models\UserDriver'; */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            /* pass access-token as a parameter */
            'class' => yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                \yii\filters\auth\QueryParamAuth::className(),
                \yii\filters\auth\HttpBearerAuth::className(), 
            ]
        ];
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    /* public function actions() */
    /* { */
    /*     /1* Unset rest controller index default behavior *1/ */
    /*     $actions = parent::actions(); */
    /*     unset($actions['index']); */
    /*     unset($actions['create']); */
    /*     unset($actions['update']); */

    /*     return $actions; */
    /* } */

    /**
    * @api {get} /profile Profile
    * @apiGroup Profile
    * @apiSuccess {Object[]} profile Profile
    * @apiParam {Integer} userID User ID
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *       "vip_customer_id": 30,
    *       "userID": 64,
    *       "clientID": 12,
    *       "full_name": "Shihan API",
    *       "address_1": "52 JLN Seremban",
    *       "city": "Seremban",
    *       "CIDB_registration_number": "A 8585",
    *       "CIDB_Grade": "G",
    *       ...
    *       },
    *   ]
    * @apiSuccessExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "empty"
    *   }
    */
    public function actionIndex()
    {
        $user_id = Yii::$app->user->id;
        $profile = \common\models\VIPCustomer::find()
            ->where([
                'userID' => [$user_id],
        ])->one();
        if (count($profile) == 0) {
            return ['message' => 'empty'];
        }
        return $profile;
    }
    
    /**
    * @api {post} /receipt/upload-receipt Upload Receipt
    * @apiGroup Recepit
    * @apiParam {File} upfile[] Recepit attachment no of files MIN 1 MAX 5
    * @apiParam {String} invoice_no Invoice No (*)
    * @apiParam {Date} date_of_receipt Date of Receipt
    * @apiSuccess {Boolean} message Recepit upload files successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "message": true
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionUploadReceipt()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $invoice_no = Yii::$app->request->post('invoice_no');
        $date_of_receipt = Yii::$app->request->post('date_of_receipt');
        
        $msg = 0;
        $timestamp = date('Y-m', strtotime($date_of_receipt));

        $expires = strtotime('+6 month', strtotime($timestamp));
        $today = strtotime(date('Y-m'));

        if($expires >= $today) {
            $msg = 1;
        }
        
        if($msg == 0) {
            return ['message' => 'Invoice Date Out of Range.'];
        }
        
        if(!empty($date_of_receipt)){
            $date_of_receipt1 = date("Y-m-d", strtotime($date_of_receipt));
        }else {
            $date_of_receipt1 = null;
        }
        
        $rd = \common\models\ClientOption::find()->where(['clientID' => $clientID])->one();
        
        $checkduplicate = VIPReceipt::find()
                ->where(['invoice_no'=>$invoice_no, 'clientID' => $clientID])
                ->andWhere(['!=', 'status', 'D'])
                ->count();
        if($checkduplicate > 0) {
            return ['message' => 'Invoice No '.$invoice_no.' has already been taken.'];
        }

        $imagefiles = UploadedFile::getInstancesByName("upfile");
        if (empty($imagefiles)){
            return ['message' => 'Must upload at least 1 file in upfile form-data POST'];
        }
        
        $path = Yii::getAlias('@backend/web') .'/upload/upload_invoice/';
        $paththumb = Yii::getAlias('@backend/web') .'/upload/upload_invoice/thumb/';
        //$imagefiles = UploadedFile::getInstances($model, 'image');
        
        $filegroup = Yii::$app->security->generateRandomString(); 
        
            if (!is_null($imagefiles)) {
                $VIPReceipt = new VIPReceipt();
                $data= VIPReceipt::find()->select('running_no')
                        ->where(['clientID' => $clientID])
                        ->orderBy(['running_no' => SORT_DESC])->one();
                if(!empty($data)){
                    $running_no = str_pad(++$data->running_no,6,'0',STR_PAD_LEFT);
                    $VIPReceipt->running_no = $running_no;
                }else {
                    $VIPReceipt->running_no = '000001';
                }
                
                $VIPReceipt->invoice_no = $invoice_no;
                $VIPReceipt->clientID = $clientID;
                $VIPReceipt->userID = Yii::$app->user->id;
                $VIPReceipt->date_of_receipt = $date_of_receipt1;
                $VIPReceipt->order_num = $rd->receipts_prefix.$VIPReceipt->running_no;
                $VIPReceipt->reseller_name = 0;
                if($VIPReceipt->save()) {
                    $lastinsid = $VIPReceipt->vip_receipt_id;
                    foreach ($imagefiles as $file) {
                        $productimage = new \common\models\VIPUploadInvoice();
                        $new_name = $clientID.'_'.Yii::$app->user->id.'_'.Yii::$app->security->generateRandomString(). '.' . $file->extension;

                        $productimage->userID = Yii::$app->user->id;
                        $productimage->clientID = $clientID;
                        $productimage->file_group = $filegroup;
                        $productimage->upload_file_name = $file->baseName . '.' . $file->extension;
                        $productimage->upload_file_new_name = $new_name;
                        $productimage->add_date = date('Y-m-d H:i:s');
                        $productimage->upload_via = 'A';
                        $productimage->vip_receipt_id = $lastinsid;

                        if ($productimage->save()) {
                            $file->saveAs($path . $new_name);
                            $imagine = Image::getImagine();
                            $image = $imagine->open($path . $new_name);
                            $image->resize(new Box(200, 300))->save($paththumb . $new_name, ['quality' => 100]);
                        }
                    }
                    return ['message' => true];
                }else {
                    //print_r($VIPReceipt->getErrors());
                    return ['message' => false];
                }
                
            }
            return ['message' => false];
    }
    
    /**
    * @api {get} /receipt/receipt-lists-pending Receipt List Pending
    * @apiGroup Recepit
    * @apiSuccess {Boolean} message successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *       "invoice_no": "123",
    *       "order_num": "042334_03052018",
    *       "date_of_receipt": "2018-05-10",
    *       "status": "P",
    *       ...
    *       },
    *   ]
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionReceiptListPending()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $user_id = Yii::$app->user->id;
        $receipts = \common\models\VIPReceipt::find()
            ->select('vip_receipt_id,created_datetime,reseller_name,invoice_no,date_of_receipt')
            ->where([
                'userID' => [$user_id],
                'clientID' => [$clientID],
                'status' => 'P',
        ])->all();
        
        $totalpoint = array();
        foreach($receipts as $object) {
            $rd = \common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $object['vip_receipt_id']])->all();
            $sum = 0;
            $ab = 0;
            foreach($rd as $r){
                $ab = $r->product_per_value * $r->product_item_qty * $r->user_selector * $r->reseller_selector * $r->profile_selector;
                $sum += $ab;
            }
            $totalpoint[] = $sum;
        }
        
        if (count($receipts) == 0) {
            return ['message' => 'empty'];
        }
        //$receipts = \common\models\VIPReceipt::find()->joinWith('user')->andWhere('vip_receipt.userID=' . $user_id . ' AND vip_receipt.clientID = ' . $clientID)->orderBy('vip_receipt.created_datetime DESC')->all();
        //print_r($receipts);
        return [
            'receipts' => $receipts,
            'totalpoint' => $totalpoint 
        ];
    }
    
    /**
    * @api {get} /receipt/receipt-lists-processing Receipt List Processing
    * @apiGroup Recepit
    * @apiSuccess {Boolean} message successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *       "invoice_no": "123",
    *       "order_num": "042334_03052018",
    *       "date_of_receipt": "2018-05-10",
    *       "status": "N",
    *       ...
    *       },
    *   ]
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionReceiptListProcessing()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $user_id = Yii::$app->user->id;
        $receipts = \common\models\VIPReceipt::find()
            ->select('invoice_no,date_of_receipt,vip_receipt_id,created_datetime,reseller_name')    
            ->where([
                'userID' => [$user_id],
                'clientID' => [$clientID],
                'status' => 'N',
        ])->all();
        $totalpoint = array();
        $totalitem = array();
        $reseller_names = array();
        $totalitems = 0;
        foreach($receipts as $object) {

            $rd = \common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $object['vip_receipt_id']])->all();
            $sum = 0;
            $ab = 0;
            
            foreach($rd as $r){
                $ab = $r->product_per_value * $r->product_item_qty;
                $sum += $ab;
            }
            $totalitems = \common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $object['vip_receipt_id']])->count();
            if(!empty($object['reseller_name'])) {
                $reseller_name = \common\models\CompanyInformation::find()->where(['user_id' => $object['reseller_name']])->one();
                $reseller = $reseller_name->company_name;
            }else {
                $reseller = 'N/A';
            }
            
            
            $totalpoint[] = $sum; 
            $totalitem[] = $totalitems;
            $reseller_names[] = $reseller;
        }
        

        if (count($receipts) == 0) {
            return ['message' => 'empty'];
        }
        return [
            'receipts' => $receipts,
            'totalitem' => $totalitem,
            'totalpoint' => $totalpoint,
            'resellername' => $reseller_names,
        ];
    }
    
    /**
    * @api {get} /receipt/receipt-list-approves Receipt List Approves
    * @apiGroup Recepit
    * @apiSuccess {Boolean} message successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *       "invoice_no": "123",
    *       "order_num": "042334_03052018",
    *       "date_of_receipt": "2018-05-10",
    *       "status": "P",
    *       ...
    *       },
    *   ]
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Empty"
    *   }
    */
    public function actionReceiptListApproves()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $user_id = Yii::$app->user->id;
        $receipts = \common\models\VIPReceipt::find()
            ->select('invoice_no,date_of_receipt,vip_receipt_id,created_datetime,reseller_name')    
            ->where([
                'userID' => [$user_id],
                'clientID' => [$clientID],
                'status' => 'A',
        ])->all();
        $totalpoint = array();
        $totalitem = array();
        $reseller_names = array();
        $totalitems = 0;
        foreach($receipts as $object) {

            $rd = \common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $object['vip_receipt_id']])->all();
            $sum = 0;
            $ab = 0;
            
            foreach($rd as $r){
                $ab = $r->product_per_value * $r->product_item_qty;
                $sum += $ab;
            }
            $totalitems = \common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $object['vip_receipt_id']])->count();
            if(!empty($object['reseller_name'])) {
                $reseller_name = \common\models\CompanyInformation::find()->where(['user_id' => $object['reseller_name']])->one();
                $reseller = $reseller_name->company_name;
            }else {
                $reseller = 'N/A';
            }
            
            
            $totalpoint[] = $sum; 
            $totalitem[] = $totalitems;
            $reseller_names[] = $reseller;
        }
        

        if (count($receipts) == 0) {
            return ['message' => 'empty'];
        }
        return [
            'receipts' => $receipts,
            'totalitem' => $totalitem,
            'totalpoint' => $totalpoint,
            'resellername' => $reseller_names,
        ];
    }
    
    /**
    * @api {get} /receipt/receipt-list-decline Receipt List Decline
    * @apiGroup Recepit
    * @apiSuccess {Boolean} message successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *       "invoice_no": "123",
    *       "order_num": "042334_03052018",
    *       "date_of_receipt": "2018-05-10",
    *       "status": "P",
    *       ...
    *       },
    *   ]
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Empty"
    *   }
    */
    public function actionReceiptListDecline()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $user_id = Yii::$app->user->id;
        $receipts = \common\models\VIPReceipt::find()
            ->select('invoice_no,date_of_receipt,vip_receipt_id,created_datetime,reseller_name')    
            ->where([
                'userID' => [$user_id],
                'clientID' => [$clientID],
                'status' => 'D',
        ])->all();
        $totalpoint = array();
        $totalitem = array();
        $reseller_names = array();
        $totalitems = 0;
        foreach($receipts as $object) {

            $rd = \common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $object['vip_receipt_id']])->all();
            $sum = 0;
            $ab = 0;
            
            foreach($rd as $r){
                $ab = $r->product_per_value * $r->product_item_qty;
                $sum += $ab;
            }
            $totalitems = \common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $object['vip_receipt_id']])->count();
            if(!empty($object['reseller_name'])) {
                $reseller_name = \common\models\CompanyInformation::find()->where(['user_id' => $object['reseller_name']])->one();
                $reseller = $reseller_name->company_name;
            }else {
                $reseller = 'N/A';
            }
            
            
            $totalpoint[] = $sum; 
            $totalitem[] = $totalitems;
            $reseller_names[] = $reseller;
        }
        

        if (count($receipts) == 0) {
            return ['message' => 'empty'];
        }
        return [
            'receipts' => $receipts,
            'totalitem' => $totalitem,
            'totalpoint' => $totalpoint,
            'resellername' => $reseller_names,
        ];
    }
    
    /**
    * @api {get} /receipt/produc-name Produc Name
    * @apiGroup Recepit
    * @apiParam {Integer} bb_points_id FK product_list_id
    * @apiParam {String} Status "A" => "Active", "X" => "Delete"
    * @apiSuccess {Boolean} message successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *           "bb_points_id": 3,
    *           "clientID": 12,
    *           "product_code": "145000119221",
    *           "product_description": "FINISH GREY",
    *           "product_kg": "25.0",
    *           "category_bb_id": 1,
    *           "product_point": 1,
    *           "status": "A"
    *       },
    *   ]
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": "Empty"
    *   }
    */
    public function actionProducName($product_list_id)
    {
        $clientID = Yii::$app->user->identity->client_id;
        $user_id = Yii::$app->user->id;
        $product_list = \common\models\BBPoints::find()->where(['bb_points_id' => $product_list_id])->one();

        return $product_list;
    }
    
    /**
    * @api {get} /receipt/receipt-lisit-view Receipt Lisit View
    * @apiGroup Recepit
    * @apiParam {String} Status "P" => "Pending", "D" => "Disapprove", "A" => "Approve", "M" => "Modify"
    * @apiSuccess {Boolean} message successful or not
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   [
    *       {
    *       "vip_receipt_list_id": 23,
    *       "vip_receipt_id": 15,
    *       "userID": 217,
    *       "clientID": 12,
    *       "product_list_id": 1,
    *       "product_per_value": 1,
    *       "product_item_qty": 2,
    *       "price_total": "300.00",
    *       "price_per_product": "150.00",
    *       "user_selector": 2,
    *       "reseller_selector": 2,
    *       "profile_selector": 1
    *       },
    *   ]
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 200 OK
    *   {
    *       "message": false
    *   }
    */
    public function actionReceiptLisitView($id)
    {
        $clientID = Yii::$app->user->identity->client_id;
        $user_id = Yii::$app->user->id;
        $redemptionitems = \common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $id])->all();

        return $redemptionitems;
    }
    
}