<?php
namespace api\modules\v1\models;

use Yii;
use yii\base\Model;
use \yii\db\ActiveRecord;

use common\models\User;
use api\modules\v1\components\Helpers;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $clientID;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
            'client_id' => $this->clientID
        ]);
        

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        if ($user) {
            $userid = $user->id;
            $email = $user->email;
            $profile = \common\models\VIPCustomer::findOne([
                    'userID' => $userid,
                ]);
            
            $client = \common\models\Client::findOne($this->clientID);


            $name = $profile->full_name;
            $resetLink = $client->cart_domain.'/site/reset-password?token='.$user->password_reset_token;
            Helpers::passwordResetEmail($email,$name,$resetLink,$this->clientID);
            
            return true;
        }else{
           return false; 
        }

        /*return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();*/
    }
}
