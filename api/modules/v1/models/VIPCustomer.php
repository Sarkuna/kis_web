<?php
namespace api\modules\v1\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "vip_customer".
 *
 * @property integer $vip_customer_id
 * @property integer $userID
 * @property integer $clientID
 * @property integer $salutation_id
 * @property string $clients_ref_no
 * @property string $dealer_ref_no
 * @property string $full_name
 * @property string $gender
 * @property string $telephone_no
 * @property string $mobile_no
 * @property string $date_of_Birth
 * @property string $nric_passport_no
 * @property string $Race
 * @property string $Region
 * @property integer $country_id
 * @property integer $address_id
 * @property integer $nationality_id
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class VIPCustomer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $email;
    public $approved;
    public $newsletter;
    public $status;
            
    public static function tableName()
    {
        return 'vip_customer';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'clientID', 'salutation_id', 'clients_ref_no', 'dealer_ref_no', 'full_name', 'mobile_no'], 'required'],
            [['userID', 'clientID', 'salutation_id', 'country_id', 'address_id', 'nationality_id', 'created_by', 'updated_by','state'], 'integer'],
            [['approved','newsletter','status','date_of_Birth', 'created_datetime', 'updated_datetime'], 'safe'],
            [['clients_ref_no', 'dealer_ref_no', 'telephone_no', 'mobile_no', 'nric_passport_no', 'Race', 'Region'], 'string', 'max' => 20],
            [['full_name'], 'string', 'max' => 200],
            //[['CIDB_Grade'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 1],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_customer_id' => 'Vip Customer ID',
            'userID' => 'User ID',
            'clientID' => 'Client ID',
            'salutation_id' => 'Salutation',
            'clients_ref_no' => 'Clients Ref No',
            'dealer_ref_no' => 'Dealer Ref No',
            'full_name' => 'Full Name',
            'email' => 'E-Mail',
            'gender' => 'Gender',
            'telephone_no' => 'Telephone No',
            'mobile_no' => 'Mobile No',
            'date_of_Birth' => 'Date Of Birth',
            'nric_passport_no' => 'NRIC/Passport No.',
            'Race' => 'Race',
            'Region' => 'Region',
            'country_id' => 'Country',
            'address_id' => 'Address',
            'nationality_id' => 'Nationality',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'newsletter' => 'Newsletter',
            'status' => 'Status',
            'approved' => 'Approved',
        ];
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'userID']);
    }
    
    public function getCountry()
    {
        return $this->hasOne(VipCountry::className(), ['country_id' => 'country_id']);
    }
    public function getZones()
    {
        return $this->hasOne(VIPZone::className(), ['zone_id' => 'Region']);
    }
    
    public function getNationality()
    {
        return $this->hasOne(VipCountry::className(), ['country_id' => 'nationality_id']);
    }
    public function getActions() {
        $returnValue = "";
        $url = '/sales/customers/sendpassword?id='.$this->userID;

        $returnValue = $returnValue . '<a href="' . Url::to(['/sales/customers/update', 'id' => $this->vip_customer_id]) . '" title="Edit" class="btn btn-success btn-sm" ><i class="fa fa-pencil"></i></a>';
        $returnValue = $returnValue . ' <a href="' . Url::to(['/sales/customers/view', 'id' => $this->vip_customer_id]) . '" title="View" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a> ';
        
        $returnValue = $returnValue .Html::a('<span class="glyphicon glyphicon-envelope"></span>', $url, [
                                                    'title' => Yii::t('app', 'Resend password'),
                                                    'class' => "btn btn-warning btn-sm",
                                                    'data' => [
                                                        'confirm' => 'Are you sure you want resend password?',
                                                    ],
                                        ]);
        //$returnValue = $returnValue . ' <a href="' . Url::to(['/admin/clients/logo', 'logoID' => $this->clientAddress->client_address_ID]) . '" title="Logo" class="btn btn-info btn-sm"><i class="fa fa-file-image-o"></i></a>';

        return $returnValue;
    }
    
    public function getTotalAdd() {
        $total = 0;
        $totaladd = \common\models\VIPCustomerReward::find()
        ->where(['>', 'points', 0])
        ->andWhere(['clientID'=>$this->clientID, 'customer_id'=>$this->userID])
        ->sum('points');
        
        /*$totalminus = \common\models\VIPCustomerReward::find()
        ->where(['<', 'points', 0])
        ->andWhere(['clientID'=>$this->clientID, 'customer_id'=>$this->userID])
        ->sum('points');
        $total = $totaladd + $totalminus;*/
        if(empty($totaladd)){
            $totaladd = 0; 
         }
        return $totaladd;
    }
    public function getTotalMinus() {
        $totalminus = 0;

        $totalminus = \common\models\VIPCustomerReward::find()
        ->where(['<', 'points', 0])
        ->andWhere(['clientID'=>$this->clientID, 'customer_id'=>$this->userID])
        ->sum('points');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
        return $totalminus;
    }
    
    public function getTotalExpiry() {
        $totalexpiry = \common\models\VIPCustomerReward::find()
        ->where('expiry_date < NOW()')
        //->andWhere('publish_on_date <= NOW()');        
        ->andWhere(['clientID'=>$this->clientID, 'customer_id'=>$this->userID])
        ->sum('points');
        
        if(empty($totalexpiry)){
            $totalexpiry = 0; 
         }
        return $totalexpiry;
    }

}