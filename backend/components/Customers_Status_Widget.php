<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class Customers_Status_Widget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        if(!empty($clientID)){
            $users = \common\models\TypeName::find()
            ->select('name, tier_id')        
            ->where(['clientID' => $clientID])
            ->asArray()->all();
        }else{
            return $this->goHome();
        }
        
        return $this->render('customers_status',['users' => $users]);
        
    }
}