<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use common\models\VIPOrder;
use common\models\VIPReceipt;
use common\models\MPOrder;
//use yii\helpers\Html;

class LTE_LeftMenuWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $receipts_list_pending = $receipts_list_processing = 0;

        
        $receipts_list_pending = VIPReceipt::find()->where(['status' => 'P', 'clientID' => $clientID])->count();
        $receipts_list_processing = VIPReceipt::find()->where(['status' => 'N', 'clientID' => $clientID])->count();
        $receipts_list_drafts = VIPReceipt::find()->where(['status' => 'R', 'clientID' => $clientID])->count();
        
        $orders_pending = VIPOrder::find()->where(['order_status_id' => 10, 'clientID' => $clientID])->count();
        $orders_processing = VIPOrder::find()->where(['order_status_id' => 20, 'clientID' => $clientID])->count();
        $orders_hold = VIPOrder::find()->where(['order_status_id' => 25, 'clientID' => $clientID])->count();
        $orders_shipped = VIPOrder::find()->where(['order_status_id' => 30, 'clientID' => $clientID])->count();
        
        $mp_orders_pending = MPOrder::find()->where(['order_status' => 'Accepted'])->count();
        $mp_orders_processing = MPOrder::find()->where(['order_status' => 'Processing'])->count();
        
        
        return $this->render('lte_leftmenuwidget',
        [
            'receipts_list_pending' => $receipts_list_pending,
            'receipts_list_processing' => $receipts_list_processing,
            'receipts_list_drafts' => $receipts_list_drafts,
            'orders_pending' => $orders_pending,
            'orders_processing' => $orders_processing,
            'orders_hold' => $orders_hold,
            'orders_shipped' => $orders_shipped,
            'mp_orders_pending' => $mp_orders_pending,
            'mp_orders_processing' => $mp_orders_processing
        ]);
        
    }
}