<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class LeftMenuWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $receipts_list_pending = 0;
        $receipts_list_pending = \common\models\VIPReceipt::find()->where(['status' => 'P', 'clientID' => $clientID])->count();
        return $this->render('leftmenuwidget',
        [
            'receipts_list_pending' => $receipts_list_pending,
        ]);
        
    }
}