<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\models\PointOrder;
use app\modules\painter\models\PainterProfile;

class ProfileNameWidget extends Widget
{
    public $path;

    /*public function init()
    {
        parent::init();
    }

    public function run()
    {        
        return $this->render('painteroverview');
        
    }*/
    public function init()
    {
        
        parent::init();

    }

    public function run()
    {
        $userID = \Yii::$app->user->id;
        $profile = \common\models\UserProfile::find()
            ->where(['userID' => $userID])
            ->one();
        $firstname = $profile->first_name;
        return $this->render('profilename',['firstname' => $firstname]);
        
    }
}