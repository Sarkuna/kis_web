<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use common\models\VIPReceipt;

class ReceiptsListWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $query = VIPReceipt::find();
        $query->andWhere(['=','clientID', $clientID]);
        $query->andWhere(['=','status', 'N']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10, 
            ],
            'sort' => [
                'defaultOrder' => [
                    'vip_receipt_id' => SORT_DESC
                    //'id' => SORT_DESC
                ]
            ],
        ]);
        
        return $this->render('receiptslistwidget',array('receiptlists'=>$dataProvider));
        
    }
}