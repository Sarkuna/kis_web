<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\models\PointOrder;
use app\modules\painter\models\PainterProfile;

class TopCountBoxWidget extends Widget
{
    public $path;

    /*public function init()
    {
        parent::init();
    }

    public function run()
    {        
        return $this->render('painteroverview');
        
    }*/
    public function init()
    {
        
        parent::init();

    }

    public function run()
    {
        $session = Yii::$app->session;
        $totalpainter = PainterProfile::find()->count();
        $totalorder = PointOrder::find()->count();
        
        return $this->render('topcountbox',['totalpainter' => $totalpainter, 'totalorder' => $totalorder,]);
        
    }
}