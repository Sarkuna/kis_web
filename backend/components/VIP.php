<?php

namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Component;
use yii\base\Exception;
use mPDF;
use kartik\mpdf\Pdf;

class VIP extends Component {
    
    public function getManagementUsers() {
        $users = 'I am from component';

        return $users;
    }
    
    public function passwordResetEmail($email,$username,$resetLink) {
        //$emailBody = 'This test Email';
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => 'RP05'])
                ->one();

        $emailSubjectTemp = $emailTemplate->subject;
        $emailSubject = $emailSubjectTemp;
        $emailSubject = str_replace('{name}', $username, $emailSubject);
        
        $emailBodyTemp = $emailTemplate->template;
        $emailBody = $emailBodyTemp;
        $emailBody = str_replace('{name}', $username, $emailBody);
        $emailBody = str_replace('{resetLink}', $resetLink, $emailBody);
        //echo $emailBody;
        //die();
        Yii::$app->mailer->compose()
                ->setTo($email)
                //->setCc($data["owneremail"])
                //->setBcc($data["managementemails"])
                //->setFrom([\Yii::$app->params['posterMasterEmail'] => \Yii::$app->name])
                //->setReplyTo(\Yii::$app->params['replyTo'])
                ->setFrom([Yii::$app->params['supportEmail'] => 'support'])
                //->setReplyTo(\Yii::$app->params['paymentIresidenzMail'])
                ->setSubject($emailSubject)
                //->setSubject($emailSubject)
                ->setHtmlBody($emailBody)
                //->attach($path)
                ->send();
        
        /*return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();*/
    }
    
    public function sendEmail($userId, $emailTemplateCode, $data = null)
    {
        $session = Yii::$app->session;
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $admin_url = $client->admin_domain;
        $client_name = $client->clientAddress->company;
        $client_email = $client->clientAddress->email_address;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();
        
        $emailSubject = $emailTemplate->subject;
        
        
        $profile = \common\models\VIPCustomer::find()->where(['userID' => $userId])->one();
        $name = $profile->full_name;
        $email = $profile->user->email;
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }
        
        //$name = isset($data2['name']) ? $data2['name'] : null;
        //$email = isset($data2['email']) ? $data2['email'] : null;
        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        
        /*if ($subject) {
            $emailSubject = $subject;
        } else {
            $emailSubject = $emailTemplate->subject;
            //$emailSubject = $emailTemplate->subject;
            $emailSubject = str_replace('{membership_ID}', $membership_ID, $emailSubject);
        }*/
        
        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        //membership_ID
        //contact_no
        //$emailBody = str_replace('{verification_id}', $verification_id, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);

        Yii::$app->mailer->compose()
            ->setTo($email)
            //->setCc($data["owneremail"])
            //->setBcc($data["managementemails"])
            //->setFrom([\Yii::$app->params['posterMasterEmail'] => \Yii::$app->name])
            //->setReplyTo(\Yii::$app->params['replyTo'])
            ->setFrom([$client_email =>  $client_name])
            //->setReplyTo(\Yii::$app->params['paymentIresidenzMail'])
            ->setSubject($emailSubject)
            //->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            //->attach($path)
            ->send();
        //return true;
    }
    
    
    public function sendEmailOrder($order_id, $emailTemplateCode, $data = null)
    {
        $session = Yii::$app->session;
        $comment ='';
        $order = \common\models\VIPOrder::find()->where(['order_id' => $order_id, 'clientID' => $session['currentclientID']])->one();
        $userId = $order->customer_id;
        $order_date = date('d/m/Y', strtotime($order->created_datetime));
        $order_id = $order->invoice_prefix;
        $ip = $order->ip;
        $status_name = $order->orderStatus->name;
        $comment = $order->comment;
        $telephone = $order->customer->telephone_no;
        $mobile = $order->customer->mobile_no;    
        $payment_address = $order->shipping_firstname.'<br>'.$order->shipping_lastname.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_city.'<br>'.$order->shipping_postcode.'<br>'.$order->customerAddress->states->state_name.'<br>'.$order->customerAddress->country->name;
        $shipping_address = $order->shipping_firstname.'<br>'.$order->shipping_lastname.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_city.'<br>'.$order->shipping_postcode.'<br>'.$order->customerAddress->states->state_name.'<br>'.$order->customerAddress->country->name;
        $tbl = '<table border="1" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse;border:none">
	<thead>
		<tr>
                    <td style="border:solid #dddddd 1.0pt;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p style="margin-bottom:15.0pt"><b><span style="font-size:9.0pt;color:#222222">Product</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p style="margin-bottom:15.0pt"><b><span style="font-size:9.0pt;color:#222222">Model</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Quantity</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Unit Points</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Total</span></b></p>
                    </td>
                </tr>
	</thead>
	<tbody>';
        
        $total = '';
        foreach ($order->orderProducts as $subproduct) {
            //echo $subproduct->name.'<br>';
            $optlisit = '';
            $OrderOption = \common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
            if ($OrderOption > 0) {
                $OrderOptionlisits = \common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                foreach ($OrderOptionlisits as $OrderOptionlisit) {
                        $optlisit .= '<br>&nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                    }
                }
                
                $tbl .= '<tr style="height:1.15pt">
                    <td style="border:solid #dddddd 1.0pt;border-top:none;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p style="margin-bottom:15.0pt"><span style="font-size:9.0pt">' . $subproduct->name . '</span> <span style="font-size:8.0pt">' . $optlisit . ' </span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p style="margin-bottom:15.0pt"><span style="font-size:9.0pt">' . $subproduct->model . '</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . $subproduct->quantity . '</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($subproduct->point) . 'pts</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($subproduct->point_total) . 'pts</span></p>
                    </td>
                </tr>';
                $total += $subproduct->point_total;
        }
        
        $tbl .= '<tr style="height:1.15pt">
            <td colspan="4" style="border:solid #dddddd 1.0pt;border-top:none;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt">Total:</span></b><span style="font-size:9.0pt"></span></p>
            </td>
            <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($total) . 'pts</span></p>
            </td>
        </tr>
        </tbody></table>';
        
        $products = $tbl;
        
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $siteUrl2 = $client->admin_domain;
        $client_name = $client->clientAddress->company;
        $client_email = $client->clientAddress->email_address;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode, 'clientID' => $session['currentclientID']])
                ->one();
        
        $emailSubject = $emailTemplate->subject;
        
        
        $profile = \common\models\VIPCustomer::find()->where(['userID' => $userId])->one();
        $name = $profile->full_name;
        $email = $profile->user->email;
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }
        
        //$name = isset($data2['name']) ? $data2['name'] : null;
        //$email = isset($data2['email']) ? $data2['email'] : null;
        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;

        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);

        $emailBody = str_replace('{store_name}', $client_name, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        $emailBody = str_replace('/files/'.$session['currentclientID'].'', $siteUrl2.'/files/'.$session['currentclientID'].'', $emailBody);
        
        //$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['accounts/my-account/view', 'id' => $order_id]);
        $resetLink = '<a href="' . Url::to([$siteUrl2.'/accounts/my-account/view', 'id' => $order_id]) . '" title="View Order Browser" class = "btn btn-info btn-sm">View Order</a>';
        $emailBody = str_replace('{order_link}', $resetLink, $emailBody);
        $emailBody = str_replace('{order_id}', $order_id, $emailBody);
        $emailBody = str_replace('{order_date}', $order_date, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{telephone}', $telephone, $emailBody);
        $emailBody = str_replace('{mobile}', $mobile, $emailBody);
        $emailBody = str_replace('{ip}', $ip, $emailBody);
        $emailBody = str_replace('{status_name}', $status_name, $emailBody);
        $emailBody = str_replace('{comment}', $comment, $emailBody);
        $emailBody = str_replace('{payment_address}', $payment_address, $emailBody);
        $emailBody = str_replace('{shipping_address}', $shipping_address, $emailBody);
        $emailBody = str_replace('{products}', $products, $emailBody);
        
        //print_r($emailBody);
        //die();

        Yii::$app->mailer->compose()
            ->setTo($email)
            //->setCc($data["owneremail"])
            //->setBcc($data["managementemails"])
            //->setFrom([\Yii::$app->params['posterMasterEmail'] => \Yii::$app->name])
            //->setReplyTo(\Yii::$app->params['replyTo'])
            ->setFrom([$client_email =>  $client_name])
            //->setReplyTo(\Yii::$app->params['paymentIresidenzMail'])
            ->setSubject($emailSubject)
            //->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            //->attach($path)
            ->send();
        //return true;
    }
    
    public function sendSMSpainter($painterId, $mobile, $emailTemplateCode, $data = null)
    {
    //function ismscURL($link) {
        $siteUrl = Yii::$app->request->hostInfo;
        
        $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $painterId])
                ->one();
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $name = $painterinfo->full_name;
        $membership_ID = $painterinfo->card_id;
        $contact_no = $painterinfo->mobile;

        $email = isset($data2['email']) ? $data2['email'] : null;
        $password = isset($data2['password']) ? $data2['password'] : null;
        //$membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        //$contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        
        //Successful Transactions created for Member
        $tr_ID = isset($data2['transaction_ID']) ? $data2['transaction_ID'] : null;
        $tr_point = isset($data2['transaction_point']) ? $data2['transaction_point'] : null;
        $tr_rm_value = isset($data2['transaction_rm_value']) ? $data2['transaction_rm_value'] : null;
        
        $re_ID = isset($data2['redemption_ID']) ? $data2['redemption_ID'] : null;
        $re_point = isset($data2['redemption_point']) ? $data2['redemption_point'] : null;
        $re_rm_value = isset($data2['redemption_value']) ? $data2['redemption_value'] : null;
        
        $re_paid_date = isset($data2['re_paid_date']) ? $data2['re_paid_date'] : null;
        $bank = isset($data2['bank']) ? $data2['bank'] : null;
        $bank_account_no = isset($data2['bank_account_no']) ? $data2['bank_account_no'] : null;
        $account_holder_name = isset($data2['account_holder_name']) ? $data2['account_holder_name'] : null;
        
        $balance_total_points = isset($data2['balance_total_points']) ? $data2['balance_total_points'] : null;
        $balance_rm_value = isset($data2['balance_rm_value']) ? $data2['balance_rm_value'] : null;
        
        
        $emailBody = $emailTemplate->sms_text;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);

        //Successful Transactions created for Member
        $emailBody = str_replace('{transaction_ID}', $tr_ID, $emailBody);
        $emailBody = str_replace('{transaction_point}', $tr_point, $emailBody);
        $emailBody = str_replace('{transaction_rm_value}', $tr_rm_value, $emailBody);
        //
        $emailBody = str_replace('{redemption_ID}', $re_ID, $emailBody);
        $emailBody = str_replace('{redemption_point}', $re_point, $emailBody);
        $emailBody = str_replace('{redemption_value}', $re_rm_value, $emailBody);
        
        $emailBody = str_replace('{paid_date}', $re_paid_date, $emailBody);
        $emailBody = str_replace('{bank}', $bank, $emailBody);
        $emailBody = str_replace('{bank_account_no}', $bank_account_no, $emailBody);
        $emailBody = str_replace('{account_holder_name}', $account_holder_name, $emailBody);

        
        $message = $emailBody;
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
        $message = urlencode($message);
        //$destination = $myuser->cust_mobile;
        //$new = substr($myuser->mobile, 0, -3) . 'xxx';
        //echo $short.'<br>';


                    
        $username = urlencode(Yii::$app->params['sms.username']);
        $password = urlencode(Yii::$app->params['sms.password']);
        $sender_id = urlencode("66300");
        $type = '1';
        
        $link = "https://www.isms.com.my/isms_send.php";
        $link .= "?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
        
        //print_r($link);
        //die();
        //$result = Yii::$app->residenz->ismscURL($fp);
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;
    }
    
    function ismscURL($link) {
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;
    }
    
    public static function addtolog($status = null, $message = null, $uID = 0)
    {
        \common\models\Actionlog::add($status, $message, $uID);
    }
}
