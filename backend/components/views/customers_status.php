<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
$clientID = $session['currentclientID'];
?>

<div class="row">
    <?php
        foreach($users as $user) {
            $usercount = \common\models\User::find()
            //->select('name, tier_id')        
            ->where(['client_id' => $clientID, 'status' => 'P', 'type' => $user['tier_id']])
            ->asArray()->count();
            echo '<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
        <span class="info-box-text">'.$user['name'].'</span>
              <span class="info-box-number">'.$usercount.'</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>';
        }
    ?>
      </div>