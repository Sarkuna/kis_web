<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\components\ProfileNameWidget;
$clientID = $session['currentclientID'];

if(!empty($clientID)){
    $clientname = 'Name Show Here';
}else{
    $clientname = 'VIP Admin';
}
?>

<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <div class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search..." id="s">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </div>
    <div class="content">
        <?php
        $clientlisits = $dataProvider->getModels();
        $count = count($clientlisits);
        if ($clientlisits > 0) {
            echo '<ul class="control-sidebar-menu clientList" id="clientlist">';
            echo '<li><a href="/admin/switch/vip">
                <h4 class="control-sidebar-subheading"><i class="fa fa-briefcase iconclient"></i> VIP Admin</h4></a></li>';
            foreach($clientlisits as $clientlisit){
                echo '<li><a href="/admin/switch?clientID='.$clientlisit->clientID.'">
                <h4 class="control-sidebar-subheading"><i class="fa fa-briefcase iconclient"></i> '.$clientlisit->company.'</h4></a></li>';
            }
            echo '</ul>';
        }
        ?>
    </div>
  </aside>