<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

use common\models\UserProfile;
use common\helper\VmsAvatar;

class PhotoController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        $session = Yii::$app->session;

        if (parent::beforeAction($action)) {
            if (in_array($action->id, array('view'))) {
                if (!( ($session['currentRole'] == Yii::$app->params['role.name.owner']) || ($session['currentRole'] == Yii::$app->params['role.name.tenant']) )) {
                    throw new ForbiddenHttpException('You are trying to access restricted area.', 403);
                }
            }
                        
            return true;
        } else {
            return false;
        }
        
    }
    
    /**
     * Resident view photo from user id.
     * @param type GET user_id
     */
    public function actionView() {
        $userId = Yii::$app->request->get('user_id');
        $size = Yii::$app->request->get('size');
        $userProfile = UserProfile::find()->where(['user_id' => $userId])->one();
        if (!empty($userProfile->photo)) {
            $fileNames = explode('.', $userProfile->photo);
            $extension = $fileNames[count($fileNames) - 1];
            $profilePath = Yii::getAlias('@frontend') . '/web/upload/profile/';
            if (file_exists($profilePath . $userProfile->photo)) {
                $photo = $profilePath . $userProfile->photo;
            }
        }
        if(empty($photo) && !empty($userProfile->name)){
            header('Content-Type: image/png');
            $imageSize = !empty($size) ? $size : 200;
            $setting = array('size' => $imageSize);
            $letterName = str_replace(' - ', ' ', $userProfile->name);
            $letterName = str_replace(' -', ' ', $letterName);
            $avatar = VmsAvatar::generateAvatarFromLetter($letterName, $setting);
            echo $avatar;
            exit();
        }else if(empty($photo)){
            $extension = 'png';
            $photo = Yii::getAlias('@frontend') . '/web/images/avatar.png';
        }
        
        header('Content-Type: image/'.$extension);
        $file = file_get_contents($photo);
        echo $file;
        exit();
    }
    
    /**
     * Resident view photo from user id.
     * @param type GET user_id
     */
    public function actionViewClient() {
        $userId = Yii::$app->request->get('user_maid_id');
        $size = Yii::$app->request->get('size');
        $userMaid = \common\models\UserMaid::findOne($userId);
        if (!empty($userMaid->photo)) {
            $fileNames = explode('.', $userMaid->photo);
            $extension = $fileNames[count($fileNames) - 1];
            $profilePath = Yii::getAlias('@frontend') . '/web/upload/maid/';
            if (file_exists($profilePath . $userMaid->photo)) {
                $photo = $profilePath . $userMaid->photo;
            }
        }
        if(empty($photo) && !empty($userMaid->name)){
            header('Content-Type: image/png');
            $imageSize = !empty($size) ? $size : 200;
            $setting = array('size' => $imageSize);
            $letterName = str_replace(' - ', ' ', $userMaid->name);
            $letterName = str_replace(' -', ' ', $letterName);
            $avatar = VmsAvatar::generateAvatarFromLetter($letterName, $setting);
            echo $avatar;
            exit();
        }else if(empty($photo)){
            $extension = 'png';
            $photo = Yii::getAlias('@frontend') . '/web/images/avatar.png';
        }
        
        header('Content-Type: image/'.$extension);
        $file = file_get_contents($photo);
        echo $file;
        exit();
    }
    
    /**
     * Resident view photo from user id.
     * @param type GET user_id
     */
    public function actionViewDriver() {
        $userId = Yii::$app->request->get('user_driver_id');
        $size = Yii::$app->request->get('size');
        $userDriver = \common\models\UserDriver::findOne($userId);
        if (!empty($userDriver->photo)) {
            $fileNames = explode('.', $userDriver->photo);
            $extension = $fileNames[count($fileNames) - 1];
            $profilePath = Yii::getAlias('@frontend') . '/web/upload/driver/';
            if (file_exists($profilePath . $userDriver->photo)) {
                $photo = $profilePath . $userDriver->photo;
            }
        }
        if(empty($photo) && !empty($userDriver->name)){
            header('Content-Type: image/png');
            $imageSize = !empty($size) ? $size : 200;
            $setting = array('size' => $imageSize);
            $letterName = str_replace(' - ', ' ', $userDriver->name);
            $letterName = str_replace(' -', ' ', $letterName);
            $avatar = VmsAvatar::generateAvatarFromLetter($letterName, $setting);
            echo $avatar;
            exit();
        }else if(empty($photo)){
            $extension = 'png';
            $photo = Yii::getAlias('@frontend') . '/web/images/avatar.png';
        }
        
        header('Content-Type: image/'.$extension);
        $file = file_get_contents($photo);
        echo $file;
        exit();
    }
}