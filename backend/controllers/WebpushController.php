<?php

namespace backend\controllers;

use Yii;
use common\components\WebPushNotification;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\db\Expression;
use yii\filters\AccessControl;

class WebpushController extends Controller {
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionRegister() {
        $request = Yii::$app->request;
        $player_id = trim($request->post('user_id'));
        $status = strtoupper(trim($request->post('status')));

        if (!in_array($status, array("Y", "N"))) {
            $status = 'N';
        }
        

        // $player_id = '12345678';
        if ($player_id) {

            $webpush_user = \common\models\WebPushUser::find()->where([
                        'user_id' => Yii::$app->user->id,
                        'player_id' => $player_id,
                    ])->one();

            if ($webpush_user) {
                $webpush_user->status = $status;
            } else {
                $webpush_user = new \common\models\WebPushUser();
                $webpush_user->player_id = $player_id;
                $webpush_user->user_id = Yii::$app->user->id;
                $webpush_user->status = $status;
            }

            if ($webpush_user->save()) {
                $ret = array('status' => 'ok');
            } else {
                $ret = array('status' => 'failed');
            }
        } else {
            $ret = array('status' => 'failed');
        }

        echo json_encode($ret);
    }

}
