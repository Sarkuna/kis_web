<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class BannerForm extends Model
{
    public $file_image;
    
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //$client_id = '6';
        return [
            [['file_image'], 'file','maxFiles' => 6],
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
        
    public function attributeLabels()
    {
        return [
            'file_image' => 'Banner',
        ];
    }
}
