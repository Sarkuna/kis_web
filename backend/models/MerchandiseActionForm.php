<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class MerchandiseActionForm extends Model
{
    public $order_status_id;
    public $comment;
    public $notify;
    public $bb_invoice_no;
    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //$client_id = '6';
        return [
            [['order_status_id', 'comment'], 'required'],
            ['order_status_id', 'string'],
            ['comment', 'string', 'max' => 500],
            [['notify'], 'safe'],

            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
        
    public function attributeLabels()
    {
        return [
            'order_status_id' => 'Status',
            'comment' => 'Comment',
            'notify' => 'Email notify to customer',
        ];
    }
}