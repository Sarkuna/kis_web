<?php
namespace backend\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\base\Model;


/**
 * Signup form
 */
class TestForm extends Model
{
    public $main_image1;
    
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //$client_id = '6';
        return [
            ['main_image1', 'required'],
            //['password', 'string', 'min' => 4],
            //['password', 'string', 'max' => 20],
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
   
    public function attributeLabels()
    {
        return [
            'main_image1' => 'Image',
        ];
    }
}
