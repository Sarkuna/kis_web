<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAccount2Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipcustomer-account2-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'account2_id') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'indirect_id') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'points_in') ?>

    <?php // echo $form->field($model, 'points_out') ?>

    <?php // echo $form->field($model, 'date_added') ?>

    <?php // echo $form->field($model, 'created_datetime') ?>

    <?php // echo $form->field($model, 'updated_datetime') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
