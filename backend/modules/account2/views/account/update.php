<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAccount2 */

$this->title = 'Update Vipcustomer Account2: ' . $model->account2_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipcustomer Account2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->account2_id, 'url' => ['view', 'id' => $model->account2_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vipcustomer-account2-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
