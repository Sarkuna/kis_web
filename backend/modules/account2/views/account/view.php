<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAccount2 */

$this->title = $model->account2_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipcustomer Account2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipcustomer-account2-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->account2_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->account2_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'account2_id',
            'clientID',
            'customer_id',
            'indirect_id',
            'description:ntext',
            'points_in',
            'points_out',
            'date_added',
            'created_datetime',
            'updated_datetime',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
