<?php

namespace app\modules\admin\controllers;

use Yii;
use common\models\VIPOrderProduct;
use common\models\VIPOrderProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TrackingController implements the CRUD actions for VIPOrderProduct model.
 */
class TrackingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPOrderProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VIPOrderProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VIPOrderProduct model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VIPOrderProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUploadExcel()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = new \backend\models\ExcelForm();
        $msg = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = date('mmssh_dmy');
                $model->excel->saveAs(Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "type", "shipped date", "tracking no.", "tracking link", "unique id");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));


                if($checkmatch >= 1){
                    $data = array();
                    $sql = '';
                    $shipped_date = '';
                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);
                        //$data[] = [$rowData[0]['tracking type'],$rowData[0]['shipped date'],$rowData[0]['tracking no.'],$rowData[0]['tracking link'],$rowData[0]['unique id']];
                        if(!empty($rowData[0]['shipped date'])) {
                            //$shipped_date = \PHPExcel_Shared_Date::PHPToExcel($rowData[0]['shipped date']);
                            //$shipped_date = date('Y-m-d', \PHPExcel_Shared_Date::PHPToExcel($rowData[0]['shipped date']));
                            $date = \PHPExcel_Shared_Date::ExcelToPHP($rowData[0]['shipped date']); //unix value
                            $shipped_date= date("Y-m-d", $date); //you can echo this value
                        }else {
                            $shipped_date = 'NULL';
                        }
                        //$sql.="update` assign_product_excel_upload` set `tracking_type` = ". $rowData[0]['tracking type'].", `shipped_date` = ". $shipped_date.", `tracking_no` = ". $rowData[0]['tracking no.'].", `tracking_link` = ". $rowData[0]['tracking link']."  where `order_product_id` =". $rowData[0]['unique id']. ";";
                        $sql.="UPDATE vip_order_product SET tracking_type = '".$rowData[0]['type']."',shipped_date = '$shipped_date',tracking_no = '". $rowData[0]['tracking no.']."',tracking_link = '". $rowData[0]['tracking link']."' WHERE order_product_id = ". $rowData[0]['unique id'].";";

                    }
                    $result=Yii::$app->db->createCommand($sql)->execute ();                    
                    $result == 1?true:false;
                    if($result == 1) {
                        \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Order traking info successfuly update']);
                    }else {
                        \Yii::$app->getSession()->setFlash('error', ['title' => 'Success', 'text' => 'Order traking info update fail']);
                    }
                    unlink($inputFile);
                    return $this->redirect(['index']);
                }else {
                    $teml = 'type, shipped date, tracking no., tracking link, unique id';
                    \Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Sorry your excel template not match. This should on your excel ! ('.$teml.') Note:case sensitive no issue']);
                    //return $this->redirect(['uploadexcel']);
                }

                return $this->render('uploadexcelreport', [
                    'msg' => $returnedValue,
                ]);
            }
            print_r($model->getErrors());
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
        //echo("6");
    }

    /**
     * Updates an existing VIPOrderProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->shipped_date = date('Y-m-d', strtotime($model->shipped_date));
            if($model->save()){
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Order traking info successfuly update']);
            }else {
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Success', 'text' => 'Order traking info update fail']);
            }
            return $this->redirect(['index']);
        } else {
            if(!empty($model->shipped_date)) {
                $model->shipped_date = date('d-m-Y', strtotime($model->shipped_date));
            }else {
                $model->shipped_date = date('d-m-Y');
            }
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VIPOrderProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VIPOrderProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPOrderProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPOrderProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
