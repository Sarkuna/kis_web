<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPAnnouncement */

$this->title = 'Edit Page';
$this->params['breadcrumbs'][] = ['label' => 'Announcements', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Edit Page';
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
    <div class="vipannouncement-update">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
