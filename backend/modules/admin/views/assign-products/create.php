<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPAssignProducts */

$this->title = 'Create Vipassign Products';
$this->params['breadcrumbs'][] = ['label' => 'Vipassign Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipassign-products-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
