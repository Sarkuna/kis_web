<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary client-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success pull-right', 'title' => 'Add New']) ?>
    </div><!-- /.box-header -->

    <div class="box-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'clientID',
                [
                    'attribute' => 'company',
                    'label' => 'Company',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model->company;
                    },
                //'options' => ['width' => '150'],
                ],
                'cart_domain',
                'admin_domain',
                //'getClientAddress',

                //'client_SMS',
                //'client_status',
                [
                    'attribute' => 'client_status',
                    'label' => 'Status',
                    'format' => 'html',
                    'headerOptions' => ['width' => '95'],
                    'value' => function ($model) {
                //$ord =
                return $model->getStatus();
            },
                    'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],
                    'filter' => ["P" => "Pending", "A" => "Active", "D" => "Deactive", "X" => "Deleted"],
                ],
                // 'client_email:email',
                // 'client_status',
                // 'created_datetime',
                // 'updated_datetime',
                // 'created_by',
                // 'updated_by',
                //['class' => 'yii\grid\ActionColumn'],
                [
                    'attribute' => 'action',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->getActions();
                    },
                    'options' => ['width' => '150'],
                ],
            ],
        ]);
        ?>
    </div>
</div>
