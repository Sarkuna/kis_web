<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\MaskedInput;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\InternalInvoicing */
/* @var $form yii\widgets\ActiveForm */
$invoicetype = \common\models\InternalInvoicing::find()->select('invoice_type')->all();
if(count($invoicetype) > 0) {
    $data = [];
    foreach($invoicetype as $row) {
        $data[] = $row['invoice_type'];
    }
    $typeOptionsArray = $data;
}else {
    $typeOptionsArray = '';
}

?>


<style>
    .info-box-content {
        margin-left: 0px;
    }
    .info-box {min-height:0px;}
    .summery{font-size: 16px;}
    .summery input {
        font-size: 20px;
    }
</style>

<div class="internal-invoicing-form">
    <?php $form = ActiveForm::begin(['options' => ['id' => 'invoice_form']]); ?>
    <div class="box-body">
        <div class="row">
                <?= $form->field($model, 'invoice_no',[
                    'options' => [
                        'class' => 'col-lg-3 col-md-3',
                    ],
                ])->textInput(['maxlength' => true]) ?>
            
                <?= $form->field($model, 'invoice_type',[
                    'options' => [
                        'class' => 'col-lg-3 col-md-3',
                    ],
                ])->widget(\yii\jui\AutoComplete::classname(), [
        'value' => (!empty($model->floor) ? $model->floor : ''),
        'clientOptions' => [
            'source' => $typeOptionsArray,
            'enabled' => true,
            'minLength' => 0
        ],

        'options' =>
            [
                'placeholder' => 'Maintenance',
                'class' => 'form-control autocomplete-input-bg-arrow ',

                'onclick' => "(function ( ) {
                      $( '#customer-floor' ).autocomplete( 'search', '' );
                                })();",

                'onfocus' => "(function ( ) {
                      $( '#customer-floor' ).autocomplete( 'search', '' );
                                })();",
            ],
    ]); ?>

                <?= $form->field($model, 'subject',[
                    'options' => [
                        'class' => 'col-lg-6 col-md-6',
                    ],
                ])->textInput(['maxlength' => true]) ?>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'invoice_date')->widget(\yii\jui\DatePicker::classname(), [
                    //'language' => 'ru',
                    'clientOptions' =>[
                        'dateFormat' => 'dd-mm-yy',
                        'yearRange' => "2000:+0",
                        'changeMonth' => true,
                        'changeYear' => true
                    ],
                    'options' => ['class' => 'form-control picker', 'readOnly'=>'readOnly']
                    ])
                ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'due_date')->widget(\yii\jui\DatePicker::classname(), [
                    //'language' => 'ru',
                    'clientOptions' =>[
                        'dateFormat' => 'dd-mm-yy',
                        'yearRange' => "2000:+0",
                        'changeMonth' => true,
                        'changeYear' => true
                    ],
                    'options' => ['class' => 'form-control picker', 'readOnly'=>'readOnly']
                    ])
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'bill_from_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'bill_from_add')->textarea(['rows' => 6]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'bill_to_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'bill_to_add')->textarea(['rows' => 6]) ?>
            </div>
        </div>
        
        
        
        <div class="row">
            <div class='col-md-12 col-sm-12 col-xs-12'>
                <?php
                    DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-educationlevel', // required: css class selector
                            'widgetItem' => '.educationlevel', // required: css class
                            'limit' => 999, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-educationlevel', // css class
                            'deleteButton' => '.remove-educationlevel', // css class
                            'model' => $invoiceitems[0],
                            'formId' => 'invoice_form',
                            'formFields' => [
                                'internal_invoicing_items_id',
                                'invoice_id',
                                'description',
                                'quantity',
                                'per_unit',
                                'amount',
                            ],
                        ]);
                        ?>
                
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>DESCRIPTION</th>
                                    <th style="width:10%;">QUANTITY</th>
                                    <th style="width:10%;">PER UNIT, RM</th>
                                    <th style="width:10%;">AMOUNT, RM</th>
                                    <th class="text-center" style="width: 5%;">
                                        <button type="button" class="add-educationlevel btn btn-success btn-xs"><span class="fa fa-plus"></span></button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="container-educationlevel">
                                <?php foreach ($invoiceitems as $iii => $invoiceitem): ?>
                                    <tr class="educationlevel">
                                        <td>
                                            <?php
                                            // necessary for update action.
                                            if (!$invoiceitem->isNewRecord) {
                                                echo Html::activeHiddenInput($invoiceitem, "[{$iii}]internal_invoicing_items_id");
                                            }
                                            ?>
                                            <?= $form->field($invoiceitem, "[{$iii}]description",[
                                                'template' => '
                                                    <div class="dd">
                                                        {input}{error}
                                                    </div>',
                                                'inputOptions' => [
                                                    'class'=>'form-control',
                                                ]])->textArea(['rows' => '3']);
                                            ?>

                                        </td>
                                        <td>
                                            <?= $form->field($invoiceitem, "[{$iii}]quantity",[
                                                
                                                'inputOptions' => [
                                                    'class'=>'form-control quantity',
                                                    'onchange' => 'calculateSubtotal($(this))',
                                                ]])->widget(\yii\widgets\MaskedInput::className(), [
                                                        'mask' => '9',
                                                        'clientOptions' => [
                                                            'repeat' => 10,
                                                            'greedy' => false,
                                                            'rightAlign' => true,
                                                        ],
                                                    ])->label(false);
                                            ?>
                                        </td>
                                        <td>
  
                                            <?= $form->field($invoiceitem, "[{$iii}]per_unit",[
                                                'template' => '
                                                    <div class="dd">
                                                        {input}{error}
                                                    </div>',
                                                'inputOptions' => [
                                                    'class'=>'form-control per_unit',
                                                    'onchange' => 'calculateSubtotal($(this))', 
                                                ]])->widget(\yii\widgets\MaskedInput::className(), [
                                                        'clientOptions' => [
                                                            'alias' => 'decimal',
                                                            'digits' => 2,
                                                            'digitsOptional' => false,
                                                            'radixPoint' => '.',
                                                            'groupSeparator' => '',
                                                            'autoGroup' => true,
                                                            'removeMaskOnSubmit' => true,
                                                        ],
                                                    ])->label(false);
                                            ?>
                                        </td>
                                        <td>
  
                                            <?= $form->field($invoiceitem, "[{$iii}]amount",[
                                                'template' => '
                                                    <div class="dd">
                                                        {input}{error}
                                                    </div>',
                                                'inputOptions' => [
                                                    'class'=>'form-control per_unit',
                                                    'readOnly'=>'readOnly',
                                                    'onchange' => 'calculateSubtotal($(this))', 
                                                ]])->widget(\yii\widgets\MaskedInput::className(), [
                                                        'clientOptions' => [
                                                            'alias' => 'decimal',
                                                            'digits' => 2,
                                                            'digitsOptional' => false,
                                                            'radixPoint' => '.',
                                                            'groupSeparator' => '',
                                                            'autoGroup' => true,
                                                            'removeMaskOnSubmit' => true,
                                                        ],
                                                    ])->label(false);
                                            ?>
                                            
                                        </td>
                                        
                                        
                                        <td class="text-center vcenter" style="width: 90px; verti">
                                            <button type="button" class="remove-educationlevel btn btn-danger btn-xs"><span class="fa fa-minus"></span></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php DynamicFormWidget::end(); ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
                <?=
                $form->field($model, 'terms_conditions')->widget(CKEditor::className(), [
                              'options' => ['rows' => 6],
                              'preset' => 'basic',
                              //'clientOptions' => ElFinder::ckeditorOptions(['elfinder', 'path' => $currentclientID],[]), 
                          ]);

                ?>
            </div>
            <div class="col-lg-6 summery">
                <?= $form->field($model, 'sub_total',[
                        'template' => "<div class='form-group row'><div class='col-md-7 text-left'><span>{label}</span></div><div class='col-md-4'>{input}\n{hint}\n{error}</div></div>",
                        'options' => [
                            'class' => 'col-lg-12 col-md-6',
                        ],
                        'inputOptions' => [
                            'class'=>'form-control',
                            'readOnly'=>'readOnly',
                        ]])->widget(\yii\widgets\MaskedInput::className(), [
                                'clientOptions' => [
                                    'alias' => 'decimal',
                                    'digits' => 2,
                                    'digitsOptional' => false,
                                    'radixPoint' => '.',
                                    'groupSeparator' => '',
                                    'autoGroup' => true,
                                    'removeMaskOnSubmit' => true,
                                ],
                        ]);
                ?>
                <?= $form->field($model, 'adjustment',[
                        'template' => "<div class='form-group row'><div class='col-md-7 text-left'><span>{label}</span></div><div class='col-md-4'>{input}\n{hint}\n{error}</div></div>",
                        'options' => [
                            'class' => 'col-lg-12 col-md-6',
                        ],
                        'inputOptions' => [
                            'class'=>'form-control',
                        ]])->widget(\yii\widgets\MaskedInput::className(), [
                                'clientOptions' => [
                                    'alias' => 'decimal',
                                    'digits' => 2,
                                    'digitsOptional' => false,
                                    'radixPoint' => '.',
                                    'groupSeparator' => '',
                                    'autoGroup' => true,
                                    'removeMaskOnSubmit' => true,
                                ],
                        ]);
                ?>
                <?= $form->field($model, 'total',[
                        'template' => "<div class='form-group row'><div class='col-md-7 text-left'><span>{label}</span></div><div class='col-md-4'>{input}\n{hint}\n{error}</div></div>",
                        'options' => [
                            'class' => 'col-lg-12 col-md-6',
                        ],
                        'inputOptions' => [
                            'class'=>'form-control',
                            'readOnly'=>'readOnly',
                        ]])->widget(\yii\widgets\MaskedInput::className(), [
                                'clientOptions' => [
                                    'alias' => 'decimal',
                                    'digits' => 2,
                                    'digitsOptional' => false,
                                    'radixPoint' => '.',
                                    'groupSeparator' => '',
                                    'autoGroup' => true,
                                    'removeMaskOnSubmit' => true,
                                ],
                        ]);
                ?>
            </div>
        </div>

    </div>
    <div class="box-footer">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>  
<?php
$script = <<<EOD
    var getAmount = function() {

        var qnty = $(".quantity").val();
        var price = $(".per_unit").val();
        var amount = 0;

        amount = parseInt(qnty) * parseInt(price);

        //Assign the sum value to the field
        $(".amount").val(amount);
    };

    $(".quantity").on("change", function() {
        getAmount();
    });
    $(".per_unit").on("change", function() {
        getAmount();
    });
        
    $("#internalinvoicing-adjustment").on("change", function() {
        var subtotal = $("#internalinvoicing-sub_total").val();
        var adjustment = $("#internalinvoicing-adjustment").val();
        var amount = 0;
        
        amount = parseInt(subtotal) + parseInt(adjustment);
        $('#internalinvoicing-total').val(amount);
    });    
     
EOD;
$this->registerJs($script);

$js =<<< JS
    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        jQuery(".dynamicform_wrapper .add-educationlevel").each(function(index) {
            calculateTotal(index+1);
        });
        $(item).find("input[name*='quantity']").inputmask({
            "regex": "[0-9]{1,4}",
            "removeMaskOnSubmit":true,
            "rightAlign":true
        });
        
        $(item).find("input[name*='per_unit']").inputmask({
            "alias":"decimal",
            "digits":2,
            "groupSeparator": '',
            "digitsOptional":false,
            "radixPoint": '.',
            "autoGroup":true,
            "removeMaskOnSubmit":true,
            "rightAlign":true
        });
        
        $(item).find("input[name*='amount']").inputmask({
            "alias":"decimal",
            "digits":2,
            "groupSeparator": '',
            "digitsOptional":false,
            "radixPoint": '.',
            "autoGroup":true,
            "removeMaskOnSubmit":true,
            "rightAlign":true
        });
        
        
    });
        
    jQuery(".dynamicform_wrapper").on("afterDelete", function() {
        jQuery(".dynamicform_wrapper .remove-educationlevel").each(function(index) {                
            calculateTotal(index+1);
        });
    });    
    
    function calculateSubtotal(item){   
        var index  = item.attr("id").replace(/[^0-9.]/g, "");   
        var qty = $('#internalinvoicingitems-' + index + '-quantity').val();
        qty = qty == "" ? 0 : Number(qty.split(",").join(""));
        
        var price = $('#internalinvoicingitems-' + index + '-per_unit').val();
        price = price == "" ? 0 : Number(price.split(",").join(""));
        
        $('#internalinvoicingitems-' + index + '-amount').val(qty * price);     
        calculateTotal(Number(index)+1);
    }

    function calculateTotal(index){    
        var total = 0;
        var totalfinal = 0;
        for(i=0; i< index; i++){        
            var subtotal = $('#internalinvoicingitems-' + i + '-amount').val();        
            subtotal = subtotal == "" ? 0 : Number(subtotal.split(",").join(""));    
            total = total + subtotal;
        }
        var adjustment = $("#internalinvoicing-adjustment").val();
        totalfinal = parseInt(total) + parseInt(adjustment);
        $('#internalinvoicing-sub_total').val(total);
        $('#internalinvoicing-total').val(totalfinal);
    }   
JS;
$this->registerJs($js, \yii\web\View::POS_END);


?>

