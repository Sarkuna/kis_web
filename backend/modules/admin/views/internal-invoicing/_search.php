<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InternalInvoicingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="internal-invoicing-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'invoice_id') ?>

    <?= $form->field($model, 'subject') ?>

    <?= $form->field($model, 'bill_from_name') ?>

    <?= $form->field($model, 'bill_from_add') ?>

    <?= $form->field($model, 'bill_to_name') ?>

    <?php // echo $form->field($model, 'bill_to_add') ?>

    <?php // echo $form->field($model, 'invoice_date') ?>

    <?php // echo $form->field($model, 'due_date') ?>

    <?php // echo $form->field($model, 'customer_notes') ?>

    <?php // echo $form->field($model, 'terms_conditions') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
