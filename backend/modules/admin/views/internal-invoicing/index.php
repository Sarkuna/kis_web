<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>




        <?php 
        $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],

            //'invoice_id',
            [
                'header' => 'Invoice Date',
                'value' => function($model){
                    return date('d-m-Y', strtotime($model->invoice_date));
                    
                }      
            ],
            [
                'header' => 'Invoice Name',
                'value' => function($model){
                    return $model->subject;
                },       
            ], 
            
            [
                'attribute' => 'invoice_no',
                'label' => 'Invoice No',
                'format' => 'html',
                //'headerOptions' => ['width' => '100'],
                'value' => function ($model) {
                    return $model->invoice_no;
                },
            ],
            [
                'header' => 'Type',
                'value' => function($model){
                    return $model->invoice_type;
                },       
            ],
            [
                'header' => 'Status',
                'value' => function($model){
                    return ucfirst($model->internal_invoicing_status);
                },       
            ],
            [
                'header' => 'Invoice Amt',
                'contentOptions'=>['class'=>'text-right'],
                'headerOptions' => ['class'=>'text-right'],
                'value' => function($model){
                    return Yii::$app->formatter->asDecimal($model->total);
                },       
            ],            
            [
                'header' => 'Payment Due',
                'value' => function($model){
                    return date('d-m-Y', strtotime($model->due_date));
                },       
            ],
                        
            /*[
                'header' => 'Balance Due',
                'contentOptions'=>['class'=>'text-right'],
                'headerOptions' => ['class'=>'text-right'],
                'value' => function($model){
                    return 'MYR'.Yii::$app->formatter->asDecimal($model->total);
                },       
            ],*/          
            [
                'attribute' => 'action',
                'label' => 'Action',
                'format' => 'raw',
                //'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return $model->getActions();
                },
            ], 
        ];
                
        echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'exportConfig' => [
        GridView::EXCEL => ['filename'=>$this->title],
        //GridView::HTML => ['label' => 'Save as CSV'],
        //GridView::PDF => ['label' => 'Save as CSV'],
    ],        
    'toolbar' => [
        [
            'content'=>
                Html::a('Create', ['create'], [
                    'class' => 'btn btn-secondary', 
                    'title' => 'Create',
                    'class'=>'btn btn-success'
                ]),
        ],
        '{export}',
        '{toggleData}'
    ],        
    //'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'bordered' => true,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading'=>Html::encode($this->title)
    ],

]);
        ?>
        

