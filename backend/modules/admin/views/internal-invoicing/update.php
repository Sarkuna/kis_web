<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InternalInvoicing */

$this->title = 'Update Internal Invoicing: ' . $model->invoice_id;
$this->params['breadcrumbs'][] = ['label' => 'Internal Invoicings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->invoice_id, 'url' => ['view', 'id' => $model->invoice_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="internal-invoicing-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
