<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\MaskedInput;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Invoice */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .info-box-content {
        margin-left: 0px;
    }
    .info-box {min-height:0px;}
</style>

<div class="invoice-form">
    <?php $form = ActiveForm::begin(['options' => ['id' => 'invoice_form']]); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'invoice_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6">
                <div class="row">
                <div class="col-lg-6">
                    <div class="info-box bg-green">
                        <div class="info-box-content">
                            <span class="info-box-text">Item</span>
                            <span class="info-box-number"><?= $items ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-lg-6">
                    <div class="info-box bg-green">
                        <div class="info-box-content">
                            <span class="info-box-text">Voucher</span>
                            <span class="info-box-number"><?= $vouchers ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'bill_from_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'bill_from_add')->textarea(['rows' => 3]) ?>
                <?= $form->field($model, 'bill_to_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'bill_to_add')->textarea(['rows' => 3]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
                <?=
                $form->field($model, 'terms_conditions')->widget(CKEditor::className(), [
                              'options' => ['rows' => 6],
                              'preset' => 'basic',
                              //'clientOptions' => ElFinder::ckeditorOptions(['elfinder', 'path' => $currentclientID],[]), 
                          ]);

                ?>
            </div>
        </div>
                
        
        <div class="row">
            <div class='col-md-12 col-sm-12 col-xs-12'>
                <?php
                    DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-educationlevel', // required: css class selector
                            'widgetItem' => '.educationlevel', // required: css class
                            'limit' => 2, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-educationlevel', // css class
                            'deleteButton' => '.remove-educationlevel', // css class
                            'model' => $invoiceitems[0],
                            'formId' => 'invoice_form',
                            'formFields' => [
                                'invoice_item_id',
                                'invoice_id',
                                'invoice_type',
                                'invoice_no',
                                'invoice_date',
                                'invoice_pono',
                                'invoice_due_date',
                            ],
                        ]);
                        ?>
                
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>INVOICE NO</th>
                                    <th>INVOICE DATE</th>
                                    <th>P.O NO.</th>
                                    <th>PAYMENT DUE</th>
                                    <th class="text-center" style="width: 5%;">
                                        <button type="button" class="add-educationlevel btn btn-success btn-xs"><span class="fa fa-plus"></span></button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="container-educationlevel">
                                <?php foreach ($invoiceitems as $iii => $invoiceitem): ?>
                                    <tr class="educationlevel">
                                        <td>
                                            <?php
                                            // necessary for update action.
                                            if (!$invoiceitem->isNewRecord) {
                                                echo Html::activeHiddenInput($invoiceitem, "[{$iii}]invoice_item_id");
                                            }
                                            ?>
                                            <?= $form->field($invoiceitem, "[{$iii}]invoice_type",[
                                                'template' => '
                                                    <div class="dd">
                                                        {input}{error}
                                                    </div>',
                                                'inputOptions' => [
                                                    'class'=>'form-control',
                                                ]])->dropDownList(['I' => "Item", 'V' => "Voucher"], ['prompt' => ''])->label(false);
                                            ?>

                                        </td>
                                        <td>
                                            <?= $form->field($invoiceitem, "[{$iii}]invoice_no",[
                                                'template' => '
                                                    <div class="dd">
                                                        {input}{error}
                                                    </div>',
                                                'inputOptions' => [
                                                    'class'=>'form-control',
                                                ]])->textInput(['autocomplete' => 'off']);
                                            ?>
                                        </td>
                                        <td>

                                           
                                            <?= $form->field($invoiceitem, "[{$iii}]invoice_date")->widget(\yii\jui\DatePicker::classname(), [
                                            //'language' => 'ru',
                                            'clientOptions' =>[
                                                'dateFormat' => 'dd-mm-yy',
                                                'yearRange' => "2000:+1",
                                                'changeMonth' => true,
                                                'changeYear' => true
                                            ],
                                            'options' => ['class' => 'form-control picker', 'readOnly'=>'readOnly']
                                            ])->label(false); ?>
                                        </td>
                                        <td>
                                            <?= $form->field($invoiceitem, "[{$iii}]invoice_pono",[
                                                'template' => '
                                                    <div class="dd">
                                                        {input}{error}
                                                    </div>',
                                                'inputOptions' => [
                                                    'class'=>'form-control',
                                                ]])->textInput(['maxlength' => true])->label(false);
                                            ?>
                                        </td>
                                        <td>
                                            <?= $form->field($invoiceitem, "[{$iii}]invoice_due_date")->widget(\yii\jui\DatePicker::classname(), [
                                            //'language' => 'ru',
                                            //'dateFormat' => "dd-M-yyyy",
                                            
                                            'clientOptions' =>[
                                                'dateFormat' => 'dd-mm-yy',
                                                'yearRange' => "2000:+1",
                                                'changeMonth' => true,
                                                'changeYear' => true
                                            ],
                      
                                            'options' => ['class' => 'form-control picker', 'readOnly'=>'readOnly']
                                            ])->label(false); ?>
                                        </td>
                                        
                                        <td class="text-center vcenter" style="width: 90px; verti">
                                            <button type="button" class="remove-educationlevel btn btn-danger btn-xs"><span class="fa fa-minus"></span></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php DynamicFormWidget::end(); ?>
            </div>
        </div>

    </div>
    <div class="box-footer">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>  
<?php
$this->registerJs(' 
$(function () {
    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        $( ".picker" ).each(function() {
           $( this ).datepicker({
              dateFormat : "dd-mm-yy",
              yearRange : "2000:+1",
              changeMonth: true,
              changeYear: true
           });
      });          
    });
});
$(function () {
    $(".dynamicform_wrapper").on("afterDelete", function(e, item) {
        $( ".picker" ).each(function() {
           $( this ).removeClass("hasDatepicker").datepicker({
              dateFormat : "dd-mm-yy",
              yearRange : "2000:+1",
              changeMonth: true,
              changeYear: true
           });
      });          
    });
});
');
?>
