<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>




        <?php 
        $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],

            //'invoice_id',
            [
                'header' => 'Invoice Date',
                'value' => function($model){
                    return date('d-m-Y', strtotime($model->invoice_date));
                },       
            ],
            [
                'header' => 'Invoice Name',
                'value' => function($model){
                    return $model->invoice->invoice_name;
                },
                //'group' => true,        
            ],         
            
            'invoice_no',
            [
                'header' => 'Type',
                'value' => function($model){
                    if($model->invoice_type == 'I') {
                        return 'Item';
                    }else {
                        return 'Voucher';
                    }
                },       
            ],
            [
                'header' => 'Status',
                'value' => function($model){
                    return ucfirst($model->invoice_status);
                },       
            ],
            [
                'header' => 'Invoice Amt',
                'contentOptions'=>['class'=>'text-right'],
                'value' => function($model){
                    return $model->invoiceAmount;
                },       
            ],             
            [
                'header' => 'Payment Due',
                'value' => function($model){
                    return date('d-m-Y', strtotime($model->invoice_date));
                },       
            ],
                       
                       
            // 'bill_to_add:ntext',
            // 'bill_create_date',

            //['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'action',
                'label' => 'Action',
                'format' => 'raw',
                //'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return $model->getActions();
                },
            ], 
        ];
                
        echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'exportConfig' => [
        GridView::EXCEL => ['filename'=>$this->title],
        //GridView::HTML => ['label' => 'Save as CSV'],
        //GridView::PDF => ['label' => 'Save as CSV'],
    ],        
    'toolbar' => [
        [
            'content'=>
                Html::a('Create', ['create'], [
                    'class' => 'btn btn-secondary', 
                    'title' => 'Create',
                    'class'=>'btn btn-success'
                ]),
        ],
        '{export}',
        '{toggleData}'
    ],        
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'bordered' => true,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading'=>Html::encode($this->title)
    ],

]);
        ?>
        
