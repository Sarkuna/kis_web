<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StatementOfAccount */

$this->title = 'Create A Payment';
$this->params['breadcrumbs'][] = ['label' => 'Statement Of Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-6">
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
    <div class="statement-of-account-create">
        <?=
        $this->render('_form', [
            'model' => $model,
            'invoicelist' => $invoicelist,
        ])
        ?>
    </div>
</div>
    </div>
</div>