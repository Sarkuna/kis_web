<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Statement of Account';
$this->params['breadcrumbs'][] = $this->title;
?>


        <?php
        $runningtotal = 0;
        $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'class' => '\kartik\grid\CheckboxColumn',
                'checkboxOptions' => function($model) {
                    if($model->payment_status == 'pending'){
                       return [];
                    }else{
                       return ['disabled' => true];
                    }
                 },
            ],
            //'order_id',
            //'invoice_item_id',
            [
                'attribute' => 'date',
                'label' => 'Date',
                'headerOptions' => ['width' => '95'],
                'value' => function($model) {
                    return date('d-m-Y', strtotime($model->statement_date));
                },      
            ],
            [
                'attribute' => 'particularsno',
                'label' => 'Particulars',
                'format' => 'raw',
                'headerOptions' => ['width' => '50%'],
                'value' => function($model) {
                    if(!empty($model->internal_invoicing_id)) {
                        ///admin/internal-invoicing/index
                        $url = Url::to(['/admin/internal-invoicing/index', 'InternalInvoicingSearch[invoice_no]'=>$model->internalInvoice->invoice_no]);
                        $options = ['target' => '_blank'];
                        $particulars = Html::a($model->internalInvoice->invoice_no,$url,$options).' '.$model->internalInvoice->subject;
                    }else if(!empty($model->invoice_item_id)) {
                        $url = Url::to(['/admin/invoice/index', 'InvoiceItemSearch[invoice_no]'=>$model->invoiceItem->invoice_no]);
                        $options = ['target' => '_blank'];
                        $particulars = Html::a($model->invoiceItem->invoice_no,$url,$options).' '.$model->invoiceItem->invoice->invoice_name;
                    }else {
                        $particulars = $model->particulars;
                    }
                    return $particulars;
                },      
            ],            
                        
            [
                'attribute' => 'Amount',
                'label' => 'Amount',
                'contentOptions'=>['class'=>'text-right'],
                'headerOptions' => ['class'=>'text-right'],
                'value' => function($model) {
                    if($model->amount > 0) {
                        return Yii::$app->formatter->asDecimal($model->amount);
                    }else {
                        return '';
                    }
                },      
            ],
            [
                'attribute' => 'Payment',
                'label' => 'Payment',
                'contentOptions'=>['class'=>'text-right'],
                'headerOptions' => ['class'=>'text-right'],
                'value' => function($model) {
                    if($model->payment > 0) {
                        return Yii::$app->formatter->asDecimal($model->payment);
                    }else {
                        return '';
                    }
                },      
            ],            
            [
                'attribute' => 'Balance',
                'label' => 'Balance',
                'contentOptions'=>['class'=>'text-right'],
                'headerOptions' => ['class'=>'text-right'],
                'value' => function ($model, $key, $index, $widget) use(&$runningtotal){
                    $runningtotal+= ($model->amount - $model->payment);
                    
                    return Yii::$app->formatter->asDecimal($runningtotal);
                },     
            ],
            [
                'attribute' => 'Payment Type',
                'label' => 'Payment Type',                
                'value' => function($model) {
                    return $model->payment_type;
                },      
            ],
            [
                'attribute' => 'Status',
                'label' => 'Status',                
                'value' => function($model) {
                    return ucfirst($model->payment_status);
                },      
            ],            
            

        ];
    ?>

            <?=Html::beginForm(['payment'],'post');?>

    
    <?php
                echo GridView::widget([
                    'id' => 'payment-grid',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table-striped table-bordered table-condensed'],
                    'options' => ['style' => 'white-space:nowrap;'],
                    'columns' => $gridColumns,
                    'exportConfig' => [
                        GridView::EXCEL => ['filename' => $this->title],
                    //GridView::HTML => ['label' => 'Save as CSV'],
                    //GridView::PDF => ['label' => 'Save as CSV'],
                    ],
                    'toolbar' => [
                        [
                            'content' =>
                            Html::submitButton('New Payment', [
                                'title' => 'Upload Excel',
                                'class' => 'btn btn-success',
                                'id' => 'btnsubmit'
                            ])
                        ],
                        '{export}',
                        '{toggleData}'
                    ],
                    //'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                    'bordered' => true,
                    'striped' => true,
                    //'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                    //'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    //'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    //'condensed' => true,
                    'responsive' => true,
                    'hover' => true,
                    'floatHeader' => false,
                    /*'resizableColumns' => true,
                    'resizableColumnsOptions' => ['resizeFromBody' => true],
                    'persistResize' => true,
                    'hideResizeMobile' => true,*/
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => Html::encode($this->title)
                    ],
                ]);
?>
        
<?= Html::endForm();?> 

<?php
    $clientScript = '

        $("#btnsubmit").click(function()
        {
            var keys = $("#payment-grid").yiiGridView("getSelectedRows");
            if(keys > "0"){
                return true;
            }else{
                alert("Please check at least one checkbox");
                return false;
            }
            
        });

    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>
