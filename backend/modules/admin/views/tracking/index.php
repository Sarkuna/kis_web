<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tracking';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
$orderproducts = \common\models\VIPOrderProduct::find()->joinWith([
                    'order' => function($que) {
                        $que->select(['order_id', 'invoice_prefix']);
                    },
                        ])->where(['vip_order.clientID' => $session['currentclientID']])
                        //->andwhere(['!=', 'invoice_item_id', 0])
                        ->andwhere(['vip_order.order_status_id' => 20])    
                        //->andwhere(['IN', 'user.type', $type])
                        ->orderBy(['vip_order.order_id' => SORT_DESC])->asArray()->all();
        $typenames = ArrayHelper::map($orderproducts, 'order_id', 'order.invoice_prefix');
        ?>




        <?php

        $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],
            //'order_id',
            //'invoice_item_id',
            [
                'attribute' => 'order_id',
                'label' => 'Order ID',
                'value' => function($model) {
                    return $model->order->invoice_prefix;
                },
                //'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],
                'filter' => $typenames,
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                   'options' => ['prompt' => 'All', 'allowClear' => true]
                ]
            //'group' => true,        
            ],
            [
                'header' => 'Invoice #',
                'value' => function($model) {
                    if (!empty($model->invoice_item_id)) {
                        return $model->invoiceitem->invoice_no;
                    } else {
                        return 'N/A';
                    }
                },
            //'group' => true,        
            ],
            [
                //'attribute' => 'full_name',
                'label' => 'Customer',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->order->shipping_firstname . ' ' . $model->order->shipping_firstname;
                },
            ],
            [
                //'attribute' => 'company_name',
                'label' => 'Company Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '200'],
                'value' => function ($model) {
                    return $model->order->shipping_company;
                },
                'contentOptions' => [
                    'style' => 'max-width:250px; overflow: auto; white-space: normal; word-wrap: break-word;'
                ],
            ],
            [
                //'attribute' => 'company_name',
                'label' => 'Shipping Details',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    $address = $model->order->shipping_address_1 . ' ' . $model->order->shipping_address_2 . ' ' . $model->order->shipping_city . ' ' . $model->order->shipping_postcode . ' ' . $model->order->shipping_zone . ' ' . $model->order->shipping_zone_id;
                    return $address;
                },
                'contentOptions' => [
                    'style' => 'max-width:250px; overflow: auto; white-space: normal; word-wrap: break-word;'
                ],
            ],
            [
                //'attribute' => 'company_name',
                'label' => 'Mobile',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    //return $model->order->customer_id;
                    return $model->order->customer->mobile_no;
                },
            ],
            [
                //'attribute' => 'product',
                'label' => 'Product',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    //return $model->order->customer_id;
                    return $model->name;
                },
            ],
            [
                //'attribute' => 'company_name',
                'label' => 'Qty',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    //return $model->order->customer_id;
                    return $model->quantity;
                },
            ],
            [
                //'attribute' => 'tracking_type',
                'label' => 'Type',
                'format' => 'html',
                'headerOptions' => ['style' => 'width:2%'],
                'value' => function ($model) {
            //return $model->order->customer_id;
            if ($model->tracking_type == 'Y') {
                return 'Yes';
            } else if ($model->tracking_type == 'N') {
                return 'No';
            } else {
                return 'N/A';
            }
        },
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],
                'filter' => ['N' => 'No', 'Y' => 'Yes'],
            ],
            [
                //'attribute' => 'company_name',
                'label' => 'Shipped Date',
                'format' => 'raw',
                //'contentOptions' => ['style' => 'width:500px;'],
                'value' => function ($model) {
                    //return $model->order->customer_id;
                    if (!empty($model->shipped_date)) {
                        return date('d-m-Y', strtotime($model->shipped_date));
                    } else {
                        return 'N/A';
                    }
                },
            ],
            [
                //'attribute' => 'company_name',
                'label' => 'Tracking No.',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    //return $model->order->customer_id;
                    return $model->tracking_no;
                },
            ],
            [
                //'attribute' => 'company_name',
                'label' => 'Tracking Link',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    //return $model->order->customer_id;
                    return $model->tracking_link;
                },
                'contentOptions' => [
                    'style' => 'max-width:150px; overflow: auto; white-space: normal; word-wrap: break-word;'
                ],
            ],
            [
                'header' => 'Unique ID',
                'value' => function($model) {
                    return $model->order_product_id;
                },
            //'group' => true,        
            ],
            //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}', // {update}{delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    },
                            'update' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));
                    },
                        ],
                    ],
                ];

                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table-striped table-bordered table-condensed'],
                    'options' => ['style' => 'white-space:nowrap;'],
                    'columns' => $gridColumns,
                    'exportConfig' => [
                        GridView::EXCEL => ['filename' => $this->title],
                    //GridView::HTML => ['label' => 'Save as CSV'],
                    //GridView::PDF => ['label' => 'Save as CSV'],
                    ],
                    'toolbar' => [
                        [
                            'content' =>
                            Html::a('<i class="fas fa-file-excel"></i> Upload Excel', ['upload-excel'], [
                                'class' => 'btn btn-secondary',
                                'title' => 'Upload Excel',
                                'class' => 'btn btn-success'
                            ]),
                        ],
                        '{export}',
                        '{toggleData}'
                    ],
                    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                    'bordered' => true,
                    'striped' => true,
                    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'condensed' => true,
                    'responsive' => true,
                    'hover' => true,
                    'floatHeader' => false,
                    'resizableColumns' => true,
                    'resizableColumnsOptions' => ['resizeFromBody' => true],
                    'persistResize' => true,
                    'hideResizeMobile' => true,
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => Html::encode($this->title)
                    ],
                ]);
                ?>
        
