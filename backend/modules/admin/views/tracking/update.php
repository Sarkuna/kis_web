<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrderProduct */

$this->title = 'Order Product: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tracking', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->order_product_id]];
$this->params['breadcrumbs'][] = 'Update';
?>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
    <div class="viporder-product-update">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
