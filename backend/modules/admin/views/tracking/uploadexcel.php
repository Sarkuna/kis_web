<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Bulk Order Tracking Upload Excel';
$this->params['breadcrumbs'][] = ['label' => 'Tracking', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <p>Please upload Excel file:</p>
            <?= $form->field($model, 'excel')->fileInput() ?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="form-group">
                        <?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>
                    </div>
        </div>
    </div>
    <!-- /.box -->
</div>
<?php ActiveForm::end(); ?>

