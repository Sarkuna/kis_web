<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrderProduct */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vip Order Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="viporder-product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->order_product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->order_product_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'order_product_id',
            'order_id',
            'product_id',
            'name',
            'model',
            'quantity',
            'point',
            'point_admin',
            'point_total',
            'price',
            'total',
            'shipping_price',
            'shipping_price_total',
            'shipping_point',
            'shipping_point_total',
            'product_status',
            'invoice_item_id',
            'tracking_type',
            'shipped_date',
            'tracking_no:ntext',
            'tracking_link:ntext',
        ],
    ]) ?>

</div>
