<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BonusIntakeList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bonus-intake-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bonus_intake_id')->textInput() ?>

    <?= $form->field($model, 'clientID')->textInput() ?>

    <?= $form->field($model, 'month_start')->textInput() ?>

    <?= $form->field($model, 'month_end')->textInput() ?>

    <?= $form->field($model, 'list_target')->textInput() ?>

    <?= $form->field($model, 'created_datetime')->textInput() ?>

    <?= $form->field($model, 'updated_datetime')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
