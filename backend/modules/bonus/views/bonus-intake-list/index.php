<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BonusIntakeListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bonus Intake Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-intake-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Bonus Intake List', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'bonus_intake_list_id',
            'bonus_intake_id',
            'clientID',
            'month_start',
            'month_end',
            // 'list_target',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
