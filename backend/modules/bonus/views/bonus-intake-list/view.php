<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BonusIntakeList */

$this->title = $model->bonus_intake_list_id;
$this->params['breadcrumbs'][] = ['label' => 'Bonus Intake Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-intake-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->bonus_intake_list_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->bonus_intake_list_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'bonus_intake_list_id',
            'bonus_intake_id',
            'clientID',
            'month_start',
            'month_end',
            'list_target',
            'created_datetime',
            'updated_datetime',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
