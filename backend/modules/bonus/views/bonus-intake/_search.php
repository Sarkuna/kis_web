<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BonusIntakeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bonus-intake-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'bonus_intake_id') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'bonus_intake_year') ?>

    <?= $form->field($model, 'bonus_intake_month') ?>

    <?= $form->field($model, 'bonus_intake_type') ?>

    <?php // echo $form->field($model, 'bonus_intake_target') ?>

    <?php // echo $form->field($model, 'created_datetime') ?>

    <?php // echo $form->field($model, 'updated_datetime') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
