<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BonusIntake */

$this->title = 'Create Bonus Intake';
$this->params['breadcrumbs'][] = ['label' => 'Bonus Intakes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
    <div class="bonus-intake-create">
        <?=
        $this->render('_form', [
            'model' => $model,
            'bonusintakelist' => $bonusintakelist
        ])
        ?>
    </div>
</div>
