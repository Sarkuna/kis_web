<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BonusIntakeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bonus Intakes';
$this->params['breadcrumbs'][] = $this->title;
$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
?>
<div class="box box-primary bonus-intake-index">
<div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success pull-right', 'title' => 'Add New']) ?>
        </div><!-- /.box-header -->
<div class="box-body">
            <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'bonus_intake_id',
            //'clientID',
            'bonus_intake_year',
            //'bonus_intake_month',
            [
                'attribute' => 'bonus_intake_month',
                'label' => 'Month',
                'format' => 'html',
                'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                'contentOptions' =>['class' => 'text-center'],
                'value' => function ($model) {   
                    $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
                    return $months[$model->bonus_intake_month];
                },
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                'filter' => $months,         
            ], 
            //'bonus_intake_type',
            [
                'attribute' => 'bonus_intake_type',
                'label' => 'Interval',
                'format' => 'html',
                //'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    $intake_target = '';
                    if($model->bonus_intake_type == '1'){
                        $intake_target = '12 Months';
                    }else if($model->bonus_intake_type == '2'){
                        $intake_target = '6 Months';
                    }else if($model->bonus_intake_type == '3'){
                        $intake_target = '3 Months';
                    }

                    return $intake_target;
                },
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                'filter' => ["1" => "12 Months", "2" => "6 Months", "3" => "3 Months"],        
            ],
            'bonus_intake_target',            
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

            //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                  'headerOptions' => ['width' => '60'],
                'template' => '{update} {view} ', //{view} {delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    },
                    /*'update' => function ($url, $model) {
                        if($model->status == 'P' || $model->status == 'N') {
                            return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));                        
                        }
                    },*/
                ],
            ],
        ],
    ]); ?>
            </div>
</div>
</div>
