<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BonusIntake */

$this->title = 'Update Bonus Intake: ' . $model->bonus_intake_year;
$this->params['breadcrumbs'][] = ['label' => 'Bonus Intakes', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->bonus_intake_id, 'url' => ['view', 'id' => $model->bonus_intake_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
    <div class="bonus-intake-update">
        <?=
        $this->render('_form_update', [
            'model' => $model,
            'bonusintakelist' => $bonusintakelist
        ])
        ?>
    </div>
</div>
