<?php

namespace app\modules\capability\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
//use yii\web\UploadedFile;

use common\models\RoleTypes;
use common\models\RoleTypesSearch;
use common\models\RoleModulePermission;
use common\models\Model;
/**
 * RoleTypesController implements the CRUD actions for RoleTypes model.
 */
class RoleTypesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::classname(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($event){
        if(Yii::$app->VIPglobal->getPermission())
            return parent::beforeAction($event);
        else
            $this->redirect(['/site/permission']);
    }

    /**
     * Lists all RoleTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoleTypesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RoleTypes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RoleTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RoleTypes();
        $permissions = [new RoleModulePermission];

        if ($model->load(Yii::$app->request->post())) {
            $permissions = Model::createMultiple(RoleModulePermission::classname());
            Model::loadMultiple($permissions, Yii::$app->request->post());
            
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if ($flag) {
                            foreach ($permissions as $permission) {
                                if ($permission != null){
                                    $permission->role_id = $model->role_id;
                                    $permission->added_at = date('Y-m-d');
                                    $permission->added_by = '1'; 
                                    if (($flag = $permission->save(false)) === false) {        
                                        $transaction->rollBack();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    
                    if ($flag) {
                        $transaction->commit();
                        \Yii::$app->getSession()->setFlash('success',['title' => 'New', 'text' => 'Successfully created!']);
                        //return $this->redirect(['/admin/resumes']);
                        return $this->redirect(['view', 'id' => $model->role_id]);
                    }
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
            echo 'No';die;
        }

        return $this->render('create', [
            'model' => $model,
            'permissions' => $permissions,
        ]);
    }

    /**
     * Updates an existing RoleTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $permissions = $model->roleModulePermissions;

        if ($model->load(Yii::$app->request->post())) {
            $permissionsoldIDs = ArrayHelper::map($permissions, 'id', 'id');
            $permissions = Model::createMultiple(RoleModulePermission::classname());
            Model::loadMultiple($permissions, Yii::$app->request->post());
            $deletedpermissionsoldIDs = array_diff($permissionsoldIDs, array_filter(ArrayHelper::map($permissions, 'id', 'id')));
            
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if ($flag) {
                            if (! empty($deletedpermissionsoldIDs)) {
                                RoleModulePermission::deleteAll(['id' => $deletedpermissionsoldIDs]);
                            }
                            foreach ($permissions as $permission) {
                                if ($permission != null){
                                    $permission->role_id = $model->role_id;
                                    $permission->added_at = date('Y-m-d');
                                    $permission->added_by = '1'; 
                                    if (($flag = $permission->save(false)) === false) {        
                                        $transaction->rollBack();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    
                    if ($flag) {
                        $transaction->commit();
                        \Yii::$app->getSession()->setFlash('success',['title' => 'New', 'text' => 'Successfully update!']);
                        //return $this->redirect(['/admin/resumes']);
                        return $this->redirect(['view', 'id' => $model->role_id]);
                    }
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            //'permissions' => $permissions,
            'permissions' => (empty($permissions)) ? [new common\models\RoleModulePermission()] : $permissions,
        ]);
    }

    /**
     * Deletes an existing RoleTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RoleTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RoleTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RoleTypes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
