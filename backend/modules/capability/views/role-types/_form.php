<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\MaskedInput;
use kartik\select2\Select2;

use common\models\ModulesList;
/* @var $this yii\web\View */
/* @var $model common\models\RoleTypes */
/* @var $form yii\widgets\ActiveForm */
$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Address: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Address: " + (index + 1))
    });
});

$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
    console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
    alert("Limit reached");
});
';

$this->registerJs($js);
?>

<div class="role-types-form">
    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'role_name')->textInput(['maxlength' => true]) ?>
            </div>            
        </div>
        <?php
                DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    //'limit' => 4, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $permissions[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                       'module_id',
                       'new',
                       'save',
                       'view',
                       'remove',
                    ],
                ]);
                ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="glyphicon glyphicon-tag"></i> Modules Permission
                <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Permission</button>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body container-items">

                        <?php $a = 1 ?>
                        <?php foreach ($permissions as $i => $permission): ?>

                            <div class="item panel panel-default"><!-- widgetBody -->
                                <div class="panel-heading">
                                    <span class="panel-title-address">Permission: <?= ($i + 1) ?></span>
                                    <div class="pull-right">                                        
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    // necessary for update action.
                                    if (!$permission->isNewRecord) {
                                        echo Html::activeHiddenInput($permission, "[{$i}]id");
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                          <?= $form->field($permission, "[{$i}]module_id")->dropDownList(ArrayHelper::map(ModulesList::find()->where(['is_active' => 1])->all(), 'module_id', 'module_name'), ['prompt' => ''])->label(false) ?>
                                       </div>
                                       <div class="col-sm-2">
                                          <?= $form->field($permission, "[{$i}]new")->checkbox(); ?>
                                       </div>
                                        <div class="col-sm-2">
                                          <?= $form->field($permission, "[{$i}]view")->checkbox(); ?>
                                       </div>
                                        <div class="col-sm-2">
                                          <?= $form->field($permission, "[{$i}]save")->checkbox(); ?>
                                       </div>
                                        <div class="col-sm-2">
                                          <?= $form->field($permission, "[{$i}]remove")->checkbox(); ?>
                                       </div>
                                    </div>

                                </div>
                            </div>
                        <?php $a++ ?>
                        <?php endforeach; ?>

                
            </div>
        </div>
        <?php DynamicFormWidget::end(); ?>
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'is_active')->dropDownList(['1' => 'Active', '2' => 'Deactive'], ['prompt'=>'']) ?>
            </div>            
        </div>
        
    </div>
    <div class="box-footer">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
