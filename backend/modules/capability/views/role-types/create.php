<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RoleTypes */

$this->title = 'Create Role';
$this->params['breadcrumbs'][] = ['label' => 'Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-12 role-types-create">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
        </div>


        <?=
        $this->render('_form', [
            'model' => $model,
            'permissions' => $permissions,
        ])
        ?>

        <!-- /.box-body -->
    </div>
</div>
