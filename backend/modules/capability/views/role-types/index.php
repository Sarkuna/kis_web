<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RoleTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Roles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-types-index">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list-alt"></i> <?= Html::encode($this->title) ?></h3>
                <div class="box-tools pull-right">
                    <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => 'Add New']) ?>
                </div>
            </div>
            <div class="box-body">

            <div class="table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'role_id',
            'role_name',
            //'is_active',
            [
                'attribute' => 'is_active',
                'label' => 'Is Active',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->is_active == 1 ? 'Active' : 'Deactive';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    </div>
            </div>
        </div>
    </div>
</div>