<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RoleTypes */

$this->title = 'Update Role : ' . $model->role_name;
$this->params['breadcrumbs'][] = ['label' => 'Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->role_name, 'url' => ['view', 'id' => $model->role_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="col-xs-12 role-types-update">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
        </div>


        <?=
        $this->render('_form', [
            'model' => $model,
            'permissions' => $permissions,
            //'permissions' => (empty($permissions)) ? [new common\models\RoleModulePermission()] : $permissions,
            'permissions' => (empty($permissions)) ? [new common\models\RoleModulePermission()] : $permissions,
        ])
        ?>

        <!-- /.box-body -->
    </div>
</div>
