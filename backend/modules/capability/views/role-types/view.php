<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RoleTypes */

$this->title = $model->role_name;
$this->params['breadcrumbs'][] = ['label' => 'Role Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="role-types-view">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list-alt"></i> <?= Html::encode($this->title) ?></h3>
                <div class="box-tools pull-right">
                    <?= Html::a('Update', ['update', 'id' => $model->role_id], ['class' => 'btn btn-primary']) ?>
                    <?=
                    Html::a('Delete', ['delete', 'id' => $model->role_id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //'role_id',
                            'role_name',
                            [
                                'attribute' => 'is_active',
                                'label' => 'Is Active',
                                'format' => 'html',
                                'value' => function ($model) {
                                    return $model->is_active == 1 ? 'Active' : 'Deactive';
                                },
                            ],
                        ],
                    ])
                    ?>

                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list-alt"></i> Modules Permissions</h3>
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Module Name</th>
                                <th>Create</th>
                                <th>View</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                            
                            <?php
                                if(count($model->roleModulePermissions) > 0) {
                                    $pr = 1;
                                    foreach($model->roleModulePermissions as $permissions) {
                                        $new = $permissions->new == 1 ? '<i class="fa fa-fw fa-check text-success"></i>' : '<i class="fa fa-fw fa-times text-danger"></i>';
                                        $view = $permissions->view == 1 ? '<i class="fa fa-fw fa-check text-success"></i>' : '<i class="fa fa-fw fa-times text-danger"></i>';
                                        $save = $permissions->save == 1 ? '<i class="fa fa-fw fa-check text-success"></i>' : '<i class="fa fa-fw fa-times text-danger"></i>';
                                        $remove = $permissions->remove == 1 ? '<i class="fa fa-fw fa-check text-success"></i>' : '<i class="fa fa-fw fa-times text-danger"></i>';
                                        
                                        echo '<tr>';
                                        echo '<td>'.$pr.'</td>';
                                        echo '<td>'.$permissions->module->module_name.'</td>';
                                        echo '<td>'.$new.'</td>';
                                        echo '<td>'.$view.'</td>';
                                        echo '<td>'.$save.'</td>';
                                        echo '<td>'.$remove.'</td>';
                                        echo '</tr>';
                                        $pr++;
                                    }                                    
                                }else {
                                    echo '<tr><td colspan="6" class="text-center">No record</td></tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
