<?php

namespace app\modules\catalog\controllers;

use Yii;
use common\models\VIPOptionDescription;
use common\models\VIPOptionDescriptionSearch;
use common\models\VIPOptionValueDescription;
use yii\web\Controller;
use backend\models\Model;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * OptionController implements the CRUD actions for VIPOptionDescription model.
 */
class OptionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPOptionDescription models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VIPOptionDescriptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VIPOptionDescription model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VIPOptionDescription model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VIPOptionDescription();
        //$modelsOptionvalue = [[new VIPOptionValueDescription()]];
        $modelsOptionvalue[] = new VIPOptionValueDescription();

        if ($model->load(Yii::$app->request->post())) {
            $modelsOptionvalue = Model::createMultiple(VIPOptionValueDescription::classname(), $modelsOptionvalue);
            Model::loadMultiple($modelsOptionvalue, Yii::$app->request->post());
            
            $valid = Model::validateMultiple($modelsOptionvalue);
            
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if ($flag) {
                            foreach ($modelsOptionvalue as $modelsOptionvalue) {
                                $modelsOptionvalue->language_id = '0';
                                $modelsOptionvalue->option_id = $model->option_id;
                                if (($flag = $modelsOptionvalue->save(false)) === false) {        
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        //return $this->redirect(['view', 'id' => $modelCatalogOption->id]);
                        return $this->redirect(['index']);
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }else {
                echo 'noooo';
            }
            //return $this->redirect(['view', 'id' => $model->option_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelsOptionvalue' => $modelsOptionvalue
            ]);
        }
    }

    /**
     * Updates an existing VIPOptionDescription model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //$modelsOptionvalue[] = $id;
        //$modelsOptionvalue = [new VIPOptionValueDescription()];
        $modelsOptionvalue = $model->vipOptionValueDescriptions;

        if ($model->load(Yii::$app->request->post())) {
            $oldHouseIDs = ArrayHelper::map($modelsOptionvalue, 'option_value_id', 'option_value_id');
            $modelsOptionvalue = Model::createMultiple(VIPOptionValueDescription::classname(), $modelsOptionvalue);
            Model::loadMultiple($modelsOptionvalue, Yii::$app->request->post());
            $deletedIDs = array_diff($oldHouseIDs, array_filter(ArrayHelper::map($modelsOptionvalue, 'option_value_id', 'option_value_id')));

            $valid = Model::validateMultiple($modelsOptionvalue);

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            $flag = VIPOptionValueDescription::deleteAll(['option_value_id' => $deletedIDs]);
                        }

                        if ($flag) {
                            foreach ($modelsOptionvalue as $modelsOptionvalue) {
                                $modelsOptionvalue->language_id = '0';
                                $modelsOptionvalue->option_id = $model->option_id;
                                if (($flag = $modelsOptionvalue->save(false)) === false) {
                                    //$modelsOptionvalue
                                    //print_r($modelsOptionvalue->getErrors());        
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        //return $this->redirect(['view', 'id' => $modelCatalogOption->id]);
                        return $this->redirect(['index']);
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }else {
                echo 'noooo';
            }
        }else {
            return $this->render('update', [
                'model' => $model,
                'modelsOptionvalue' => (empty($modelsOptionvalue)) ? [new VIPOptionValueDescription()] : $modelsOptionvalue
            ]);
        }
    }

    /**
     * Deletes an existing VIPOptionDescription model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/
    
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $optonValuesIDs = ArrayHelper::map($model->vipOptionValueDescriptions, 'option_value_id', 'option_value_id');
        VIPOptionValueDescription::deleteByIDs($optonValuesIDs);
        //$name = $model->name;

        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Record deleted successfully.');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the VIPOptionDescription model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPOptionDescription the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPOptionDescription::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
