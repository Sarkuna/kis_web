<?php

namespace app\modules\catalog\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

use yii\imagine\Image;
use Imagine\Image\Box;
use backend\models\DynamicForms;
use backend\models\Model;
use common\models\Uploads;
use common\models\VIPProduct;
use common\models\VIPProductSearch;
use common\models\VIPProductOptionValue;
use common\models\VIPOptionDescription;

/**
 * ProductsController implements the CRUD actions for VIPProduct model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VIPProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VIPProduct model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VIPProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VIPProduct();
        $taboptions = VIPOptionDescription::find()->all();
        foreach ($taboptions as $i => $taboption) {
            $modelsOptionvalue[$i] = [new VIPProductOptionValue];
        }
        //$modelsOptionvalue[] = [new VIPProductOptionValue];
        //VIPOptionValueDescription()

        if ($model->load(Yii::$app->request->post())) {
            if(!empty(Yii::$app->request->post()['VIPProductOptionValue'])){
                for ($i=0; $i<count($taboptions); $i++) {
                    $loadsData['VIPProductOptionValue'] =  Yii::$app->request->post()['VIPProductOptionValue'][$i];
                    $modelsOptionvalue[$i] = DynamicForms::createMultiple(VIPProductOptionValue::classname(),[] ,$loadsData);
                    //$modelsOptionvalue = Model::createMultiple(VIPOptionValueDescription::classname(), $modelsOptionvalue);
                    DynamicForms::loadMultiple($modelsOptionvalue[$i], $loadsData);
                }
                $valid = $this::validateDeposit($taboptions,$modelsOptionvalue);
            }else {
                $valid = true;
            }

            ///$valid = Model::validateMultiple($modelsOptionvalue[$i]);
            
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if ($flag) {
                            $this->Uploads(false);
                            $newCover = UploadedFile::getInstance($model, 'main_image1');
                            if (!empty($newCover)) {
                                //unlink(Yii::$app->basePath .'/web/upload/product_cover/'.$model->main_image);
                                $path = Yii::$app->basePath .'/web/upload/product_cover/';
                                $thumbnailImagePath = Yii::$app->basePath .'/web/upload/product_cover/thumbnail/';
                                $newCoverName = Yii::$app->security->generateRandomString();
                                $imgname = $newCoverName . '.' . $newCover->extension;
                                $model->main_image = $imgname;
                                $newCover->saveAs($path . $newCoverName . '.' . $newCover->extension);
                                //$imagine = Image::getImagine();
                                //$image = $imagine->open($path . $imgname);
                                //$image->resize(new Box(500, 300))->save($thumbnailImagePath . $imgname, ['quality' => 100]);
                            }
                            if(!empty(Yii::$app->request->post()['VIPProductOptionValue'])){
                                $optionID = '';
                                for ($i=0; $i<count($taboptions); $i++) {
                                    $optionID = $taboptions[$i]->option_id;
                                    foreach ($modelsOptionvalue[$i] as $ix => $optionvalue) {
                                        // save the load record
                                        $optionvalue->product_id = $model->product_id;
                                        $optionvalue->option_id = $optionID;
                                        //$modelPaymentLoads->option_value_id = $optionID;
                                        if (($flag = $optionvalue->save(false)) === false) {
                                            //$modelsOptionvalue
                                            //print_r($modelsOptionvalue->getErrors());        
                                            $transaction->rollBack();
                                            break;
                                        }

                                    }
                                }
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        //return $this->redirect(['view', 'id' => $modelCatalogOption->id]);
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified clients!']);
                        return $this->redirect(['index']);
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }else {
                \Yii::$app->getSession()->setFlash('danger',['title' => 'Fail', 'text' => 'You have modified clients!']);
                return $this->redirect(['index']);
            }
            /*if($model->save()){
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified clients!']);
                return $this->redirect(['index']);
            }*/
            //return $this->redirect(['view', 'id' => $model->product_id]);
        } else {
            $model->ref = substr(Yii::$app->getSecurity()->generateRandomString(),10); 
        }
        
        return $this->render('create', [
                'model' => $model,
                //'modelsOptionvalue' => $modelsOptionvalue,
                'modelsOptionvalue' => (empty($modelsOptionvalue)) ? [new VIPProductOptionValue] : $modelsOptionvalue,
                'taboptions' => $taboptions
            ]);
    }
    

    /**
     * Updates an existing VIPProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $taboptions = VIPOptionDescription::find()->all();
        $oldLoadIds = [];
        foreach ($taboptions as $i => $taboption) {
            //$modelsOptionvalue[$i] = [new VIPProductOptionValue];
            $optionID = $taboptions[$i]->option_id;
            $oldLoads = VIPProductOptionValue::findAll(['product_id' => $id, 'option_id' => $optionID]);
            $modelsOptionvalue[$i] = $oldLoads;
            $oldLoadIds = array_merge($oldLoadIds,ArrayHelper::getColumn($oldLoads,'product_option_value_id'));
            $modelsOptionvalue[$i] = (empty($modelsOptionvalue[$i])) ? [new VIPProductOptionValue] : $modelsOptionvalue[$i];
        }
        list($initialPreview,$initialPreviewConfig) = $this->getInitialPreview($model->ref);
        if ($model->load(Yii::$app->request->post())) {
            $this->Uploads(false);
            $newCover = UploadedFile::getInstance($model, 'main_image1');
            if (!empty($newCover)) {
                //unlink(Yii::$app->basePath .'/web/upload/product_cover/'.$model->main_image);
                $newCoverName = Yii::$app->security->generateRandomString();
                $imgname = $newCoverName . '.' . $newCover->extension;
                $model->main_image = $imgname;
                $newCover->saveAs(Yii::$app->basePath .'/web/upload/product_cover/' . $newCoverName . '.' . $newCover->extension);
            }
            $newLoadIds = [];
            $deletedIDs = [];
            $postdata = [];
            if(!empty(Yii::$app->request->post()['VIPProductOptionValue'])){
                //$del = VIPProductOptionValue::deleteAll(['product_id' => $id]);
                
                for ($i=0; $i<count($taboptions); $i++) {
                    $postdata[] = Yii::$app->request->post()['VIPOptionDescription'][$i];
                    if(!empty($postdata[$i]['testaaa'])){
                        $loadsData['VIPProductOptionValue'] =  Yii::$app->request->post()['VIPProductOptionValue'][$i];
                        $modelsOptionvalue[$i] = DynamicForms::createMultiple(VIPProductOptionValue::classname(),[$modelsOptionvalue[$i]] ,$loadsData);
                        DynamicForms::loadMultiple($modelsOptionvalue[$i], $loadsData);
                        $newLoadIds = array_merge($newLoadIds,ArrayHelper::getColumn($loadsData['VIPProductOptionValue'],'product_option_value_id'));
                    }
                }
                //print_r($newLoadIds);
                //$oldLoadIds = ArrayHelper::getColumn($oldLoads,'product_option_value_id');
                $delLoadIds = array_diff($oldLoadIds,$newLoadIds);
                if (! empty($delLoadIds)){
                   $del = VIPProductOptionValue::deleteAll(['product_option_value_id' => $delLoadIds]);
                }

                //$valid = $this::validateDeposit($taboptions,$modelsOptionvalue);
                $valid = true;
            }else {
                $valid = true;
            }

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if ($flag) {
                            if(!empty(Yii::$app->request->post()['VIPProductOptionValue'])){
                                $optionID = '';
                                $del = VIPProductOptionValue::deleteAll(['product_id' => $id]);
                                for ($i=0; $i<count($taboptions); $i++) {
                                    $optionID = $taboptions[$i]->option_id;
                                    $postdata[] = Yii::$app->request->post()['VIPOptionDescription'][$i];
                                    if(!empty($postdata[$i]['testaaa'])){
                                        //$modelsOptionvalue = VIPProductOptionValue::findAll(['product_id' => $id, 'option_id' => $optionID]);
                                        foreach ($modelsOptionvalue[$i] as $ix => $optionvalue) {
                                            // save the load record

                                            $optionvalue->product_id = $model->product_id;
                                            $optionvalue->option_id = $optionID;
                                            //$modelPaymentLoads->option_value_id = $optionID;
                                            if (($flag = $optionvalue->save(false)) === false) {
                                                //$modelsOptionvalue
                                                //print_r($modelsOptionvalue->getErrors());        
                                                $transaction->rollBack();
                                                break;
                                            }

                                        }
                                    }
                                }
                            }
                        }//Finis Flag
                    }

                    if ($flag) {
                        $transaction->commit();
                        //return $this->redirect(['view', 'id' => $modelCatalogOption->id]);
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified clients!']);
                        return $this->redirect(['index']);
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }else {
                echo 'no valid';
            }
            
            
            /*if($model->save()){
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified clients!']);
                return $this->redirect(['index']);
            }*/
            //eturn $this->redirect(['view', 'id' => $model->product_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'initialPreview'=>$initialPreview,
                'initialPreviewConfig'=>$initialPreviewConfig,
                'modelsOptionvalue' => (empty($modelsOptionvalue)) ? [new VIPProductOptionValue] : $modelsOptionvalue,
                'taboptions' => $taboptions
            ]);
        }
    }

    /**
     * Deletes an existing VIPProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VIPProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
  /*|*********************************************************************************|
  |================================ Upload Ajax ====================================|
  |*********************************************************************************|*/
    public function actionUploadajax(){
           $this->Uploads(true);
     }
    private function CreateDir($folderName){
        if($folderName != NULL){
            $basePath = VIPProduct::getUploadPath();
            if(FileHelper::createDirectory($basePath.$folderName,0777)){
                FileHelper::createDirectory($basePath.$folderName.'/thumbnail',0777);
            }
        }
        return;
    }
    private function removeUploadDir($dir){
        FileHelper::removeDirectory(VIPProduct::getUploadPath().$dir);
    }
    private function Uploads($isAjax=false) {
             if (Yii::$app->request->isPost) {
                $images = UploadedFile::getInstancesByName('upload_ajax');
                if ($images) {
                    if($isAjax===true){
                        $ref =Yii::$app->request->post('ref');
                    }else{
                        $PhotoLibrary = Yii::$app->request->post('VIPProduct');
                        $ref = $PhotoLibrary['ref'];
                    }
                    $this->CreateDir($ref);
                    foreach ($images as $file){
                        $fileName       = $file->baseName . '.' . $file->extension;
                        $realFileName   = md5($file->baseName.time()) . '.' . $file->extension;
                        $savePath       = VIPProduct::UPLOAD_FOLDER.'/'.$ref.'/'. $realFileName;
    
                    
                        if($file->saveAs($savePath)){
                            if($this->isImage(Url::base(true).'/'.$savePath)){
                                //var_dump(createThumbnail($ref,$realFileName));
                                $this->createThumbnail($ref,$realFileName);
                            } 
                          
                            $model                  = new Uploads;
                            $model->ref             = $ref;
                            $model->file_name       = $fileName;
                            $model->real_filename   = $realFileName;
                            $model->save();
                            if($isAjax===true){
                                echo json_encode(['success' => 'true']);
                            }
                            
                        }else{
                            if($isAjax===true){
                                echo json_encode(['success'=>'false','eror'=>$file->error]);
                            }
                        }
                        
                        
                        
                    }
                }
            }
    }
    private function getInitialPreview($ref) {
            $datas = Uploads::find()->where(['ref'=>$ref])->all();
            $initialPreview = [];
            $initialPreviewConfig = [];
            foreach ($datas as $key => $value) {
                array_push($initialPreview, $this->getTemplatePreview($value));
                array_push($initialPreviewConfig, [
                    'caption'=> $value->file_name,
                    'width'  => '120px',
                    'url'    => Url::to(['/catalog/products/deletefile-ajax']),
                    'key'    => $value->upload_id
                ]);
            }
            return  [$initialPreview,$initialPreviewConfig];
    }
    public function isImage($filePath){
            return @is_array(getimagesize($filePath)) ? true : false;
    }
    private function getTemplatePreview(Uploads $model){     
            $filePath = VIPProduct::getUploadUrl().$model->ref.'/thumbnail/'.$model->real_filename;
            $isImage  = $this->isImage($filePath);
            if($isImage){
                $file = Html::img($filePath,['class'=>'file-preview-image', 'alt'=>$model->file_name, 'title'=>$model->file_name]);
            }else{
                $file =  "<div class='file-preview-other'> " .
                         "<h2><i class='glyphicon glyphicon-file'></i></h2>" .
                         "</div>";
            }
            return $file;
    }
    private function createThumbnail($folderName,$fileName,$width=270){
      $uploadPath   = VIPProduct::getUploadPath().'/'.$folderName.'/'; 
      $file         = $uploadPath.$fileName;
      $image        = Yii::$app->image->load($file);
      $image->resize($width);
      $image->save($uploadPath.'thumbnail/'.$fileName);
      return;
    }
    
    public function actionDeletefileAjax(){
        $model = Uploads::findOne(Yii::$app->request->post('key'));
        if($model!==NULL){
            $filename  = VIPProduct::getUploadPath().$model->ref.'/'.$model->real_filename;
            $thumbnail = VIPProduct::getUploadPath().$model->ref.'/thumbnail/'.$model->real_filename;
            if($model->delete()){
                @unlink($filename);
                @unlink($thumbnail);
                echo json_encode(['success'=>true]);
            }else{
                echo json_encode(['success'=>false]);
            }
        }else{
          echo json_encode(['success'=>false]);  
        }
    }
    
    public static function validateDeposit($taboptions, $modelsOptionvalue)
    {
        $valid = true;
        for ($i=0; $i<count($taboptions); $i++) {
            $valid = Model::validateMultiple($modelsOptionvalue[$i]);
        }

        return $valid;
    }
}
