<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPAssignSubCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vipassign Sub Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipassign-sub-categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Vipassign Sub Categories', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'vip_assign_sub_categories_id',
            'clientID',
            'categories_id',
            'sub_categories_id',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
