<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;

use common\models\VIPManufacturer;
use common\models\VIPCategories;
use common\models\VIPSubCategories;
use common\models\VIPStockStatus;

use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\DatePicker;
use yii\widgets\MaskedInput;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipproduct-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'vipproduct-form','class' => 'form-horizontal form-label-left','enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">
    <?php
                    echo $form->field($model, 'main_image1')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'allowedFileExtensions' => ['jpg','png','jpeg','bmp'],
                            'initialPreview' => [
                                //Html::img("/images/moon.jpg", ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon'])
                                //$model->main_image ? Html::img('/upload/product_cover/' . $model->main_image, ['class' => 'thumbnail', 'alt' => 'Logo', 'title' => 'Logo']) : null, // checks the models to display the preview
                            ],
                        ]
                    ])->label(false);
                    ?>
    
    </div>
    <div class="box-footer">
        <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10 col-md-12 col-sm-6 col-xs-12">
            <?= Html::submitButton('<i class="fa fa-save"></i>', ['class' => 'btn btn-primary']) ?>
            <?= \yii\helpers\Html::a( '<i class="fa fa-reply"></i>', Yii::$app->request->referrer, ['class' => 'btn btn-default']); ?>
        </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>