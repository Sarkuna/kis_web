<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */

$this->title = $model->product_code.'Update Product: ' . $model->product_name;
$this->params['breadcrumbs'][] = ['label' => 'Product', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->product_id, 'url' => ['view', 'id' => $model->product_id]];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary vipproduct-update">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>            
    </div><!-- /.box-header -->


    <div class="vipproduct-update">
        <?=
        $this->render('_form', [
            'model' => $model,
            'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,
        ])
        ?>

    </div>


</div>    

