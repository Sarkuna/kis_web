<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\VIPSubCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipsub-categories-form">
    <div class="box-body">
    <?php $form = ActiveForm::begin(['options' => ['id' => 'vipsub-categories-form','class' => 'form-horizontal form-label-left']]); ?>

    <?php
    $categorie = common\models\VIPCategories::find()->all();
    $categories = ArrayHelper::map($categorie, 'vip_categories_id', 'name');
    //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
    echo $form->field($model, 'parent_id', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6'>{input}</div>\n{hint}\n{error}"
                        ])
                        ->dropDownList(
                            $categories,         
                            ['prompt'=>'Select Category', 'id' => 'relation1']    
                        );
    ?>

<?= $form->field($model, 'name', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>
    
    <?= $form->field($model, 'slug', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>

<?= $form->field($model, 'description', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textarea(['rows' => 6]);  ?>

<?= $form->field($model, 'meta_title', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>
    
    <?= $form->field($model, 'meta_keywords', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>

<?= $form->field($model, 'meta_description', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>
    
 <?= $form->field($model, 'position', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>   

    
    <?php
    //$themes = common\models\VipCountry::find()->all();
    $listData = ['A' => 'Active', 'D' => 'Detective'];
    //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
    echo $form->field($model, 'is_active', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6'>{input}</div>\n{hint}\n{error}"
                        ])
                        ->dropDownList(
                            $listData         
                            //['prompt'=>'--', 'id' => 'relation1']    
                        );
    ?>
    </div>
    <div class="box-footer">
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
