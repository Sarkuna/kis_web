<?php

namespace app\modules\clients\controllers;

use Yii;
use common\models\ProductList;
use common\models\ProductListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductListController implements the CRUD actions for ProductList model.
 */
class ProductListController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new ProductListSearch();
        $searchModel->clientID = $clientID;
        $searchModel->del = 'A';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$model = $searchModel->getData();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = new ProductList();
        $model->clientID = $clientID;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->product_list_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUploadexcel() {
        
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $model = new \backend\models\ExcelForm();
        $msg = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = date('mmssh_dmy');
                $model->excel->saveAs(Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                //

                $pre_defined = array( "product_name_id", "description", "metric", "pack_size", "total_points_awarded");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch > 4){
                    $data = array();

                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);
                        
                        $product_name_id = $rowData[0]['product_name_id'];
                        
                        if(!empty($product_name_id)){
                            $productName = \common\models\ProductName::find()->where(['name' => $product_name_id])->one();
                            if(count($productName) == 0) {
                                $productName = new \common\models\ProductName();
                                $productName->name = $product_name_id;
                                $productName->clientID = $clientID;
                                $productName->save(false);
                            }
                            $product_name_id = $productName->product_name_id;
                        }

                        $data[] = [$clientID,$product_name_id,$rowData[0]['description'],$rowData[0]['metric'],$rowData[0]['pack_size'],$rowData[0]['total_points_awarded'],'A'];

                    }

                    Yii::$app->db
                    ->createCommand()
                    ->batchInsert('product_list', ['clientID','product_name_id','description','metric','pack_size','total_points_awarded','del'],$data)
                    ->execute();
                    
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Import Success', 'text' => 'Product Import Sccessfully']);
                    unlink($inputFile);
                }else {
                    $teml = 'product name, links, meta tag title, product code, suppliers product code (isku), variant, msrp, partner price, peninsular, sabah, sarawak, singapore, manufacturer id, category id, sub category id';
                    \Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Sorry your excel template not match. This should on your excel ! ('.$teml.') Note:case sensitive no issue']);
                    //return $this->redirect(['uploadexcel']);
                }
                return $this->redirect(['index']);
                /*return $this->render('uploadexcelreport', [
                    'msg' => $returnedValue,
                ]);*/
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
        //echo("6");
    }

    /**
     * Updates an existing ProductList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->product_list_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->del = 'X';
        if($model->save(false)) {
            return $this->redirect(['index']);
        }
        //$this->findModel($id)->delete();

        
    }

    /**
     * Finds the ProductList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
