<?php

namespace app\modules\clients\controllers;

use Yii;
use common\models\TotalProductList;
use common\models\TotalProductListSearch;

use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TotalProductListController implements the CRUD actions for TotalProductList model.
 */
class TotalProductListController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TotalProductList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new TotalProductListSearch();
        $searchModel->clientID = $clientID;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionUploadExcel() {
        
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $model = new \backend\models\ExcelTotalProductList();
        $msg = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = date('mmssh_dmy');
                $model->excel->saveAs(Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "brand", "product name (total)", "region", "distributor code", "product name (distirbutor)", "iws", "mz");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch == 7){
                    $data = array();                    
                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);


                        if(!empty($rowData[0]['product name (distirbutor)'])) {
                            $product_name_distirbutor = $rowData[0]['product name (distirbutor)'];
                        }else {
                            $product_name_distirbutor = '';
                        }

                        $data[] = [$clientID,$rowData[0]['distributor code'],$rowData[0]['brand'],$rowData[0]['product name (total)'],$product_name_distirbutor,$rowData[0]['region'],$rowData[0]['iws'],$rowData[0]['mz'],'A'];

                    }
                    $returnedValue = Yii::$app->db
                    ->createCommand()
                    ->batchInsert('total_product_list', ['clientID','distributor_code','brand', 'product_name','product_name_distirbutor','region','iws','mz','del'],$data)
                    ->execute();  
                    //}
                    unlink($inputFile);
                    if ($returnedValue) {
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Import Success', 'text' => 'Users Import Sccessfully']);
                        
                    } else {
                        \Yii::$app->getSession()->setFlash('warning',['title' => 'Import Fail', 'text' => 'Sorry users import fail!']);
                        //return $this->redirect(['uploadexcel']);
                    }
                    return $this->redirect(['index']);
                    //return $this->redirect(['view', 'id' => $modelimport->import_customer_upload_summary_id]);
                    /*return $this->render('uploadexcelreport', [
                        'msg' => $returnedValue,
                    ]);*/
                    
                }else {
                    $teml = 'Brand,Product Name (TOTAL),Region,Distributor Code,Product Name (Distirbutor),IWZ,MZ';
                    \Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Sorry your excel template not match. This should on your excel ! ('.$teml.') Note:case sensitive no issue']);
                    return $this->redirect(['uploadexcel']);
                }
            }
        } else {
            return $this->render('uploadexcel_form', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single TotalProductList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TotalProductList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    /*public function actionCreate()
    {
        $model = new TotalProductList();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->total_product_list_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Updates an existing TotalProductList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
               \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Product has been updated']); 
            }else {
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Fail', 'text' => 'Sorry product has been not updated']); 
            }
            //return $this->redirect(['view', 'id' => $model->total_product_list_id]);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TotalProductList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TotalProductList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TotalProductList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TotalProductList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
