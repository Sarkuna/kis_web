<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\VIPManufacturer;
use common\models\VIPCategories;
use common\models\VIPSubCategories;

/* @var $this yii\web\View */
/* @var $model common\models\VIPAssignCategories */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
$clientID = $session['currentclientID'];
?>

<div class="well">


    <?php $form = ActiveForm::begin(['action' => '/clients/assign/productfilter','options' => ['method' => 'post']]) ?>
    
    <div class="row">
        <div class="col-sm-4">
            <?php
                //$categories = VIPCategories::find()->all();
                $categories = common\models\VIPAssignCategories::find()
                                    ->joinWith(['categories'])
                                    ->where(['clientID' => $session['currentclientID']])->all();
                $categoriesData = ArrayHelper::map($categories, 'categories_id', 'categories.name');
                //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
                echo $form->field($formfilter, 'categories', [
                            //'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                        ])
                        ->dropDownList(
                                $categoriesData, [
                            'prompt' => '--',
                            'id' => 'categories_id',
                            'onchange' => '
                                            $.get( "' . Url::toRoute('/catalog/sub-categories/lists') . '", { id: $(this).val() } )
                                                .done(function( data ) {
                                                    $( "select#vip_sub_categories_id" ).html( data );
                                                }
                                            );
                                        '
                                ]
                );
            ?>
            <?=
            $form->field($formfilter, 'price', [
                //'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
        </div>
        <div class="col-sm-4">
            <?php
                $subcategories = VIPSubCategories::find()->all();
                $subcategoriesData = ArrayHelper::map($subcategories, 'vip_sub_categories_id', 'name');
                //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
                echo $form->field($formfilter, 'subcategories', [
                            //'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                        ])
                        ->dropDownList(
                                $subcategoriesData, ['prompt' => '--', 'id' => 'vip_sub_categories_id']
                );
            ?>
            <?=
            $form->field($formfilter, 'quantity', [
                //'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
        </div>
        <div class="col-sm-4">
            <?php
            $manufacturer = VIPManufacturer::find()->all();
            $manufacturerData = ArrayHelper::map($manufacturer, 'manufacturer_id', 'name');
            //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
            echo $form->field($formfilter, 'manufacturer', [
                        //'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                    ])
                    ->dropDownList(
                            $manufacturerData, ['prompt' => '--', 'id' => 'relation1']
            );
            ?>
            <div class="form-group">
                <div class="col-md-12 col-sm-6 col-xs-12 text-right">
                    <?= Html::submitButton('<i class="fa fa-search"></i> Filter', ['class' => 'btn btn-primary']) ?>

                </div>
            </div>
        </div>
        
    
        
        </div>


    

    <?php ActiveForm::end(); ?>

</div>

<?php
    $script = <<<EOD
            
$("input[name='AssignCategoriesForm[categories_id][]']").change(function () {
    var maxAllowed = 2;
    var cnt = $("input[name='AssignCategoriesForm[categories_id][]']:checked").length;
    if (cnt > maxAllowed){
        $(this).prop("checked", "");
        alert('Select maximum ' + maxAllowed + ' Categorie[s]');
    }
});

EOD;
$this->registerJs($script);
?>            