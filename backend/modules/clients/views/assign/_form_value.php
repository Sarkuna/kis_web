<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPManufacturer */
/* @var $form yii\widgets\ActiveForm */
//$this->title = 'Update Product Name: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['products']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->product_name_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php $form = ActiveForm::begin();?>
            <div class="box-body">
                <?php
                    echo $form->field($model, 'p_type')->dropDownList(
                                ['P' => 'Percentage', 'R' => 'RM']
                        ); 
                ?>
                <?= $form->field($model, 'p_value')->textInput() ?>
                
                <?php
                    echo $form->field($model, 'assign_status')->dropDownList(
                                ['A' => 'Active', 'D' => 'Deactive']
                        ); 
                ?>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
