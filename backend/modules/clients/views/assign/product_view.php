<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */
/* @var $form yii\widgets\ActiveForm */
$this->title = Html::encode($model->product_name);
$this->params['breadcrumbs'][] = ['label' => 'Assign Products', 'url' => ['products']];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>
<style>
    div#general img {max-width: 100%;}
    
</style>
<div class="row">
    <div class="col-md-3">
        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Product</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?= $model->main_image ? Html::img('/upload/product_cover/thumbnail/' . $model->main_image, ['class' => 'thumbnail img-responsive', 'alt' => 'Logo', 'title' => 'Logo']) : null ?>
                
                
                
                <strong><?= $model->getAttributeLabel('manufacturer_id') ?></strong>
                <p class="text-muted">
                    <?= $model->manufacturer_id === null ? 'N\A' : $model->manufacturer->name ?>
                </p>

                <hr>

                <strong><?= $model->getAttributeLabel('category_id') ?></strong>

                <p class="text-muted">
                    <?= $model->categories->name ?>
                </p>

                <hr>

                <strong><?= $model->getAttributeLabel('sub_category_id') ?></strong>

                <p class="text-muted">
                    <?= Html::encode($model->subCategories->name) ?>
                </p>

                <hr>
                <strong><?= $model->getAttributeLabel('product_code') ?></strong>
                <p class="text-muted">
                    <?= Html::encode($model->product_code) ?>
                </p>
                <hr>
                
                <?php
                if($session['currentusertype'] == 'A') {
                ?>
                <strong>Retail Price RM</strong>
                <p class="text-muted">
                    <?php
                    $price = $productcheck->msrp;
                    echo Yii::$app->formatter->asDecimal($price);
                    ?>
                </p>
                <hr>
                <strong>Mark Up</strong>
                <p class="text-muted">
                    <?php
                    $mark_up = Yii::$app->VIPglobal->markUp($productcheck->product->product_id);
                    echo Yii::$app->formatter->asDecimal($mark_up);
                    ?>
                </p>
                <hr>
                <strong>Type</strong>
                <p class="text-muted">
                    <?php
                    $price = Yii::$app->VIPglobal->markUpType($productcheck->product->product_id);
                    echo $price;
                    ?>
                </p>
                <hr>
                <strong>Display Price RM</strong>
                <p class="text-muted">
                    <?php
                    $display_price = Yii::$app->VIPglobal->myPrice($productcheck->product->product_id);
                    echo Yii::$app->formatter->asDecimal($display_price);
                    ?>
                </p>
                <hr>
                <strong>Point (0.50)</strong>
                <p class="text-muted">
                    <?php
                    $point = Yii::$app->VIPglobal->myPoint($productcheck->product->product_id);
                    echo Yii::$app->formatter->asInteger($point);
                    ?>
                </p>
                <hr>
                <?php
                }
                ?>

                <strong><?= $model->getAttributeLabel('quantity') ?></strong>
                <p class="text-muted">
                    <?= Html::encode($model->quantity) ?>
                </p>
                <hr>
                <strong><?= $model->getAttributeLabel('minimum') ?></strong>
                <p class="text-muted">
                    <?= Html::encode($model->minimum) ?>
                </p>
                <hr>
                <strong><?= $model->getAttributeLabel('stock_status_id') ?></strong>
                <p class="text-muted">
                    <?= $model->stockStatus->name ?>
                </p>
                <hr>
                <strong><?= $model->getAttributeLabel('date_available') ?></strong>
                <p class="text-muted">
                    <?= date('d-m-Y', strtotime($model->date_available)) ?>
                </p>
                <hr>
                <strong><?= $model->getAttributeLabel('status') ?></strong>
                <p class="text-muted">
                    <?= $model->status ?>
                </p>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>


    <div class="vipproduct-form col-md-9">
        <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="#general" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">General</a></li>
                    <li role="presentation" class=""><a href="#tab_option" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Shipping</a></li>
                    <li role="presentation" class=""><a href="#tab_images" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Images</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="general">
                        <strong><?= $model->getAttributeLabel('product_name') ?></strong>
                        <p class="text-muted">
                            <?= Html::encode($model->product_name) ?>
                        </p>
                        <hr>
                        <strong><?= $model->getAttributeLabel('meta_tag_title') ?></strong>
                        <p class="text-muted">
                            <?= Html::encode($model->meta_tag_title) ?>
                        </p>
                        <hr>
                        <?= $model->product_description ?>
                    </div>

                    <div class="tab-pane" id="tab_option">
                        <div class="row">
                            <div class="col-xs-12 table-responsive">
                                <?php
                                $session = Yii::$app->session;
$clientID = $session['currentclientID'];
                            $clientsetup = Yii::$app->VIPglobal->clientSetup();
                            $pointpersent = $clientsetup['point_value'];
                            $deleerycost = $profile = \common\models\VIPProductDeliveryZone::find()->where(['product_id' => $model->product_id, 'clientID' => $clientID])->count();
                            if($deleerycost > 0) {
                                $deleerycostlists = $profile = \common\models\VIPProductDeliveryZone::find()
                                        ->where(['product_id' => $model->product_id, 'clientID' => $clientID])
                                        ->andWhere(['>','price',0])       
                                        ->all();
                                echo '<ul class="list-group list-group-flush">';
                                //echo '<tr><td>Zone</td><td>Points</td></tr>';
                                foreach($deleerycostlists as $deleerycostlist) {
                                    $zone = \common\models\VIPZone::find()->where(['zone_id' => $deleerycostlist->zone_id])->one();
                                    if(!empty($deleerycostlist->price)) {
                                        $newpoint = $deleerycostlist->price / $pointpersent;
                                    }else {
                                        $newpoint = 0;
                                    }
                                    echo '<li class="list-group-item">
                                        <span class="badge badge-pill bg-info float-right">'.Yii::$app->formatter->asInteger($newpoint).' pts</span>
                                        '.$zone->name.'</li>';
                                    //echo '<tr><td>'.$zone->name.'</td><td>'.Yii::$app->formatter->asInteger($newpoint).'pts</td></tr>';
                                }
                                echo '</ul>';
                            }
                        ?>

                               
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab_images">
                        <div class="row">
                            <div class="col-lg-12 gallery">
                        <?= dosamigos\gallery\Gallery::widget(['items' => $model->getThumbnails($model->ref, Html::encode($model->product_code))]); ?>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

    </div>
</div>


