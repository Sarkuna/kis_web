<style>
    .nav>li>a {
        padding: 10px 10px;
    }
    .nav-tabs-custom>.nav-tabs>li.active>a {
    background-color: #00C0EF;
    color: #ffffff;
}
</style>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */

$this->title = $model->first_name;
$this->params['breadcrumbs'][] = ['label' => 'Manage Dealers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="vipcustomer-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <strong><i class="fa fa-book margin-r-5"></i> Code</strong>
                    <p class="text-muted"><?= $model->dealer_code ?></p>
                    <hr>
                    
                    <strong><i class="fa fa-building margin-r-5"></i> Company</strong>
                    <p class="text-muted"><?= $model->first_name ?> <?= $model->company ?></p>
                    <hr>
                    
                    <strong><i class="fa fa-user margin-r-5"></i> Name</strong>
                    <p class="text-muted"><?= $model->first_name ?> <?= $model->last_name ?></p>
                    <hr>
                    
                    <strong><i class="fa fa-phone margin-r-5"></i> Contact No</strong>
                    <p class="text-muted"><?=  $model->tel ?></p>
                    <hr>
                    
                    <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                    <p class="text-muted"><?= $model->user->email ?></p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                    <p class="text-muted">
                        <?=  $model->address1.' '.$model->address2 ?>
                        <br><?= $model->city.' '.$model->postcode ?>
                        <br><?= $model->region.' '.$model->countrys->name ?>
                    </p>
                </div>
            </div>
        </div>
        <!-- /.col -->       
    </div>
</div>