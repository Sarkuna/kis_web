<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;

use common\models\ProductName;

/* @var $this yii\web\View */
/* @var $model common\models\ProductList */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
$productnames = ProductName::find()
            ->where([
                'clientID' => $session['currentclientID'],
        ])->all();
$productname = ArrayHelper::map($productnames, 'product_name_id', 'name');
?>
<div class="box-body">
<div class="product-list-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'product_name_id', [
  
])->widget(Select2::classname(), [
                'data' => $productname,
                //'language' => 'de',
                'options' => [
                    'placeholder' => 'Select ...',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],               
                                
            ]);  ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pack_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_points_awarded')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>