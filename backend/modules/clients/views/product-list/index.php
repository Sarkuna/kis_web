<style>
    .space{margin-left: 5px;}
</style>
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-list-index">
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-bookmark"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Product Names</span>
              <span class="info-box-number"><?= $searchModel->getProductNameTotal(); ?></span>
              <span class="info-box-text"><a href="/clients/product-name">View</a></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-bookmark"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Active Product Items</span>
              <span class="info-box-number"><?= $searchModel->getProductItemTotal(); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-bookmark"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Deleted Product Items</span>
              <span class="info-box-number"><?= $searchModel->getProductItemTotalDel(); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <!-- /.col -->
      </div>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            
            <?= Html::a('<i class="fa fa-upload"></i>', ['create'], ['class' => 'btn btn-info pull-right space', 'title' => 'Upload Excel File']) ?> 
            <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success pull-right', 'title' => 'Add New']) ?>
        </div><!-- /.box-header -->

        <div class="box-body">

            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'product_list_id',
                    //'clientID',
                    [
                        'attribute' => 'product_name_id',
                        'label' => 'Product Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->productName->name;
                        },
                    ],
                    //'product_name_id',
                    'description:ntext',
                    'metric',                    
                    'pack_size',
                    'total_points_awarded',
                    /*[
                        'attribute' => 'rm_value',
                        'label' => 'RM Value',
                        'format' => 'html',
                        'headerOptions' => ['width' => '100'],
                        'contentOptions' => ['class' => 'text-right'],
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDecimal($model->total_points_awarded * $model->getValuerm());
                        },
                    ],*/            
                    //['class' => 'yii\grid\ActionColumn'],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}', //{view} {delete}
                        'buttons' => [
                            'update' => function ($url, $model) {
                                //$url = 'update?id=' . $model->adminUserProfile->profile_ID;
                                return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                            },
                            'delete' => function ($url, $model) {
                                return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                            }
                                /* ,
                                  'view' => function ($url, $model) {
                                  return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                                  },
                                 */
                                ],
                            //'visible' => $visible,
                            ],
                                    
                ],
            ]);
            ?>


        </div>
    </div>

</div>


