<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductName */

$this->title = 'Update Product Name: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Product Names', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->product_name_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="box box-primary product-name-update">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


</div>
