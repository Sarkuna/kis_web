<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'projects_id') ?>

    <?= $form->field($model, 'userID') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'dealerID') ?>

    <?= $form->field($model, 'projects_ref') ?>

    <?php // echo $form->field($model, 'projects_date') ?>

    <?php // echo $form->field($model, 'project_name') ?>

    <?php // echo $form->field($model, 'project_status') ?>

    <?php // echo $form->field($model, 'project_client_name') ?>

    <?php // echo $form->field($model, 'project_client_email') ?>

    <?php // echo $form->field($model, 'project_client_tel') ?>

    <?php // echo $form->field($model, 'project_details') ?>

    <?php // echo $form->field($model, 'project_nominated_points') ?>

    <?php // echo $form->field($model, 'project_approved_points') ?>

    <?php // echo $form->field($model, 'ref') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
