<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TotalProductListSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="total-product-list-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'total_product_list_id') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'distributor_code') ?>

    <?= $form->field($model, 'brand') ?>

    <?= $form->field($model, 'product_name') ?>

    <?php // echo $form->field($model, 'product_name_distirbutor') ?>

    <?php // echo $form->field($model, 'region') ?>

    <?php // echo $form->field($model, 'iws') ?>

    <?php // echo $form->field($model, 'mz') ?>

    <?php // echo $form->field($model, 'del') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
