<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TotalProductList */

$this->title = 'Create Total Product List';
$this->params['breadcrumbs'][] = ['label' => 'Total Product Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="total-product-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
