<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TotalProductListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary total-product-list-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    </div>

    <div class="box-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'total_product_list_id',
                'brand',
                'product_name',
                'distributor_code',
                'product_name_distirbutor',
                //'clientID',
                // 'product_name_distirbutor',
                // 'region',
                //'iws',
                [
                    'attribute' => 'iws',
                    'label' => 'IWS',
                    'format' => 'html',
                    'headerOptions' => ['width' => '100'],
                    'value' => function ($model) {
                        return $model->iws;
                    },
                ],
                [
                    'attribute' => 'mz',
                    'label' => 'MZ',
                    'format' => 'html',
                    'headerOptions' => ['width' => '100'],
                    'value' => function ($model) {
                        return $model->mz;
                    },
                ],
                // 'del',
                //['class' => 'yii\grid\ActionColumn'],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}', //{view} {delete}
                    'buttons' => [ 
                        'update' => function ($url, $model) {
                            return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                        },
                        'delete' => function ($url, $model) {
                            return (Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                        },        
                    ],
                ],             
            ],
        ]);
        ?>
    </div>
</div>
