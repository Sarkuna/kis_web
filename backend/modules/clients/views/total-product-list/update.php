<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TotalProductList */

$this->title = $model->product_name;
$this->params['breadcrumbs'][] = ['label' => 'Product Lists', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->total_product_list_id, 'url' => ['view', 'id' => $model->total_product_list_id]];
$this->params['breadcrumbs'][] = 'Update';
?>


<div class="total-product-list-update">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>

        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
