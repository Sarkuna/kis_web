<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Import Product Items Excel Files';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Lists'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Guide'), 'url' => ['guide']];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;

?>
<div class="row">
    <div class="col-lg-5">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="box-body">
                <?= $form->field($model, 'excel')->fileInput() ?>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>