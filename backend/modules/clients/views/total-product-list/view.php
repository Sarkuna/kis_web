<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TotalProductList */

$this->title = $model->total_product_list_id;
$this->params['breadcrumbs'][] = ['label' => 'Total Product Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="total-product-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->total_product_list_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->total_product_list_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'total_product_list_id',
            'clientID',
            'distributor_code',
            'brand',
            'product_name',
            'product_name_distirbutor',
            'region',
            'iws',
            'mz',
            'del',
        ],
    ]) ?>

</div>
