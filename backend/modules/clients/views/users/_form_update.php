<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPManufacturer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipmanufacturer-form">

    <?php
    $form = ActiveForm::begin(['options' => [
                    'class' => 'form-horizontal form-label-left'
    ]]);
    ?>
<div class="box-body">

    <?=
    $form->field($model, 'first_name', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'last_name', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>


</div>
<div class="box-footer">    
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

</div>
