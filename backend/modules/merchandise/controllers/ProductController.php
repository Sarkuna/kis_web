<?php

//namespace app\modules\support\controllers;
namespace app\modules\merchandise\controllers;

use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

use common\models\MPOrder;
use common\models\MPOrderSearch; 
use common\models\MPOrderHistory;
use common\models\MPProductName;
use common\models\MPProductType;
use common\models\MPProductColour;
use common\models\MPProductSize;
use common\models\VIPCustomerReward;
use common\models\VIPCustomerAddress;


/**
 * Default controller for the `support` module
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new MPOrderSearch();
        $searchModel->order_status = 'Accepted';
        //$searchModel->customer_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->query->andWhere(['flash_deals_id' => 0]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionProcessing()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new MPOrderSearch();
        $searchModel->order_status = 'Processing';
        //$searchModel->customer_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->query->andWhere(['flash_deals_id' => 0]);

        return $this->render('processing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionApproves()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new MPOrderSearch();
        $searchModel->order_status = 'Approves';
        //$searchModel->customer_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->query->andWhere(['flash_deals_id' => 0]);

        return $this->render('approves', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDecline()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new MPOrderSearch();
        $searchModel->order_status = 'Decline';
        //$searchModel->customer_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->query->andWhere(['flash_deals_id' => 0]);

        return $this->render('decline', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionOrder(){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new MPOrderSearch();
        //$searchModel->clientID = $clientID;
        $searchModel->customer_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->query->andWhere(['flash_deals_id' => 0]);

        return $this->render('order', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
    }
    
    /**
     * Displays a single VIPOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    public function actionPrint($id)
    {
        $model = $this->findModel($id);
        return $this->render('view_print', [
            'model' => $model,
        ]);
    }
    
    public function actionOrderapprove($id=null)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $model = $this->findModel($id);
        $actionform = new \backend\models\MerchandiseActionForm();
        $totalpoint = 0;

        if (Yii::$app->request->isAjax && $actionform->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($actionform);
        }
        if ($actionform->load(Yii::$app->request->post())) {
            $totalpoint = $model->getTotalPoints();
            $model->order_status = $actionform->order_status_id;
            if($model->save()){
                if($model->order_status == 'Decline'){
                    $reward = new VIPCustomerReward();
                    $reward->clientID = $clientID;
                    $reward->customer_id = $model->customer_id;
                    $reward->order_id = $model->mp_order_id;
                    $reward->description = 'Product '.$model->order_status.' : Merchandise Order # '.$model->invoice_prefix.'';
                    $reward->points = $totalpoint;
                    $reward->date_added = date('Y-m-d H:i:s');
                    $reward->bb_type = 'V';
                    $reward->mp_order_id = 1;
                    $reward->save(false);
                }
                
                $history = new \common\models\MPOrderHistory();
                $history->mp_order_id = $model->mp_order_id;
                $history->order_status = $actionform->order_status_id;
                $history->comment = $actionform->comment;
                $history->date_added = date('Y-m-d');
                $history->save(false);
                
                if($actionform->notify == 1) {
                    $data = \yii\helpers\Json::encode(array(
                        'email_comment' => $actionform->comment,
                    ));
                    $code = 'mp_order.status_'.strtolower($model->order_status);
                    $returnedValue = Yii::$app->VIPglobal->sendEmailMPOrder($model->mp_order_id, Yii::$app->params[$code], $data);
                }
                
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified order!']);
                               
            }else {
                //print_r($model->getErrors());die;
                \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail', 'text' => 'You have modified order!']);
            }
            return $this->redirect(['view', 'id' => $model->mp_order_id]);
        } else {
            return $this->renderAjax('_formaction', [
                'model' => $model,
                'actionform' => $actionform,
            ]);
        }
    }
    
    public function actionBulkOrder(){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        $vstatus= Yii::$app->request->post('redemption_status');
        $vcomment= Yii::$app->request->post('comment');
        //$invoice_num = Yii::$app->request->post('invoice_num');
        $notify = Yii::$app->request->post('notify');
        $totalpoint = 0;
        if(count($selection) > 0){
            $data = array();
            foreach($selection as $valueid){
                $id = $valueid;
                $model = $this->findModel($id);
                //$balancepoint = Yii::$app->VIPglobal->customersAvailablePoint($model->customer_id);
                /*$balancepoint = $model->last_point_balance;
                $totalpoint = $model->totalPoints + $model->shippingPointTotal;

                if($balancepoint < 0) {
                    $vstatus = 10;
                    $vcomment = 'Point balance is insufficient';
                }

                $model->order_status_id = $vstatus;
                
                if(!empty($invoice_num)) {
                    $model->bb_invoice_no = $invoice_num;
                }*/
                $model->order_status = $vstatus;
                if($model->save()){
                    if($model->order_status == 'Decline'){
                        $totalpoint = $model->getTotalPoints();
                        $reward = new VIPCustomerReward();
                        $reward->clientID = $clientID;
                        $reward->customer_id = $model->customer_id;
                        $reward->order_id = $model->mp_order_id;
                        $reward->description = 'Merchandise Product '.$vstatus.' : Order # '.$model->invoice_prefix.'';
                        $reward->points = $totalpoint;
                        $reward->date_added = date('Y-m-d H:i:s');
                        $reward->bb_type = 'V';
                        $reward->mp_order_id = 1;
                        $reward->save(false);
                    }
                    $data[] = [$model->mp_order_id,$vstatus,$vcomment,date('Y-m-d'),date("Y-m-d H:i:s"),date("Y-m-d H:i:s"),Yii::$app->user->id,Yii::$app->user->id];
                }
                
                if($notify == 1) {
                    $dataemail = \yii\helpers\Json::encode(array(
                        'email_comment' => $vcomment,
                    ));
                    $code = 'mp_order.status_'.strtolower($model->order_status);
                    $returnedValue = Yii::$app->VIPglobal->sendEmailMPOrder($model->mp_order_id, Yii::$app->params[$code], $dataemail);
                }
            }
            
            Yii::$app->db
                ->createCommand()
                ->batchInsert('mp_order_history', ['mp_order_id','order_status', 'comment','date_added','created_datetime','updated_datetime','created_by','updated_by'],$data)
                ->execute();
            \Yii::$app->getSession()->setFlash('success',['title' => 'Bulk Success', 'text' => 'Acction Sccessfully']);
            return $this->redirect(['index']);
        }
    }
    
    public function actionRedeem(){
        Yii::$app->cache->flush();
        $session = Yii::$app->session;
        //$data=Yii::$app->request->post('action');
        $data = Yii::$app->request->post();

        $mp_products_id = $data['mp_products_id'];
        $totqty = $data['qty'];
        $keys = array_combine($mp_products_id, $totqty);
        if (!empty($mp_products_id)) {
            $userAgent = \xj\ua\UserAgent::model();
            $platform = $userAgent->platform;
            $browser = $userAgent->browser;
            $version = $userAgent->version;
            
            $model = new MPOrder();
            $model->customer_id = Yii::$app->user->id;
            $model->ip = $_SERVER['REMOTE_ADDR'];
            $model->user_agent = $platform.'-'.$browser.'-'.$version;
            
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {
                    if ($flag) {
                        $data = array();
                        $pointtotal = 0;$sum = 0;
                        foreach ($keys as $key => $value) {
                            $product_id = $key;
                            $qty = $value;
                            $product = \common\models\MPProducts::find()
                                    ->where(['mp_products_id' => $product_id])
                                    ->one();

                            if (!empty($product->product_type_id)) {
                                $type = $product->productType->product_type_name;
                            } else {
                                $type = null;
                            }

                            if (!empty($product->product_colour_id)) {
                                $colour = $product->productColour->product_colour_name;
                            } else {
                                $colour = null;
                            }
                            $admin_fee = $product->products_price * 0.05;
                            $total_fee = $product->products_price + $admin_fee;
                            $point = $total_fee * 2;
                            $pointtotal = round($point) * $qty;
                            $sum+= $pointtotal;

                            $data[] = [$model->mp_order_id,$product_id,$product->productName->product_name, $type, $colour, $product->productSize->product_size_name, $qty, $product->products_price, $admin_fee];
                            //echo $product->productName->product_name.'<br>';
                        }

                        $customerReward = new VIPCustomerReward();
                        $customerReward->clientID = $session['currentclientID'];
                        $customerReward->customer_id = Yii::$app->user->id;        
                        $customerReward->order_id = $model->mp_order_id;
                        $customerReward->description = $model->invoice_prefix;
                        $customerReward->points = '-'.$sum;
                        $customerReward->bb_type = 'V';
                        $customerReward->date_added = date('Y-m-d');
                        $customerReward->mp_order_id = 1;
                        if (($flag = $customerReward->save(false)) === false) {        
                            $transaction->rollBack();
                        }
                        
                        $history = new MPOrderHistory();
                        $history->mp_order_id = $model->mp_order_id;
                        $history->order_status = 'Accepted';
                        $history->comment = 'New Order - '.$model->invoice_prefix;
                        $history->date_added = date('Y-m-d');
                        $history->type = 2;
                        if (($flag = $history->save(false)) === false) {        
                            $transaction->rollBack();
                        }
                    }                    
                }
                
                if ($flag) {
                    $transaction->commit();
                    Yii::$app->db
                    ->createCommand()
                    ->batchInsert('mp_order_item', ['mp_order_id','product_id','product_name', 'product_type','product_colour','product_pack_size','quantity','price','admin_fee'],$data)
                    ->execute();
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Thank you', 'text' => 'Thank you for order.']);
                    $data = '';

                    //$returnedValue = Yii::$app->VIPglobal->sendEmailNewOrder($model->mp_order_id, Yii::$app->params[$code], $data);
                    //return $this->redirect([$rpage.'/view', 'id' => $model->order_id]);
                    //return $this->redirect($rpage . '/view/' . $model->order_id);
                    return $this->redirect(['index']);
                } else {
                    \Yii::$app->getSession()->setFlash('error', ['title' => 'Sorry', 'text' => 'Thank you for order.']);
                    return $this->redirect(['index']);
                }
            } catch (Exception $ex) {
                $transaction->rollBack();
            }
            
            
            
        }
    }
    
    /**
     * Finds the VIPOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MPOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}