<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;

use common\models\MPProductName;
use common\models\MPProductType;
use common\models\MPProductColour;
use common\models\MPProductSize;


/* @var $this yii\web\View */
/* @var $model common\models\ProductList */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
$productnames = MPProductName::find()->where(['product_name_status' => 'active',])->all();
$productname = ArrayHelper::map($productnames, 'product_name_id', 'product_name');

$producttypes = MPProductType::find()->where(['product_type_status' => 'active',])->all();
$producttype = ArrayHelper::map($producttypes, 'product_type_id', 'product_type_name');

$productcolours = MPProductColour::find()->where(['product_colour_status' => 'active',])->all();
$productcolour = ArrayHelper::map($productcolours, 'product_colour_id', 'product_colour_name');

$productsizes = MPProductSize::find()->where(['product_size_status' => 'active',])->all();
$productsize = ArrayHelper::map($productsizes, 'product_size_id', 'product_size_name');

?>
<div class="box-body">
<div class="product-list-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'type')->dropDownList([ 'Bases' => 'Bases', 'Colour' => 'Colour', ], ['prompt' => '']) ?>
    <?= $form->field($model, 'product_name_id', [])->widget(Select2::classname(), [
            'data' => $productname,
            'options' => [
                'placeholder' => 'Select ...',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],               

        ]);
    ?>
    <?= $form->field($model, 'product_type_id', [])->widget(Select2::classname(), [
            'data' => $producttype,
            'options' => [
                'placeholder' => 'Select ...',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],               

        ]);
    ?>
    <?= $form->field($model, 'product_colour_id', [])->widget(Select2::classname(), [
            'data' => $productcolour,
            'options' => [
                'placeholder' => 'Select ...',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],               

        ]);
    ?>
    <?= $form->field($model, 'product_size_id', [])->widget(Select2::classname(), [
            'data' => $productsize,
            'options' => [
                'placeholder' => 'Select ...',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],               

        ]);
    ?>
    <?= $form->field($model, 'products_price')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'products_status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
