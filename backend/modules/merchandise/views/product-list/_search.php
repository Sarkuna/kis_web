<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MPProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpproducts-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'mp_products_id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'product_name_id') ?>

    <?= $form->field($model, 'product_type_id') ?>

    <?= $form->field($model, 'product_colour_id') ?>

    <?php // echo $form->field($model, 'product_size_id') ?>

    <?php // echo $form->field($model, 'products_price') ?>

    <?php // echo $form->field($model, 'products_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
