<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductList */

$this->title = 'Create Product List';
$this->params['breadcrumbs'][] = ['label' => 'Product Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-list-create">
<div class="box box-primary product-name-creat">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>
