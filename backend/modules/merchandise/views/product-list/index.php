<style>
    .space{margin-left: 5px;}
</style>
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .ml-5{margin-left: 5px;}
</style>
<div class="product-list-index">
    
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            <?= Html::a(' <i class="fa fa-file-excel"></i>', ['/reports/report/download-merchandise-product-list'], ['class' => 'btn btn-info pull-right ml-5', 'title' => 'Download Excel']) ?> 
            <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success pull-right', 'title' => 'Add New']) ?>
            
        </div><!-- /.box-header -->

        <div class="box-body">

            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'product_list_id',
                    //'clientID',
                    'type',
                    [
                        'attribute' => 'product_name',
                        'label' => 'Product Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->productName->product_name;
                        },
                    ],
                   
                    [
                        'attribute' => 'product_type',
                        'label' => 'Product Type',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            if (!empty($model->product_type_id)) {
                                return $model->productType->product_type_name;
                            }else {
                                return '';
                            }
                        },
                    ],
                    [
                        'attribute' => 'product_colour',
                        'label' => 'Product Colour',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            if (!empty($model->product_colour_id)) {
                                return $model->productColour->product_colour_name;
                            }else {
                                return '';
                            }
                        },
                    ],
                                
                    [
                        'attribute' => 'product_size',
                        'label' => 'Product Size',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->productSize->product_size_name;
                        },
                    ],            
                    [
                        'attribute' => 'products_price',
                        'label' => 'RM Value',
                        'format' => 'html',
                        'headerOptions' => ['width' => '100'],
                        'contentOptions' => ['class' => 'text-right'],
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDecimal($model->products_price);
                        },
                    ],          
                    //['class' => 'yii\grid\ActionColumn'],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}', //{view} {delete}
                        'buttons' => [
                            'update' => function ($url, $model) {
                                //$url = 'update?id=' . $model->adminUserProfile->profile_ID;
                                return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                            },
                            'delete' => function ($url, $model) {
                                return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                            }
                                /* ,
                                  'view' => function ($url, $model) {
                                  return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                                  },
                                 */
                                ],
                            //'visible' => $visible,
                            ],
                                    
                ],
            ]);
            ?>


        </div>
    </div>

</div>



