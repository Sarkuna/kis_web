<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MPProducts */

$this->title = 'Products: ' . $model->productName->product_name;
$this->params['breadcrumbs'][] = ['label' => 'Product Lists', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->mp_products_id, 'url' => ['view', 'id' => $model->mp_products_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-list-update">
<div class="box box-primary product-name-creat">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>
