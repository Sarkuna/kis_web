<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MPProducts */

$this->title = $model->mp_products_id;
$this->params['breadcrumbs'][] = ['label' => 'Mp Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpproducts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->mp_products_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->mp_products_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'mp_products_id',
            'type',
            'product_name_id',
            'product_type_id',
            'product_colour_id',
            'product_size_id',
            'products_price',
            'products_status',
        ],
    ]) ?>

</div>
