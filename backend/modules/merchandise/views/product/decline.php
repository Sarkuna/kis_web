<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

use common\models\VIPOrderStatus;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Merchandise Orders';
$this->params['breadcrumbs'][] = $this->title;


$OrderStatuslist = [];
?>
<style>
    .actionbulk{display: none;}
</style>
<div class="box box-primary viporder-index">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?> - Decline</h3>
        </div><!-- /.box-header -->

        <div class="box-body">

            <div class="table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
            //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
            
            [
                'attribute' => 'created_datetime1',
                'label' => 'Order Date',
                'format' => 'html',
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->created_datetime));
                },
            ],
            [
                'attribute' => 'invoice_prefix',
                'label' => 'Order ID',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return $model->invoice_prefix;
                },
            ],
            [
                'attribute' => 'clients_ref_no',
                'label' => 'Code',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->customer->clients_ref_no;
                },
            ],            
            /*[
                'attribute' => 'full_name',
                'label' => 'Customer',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->customer->full_name;
                },
            ],*/
            [
                'attribute' => 'company_name',
                'label' => 'Company Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->companyInfo->company_name;
                },
            ],            
            [
                'attribute' => 'No_of_Items',
                'label' => 'No of Items',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-center'],
                'value' => function ($model) {
                    return $model->getTotalTransactions();
                },
            ],
            [
                'attribute' => 'total_point',
                'label' => 'Total Point',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    $totalpoint = $model->getTotalPoints();
                    return Yii::$app->formatter->asInteger($totalpoint);
                },
            ], 
            /*[
                'attribute' => 'total_point1',
                'label' => 'Total RM',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    if(!empty($model->flash_deals_rm)) {
                        return Yii::$app->formatter->asInteger($model->flash_deals_rm);
                    }else {
                        return '0';
                    }
                    
                },
            ],*/  
                       
            /*[
                'attribute' => 'updated_datetime1',
                'label' => 'Updated Date',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->updated_datetime));
                },
            ],*/
                                
            

            //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {download}', // {update}{delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    },
                    'download' => function ($url, $model) {
                        $url = '/reports/report/download-merchandise-product?id='.$model->mp_order_id;
                        return (Html::a('<span class="glyphicon glyphicon-download"></span>', $url, ['title' => Yii::t('app', 'Download'), 'target' => '_blak']));
                    }, 
                ],
            ],             
        ],
    ]); ?>
            </div>
        </div>
    </div>