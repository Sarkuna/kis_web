<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = $model->invoice_prefix;
$this->params['breadcrumbs'][] = ['label' => 'Manage Orders', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
$balancepoint = Yii::$app->VIPglobal->customersAvailablePoint($model->customer_id);
$clientsetup = Yii::$app->VIPglobal->clientSetup();
$pointpersent = $clientsetup['point_value'];
$mark_up = $clientsetup['pervalue'];
?>
<div class="row">
    <div class="col-lg-12">
        <section class="invoice no-margin">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> <?= $model->invoice_prefix ?>
                        <small class="pull-right">Order date: <?= date('d/m/Y', strtotime($model->created_datetime)) ?></small>
                        
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <b>Dealer info</b>
                    <address>
                        Code: <?= $model->customer->clients_ref_no ?><br>
                        Company Name: <?= $model->customer->company->company_name ?><br><br>
                        
                        Name: <strong><a href="<?php echo Url::to(['/sales/customers/view', 'id' => $model->customer->vip_customer_id]); ?>" target="_blank"><?= $model->customer->full_name ?></a></strong><br>
                        Email: <a href="mailto:<?= $model->customeremail->email ?>"><?= $model->customeremail->email ?></a><br>
                        Mobile: <?= $model->customer->mobile_no ?><br>                      
                        Telephone: <?= $model->customer->telephone_no ?><br><br>
                        
                        

                    </address>
                    
                    
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <?php
                    //$invoice_num = $model->bb_invoice_no ? $model->bb_invoice_no : 'N/A';
                    echo '<h3 style="margin-top: 0px;" class="pull-right"><span class="label label-' . $model->getStatusbg() . '">' . $model->order_status . '</span></h3>';
                    //echo '<div class="clear"></div><b>Invoice #'.$model->newInvoiceNumber.'</b><br>';
                    //echo '<b>Last Balance Points:</b> '.Yii::$app->formatter->asInteger($model->last_point_balance).'pts';
                    ?>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-left">Product</th>
                                <th class="text-left">Type/Colour</th>
                                <th class="text-left">Pack Size</th>
                                <th class="text-right">Qty</th>
                                <th class="text-right">Unit pts</th>
                                <th class="text-right">Total pts</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                    $total = ''; $productpoint = 0;
                                    $subtotal = 0; $total = 0; $shipping_point_total = 0;
                                    foreach ($model->orderProducts as $product) {
                                        $qty = $product->quantity;
                                        $admin_fee = $product->admin_fee;
                                        $total_fee = $product->price + $admin_fee;
                                        $point = $total_fee * 2;
                                        $pointtotal = round($point) * $qty;
                                            
                                            echo '<tr>
                                              <td>' . $product->product_name . '</td>
                                              <td>' . $product->product_type . $product->product_colour. '</td>
                                              <td>' . $product->product_pack_size.'</td>
                                              <td class="text-right">' . $qty . '</td>
                                              <td class="text-right">' .round($point). 'pts</td>
                                              <td class="text-right">' . Yii::$app->formatter->asInteger($pointtotal) . 'pts</td>
                                            </tr>';
                                            $subtotal += $pointtotal;
                                    }
                                    ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-8">
                    <p class="lead">History:</p>


                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        <?php
                            $comments = common\models\MPOrderHistory::find()->where(['mp_order_id' => $model->mp_order_id])->all();
                            if(count($comments) > 0){
                                foreach($comments as $comment){
                                    if(!empty($comment->comment)){
                                        //$redemptionstatus = common\models\VIPOrderStatus::find()->where(['order_status_id' => $comment->order_status_id])->one();
                                        $actionlabel = '<span class="label label-'.$comment->getStatusbg().'">'.$comment->order_status.'</span>';
                                        echo $actionlabel.' <em class="pull-right">'.date('d-m-Y', strtotime($comment->date_added)).'</em><br>';
                                        if($comment->type == 1) {
                                            echo '<em>by: System</em><br>';
                                        }else {
                                            echo '<em>by: '.$comment->by->first_name.' '.$comment->by->last_name.'</em><br>';
                                        }
                                        echo $comment->comment.'<br><br>';
                                    }
                                }
                            }else{
                                echo 'No Comments';
                            }
                        ?>
                    </p>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <p class="lead">Order Summary</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th style="width:50%">Sub Total:</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($subtotal) ?>pts</td>
                                </tr>
                                <tr>
                                    <th>Delivery Charges:</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($shipping_point_total) ?>pts</td>
                                </tr>
                                <tr>
                                    <th>Total:</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($subtotal + $shipping_point_total) ?>pts</td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                    
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    <?php
                    //if($model->order_status_id != '40' && $model->order_status_id != '50' && $model->order_status_id != '60') {
                        echo '<button class="btn btn-success pull-right" onclick="updateGuard('.$model->mp_order_id.');return false;"><i class="fa fa-credit-card"></i> Approval Action</button>';
                    //}
                    ?>
                    <?= Html::a('<i class="fa fa-print"></i> Print</a>', ['print', 'id' => $model->mp_order_id], ['class' => 'btn btn-default','target'=>'_blank',]) ?>


                    <button type="button" class="btn btn-primary pull-right hide" style="margin-right: 5px;">
                        <i class="fa fa-download"></i> Generate PDF
                    </button>
                </div>
            </div>
        </section>
    </div>
</div>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Action </h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<script>
/***
  * Start Update Gardian Jquery
***/
function updateGuard(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["/merchandise/product/orderapprove"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal').modal();

		   }
	});
}


</script>

 <?php
    $script = <<<EOD
            $('[data-gallery=manual]').click(function (e) {

      e.preventDefault();

      var items = [],
        options = {
          index: $(this).index(),
            footerToolbar: [
            'zoomIn',
            'zoomOut',
            'fullscreen',
            'actualSize',
            'rotateLeft',
            'rotateRight',
          ],
        };
        
        var items = [
            {
                src: $(this).attr('href'), // path to image
                title: 'Image Caption 1' // If you skip it, there will display the original image name(image1)
            }
        ];

      new PhotoViewer(items, options);

    });
EOD;
$this->registerJs($script);
    ?>