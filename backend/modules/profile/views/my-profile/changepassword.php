<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="vipcustomer-update">
         <div class="site-changepassword">    
                    <p>Please fill out the following fields to change password :</p>    
                    <?php
                    $form = ActiveForm::begin([
                                'id' => 'changepassword-form',
                                'options' => ['class' => 'form-horizontal form-label-left'],
                                'fieldConfig' => [
                                    'template' => "{label}\n<div class=\"col-lg-3\">
                        {input}</div>\n<div class=\"col-lg-5\">
                        {error}</div>",
                                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                                ],
                    ]);
                    ?>
                    
                    <?= $form->field($model, 'password', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->passwordInput(array('placeholder' => 'New Password'));  ?>
                    
                    <?= $form->field($model, 'repassword', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->passwordInput(array('placeholder' => 'Repeat New Password'));  ?>



                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-11">
                            <?=
                            Html::submitButton('Change password', [
                                'class' => 'btn btn-primary'
                            ])
                            ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>

    </div>
</div>

