<?php

namespace app\modules\receipt\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;

use common\models\VIPReceipt;
use common\models\VIPReceiptSearch;
use common\models\BBPoints;
use common\models\VIPReceiptList;
use common\models\ResellerList;
use common\models\ReceiptHistory;
use common\models\VIPTempReceipt;
use common\models\VIPCustomerReward;
use common\models\ProductList;
use common\models\BonusProduct;
use common\models\ClientPointOption;

/**
 * ReceiptController implements the CRUD actions for VIPReceipt model.
 */
class ReceiptController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPReceipt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPReceiptSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'P';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSearch()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $searchModel = new VIPReceiptSearch();
        $searchModel->clientID = '125';
        
        if ($searchModel->load(Yii::$app->request->post())) {
            $searchModel->clientID = $clientID;
        }
        
        $dataProvider = $searchModel->searchall(Yii::$app->request->queryParams);
        return $this->render('all_index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'model' => $model,
        ]);
    }
    
    public function actionDrafts()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPReceiptSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'R';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('drafts', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionProcessing()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPReceiptSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'N';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('processing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionApproves()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPReceiptSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'A';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('approves', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDecline()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPReceiptSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'D';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('decline', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VIPReceipt model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //$this->layout = '/main_model';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VIPReceipt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($invoice_no,$id)
    {
        $tempreceipt = $this->findTempReceipt($id);
        if($tempreceipt->receipt_status == 'P') {
            $model = new VIPReceipt();
            $model->invoice_no = $invoice_no;
            if ($model->load(Yii::$app->request->post())) {
                $data = Yii::$app->request->post();
                $totalqty = $data['totalqty'];
                $scanbarcodes = $data['barcodeid'];
                $totqty = $data['totqty'];
                $price_total = $data['price_total'];

                $keys = array_combine($scanbarcodes, $totqty);

                $model->userID = $tempreceipt->userID;
                $model->clientID = $tempreceipt->clientID;
                $model->date_of_receipt = date('Y-m-d', strtotime($model->date_of_receipt));
                $model->order_num = date('his_dmY');
                $model->selected_image = $tempreceipt->selected_image;
                $model->status = 'P';
                
                $customer = \common\models\VIPCustomer::find()->where(['userID' => $tempreceipt->userID, 'clientID' => $tempreceipt->clientID])->one();
                $user_selector = \common\models\EndUserLevel::find()->where(['end_user_level_id' => $customer->end_user_level_id])->one();
                if(empty($customer->profile_selection_id)){
                    $profile_selector = '1';
                }else {
                    $profile_selector = $customer->profile_selection_id;
                }
                $reseller_name = \common\models\ResellerList::find()->where(['reseller_list_id' => $model->reseller_name])->one();
                $reseller_category = \common\models\ResellerCategory::find()->where(['reseller_category_id' => $reseller_name->reseller_category_id])->one();
                
                if($model->save()) {
                    $lastinsid = $model->vip_receipt_id;
                    $p = 0;
                    foreach ($keys as $key2 => $value2) {
                        $product_list_id = $key2;
                        $bbpoint = BBPoints::find()->where(['bb_points_id' => $product_list_id])->one();
                        $receiptlist = new VIPReceiptList();
                        $receiptlist->vip_receipt_id = $lastinsid;
                        $receiptlist->userID = $tempreceipt->userID;
                        $receiptlist->clientID = $tempreceipt->clientID;
                        $receiptlist->product_list_id = $key2;
                        $receiptlist->product_per_value = $bbpoint->product_point;
                        $receiptlist->product_item_qty = $value2;
                        $receiptlist->price_total = $price_total[$p];
                        $receiptlist->price_per_product = $price_total[$p] / $value2;
                        $receiptlist->user_selector = $user_selector->prefix_value;
                        //$receiptlist->reseller_selector = $reseller_category->prefix_value;
                        $receiptlist->reseller_selector = $model->reseller_name;
                        $receiptlist->profile_selector = $profile_selector;
                        $receiptlist->save(false);
                        unset($receiptlist);
                        $p++;
                    }

                    $receipthistory = new ReceiptHistory();
                    $receipthistory->vip_receipt_id = $lastinsid;
                    $receipthistory->vip_receipt_status = 'P';
                    $receipthistory->notify = '0';
                    $receipthistory->comment = $model->remark;
                    $receipthistory->date_added = date('Y-m-d H:i:s');
                    $receipthistory->save(false);

                    $tempreceipt->receipt_status = 'X';
                    $tempreceipt->save(false);



                    \Yii::$app->getSession()->setFlash('success',['title' => 'Receipts', 'text' => 'Receipts successfully created!']);
                }
                //print_r($model->getErrors());
                return $this->redirect(['index']);
                //return $this->redirect(['view', 'id' => $model->vip_receipt_id]);
            } else {
                $model->price_total = '0';
                $model->price_per_product = '0';
                return $this->render('create', [
                    'model' => $model,
                    'tempreceipt' => $tempreceipt,
                ]);
            }
        }else {
            $name = $invoice_no;
            $message = 'This invoice number already exists';
           return $this->render('error', [
                    'name' => $name,
                    'message' => $message,
                ]); 
        }
    }

    /**
     * Updates an existing VIPReceipt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $global_point_value = Yii::$app->VIPglobal->clientSetup()['point_value'];

        $model = $this->findModel($id);
        $pointorderitemlistsdel = VIPReceiptList::find()->where(['vip_receipt_id' => $id])->all();
        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            
            

            if(!empty($data['barcodeid'])) {
                $totalqty = $data['totalqty'];
                $scanbarcodes = $data['barcodeid'];
                $totqty = $data['totqty'];
                $price_total = $data['price_total'];
                $keys = array_combine($scanbarcodes, $totqty);
            }
            

            $customer = \common\models\VIPCustomer::find()->where(['userID' => $model->userID, 'clientID' => $model->clientID])->one();


            $model->date_of_receipt = date('Y-m-d', strtotime($model->date_of_receipt));
            $model->state = $model->state;
            //$model->status = "N";
            $model->reseller_name = $model->reseller_name;
            
            if($model->save()) {
                $lastinsid = $model->vip_receipt_id;
                $p = 0;
                if(!empty($data['barcodeid'])) {
                    foreach ($keys as $key2 => $value2) {
                        $product_list_id = $key2;
                        $productlists = ProductList::find()->where(['product_list_id' => $product_list_id])->one();

                        $receiptlist = new VIPReceiptList();
                        $receiptlist->vip_receipt_id = $lastinsid;
                        $receiptlist->userID = $model->userID;
                        $receiptlist->clientID = $model->clientID;
                        $receiptlist->product_list_id = $key2;
                        $receiptlist->pack_size = $value2 * $productlists->pack_size;
                        $receiptlist->points_awarded = $productlists->total_points_awarded;
                        $receiptlist->points_awarded_rm_value = $productlists->total_points_awarded * $global_point_value;
                        $receiptlist->product_item_qty = $value2;
                        $receiptlist->qty_points_awarded = $value2 * $productlists->total_points_awarded;
                        $receiptlist->qty_points_awarded_rm_value = $value2 * $productlists->total_points_awarded * $global_point_value;
                        $receiptlist->product_per_value = $productlists->total_points_awarded;
                        $receiptlist->price_total = $price_total[$p];
                        $receiptlist->price_per_product = $price_total[$p] / $value2;
                        $receiptlist->user_selector = 0;
                        //$receiptlist->reseller_selector = $reseller_category->prefix_value;
                        $receiptlist->reseller_selector = $model->reseller_name;
                        $receiptlist->profile_selector = 1;     
                        $receiptlist->save(false);
                        unset($receiptlist);

                        if($productlists->product_name_id == 13) {
                            $bonusproduct = new BonusProduct();
                            $bonusproduct->clientID = $model->clientID;
                            $bonusproduct->userID = $model->userID;
                            $bonusproduct->vip_receipt_id = $lastinsid;
                            $bonusproduct->product_description = $productlists->description;
                            $bonusproduct->pack_size = $productlists->pack_size;
                            $bonusproduct->base_points = $productlists->total_points_awarded;
                            $bonusproduct->base_rm = $productlists->total_points_awarded * $global_point_value;
                            $bonusproduct->qty = $value2;
                            $bonusproduct->total_pack_size = $value2 * $productlists->pack_size;
                            $bonusproduct->total_pack_point = $value2 * $productlists->total_points_awarded;
                            $bonusproduct->total_pack_rm = $value2 * $productlists->total_points_awarded * $global_point_value;
                            $bonusproduct->date_of_receipt = date('Y-m-d', strtotime($model->date_of_receipt));
                            $bonusproduct->save(false);
                            unset($bonusproduct);
                        }
                        $p++;
                    }
                }
                
                $receipthistory = new ReceiptHistory();
                $receipthistory->vip_receipt_id = $lastinsid;
                $receipthistory->vip_receipt_status = $model->status;
                $receipthistory->notify = '0';
                $receipthistory->comment = $model->remark;
                $receipthistory->date_added = date('Y-m-d H:i:s');
                $receipthistory->save(false);



                \Yii::$app->getSession()->setFlash('success',['title' => 'Receipts', 'text' => 'Receipts has been update successfully.']);
            }
            return $this->redirect(['view', 'id' => $model->vip_receipt_id]);
        } else {
            $model->reseller_name = $model->reseller_name;
            if($model->date_of_receipt == NULL || $model->date_of_receipt == "1970-01-01") {
                $model->date_of_receipt = '';
            }else {
                $model->date_of_receipt = date('d-m-Y', strtotime($model->date_of_receipt));                
            }
            
            return $this->render('update', [
                'model' => $model,
                'pointorderitemlistsdel' => $pointorderitemlistsdel,
            ]);
        }
    }
    
    public function actionGetProduct($id=null,$qty=null)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $global_point_value = Yii::$app->VIPglobal->clientSetup()['point_value'];
        
        //$response = null;
        if(empty($qty)){
           $qty = '1'; 
        }
        //$chkbarcode = BBPoints::find()->where(['bb_points_id' => $id])->count();
        $chkbarcode = ProductList::find()->where(['product_list_id' => $id])->count();
        if($chkbarcode > 0){
            $product_list_id = $id;
            $productlist = ProductList::find()->where(['product_list_id' => $id])->one();

            $campaign_id = '';
            $campaign_rm_per_point = '';
            $Item_status = 'Good';

            /*$response['barcodename'] = $productlist->product_code;
            $response['barcodeid'] = $id;
            $response['product'] = $productlist->product_description;
            $response['kg'] = $productlist->product_kg;
            $response['qty'] = $qty;
            $response['per_point'] = $productlist->product_point;
            $response['point'] = $productlist->product_point * $qty;
            $response['status']       = $Item_status; */
            
            $response['barcodeid'] = $id;
            $response['barcodename'] = $productlist->product_name_id;
            $response['product'] = $productlist->description.$productlist->metric;
            $response['pack_size'] = $productlist->pack_size;
            $response['points_awarded'] = $productlist->total_points_awarded;
            $response['total_points_awarded'] = $qty * $productlist->total_points_awarded;
            $response['total_rm_awarded'] = $qty * $productlist->total_points_awarded * $global_point_value;
            $response['qty'] = $qty;
            
        }else{
           $response = null; 
        }
        echo json_encode($response);
        /*echo Json::encode(array(
            'item_price'=>12, 
        ));*/
    }
    
    public function actionGetitem($id=null,$qty=null)
    {
        //$response = null;
        if(empty($qty)){
           $qty = '1'; 
        }
        $chkbarcode = BBPoints::find()->where(['bb_points_id' => $id])->count();
        if($chkbarcode > 0){
            $product_list_id = $id;
            $productlist = BBPoints::find()->where(['bb_points_id' => $id])->one();

            $campaign_id = '';
            $campaign_rm_per_point = '';
            $Item_status = 'Good';

            $response['barcodename'] = $productlist->product_code;
            $response['barcodeid'] = $id;
            $response['product'] = $productlist->product_description;
            $response['kg'] = $productlist->product_kg;
            $response['qty'] = $qty;
            $response['per_point'] = $productlist->product_point;
            $response['point'] = $productlist->product_point * $qty;
            $response['status']       = $Item_status;            
        }else{
           $response = null; 
        }
        echo json_encode($response);
        /*echo Json::encode(array(
            'item_price'=>12, 
        ));*/
    }
    
    public function actionApprove($id=null)
    {          
        $model = new \common\models\Approve_Form();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $models = $this->findModel($id);
            $models->status = $model->status;
            if($models->status == 'D') {
                $models->reasons_id = $model->reasons_id;
            }
            if($models->save()){
                $receipthistory = new ReceiptHistory();
                $receipthistory->vip_receipt_id = $id;
                $receipthistory->vip_receipt_status = $model->status;
                $receipthistory->notify = '0';
                $receipthistory->comment = $model->remark;
                $receipthistory->date_added = date('Y-m-d H:i:s');
                $receipthistory->save(false);
                if($models->status == 'A'){
                    //$redemptionitems = common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $model->vip_receipt_id])->all();
                    $point = \common\models\VIPReceiptList::find()
                    ->where(['vip_receipt_id' => $id])
                    //->andWhere(['clientID'=>$model->distributor->clientID, 'customer_id'=>$model->distributor->userID])
                    ->sum('qty_points_awarded');
                    //$point = $models->getTotalpoint();
                    $pointawerd = new VIPCustomerReward();
                    $pointawerd->clientID = $models->clientID;
                    $pointawerd->customer_id = $models->userID;
                    $pointawerd->order_id = $id;
                    //$pointawerd->expiry_date = date('Y-m-d', strtotime($model->expiry_date));
                    $pointawerd->description = 'New Point Rewarded '.$models->invoice_no.','.date('d-m-Y', strtotime($models->date_of_receipt));
                    $pointawerd->points = $point;
                    $pointawerd->date_added = date('Y-m-d');
                    $pointawerd->bb_type = 'A';
                    $pointawerd->save(false);
                    
                    $pointawerd2 = new \common\models\VIPCustomerAccount2();
                    $pointawerd2->clientID = $models->clientID;
                    $pointawerd2->customer_id = $models->reseller_name;
                    $pointawerd2->indirect_id = $models->userID;
                    $pointawerd->bb_type = 'V';
                    //$pointawerd->expiry_date = date('Y-m-d', strtotime($model->expiry_date));
                    $pointawerd2->description = 'New Point Rewarded '.$models->invoice_no.','.date('d-m-Y', strtotime($models->date_of_receipt));
                    $pointawerd2->points_in = '-'.$point;
                    $pointawerd2->date_added = date('Y-m-d');
                    $pointawerd2->save(false);
                }
                //
                if($models->status == 'D'){
                    $mobile_no = $models->customer->mobile_no;
                    if(!empty($mobile_no)) {
                        /*if($models->status == 'D'){
                            $code = 'RAN01B';
                        }else {
                            $code = 'RAN01';
                        }*/
                        $code = 'RAN01B';
                        $data = \yii\helpers\Json::encode(array(
                            //'point' => $point,
                            'reason' => $models->reasons->name,
                            'invoice_no' => $models->invoice_no,
                        ));
                        $returnedValue = Yii::$app->VIPglobal->sendSMS($models->userID, $code, $data);
                        if($returnedValue == '2000 = SUCCESS' || $returnedValue == 'EMPTY/BLANK'){
                            $status = "S";
                            $datetosend = date('Y-m-d');
                        }else {
                            $status = "N";
                            $datetosend = "";
                        }
                        $smsTemplate = \common\models\SMSTemplate::find()
                        ->where(['code' => $code, 'clientID' => $models->clientID])
                        ->one();
                        $smsQueue = new \common\models\SMSQueue();
                        $smsQueue->sms_template_id = $smsTemplate->id;
                        $smsQueue->sms_type = 'receipts';
                        $smsQueue->clientID = $models->clientID;
                        $smsQueue->user_id = $models->userID;
                        $smsQueue->mobile = $mobile_no;
                        $smsQueue->data = $data;
                        $smsQueue->status = $status;
                        $smsQueue->date_to_send = $datetosend;
                        $smsQueue->remark = $returnedValue;
                        $smsQueue->save(false);
                        
                        Yii::$app->VIPglobal->addtolog('success', $returnedValue, $models->userID);
                    }
                }
                
                \Yii::$app->getSession()->setFlash('success',['title' => 'Receipts', 'text' => 'Receipts successfully update!']);
                return $this->redirect(['processing']);
            }
            
        } else {
            return $this->renderAjax('_formpop', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDstate($id){
        $dealerlists = ResellerList::find()
                ->where(['reseller_category_id' => $id])
                ->all();
        if ($dealerlists) {
            //echo $sle;
            foreach ($dealerlists as $dealerlist) {
                echo "<option value='" . $dealerlist->reseller_list_id . "'>" . $dealerlist->reseller_name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
    }
    
    public function actionProductDescription($id){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $dealerlists = ProductList::find()
                ->where(['clientID' => $clientID, 'product_name_id' => $id])
                ->all();
        if ($dealerlists) {
            //echo $sle;
            foreach ($dealerlists as $dealerlist) {
                echo "<option value='" . $dealerlist->product_list_id . "'>" . $dealerlist->description . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
    }
    
    public function actionCampingIntake(){
        return $this->render('intake');
    }
    
    public function actionCampingIntakeDetail(){
        return $this->render('intake_detail');
    }

    /**
     * Deletes an existing VIPReceipt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteItem($delete_id)
    {
        $this->findReceiptList($delete_id)->delete();
    }
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VIPReceipt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPReceipt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPReceipt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findTempReceipt($id)
    {
        if (($model = \common\models\VIPTempReceipt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findReceiptList($delete_id)
    {
        $id = $delete_id;
        if (($model = VIPReceiptList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}