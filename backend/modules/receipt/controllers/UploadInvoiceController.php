<?php

namespace app\modules\receipt\controllers;

use Yii;
use common\models\VIPUploadInvoice;
use common\models\VIPUploadInvoiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UploadInvoiceController implements the CRUD actions for VIPUploadInvoice model.
 */
class UploadInvoiceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPUploadInvoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $searchModel = new VIPUploadInvoiceSearch();
        $searchModel->clientID = $clientID;
        $searchModel->invoice_status = 'P';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionList($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = new \common\models\VIPTempReceipt();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->save()){
                $selection = explode(',', $model->selected_image);
                foreach ($selection as $image) {
                    $id = $image;
                    $modelimage = $this->findModel($id);
                    $modelimage->invoice_status = 'A';
                    $modelimage->save();
                }
                return $this->redirect(['/receipt/receipt/create', 'invoice_no' => $model->invoice_no, 'id' => $model->id]);
            }else {
                print_r($model->getErrors());
            }
            
        }
        
        $uploadimagelist = VIPUploadInvoice::find()->where(['file_group' => $id, 'invoice_status' => 'P', 'clientID' => $clientID])->all();
        
        $model->clientID = $uploadimagelist[0]->clientID;
        $model->userID = $uploadimagelist[0]->userID;        
        return $this->render('indexlist', [
            'uploadimagelist' => $uploadimagelist,
            'model' => $model
        ]);
    }
    
    public function actionTempReceipt()
    {
        $model = new \common\models\VIPTempReceipt();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->save()){
                $selection = explode(',', $model->selected_image);
                foreach ($selection as $image) {
                    $id = $image;
                    $modelimage = $this->findModel($id);
                    $modelimage->invoice_status = 'A';
                    $modelimage->save();
                }
                return $this->redirect(['/receipt/receipt/create', 'invoice_no' => $model->invoice_no, 'id' => $model->id]);
            }else {
                print_r($model->getErrors());
            }
            
        } else {
            $userID = '';
        $clientID = '';
        $selection=Yii::$app->request->post('selection');

        foreach ($selection as $image) {
            $id = $image;
            $modelimage = $this->findModel($id);
            $userID = $modelimage->userID;
            $clientID = $modelimage->clientID;
        }

        $poselected_image = implode( ",", $selection );
        
        
        $model->selected_image = $poselected_image;
        $model->clientID = $clientID;
        $model->userID = $userID;
            return $this->render('_form_invoice_no', [
                'model' => $model,
            ]);
        }

    }
    
    public function actionTempReceipt1()
    {
        $model = new \common\models\VIPTempReceipt();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->save()){
                $selection = explode(',', $model->selected_image);
                foreach ($selection as $image) {
                    $id = $image;
                    $modelimage = $this->findModel($id);
                    $modelimage->invoice_status = 'A';
                    $modelimage->save();
                }
                return $this->redirect(['/receipt/receipt/create', 'invoice_no' => $model->invoice_no, 'id' => $model->id]);
            }else {
                print_r($model->getErrors());
            }
            
        } else {
            $userID = '';
        $clientID = '';
        $selection=Yii::$app->request->post('selection');

        foreach ($selection as $image) {
            $id = $image;
            $modelimage = $this->findModel($id);
            $userID = $modelimage->userID;
            $clientID = $modelimage->clientID;
        }

        $poselected_image = implode( ",", $selection );
        
        
        $model->selected_image = $poselected_image;
        $model->clientID = $clientID;
        $model->userID = $userID;
            return $this->render('_form_invoice_no', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Displays a single VIPUploadInvoice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VIPUploadInvoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VIPUploadInvoice();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->upload_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VIPUploadInvoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->upload_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VIPUploadInvoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VIPUploadInvoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPUploadInvoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPUploadInvoice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
