<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPReceiptList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipreceipt-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vip_receipt_id')->textInput() ?>

    <?= $form->field($model, 'userID')->textInput() ?>

    <?= $form->field($model, 'clientID')->textInput() ?>

    <?= $form->field($model, 'product_list_id')->textInput() ?>

    <?= $form->field($model, 'product_per_value')->textInput() ?>

    <?= $form->field($model, 'product_item_qty')->textInput() ?>

    <?= $form->field($model, 'user_selector')->textInput() ?>

    <?= $form->field($model, 'reseller_selector')->textInput() ?>

    <?= $form->field($model, 'profile_selector')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
