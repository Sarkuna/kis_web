<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPReceiptListSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipreceipt-list-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'vip_receipt_list_id') ?>

    <?= $form->field($model, 'vip_receipt_id') ?>

    <?= $form->field($model, 'userID') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'product_list_id') ?>

    <?php // echo $form->field($model, 'product_per_value') ?>

    <?php // echo $form->field($model, 'product_item_qty') ?>

    <?php // echo $form->field($model, 'user_selector') ?>

    <?php // echo $form->field($model, 'reseller_selector') ?>

    <?php // echo $form->field($model, 'profile_selector') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
