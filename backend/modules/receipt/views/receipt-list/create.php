<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPReceiptList */

$this->title = 'Create Vipreceipt List';
$this->params['breadcrumbs'][] = ['label' => 'Vipreceipt Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipreceipt-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
