<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPReceiptList */

$this->title = 'Update Vipreceipt List: ' . $model->vip_receipt_list_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipreceipt Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vip_receipt_list_id, 'url' => ['view', 'id' => $model->vip_receipt_list_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vipreceipt-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
