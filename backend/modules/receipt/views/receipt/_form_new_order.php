<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\date\DatePicker;

use common\models\State;
use common\models\BBPoints;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .point-order-form .dropdown-menu.open {
        z-index: 5000 !important;
    }
    
    .table > thead:first-child > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table-striped thead tr.primary:nth-child(odd) th {
    background-color: #428BCA;
    color: white;
    border-color: #357EBD;
    border-top: 1px solid #357EBD;
    text-align: center;
}
input#totalqty {
    text-align: right;
    width: 50px;
}

input#grandtotal {
    text-align: right;
    width: 100px;
}
.form-group.field-vipreceipt-remark {
    margin-top: 100px;
    /* clear: both; */
}
</style>


<?php
$d_area = ArrayHelper::map(State::find()->where(['status' => 'A'])->orderBy('state_name ASC')->all(), 'state_id', 'state_name');
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-12">

    <div class="col-md-5">
        <?= $form->field($model, 'invoice_no')->textInput(['autocomplete' => 'off', 'disabled' => true]) ?>
         <div class="box-group" id="accordion">
             
        <?php
        $uploadimagelist = explode(',', $tempreceipt->selected_image);
        $imagelisitprimary = \common\models\VIPUploadInvoice::find()->where(['upload_id' => $uploadimagelist[0]])->one();

        //$uploadimagelist[0]
        $n = 1;
        foreach ($uploadimagelist as $image) {
            $imagelisit = \common\models\VIPUploadInvoice::find()->where(['upload_id' => $image])->one();
            $img = '/upload/upload_invoice/' . $imagelisit->upload_file_new_name;
            if($n == 1){
                $in = 'in';
            }else {
                $in = '';
            }
            echo '<div class="panel box box-primary">
                 <div class="box-header with-border">
                     <h4 class="box-title">
                         <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$n.'">
                             Image #'.$n.'
                         </a>
                     </h4>
                 </div>
                 <div id="collapse'.$n.'" class="panel-collapse collapse '.$in.'">
                     <div class="box-body">
                        <iframe src="'.$img.'" style="width:100%;min-height: 400px; height:auto; " frameborder="0"></iframe>
                     </div>
                 </div>
             </div>';
            $n++;
        }
        ?>
         </div>
    </div>
    
    <div class="col-md-7">
        
        <?php
        echo $form->field($model, 'date_of_receipt')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Date of Receipt'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd-M-yyyy',
                //'endDate' => '-18y',
                'maxDate' => date('d-m-Y'),
            ],
            'pluginEvents' => [
                'changeDate' => 'function(ev) {
 
                                }'
            ]
        ]);
        ?>
        <?=
                    $form->field($model, 'state')->dropDownList($d_area, [
                        'prompt' => '-- Select --',
                        'onchange' => '$.get( "' . Url::toRoute('/receipt/receipt/dstate') . '", {id: $(this).val() } )
                                 .done(function( data ) {
                                    $("#vipreceipt-reseller_name").html(data);
                                 }
                             );'
                            ]
                    )
                    ?>
        <?= $form->field($model, 'reseller_name')->dropDownList([]); ?>
        

        <?= $form->field($model, 'handwritten_receipt')->checkbox(); ?>

                   <div class="row">
                       <?php
                       $d_product_name = ArrayHelper::map(BBPoints::find()->orderBy('product_description ASC')->all(), 'bb_points_id', 'product_description');
                       //$d_product_description = ArrayHelper::map(ProductList::find()->orderBy('product_description ASC')->all(), 'product_list_id', 'product_description');
                       ?>
                       <div class="col-md-12 no-padding">
                           <div class="col-md-4">
                               <?=
                               $form->field($model, 'product_name')->dropDownList($d_product_name, [
                                   'prompt' => '-- Select --',
                                ]
                               )
                               ?>
                           </div>
                           <div class="col-md-2">
                               <?= $form->field($model, 'vqty')->textInput() ?>
                           </div>
                           <div class="col-md-2">
                               <?php $model->price_total = '0.00'; ?>
                               <?= $form->field($model, 'price_total')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                                    //'mask' => '9999999999999',
                                    //'mask' => '9',
                                    'clientOptions' => ['alias' =>  'decimal','groupSeparator' => ',',],
                                    //'options' => ['placeholder' => '60XXXXXXXXX', 'class' => 'form-control',],
                                ]) ?> 
                           </div>

                           <div class="col-md-1" style="padding-left: 0px;">
                               <label class="control-label" for="pointorder-vqtybtn" style="height: 14px;"></label>
                               <input type="button" id="addButton" class="addButtonc btn btn-primary" value="Add" />
                           </div>
                       </div>
                   </div>

                   <div class="direct-chat-messages no-padding">
                       <table class="table fixed" id="retc"> 
                           <thead>
                               <tr>
                                   <th class="hide">barcodename</th>
                                   <th class="hide">barcodeid</th>
                                   <th width="40%">Product</th>
                                   <th width="10%">KG</th>
                                   <th width="5%">Qty</th>
                                   <th width="14%">Per Point</th>
                                   <th width="9%">Point</th>
                                   <th width="9%">Price</th>
                                   <th width="15%">Per Price</th>
                                   <th style="width: 5%; text-align: center;"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                               </tr>
                           </thead>
                           <tbody class="product_item_list" id="product_item_list"></tbody>
                       </table>
                   </div>
                   <table style="width:100%; float:right; padding:5px; color:#000; background: #FFF;" id="totalTable">
                       <tbody>
                           <tr>
                               <td colspan="2" style="padding: 5px 10px; border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;">
                                   Total Items                                        </td>
                               <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" class="text-right">

                               </td>
                               <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;">
                                   <input class="form-control" type="text" id="totalqty" name="totalqty" value="0" />
                               </td>
                           </tr>
                           <tr>
                               <td colspan="2" style="padding: 5px 10px; border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;">
                                   Total Points                                      </td>
                               <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" class="text-right">

                               </td>
                               <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;">
                                   <input class="form-control" type="text" id="grandtotal" name="grandtotal" value="0.00" />
                               </td>
                           </tr>
                       </tbody>
                   </table>
    <?php
    $script = <<<EOD
                var handledCount = 0;
                $('.addButtonc').unbind('click').click(function() {
                    var test = $("#vipreceipt-product_name").val();
                    var price_total = $("#vipreceipt-price_total").val();
                    var price_per_product = $("#vipreceipt-price_total").val() / $("#vipreceipt-vqty").val();
                    //alert(test);
                    var flag = 0;
                    $("#retc").find("tr").each(function () {
                        var td1 = $(this).find("td:eq(0)").text();
                        if (test == td1) {
                            flag = 1;
                        }
                    });
            
                    if (handledCount == 0){
                        var itemID = $("#vipreceipt-product_name").val(),vNode  = $(this);
                        var qty = $("#vipreceipt-vqty").val();
                        var campaignid = $("#pointorder-listing_campaign").val();
                        var sum = 0.0;
                        $.ajax({
                            url:'getitem?id='+itemID+'&qty='+qty,
                            dataType: 'json',
                            success : function(data) {
                                if (flag == 1) {
                                    alert('Item already exists');
                                    $("#add_item").val('');
                                    $("#add_item").focus();
                                    $("#vipreceipt-vqty").val('');
                                    $("#vipreceipt-price_total").val('0');
                                } else {
                                    $('#retc tbody').append("<tr id="+ data.barcodeid + "><td class='hide'>" + data.barcodeid+ "</td><td class='hide'><input class='form-control' type='text' name='barcodeid[]' value='"+ data.barcodeid +"'/><input class='form-control' type='text' name='totqty[]' value='"+ data.qty+ "'/><input type='text' name='price_total[]' value='"+ price_total+ "'/></td><td>" + data.product + "</td><td class='text-center'>" + data.kg + "</td><td class='text-center'>" + data.qty+ "</td><td class='text-center'>" + data.per_point+ "</td><td class='text-center price'>" + data.point+ "</td><td class='text-center'>" + price_total+ "</td><td class='text-center'>" + price_per_product.toFixed(2)+ "</td><td class='text-center'><a href='javascript::;' class='btnDelete'><i class='fa fa-trash-o' style='opacity:0.5; filter:alpha(opacity=50);'></i></a></td></tr>");
                                    $("#add_item").val('');
                                    $("#add_item").focus();
                                    var count = $('#retc tbody').children('tr').length;
                                    update_amounts();
                                    update_percentage();
                                    $("#totalitem").html(count);
                                    $("#totalqty").val(count);
                                    $("#vipreceipt-vqty").val('');
                                    $("#vipreceipt-price_total").val('0');
                                }
                            },
                            error : function() {
                                console.log('error');
                            }
                        });
                    }
                    handledCount++;
                    if (handledCount == 1)
                        handledCount = 0;
                    return false;
                    $("#add_item").val('');
                });

            function update_amounts()
            {
                var sum = 0.00;
                $('#retc > tbody  > tr').each(function() {
                    var price = $(this).find('.price').html();
                    sum = sum + Number(price);
                });
                $("#grandtotal").val(sum);
            }
            function update_percentage()
            {
                var priceOne = parseFloat($('#pointorder-dealer_invoice_price').val());
                var priceTwo = parseFloat($('#grandtotal').val());
                var price3 = priceTwo / priceOne * 100;
                $('#pointorder-pay_out_percentage').val(price3.toFixed(2)); // Set the rate
            }
            
            $('#pointorder-dealer_invoice_price').on('input',function(e){
                update_percentage();
            });

            
            $("#retc").on('click','.btnDelete',function(){
                //$(this).closest('tr').remove();
                var tableRow = $(this).closest('tr');
                
                tableRow.find('td').fadeOut('fast', 
                    function(){ 
                        tableRow.remove();
                        var count = $('#retc tbody').children('tr').length;
                        update_amounts();
                        update_percentage();
                        $("#totalitem").html(count);
                        $("#totalqty").val(count);
                    }
                );
            });
            $('#btnsubmit').on('click', function() {
                if($("#product_item_list tr").length > 0){
                    return true;
                }else{
                    alert('Please ensure there is at least one row in product list table');
                    return false;
                }
            });
            

EOD;
$this->registerJs($script);
    ?>

        <?= $form->field($model, 'remark')->textarea(['rows' => '6']) ?>
                   
               </div> <!-- close right DIV -->
    </div>
</div>
<div class="row">
<div class="col-md-12">
    <div class="box-footer">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Submit') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-info btgr' : 'btn btn-info btgr', 'id' => 'btnsubmit']) ?>
    </div>
</div>
</div>
<?php ActiveForm::end(); ?>
           

