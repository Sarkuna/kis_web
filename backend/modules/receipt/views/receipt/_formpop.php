<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\MembershipPack */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Send Membership Pack';
$reasons_list = ArrayHelper::map(common\models\Reasons::find()->orderBy('reasons_id ASC')->all(), 'reasons_id', 'name');
?>



    <div class="membership-pack-form">
        <?php
        $form = ActiveForm::begin([
                'id' => 'stu-master-update',
                //'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'fieldConfig' => [
                    'template' => "{label}{input}{error}",
                ],
            ]);
        ?>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <?php echo $form->field($model, 'status')->dropDownList(['A' => 'Approve', 'D' => 'Decline'],['prompt'=>'Select...']); ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-8">
               <?php echo $form->field($model, 'reasons_id')->dropDownList($reasons_list,['prompt'=>'Select...']); ?> 
            </div>
            
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <?= $form->field($model, 'remark')->textarea(['rows' => '6']) ?>
            </div>
        </div>


    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
        
    </div>    
<style>
    .field-approve_form-reasons_id {display: none;}
</style>
<?php
    $script = <<<EOD
     $(function() { 
                $('#approve_form-status').change(function(){
                    var statusval = $('#approve_form-status').val();
                    if( statusval == 'R') {
                        $('.field-approve_form-reasons_id').show(); 
                    } else {
                        $('.field-approve_form-reasons_id').hide(); 
                    }
                    
                    if(statusval == 'D') {
                        $('.field-approve_form-reasons_id').show(); 
                    } else {
                        $('.field-approve_form-reasons_id').hide(); 
                    }
            
                });
            });       
EOD;
$this->registerJs($script);
?>
