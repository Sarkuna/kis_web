<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPReceiptSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipreceipt-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'vip_receipt_id') ?>

    <?= $form->field($model, 'userID') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'order_num') ?>

    <?= $form->field($model, 'date_of_receipt') ?>

    <?php // echo $form->field($model, 'reseller_name') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'invoice_no') ?>

    <?php // echo $form->field($model, 'handwritten_receipt') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'order_IP') ?>

    <?php // echo $form->field($model, 'created_datetime') ?>

    <?php // echo $form->field($model, 'updated_datetime') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
