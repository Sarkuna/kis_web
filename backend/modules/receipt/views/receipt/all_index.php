<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receipts List Search';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary vipreceipt-index">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <?= $this->render('_formSearch', ['searchModel' => $searchModel]); ?>
            <div class="table table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'vip_receipt_id',
            [
                'attribute' => 'order_num',
                'label' => 'Order Num',
                'format' => 'html',
                //'headerOptions' => ['width' => '100'],
                'value' => function ($model) {
                    return $model->order_num;
                },
            ],
            [
                'attribute' => 'full_name',
                'label' => 'Full Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    if(!empty($model->customer->full_name)) {
                        return $model->customer->full_name;
                    }else {
                        return 'N/A';
                    }
                },
            ],
            [
                'attribute' => 'mobile_no',
                'label' => 'Mobile No',
                'format' => 'html',
                'value' => function ($model) {
                    if(!empty($model->customer->mobile_no)) {
                        return $model->customer->mobile_no;
                    }else {
                        return 'N/A';
                    }
                },
            ],
            /*[
                'attribute' => 'company_name1',
                'label' => 'Company Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->companyInfo->company_name;
                },
            ], */           
          
            'invoice_no',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->getStatustext();
                    //return $model->indirectCompany->company_name;
                },
            ],           
              [
                'class' => 'yii\grid\ActionColumn',
                  'headerOptions' => ['width' => '60'],
                'template' => '{update} {view} ', //{view} {delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    },
                    'update' => function ($url, $model) {
                        if($model->status != 'A' || $model->status != 'D') {
                            return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));                        
                        }
                    },
                ],
            ],          
        ],
    ]); ?>
            </div>
        </div>
    </div>
