<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPReceipt */

$this->title = 'New Receipt';
$this->params['breadcrumbs'][] = ['label' => 'Receipts List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="vipreceipt-create">
        <?=
        $this->render('_form_new_order', [
            'model' => $model,
            'tempreceipt' => $tempreceipt,
        ])
        ?>

    </div>
</div>
