<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receipts List';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary vipreceipt-index">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?> <span class="label label-danger">Decline</span></h3>
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'vip_receipt_id',
            [
                'attribute' => 'order_num',
                'label' => 'Order Num',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->order_num;
                },
            ], 
            [
                'attribute' => 'full_name',
                'label' => 'Full Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->full_name;
                },
            ],
            [
                'attribute' => 'mobile_no',
                'label' => 'Mobile No',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->mobile_no;
                },
            ],
            [
                'attribute' => 'company_name1',
                'label' => 'Company Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->companyInfo->company_name;
                },
            ],            
            [
                'attribute' => 'date_of_receipt',
                'label' => 'Date Of Receipt',
                'format' => 'html',
                //'headerOptions' => ['width' => '120'],
                //'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->date_of_receipt));
                },
            ], 
            [
                'attribute' => 'invoice_no',
                'label' => 'Invoice No',
                'format' => 'html',
                //'headerOptions' => ['width' => '100'],
                'value' => function ($model) {
                    return $model->invoice_no;
                },
            ],            
            [
                'attribute' => 'company_name',
                'label' => 'Distributor',
                'format' => 'html',
                //'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                //'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    if(!empty($model->reseller_name)) {
                        return $model->company->company_name;
                    }else {
                        return 'N/A';
                    }
                },
            ],
                       
            //'userID',
            //'clientID',
            [
                'attribute' => 'reasons_id',
                'label' => 'Reasons',
                'format' => 'html',
                //'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                //'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    if($model->reasons_id != 0) {
                        return $model->reasons->name;
                    }else {
                        return 'N/A';
                    }
                },
            ],
                        
            [
                'attribute' => 'created_datetime',
                'label'=>'Added Date',                                                    
                //'format' => ['raw', 'Y-m-d H:i:s'],
                //'format' => ['date', 'medium'],
                'format' => 'html',
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['width' => '120','class' => 'text-center'],
                'value' => function ($model) {
                    if($model->date_of_receipt == '1970-01-01'){
                        return 'N/A';
                    }else {
                        return Yii::$app->formatter->asDatetime($model->created_datetime, "php:d-m-Y");
                    }
                },
                //'options' => ['width' => '200']
            ],
              [
                'class' => 'yii\grid\ActionColumn',
                  'headerOptions' => ['width' => '50'],
                'template' => '{update} {view} ', //{view} {delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    },
                    'update' => function ($url, $model) {
                        if($model->status == 'P' || $model->status == 'N') {
                            return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));                        
                        }
                    },
                ],
            ],          
        ],
    ]); ?>
            </div>
        </div>
    </div>
