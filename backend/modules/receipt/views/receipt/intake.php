<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
if($this->context->action->id == 'index'){
    $acttitle = '';
}else {
    $acttitle = ucfirst($this->context->action->id);
}
$this->title = 'Camping Intake';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary vipreceipt-index">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Intake</th>
                                <th>Target Pack Size</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Apr - Jun</td>
                                <td>100</td>
                                <td>
                                    <a href="camping-intake-detail" title="View"><span class="glyphicon glyphicon-eye-open"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>Jul - Sep</td>
                                <td>200</td>
                                <td>
                                    <a href="camping-intake-detail" title="View"><span class="glyphicon glyphicon-eye-open"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>Oct - Dec</td>
                                <td>300</td>
                                <td>
                                    <a href="camping-intake-detail" title="View"><span class="glyphicon glyphicon-eye-open"></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
