<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
if($this->context->action->id == 'index'){
    $acttitle = '';
}else {
    $acttitle = ucfirst($this->context->action->id);
}
$this->title = 'View Jul - Sep';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary vipreceipt-index">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>Name</th>
                                    <th>Popularity</th>
                                    <th>Status</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="#">KP1809130005</a></td>
                                    <td>Shihan</td>
                                    <td>
                                        <div class="progress-group">

                                            <span class="progress-number"><b>150</b>/200</span>

                                            <div class="progress sm">
                                                <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td><span class="label label-info">Processing</span></td>
                                </tr>
                                <tr>
                                    <td><a href="#">KP1809130006</a></td>
                                    <td>Sarkuna</td>
                                    <td>
                                        <div class="progress-group">

                                            <span class="progress-number"><b>120</b>/200</span>

                                            <div class="progress sm">
                                                <div class="progress-bar progress-bar-blue" style="width: 50%"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td><span class="label label-info">Processing</span></td>
                                </tr>
                                <tr>
                                    <td><a href="#">KP1809130007</a></td>
                                    <td>John</td>
                                    <td>
                                        <div class="progress-group">

                                            <span class="progress-number"><b>90</b>/200</span>

                                            <div class="progress sm">
                                                <div class="progress-bar progress-bar-yellow" style="width: 40%"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td><span class="label label-info">Processing</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
        </div>
    </div>
