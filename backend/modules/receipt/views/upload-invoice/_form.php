<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPUploadInvoice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipupload-invoice-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'userID')->textInput() ?>

    <?= $form->field($model, 'clientID')->textInput() ?>

    <?= $form->field($model, 'file_group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'upload_file_name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'upload_file_new_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'add_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
