<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPUploadInvoiceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipupload-invoice-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'upload_id') ?>

    <?= $form->field($model, 'userID') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'file_group') ?>

    <?= $form->field($model, 'upload_file_name') ?>

    <?php // echo $form->field($model, 'upload_file_new_name') ?>

    <?php // echo $form->field($model, 'add_date') ?>

    <?php // echo $form->field($model, 'upload_via') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
