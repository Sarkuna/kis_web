<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPUploadInvoice */

$this->title = 'Create Vipupload Invoice';
$this->params['breadcrumbs'][] = ['label' => 'Vipupload Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipupload-invoice-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
