<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPUploadInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Upload Invoices List';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary vipupload-invoice-index">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success pull-right hide', 'title' => 'Add New']) ?>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'upload_id',
            [
                'attribute' => 'email',
                'label' => 'Email',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->full_name;
                },
            ],
            [
                'attribute' => 'mobile_no',
                'label' => 'Mobile No',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->mobile_no;
                },
            ],
            [
                'attribute' => 'attachments',
                'label' => 'Attachments',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->getTotalAdd();
                },
            ],
            
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{list}', //{view} {delete}
                'buttons' => [
                    'list' => function ($url, $model) {
                        $url = 'list?id='.$model->file_group;
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'List View'),]));
                    }
                ],
            ],            
            //'userID',
            //'clientID',
            //'file_group',
            //'upload_file_name:ntext',
            // 'upload_file_new_name',
            // 'add_date',
            // 'upload_via',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
            </div>
        </div>
    </div>


