<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPUploadInvoice */

$this->title = 'Update Vipupload Invoice: ' . $model->upload_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipupload Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->upload_id, 'url' => ['view', 'id' => $model->upload_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vipupload-invoice-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
