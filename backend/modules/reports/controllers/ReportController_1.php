<?php

namespace app\modules\reports\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use DateTime;
use kartik\mpdf\Pdf;


use common\models\VIPCustomer;
use common\models\ReportForm;
use common\models\ReportFormCustomer;
use common\models\ReportOrderForm;
use common\models\ReceiptsForm;
use common\models\TrackingForm;
use common\models\VIPReceipt;
use common\models\TypeName;
use common\models\SMSQueue;
use common\models\MPOrder;

use app\components\ExportBehavior;
use app\components\ExportExcelUtil;

use \PHPExcel;
use \PHPExcel_IOFactory;
use \PHPExcel_Settings;
use \PHPExcel_Style_Fill;
use \PHPExcel_Writer_IWriter;
use \PHPExcel_Worksheet;
use \PHPExcel_Style;
use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'export2excel' => [
                'class' => ExportBehavior::className(),
            ]
        ];
    }
    public function actionIndex()
    {
        
        $model = new \common\models\ReportBasicForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->history($model->type);
        }
        return $this->render('reportform', [
            'model' => $model
        ]);
    }
    
    protected function history($type)  //insert 50000 records using createCommand method
    {
        $session = Yii::$app->session;
        $customers = VIPCustomer::find()->joinWith([
            'user',
            'companyInfo'
        ])->where(['user.type' => $type, 'clientID' => $session['currentclientID']])
          ->orderBy(['user.created_datetime' => SORT_DESC])->each();
        
        $filename = Date('YmdGis').'Summary_Report.xls';
            header("Content-type: application/vnd-ms-excel");
            header("Content-Disposition: attachment; filename=".$filename);
            header("Pragma: no-cache"); 
            header("Expires: 0");

            echo '<table border="1" width="100%">';
            echo '<thead><tr>
                    <th>Code</th>
                    <th>Company Name</th>
                    <th>Company Address</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Email Verified</th>
                    <th>Mobile Number</th>                    
                    <th>Status</th>
                    <th>Date Added</th>
            </tr></thead>';
            echo '<tbody>';
            $tr = '';
            foreach($customers as $customer) {
                $client_status = $customer['user']['status'];
                if ($client_status == "X") {
                    $returnValue = 'Deleted';
                } else if ($client_status == "P") {
                    $returnValue = 'Pending';
                } else if ($client_status == "C") {
                    $returnValue = 'Cancel';
                } else if ($client_status == "A") {
                    $returnValue = 'Approved';
                } else if ($client_status == "D") {
                    $returnValue = 'Deactive';
                } else if ($client_status == "N") {
                    $returnValue = 'Not Approved';
                }
                
                $email_status = $customer['user']['email_verification'];
                if($email_status == 'Y' || $email_status == 'y') {
                    $verify = 'Yes';
                }else {
                    $verify = 'No';
                }
                
                $address = $customer['companyInfo']['mailing_address'];

                $tr .= '<tr>
                <td>'.$customer['clients_ref_no'].'&nbsp;</td>
                <td>'.$customer['companyInfo']['company_name'].'</td>
                <td>'.$address.'</td>
                <td>'.$customer['full_name'].'</td>
                <td>'.$customer['user']['email'].'</td>
                <td>'.$verify.'</td>    
                <td>'.$customer['mobile_no'].'&nbsp;</td> 
                <td>'.$returnValue.'</td> 
                <td>'.date('d-m-Y', strtotime($customer['user']['created_datetime'])).'</td>
 
                </tr>';
            }
            echo $tr;
            echo '</tbody>';
            echo '</table>';
            die;
    }
    
    public function actionCustomerAccountSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportFormCustomer();
        
        $actyps = TypeName::find()
            ->where([
                'clientID' => $session['currentclientID'],              
        ])->all();
        $actypslist = ArrayHelper::map($actyps, 'tier_id', 'name');
        
        $type = Yii::$app->getRequest()->getQueryParam('type');
        if(!empty($type)){
            $type = explode('-', $type);
            $modelForm->type = $type;
        }else {
            $type = $actypslist;
        }
        
        if($modelForm->load(Yii::$app->request->post())){
            $type = implode('-', $modelForm->type);
            $filter_string = '/reports/report/customer-account-summary?type='.$type;
            return $this->redirect([$filter_string]);            
        }
        
        $data = VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                    'user' => function($que) {
                        $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                    },
                            'companyInfo' => function($que) {
                        $que->select(['user_id', 'company_name', 'mailing_address']);
                    },
                        ])
                        ->where(['clientID' => $session['currentclientID']])
                        ->andwhere(['IN', 'user.type', $type])
                        ->orderBy(['user.created_datetime' => SORT_DESC]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $data,
                        //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
                ]);

            return $this->render('customers', [
                'actypslist' => $actypslist,
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionCustomerAccountIndirectsSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $type = 5;
        $status = array("A");
        $data = VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                    'user' => function($que) {
                        $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                    },
                            'companyInfo' => function($que) {
                        $que->select(['user_id', 'company_name', 'mailing_address']);
                    },
                        ])
                        ->where(['clientID' => $session['currentclientID']])
                        ->andwhere(['IN', 'user.type', $type])
                        ->andwhere(['IN', 'user.status', $status])
                        ->orderBy(['user.created_datetime' => SORT_DESC]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $data,
                        //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
                ]);

            return $this->render('customers_indirects', [
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionPointsSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportFormCustomer();
        
        $actyps = TypeName::find()
            ->where([
                'clientID' => $session['currentclientID'],              
        ])->all();
        $actypslist = ArrayHelper::map($actyps, 'tier_id', 'name');
        
        $type = Yii::$app->getRequest()->getQueryParam('type');
        if(!empty($type)){
            $type = explode('-', $type);
            $modelForm->type = $type;
        }else {
            $type = $actypslist;
        }
        
        
        
        if($modelForm->load(Yii::$app->request->post())){
            $type = implode('-', $modelForm->type);
            $filter_string = '/reports/report/points-summary?type='.$type;
            return $this->redirect([$filter_string]);            
        }
        
        if(Yii::$app->request->getQueryParam('type') == '5') {
            $status = array("A");
        }else {
            $status = array("P", "A", "X", "D", "N");
        }
        
        $data = VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                    'user' => function($que) {
                        $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                    },
                            'companyInfo' => function($que) {
                        $que->select(['user_id', 'company_name', 'mailing_address']);
                    },
                        ])
                        ->where(['clientID' => $session['currentclientID']])
                        ->andwhere(['IN', 'user.type', $type])
                        ->andwhere(['IN', 'user.status', $status])
                        ->orderBy(['user.created_datetime' => SORT_DESC]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $data,
                        //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
                ]);

            return $this->render('points_summary', [
                'actypslist' => $actypslist,
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionLastLogin()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';

        $status = array("P", "A", "X", "D", "N");
        $type = array("1", "A", "X", "D", "N");
        
        $data = VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                    'user' => function($que) {
                        $que->select(['id', 'email', 'date_last_login', 'status', 'type', 'created_datetime']);
                    },
                            'companyInfo' => function($que) {
                        $que->select(['user_id', 'company_name', 'mailing_address']);
                    },
                        ])
                        ->where(['clientID' => $session['currentclientID']])
                        ->andwhere(['IN', 'user.user_type', 'D'])
                        ->andWhere(['not', ['user.date_last_login' => null]])    
                        ->andwhere(['IN', 'user.status', $status])
                        ->orderBy(['user.created_datetime' => SORT_DESC]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $data,
                        //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
                ]);

            return $this->render('last_login', [
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionPointsSummarySms()
    {
        /*$siteUrl = Yii::$app->params['siteUrl'];
        
        foreach (\common\models\EmailQueue::find()->where("email_queue_type= 'R' AND status = 'P' AND email_template_id = '109' AND (email IS NOT NULL)")->limit(50)->all() as $emailQueue) {
            //Check date_to_send 
            $today = new \DateTime('now');

            $emailTemplate = \common\models\EmailTemplate::findOne($emailQueue->email_template_id);

            $emailBody = $emailTemplate->template;
            $emailSubject = $emailTemplate->subject;
            
            $userlisit = \common\models\VIPCustomer::find()     
                        ->where(['userID' => $emailQueue->user_id]) 
                        ->one();
            

            $data2 = \yii\helpers\Json::decode($emailQueue['data']);

            $name = isset($data2['name']) ? $data2['name'] : null;
            $company_name = isset($data2['company_name']) ? $data2['company_name'] : null;
            $expired_point = isset($data2['expired_point']) ? $data2['expired_point'] : null;
            $expired_date  = isset($data2['expired_date']) ? $data2['expired_date'] : null;
            $today_date  = isset($data2['today_date']) ? $data2['today_date'] : null;
            $balance_point = isset($data2['balance_point']) ? $data2['balance_point'] : null;
            
            if(empty($name)) {
                $name = $userlisit->clients_ref_no;
            }

            $emailBody = str_replace('{name}', $name, $emailBody);
            $emailBody = str_replace('{company_name}', $company_name, $emailBody);
            $emailBody = str_replace('{expired_point}', $expired_point, $emailBody);
            $emailBody = str_replace('{expired_date}', $expired_date, $emailBody);
            $emailBody = str_replace('{today_date}', $today_date, $emailBody);
            $emailBody = str_replace('{balance_point}', $balance_point, $emailBody);    
            $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
            
            $emailSubject = str_replace('{name}', $name, $emailSubject);




            if (!empty($emailQueue->email)) {
                Yii::$app->mailer->compose()
                    ->setTo($emailQueue->email)
                    ->setFrom([Yii::$app->params['supportEmail'] =>  'Mykawankansai'])
                    //->setBcc('shihanagni@gmail.com')
                    ->setSubject($emailSubject)
                    ->setHtmlBody($emailBody)
                    ->send();
                
                $emailQueueToUpdate = \common\models\EmailQueue::findOne($emailQueue->id);
                $emailQueueToUpdate->status = "C";
                $emailQueueToUpdate->date_to_send = date('Y-m-d');
                $emailQueueToUpdate->save();
            }

            
        }
        die; */        
        $session = Yii::$app->session;
        $type = array(1,2,5);
        $result = VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                'user' => function($que) {
                    $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                },
                'companyInfo' => function($que) {
                    $que->select(['user_id', 'company_name']);
                },
            ])
            ->where(['clientID' => $session['currentclientID']])
            ->andwhere(['IN', 'user.type', $type])
            //->andWhere(['not', ['mobile_no' => null]])            
            ->andwhere(['IN', 'user.status', 'A'])
            ->andwhere(['!=', 'company_information.company_name', 'kansai_reserve'])            
            ->orderBy(['user.created_datetime' => SORT_DESC])->all();

            $mydata = array();
            $mydataemail = array();
            foreach ($result as $model) {
                $balance = $model['totalAdd'] - $model['totalMinus'] - $model['expired'] + $model['adjustment'];
                $goingtoexpire = $model['goingtoExpire'] - $model['currentYearRedeemed'];

                /*$type = $model['user']['type'];
                if($type == 1) {
                    $typename = 'Distributors';
                }else if($type == 2) {
                    $typename = 'Dealer';
                }else {
                    $typename = 'Indirect';
                }*/
                if ($goingtoexpire > 0) {
                    //$mydata[] = [$model['companyInfo']['company_name'],$model['full_name'],$model['mobile_no'],$model['user']['email'],$typename,$tot,$goingtoexpire];
                    
                    if(!empty($model['full_name'])) {
                        $name = $model['companyInfo']['company_name'];
                    }else {
                        $name = $model['full_name'];
                    }
                    
                    if(!empty($model['companyInfo']['company_name'])) {
                        $company_name = $model['companyInfo']['company_name'];
                    }else {
                        $company_name = 'NA';
                    }
                    
                    $jsonData = \yii\helpers\Json::encode(array(
                        'name' => $name,
                        'company_name' => $company_name,
                        'expired_point' => $goingtoexpire,
                        'expired_date' => '31-12-'.date('y'),
                        'today_date' => date('d-m-y'),
                        'balance_point' => $balance
                    ));
                    
                    if(!empty($model['mobile_no'])) {                        
                        $mydata[] = [4,'reminder',$session['currentclientID'],$model['userID'],$model['mobile_no'],$jsonData,'P'];
                    }
                    
                    if(!empty($model['user']['email'])) {
                       $mydataemail[] = [109,'R',$session['currentclientID'],$model['userID'],$model['clients_ref_no'],$model['user']['email'],$jsonData,'P']; 
                    }
                }  
            }
            
            if($mydata > 0) {
                Yii::$app->db
                    ->createCommand()
                    ->batchInsert('sms_queue', ['sms_template_id','sms_type','clientID','user_id','mobile','data','status'],$mydata)
                    ->execute(); 
            }
            
            if($mydataemail > 0) {
                Yii::$app->db
                    ->createCommand()
                    ->batchInsert('email_queue', ['email_template_id','email_queue_type','clientID','user_id','name', 'email','data','status'],$mydataemail)
                    ->execute();
            }
            echo 'Done';
            //echo '<pre>';
            //print_r($mydata);
            exit;
    }
    
    public function actionPointsSummaryAcc2()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportFormCustomer();

        $type = 1;
        
        $data = VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                    'user' => function($que) {
                        $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                    },
                            'companyInfo' => function($que) {
                        $que->select(['user_id', 'company_name', 'mailing_address']);
                    },
                        ])
                        ->where(['clientID' => $session['currentclientID']])
                        ->andwhere(['IN', 'user.type', $type])
                        ->orderBy(['user.created_datetime' => SORT_DESC]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $data,
                        //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
                ]);

            return $this->render('points_summary_acc2', [
                //'actypslist' => $actypslist,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionPointsSummaryAcc2Detail($code)
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $data1 = VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                    'companyInfo' => function($que) {
                        $que->select(['user_id', 'company_name']);
                    },
                        ])
                        ->where(['clients_ref_no' => $code])
                        ->one();

        $data = \common\models\VIPCustomerAccount2::find()->select(['account2_id', 'customer_id', 'indirect_id', 'description', 'points_in', 'date_added', 'bb_type'])
                /*->joinWith([
                    'user' => function($que) {
                        $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                    },
                    'companyInfo' => function($que) {
                        $que->select(['user_id', 'company_name', 'mailing_address']);
                    },
                ])*/
                ->where(['customer_id' => $data1->userID])
                //->andwhere(['IN', 'user.type', $type])
                ->orderBy(['date_added' => SORT_DESC]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $data,
                        //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
                ]);

            return $this->render('points_summary_acc2_detail', [
                //'actypslist' => $actypslist,
                'dataProvider' => $dataProvider,
                'code' => $code,
            ]);
    }
    
    public function actionOrdersSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportOrderForm();
        
        $date_from = Yii::$app->getRequest()->getQueryParam('date_from');
        if(empty($date_from)){
            $date_from = date('Y-m-d');
        }
        $date_to = Yii::$app->getRequest()->getQueryParam('date_to');
        if(empty($date_to)){
            $date_to = date('Y-m-d');
        }
        
        $modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));

        if($modelForm->load(Yii::$app->request->post())){
            $date_range = str_replace(',', '', $modelForm->date_range);
            $daterange = explode( 'to', $date_range);
            $date_from_arr = explode(' ', ltrim($daterange[0]));
            $date_to_arr = explode(' ', ltrim($daterange[1]));
            
            if(!empty($date_from_arr)){
                $dateParse = date_parse($date_from_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_from = new DateTime();
                $date_from->setDate((int)$date_from_arr[2], $monthNum, (int)$date_from_arr[0]);
            }

            if(!empty($date_to_arr)){
                $dateParse = date_parse($date_to_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_to = new DateTime();
                $date_to->setDate((int)$date_to_arr[2], $monthNum, (int)$date_to_arr[0]);
            }

            $filter_string = '/reports/report/orders-summary?date_from='.$date_from->format('Y-m-d').'&date_to='.$date_to->format('Y-m-d');
            return $this->redirect([$filter_string]);          
        }
        
        $data = \common\models\VIPOrderProduct::find()->joinWith([
                'order',
                'order.customer' => function($que) {
                    $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
                },
                'order.company' => function($que) {
                    $que->select(['user_id','company_name']);
                },
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['between', 'vip_order.created_datetime', $date_from, $date_to ])            
            //->andwhere(['IN', 'user.type', $type])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC]);

            $dataProvider = new ActiveDataProvider([
                'query' => $data,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('orders_summary', [
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionMerchandiseOrdersSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportOrderForm();
        
        $date_from = Yii::$app->getRequest()->getQueryParam('date_from');
        if(empty($date_from)){
            $date_from = date('Y-m-d');
        }
        $date_to = Yii::$app->getRequest()->getQueryParam('date_to');
        if(empty($date_to)){
            $date_to = date('Y-m-d');
        }
        
        $modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));

        if($modelForm->load(Yii::$app->request->post())){
            $date_range = str_replace(',', '', $modelForm->date_range);
            $daterange = explode( 'to', $date_range);
            $date_from_arr = explode(' ', ltrim($daterange[0]));
            $date_to_arr = explode(' ', ltrim($daterange[1]));
            
            if(!empty($date_from_arr)){
                $dateParse = date_parse($date_from_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_from = new DateTime();
                $date_from->setDate((int)$date_from_arr[2], $monthNum, (int)$date_from_arr[0]);
            }

            if(!empty($date_to_arr)){
                $dateParse = date_parse($date_to_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_to = new DateTime();
                $date_to->setDate((int)$date_to_arr[2], $monthNum, (int)$date_to_arr[0]);
            }

            $filter_string = '/reports/report/merchandise-orders-summary?date_from='.$date_from->format('Y-m-d').'&date_to='.$date_to->format('Y-m-d');
            return $this->redirect([$filter_string]);          
        }
        
        $data = \common\models\MPOrderItem::find()->joinWith([
                'order',
            ])
            ->where(['between', 'mp_order.created_datetime', $date_from, $date_to ])            
            //->andwhere(['IN', 'user.type', $type])
            ->orderBy(['mp_order.created_datetime' => SORT_DESC]);

            $dataProvider = new ActiveDataProvider([
                'query' => $data,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('merchandise_orders_summary', [
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionInvoiceSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportOrderForm();
        
        $date_from = Yii::$app->getRequest()->getQueryParam('date_from');
        if(empty($date_from)){
            $date_from = date('Y-m-d');
        }
        $date_to = Yii::$app->getRequest()->getQueryParam('date_to');
        if(empty($date_to)){
            $date_to = date('Y-m-d');
        }
        
        $modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));

        if($modelForm->load(Yii::$app->request->post())){
            $date_range = str_replace(',', '', $modelForm->date_range);
            $daterange = explode( 'to', $date_range);
            $date_from_arr = explode(' ', ltrim($daterange[0]));
            $date_to_arr = explode(' ', ltrim($daterange[1]));
            
            if(!empty($date_from_arr)){
                $dateParse = date_parse($date_from_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_from = new DateTime();
                $date_from->setDate((int)$date_from_arr[2], $monthNum, (int)$date_from_arr[0]);
            }

            if(!empty($date_to_arr)){
                $dateParse = date_parse($date_to_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_to = new DateTime();
                $date_to->setDate((int)$date_to_arr[2], $monthNum, (int)$date_to_arr[0]);
            }

            $filter_string = '/reports/report/orders-summary?date_from='.$date_from->format('Y-m-d').'&date_to='.$date_to->format('Y-m-d');
            return $this->redirect([$filter_string]);          
        }
        
        $data = \common\models\VIPOrderProduct::find()->joinWith([
                'order',
                'order.customer' => function($que) {
                    $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
                },
                'order.company' => function($que) {
                    $que->select(['user_id','company_name']);
                },
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['between', 'vip_order.created_datetime', $date_from, $date_to ])            
            //->andwhere(['IN', 'user.type', $type])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC]);

            $dataProvider = new ActiveDataProvider([
                'query' => $data,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('orders_summary', [
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionReceipts()
    {
        $session = Yii::$app->session;
        //$this->layout = '/main_krajee';
        $modelForm = new ReportOrderForm();
        
        $date_from = Yii::$app->getRequest()->getQueryParam('date_from');
        if(empty($date_from)){
            $date_from = date('Y-m-d');
        }
        $date_to = Yii::$app->getRequest()->getQueryParam('date_to');
        if(empty($date_to)){
            $date_to = date('Y-m-d');
        }
        
        $modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));

        if($modelForm->load(Yii::$app->request->post())){
            $date_range = str_replace(',', '', $modelForm->date_range);
            $daterange = explode( 'to', $date_range);
            $date_from_arr = explode(' ', ltrim($daterange[0]));
            $date_to_arr = explode(' ', ltrim($daterange[1]));
            
            if(!empty($date_from_arr)){
                $dateParse = date_parse($date_from_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_from = new DateTime();
                $date_from->setDate((int)$date_from_arr[2], $monthNum, (int)$date_from_arr[0]);
            }

            if(!empty($date_to_arr)){
                $dateParse = date_parse($date_to_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_to = new DateTime();
                $date_to->setDate((int)$date_to_arr[2], $monthNum, (int)$date_to_arr[0]);
            }

            $filter_string = '/reports/report/receipts?date_from='.$date_from->format('Y-m-d').'&date_to='.$date_to->format('Y-m-d');
            return $this->redirect([$filter_string]);          
        }
        /*$query->joinWith(['customer', 'reseller',
                'indirectCompany' => function ($q) {
                        $q->from('company_information uc');
                }
            ]);*/

        $data = \common\models\VIPReceipt::find()->joinWith([
                //'order',
                'customer' => function($que) {
                    $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
                },
                'reseller' => function($que) {
                    $que->select(['user_id','company_name']);
                },
                'indirectCompany' => function ($q) {
                        $q->from('company_information uc');
                }        
            ])->where(['vip_receipt.clientID' => $session['currentclientID']])
            ->andwhere(['between', 'vip_receipt.created_datetime', $date_from.' 00:00:00', $date_to.' 23:59:59' ])            
            //->andwhere(['IN', 'user.type', $type])
            ->orderBy(['vip_receipt.created_datetime' => SORT_DESC]);

            $dataProvider = new ActiveDataProvider([
                'query' => $data,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('receipts_list', [
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionReceiptsSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReceiptsForm();
        
        $receiptstartdate = VIPReceipt::find()->where(['clientID' => $session['currentclientID']])->count();
        if($receiptstartdate > 0){
            $receiptstartdate = VIPReceipt::find()->where(['clientID' => $session['currentclientID']])
            ->orderBy(['created_datetime' => SORT_ASC])        
            ->one();
            $startyear = date('Y', strtotime($receiptstartdate->created_datetime));
        }else {
            $startyear = '2019';
        }
        

        $year = Yii::$app->getRequest()->getQueryParam('year');
        if(empty($year)){
            $year = date('Y');
        }
        
        $reseller_name = Yii::$app->getRequest()->getQueryParam('name');
        $indirect = Yii::$app->getRequest()->getQueryParam('indirect');
        
        if(!empty($reseller_name) && !empty($indirect)){
           $rname = '&name='.$reseller_name;
           $gindirect = '&indirect='.$indirect;
           $datawhere = ['reseller_name' => $reseller_name, 'userID' => $indirect];
           $datawhere_pts = ['vip_receipt.reseller_name' => $reseller_name, 'vip_receipt.userID' => $indirect];
        }elseif(!empty($reseller_name)){
           $rname = '&name='.$reseller_name;
           $datawhere = ['reseller_name' => $reseller_name];
           $datawhere_pts = ['vip_receipt.reseller_name' => $reseller_name];
        }elseif(!empty($indirect)){
           $gindirect = '&indirect='.$indirect;
           $datawhere = ['userID' => $indirect];
           $datawhere_pts = ['vip_receipt.userID' => $indirect];
        }else {
            $rname = '';
            $datawhere = '';
            $datawhere_pts = '';
        }

        $modelForm->date_range = $year;
        $modelForm->reseller_name = $reseller_name;
        $modelForm->indirect = $indirect;
        
        if($modelForm->load(Yii::$app->request->post())){
            $year = $modelForm->date_range;
            if(!empty($modelForm->reseller_name)) {
                $rname = '&name='.$modelForm->reseller_name;
            }else {
                $rname = '';
            }
            
            if(!empty($modelForm->indirect)) {
                $gindirect = '&indirect='.$modelForm->indirect;
            }else {
                $gindirect = '';
            }
            
            $filter_string = '/reports/report/receipts-summary?year='.$year.$rname.$gindirect;
            return $this->redirect([$filter_string]);          
        }

            $i = 1;
            $data = array();
            
            
            $mydaate = $year.'-01-01';
            $month = strtotime($mydaate);
            $pendingtotal = 0; $draftstotal = 0; $processingtotal = 0; $approvedtotal = 0; $approvedtotalsum = 0; $declinetotal = 0; $totalsum = 0;
            while($i <= 12)
            {
                
                $month_name_no = date('m', $month);
                $start_month = $year.'-'.$month_name_no.'-01 00:00:00';
                $a_date = $year.'-'.$month_name_no.'-01';                
                $a_date = date("Y-m-t", strtotime($a_date));
                $end_month = $a_date.' 23:59:59';

                $month_name = date('F', $month);                
                $pending = VIPReceipt::find()
                        //->where($datawhere)
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'P'])
                        ->andwhere($datawhere)
                        ->andwhere(['between', 'created_datetime', $start_month, $end_month ])
                        ->count();
                $drafts = VIPReceipt::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'R'])
                        ->andwhere($datawhere)
                        ->andwhere(['between', 'created_datetime', $start_month, $end_month ])
                        ->count();
                $processing = VIPReceipt::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'N'])
                        ->andwhere($datawhere)
                        ->andwhere(['between', 'created_datetime', $start_month, $end_month ])
                        ->count();
                $approved = VIPReceipt::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'A'])
                        ->andwhere($datawhere)
                        ->andwhere(['between', 'created_datetime', $start_month, $end_month ])
                        ->count();
                
                $approvedpts = VIPReceipt::find()
                        ->joinWith('receiptList')
                        ->where(['vip_receipt.clientID' => $session['currentclientID'], 'vip_receipt.status' => 'A'])
                        ->andwhere($datawhere_pts)
                        ->andwhere(['between', 'vip_receipt.created_datetime', $start_month, $end_month ])
                        ->sum('vip_receipt_list.qty_points_awarded');
                
                if($approvedpts > 0) {
                    $approvedpts = $approvedpts;
                }else {
                    $approvedpts = 0;
                }
                
                $decline = VIPReceipt::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'D'])
                        ->andwhere($datawhere)
                        ->andwhere(['between', 'created_datetime', $start_month, $end_month ])
                        ->count();
                
                $total = $pending + $drafts + $processing + $approved + $decline;
                $pendingtotal += $pending;
                $draftstotal += $drafts;
                $processingtotal += $processing;
                $approvedtotal += $approved;
                $declinetotal += $decline;
                $totalsum += $total;
                $approvedtotalsum += $approvedpts;
                
                $data[] = array(
                    'Month' => $month_name, 
                    'Pending' => $pending, 
                    'Drafts' => $drafts, 
                    'Processing' => $processing, 
                    'Approved' => $approved,
                    'Approved Pts' => $approvedpts,
                    'Decline' => $decline, 
                    'Total' => $total
                );
                $month = strtotime('+1 month', $month);
                $i++;
            }
            $data2 = [
                ['Month' => 'Total', 'Pending' => $pendingtotal, 'Drafts' => $draftstotal, 'Processing' => $processingtotal, 'Approved' => $approvedtotal, 'Approved Pts' => $approvedtotalsum, 'Decline' => $declinetotal, 'Total' => $totalsum],
            ];
            
            $data3 = array_merge($data, $data2);
            /*$data = [
                ['id' => 1, 'name' => 'name 1'],
                ['id' => 2, 'name' => 'name 2'],
                ['id' => 100, 'name' => 'name 100'],
            ];*/

            $dataProvider = new ArrayDataProvider([
                'allModels' => $data3,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('receipts_summary', [
                'startyear' => $startyear,
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionSmsSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReceiptsForm();
        

        $year = Yii::$app->getRequest()->getQueryParam('year');
        if(empty($year)){
            $year = date('Y');
        }
        
        $startyear = '2019';
        $modelForm->date_range = $year;
        //$modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));

        if($modelForm->load(Yii::$app->request->post())){
            $year = $modelForm->date_range;
            $filter_string = '/reports/report/sms-summary?year='.$year;
            return $this->redirect([$filter_string]);          
        }

            $i = 1;
            $data = array();
            
            
            $mydaate = $year.'-01-01';
            $month = strtotime($mydaate);
            $pts_upload_total = 0; $receipts_total = 0; $pts_monthly_expiry_total = 0; $totalsum = 0;
            while($i <= 12)
            {
                
                $month_name_no = date('m', $month);
                $start_month = $year.'-'.$month_name_no.'-01';
                $a_date = $year.'-'.$month_name_no.'-01';                
                $a_date = date("Y-m-t", strtotime($a_date));
                $end_month = $a_date;

                $month_name = date('F', $month);                
                $pts_upload = SMSQueue::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'S', 'sms_type' => 'pts_upload'])
                        ->andwhere(['between', 'date_to_send', $start_month, $end_month ])
                        ->count();
                $receipts = SMSQueue::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'S', 'sms_type' => 'receipts'])
                        ->andwhere(['between', 'date_to_send', $start_month, $end_month ])
                        ->count();
                $pts_monthly_expiry = SMSQueue::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'S', 'sms_type' => 'pts_monthly_expiry'])
                        ->andwhere(['between', 'date_to_send', $start_month, $end_month ])
                        ->count();
                
                
                $total = $pts_upload + $receipts + $pts_monthly_expiry;
                $pts_upload_total += $pts_upload;
                $receipts_total += $receipts;
                $pts_monthly_expiry_total += $pts_monthly_expiry;
                $totalsum += $total;
                
                $data[] = array(
                    'Month' => $month_name, 
                    'Dealer & Distributor Pts Upload' => $pts_upload, 
                    'Receipts Approved' => $receipts, 
                    'Monthly Notifications' => $pts_monthly_expiry,
                    'Total' => $total
                );
                $month = strtotime('+1 month', $month);
                $i++;
            }
            $data2 = [
                ['Month' => 'Total', 'Dealer & Distributor Pts Upload' => $pts_upload_total, 'Receipts Approved' => $receipts_total, 'Monthly Notifications' => $pts_monthly_expiry_total],
            ];
            
            $data3 = array_merge($data, $data2);
            $dataProvider = new ArrayDataProvider([
                'allModels' => $data3,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('sms_summary', [
                'startyear' => $startyear,
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionDetailedPointSummary()
    {
        $this->layout = '/main_krajee';
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new \common\models\VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        //$searchModel->type = $type;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;


        return $this->render('detailed_point_summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,            
        ]);
    }
    
    public function actionDetailedPointSummary2()
    {
        $this->layout = '/main_krajee';
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new \common\models\VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;


        return $this->render('detailed_point_summary2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,            
        ]);
    }
    
    //Not use
    public function actionCustomerAccount()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportForm();
        
        $date_from = Yii::$app->getRequest()->getQueryParam('date_from');
        if(empty($date_from)){
            $date_from = date('Y-m-d');
        }
        $date_to = Yii::$app->getRequest()->getQueryParam('date_to');
        if(empty($date_to)){
            $date_to = date('Y-m-d');
        }

        $type = Yii::$app->getRequest()->getQueryParam('type');
        if(!empty($type)){
            $type = json_decode($type);
        }

        $modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));
        
        if($modelForm->load(Yii::$app->request->post())){
            //$type = implode(',', $modelForm->type);
            $type=json_encode($modelForm->type);
            $date_range = str_replace(',', '', $modelForm->date_range);
            $daterange = explode( 'to', $date_range);
            $date_from_arr = explode(' ', ltrim($daterange[0]));
            $date_to_arr = explode(' ', ltrim($daterange[1]));
            
            if(!empty($date_from_arr)){
                $dateParse = date_parse($date_from_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_from = new DateTime();
                $date_from->setDate((int)$date_from_arr[2], $monthNum, (int)$date_from_arr[0]);
            }

            if(!empty($date_to_arr)){
                $dateParse = date_parse($date_to_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_to = new DateTime();
                $date_to->setDate((int)$date_to_arr[2], $monthNum, (int)$date_to_arr[0]);
            }
            
            $filter_string = '/reports/report/customer-account?date_from='.$date_from->format('Y-m-d').'&date_to='.$date_to->format('Y-m-d').'&type='.$type;
            return $this->redirect([$filter_string]);
        }


            
            $data = VIPCustomer::find()->select(['vip_customer_id','userID','clientID','clients_ref_no','full_name','mobile_no'])->joinWith([
                    'user' => function($que) {
                        $que->select(['id','email','email_verification','status','type','created_datetime']);
                    },
                    'companyInfo' => function($que) {
                        $que->select(['user_id','company_name','mailing_address']);
                    },
                ])
                ->where(['clientID' => $session['currentclientID']])
                ->andwhere(['IN', 'user.type', $type])
                ->orderBy(['user.created_datetime' => SORT_DESC]);
                    
                 $dataProvider = new ActiveDataProvider([
                    'query' => $data,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
                ]);
                 
            return $this->render('customers', [
                //'model' => $model,
                'dataProvider' => $dataProvider,
            ]);      
    }
    
    public function actionDownloadInvoice($id){
        $session = Yii::$app->session;
        $orderproducts = \common\models\VIPOrderProduct::find()->joinWith([
                'order',
                'order.customer' => function($que) {
                    $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
                },
                'order.company' => function($que) {
                    $que->select(['user_id','company_name']);
                },
                'masterProduct' => function($que) {
                    $que->select(['product_id','voucher']);
                },        
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['vip_order.invoice_id' => $id])       
            //->andwhere(['IN', 'user.type', $type])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->all();
        $allItems[] = '';        
        if(!empty($orderproducts)){
            $dataProducts = array(); $product_temp1 = array();
            $mytotal = 0;
            foreach($orderproducts as $key=>$model){
                if($model->masterProduct->voucher == 'Y') {
                    $myvpoucher = 'Voucher';
                    $total_pro = $model->point - $model->point_admin + $model->shipping_point;
                    $totl_f = $model->quantity * $total_pro;
                    $unit = $model->point - $model->point_admin;
                }else {
                    $myvpoucher = 'Items';
                    $total_pro = $model->point + $model->shipping_point;
                    $totl_f = $model->quantity * $total_pro;
                    $unit = $model->point;
                }
                $mytotal += $totl_f / 2;
                
                $product_temp1 = array(                    
                    'Company Name' => $model->order->company->company_name,
                    'Code' => $model->order->customer->clients_ref_no,
                    'Order No.' => $model->order->invoice_prefix,
                    'Order Date' => date("d-m-Y", strtotime($model->order->created_datetime)),
                    'Invoice Number' => $model->invoiceno->invoice_no,
                    'Product Code' => $model->model,
                    'Product Description' => $model->name,
                    'Qty' => $model->quantity,
                    'Unit/pts' => $unit,
                    'Shipping/pts' => $model->shipping_point,
                    'Total Pts' => $totl_f,
                    'Unit/RM' => $total_pro / 2,
                    'Total RM' => $totl_f / 2,
                    'Status' => $model->order->orderStatus->name,
                    'Product Type' => $myvpoucher,
                    
                );
                $dataProducts[] = $product_temp1;
            }
        }
            
            $dataProducts[] = array(                    
                    'Company Name' => 'Admin Fee',
                    'Code' => '',
                    'Order No.' => '',
                    'Order Date' => '',
                    'Invoice Number' => '',
                    'Product Code' => '',
                    'Product Description' => '',
                    'Qty' => '',
                    'Unit/pts' => '',
                    'Shipping/pts' => '',
                    'Total Pts' => $mytotal,
                    'Unit/RM' => '',
                    'Total RM' => '',
                    'Status' => '',
                    
                );
            
            $orderproducts = \common\models\VIPOrderProduct::find()->joinWith([
                'order',
                'order.customer' => function($que) {
                    $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
                },
                'order.company' => function($que) {
                    $que->select(['user_id','company_name']);
                },
                'masterProduct' => function($que) {
                    $que->select(['product_id','voucher']);
                },        
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['vip_product.voucher' => 'Y'])            
            ->andwhere(['vip_order.invoice_id' => $id]) 
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->all();
        $allItems[] = '';        
        if(!empty($orderproducts)){
            $product_temp2 = array();
            $mytotal2 = 0;
            foreach($orderproducts as $key=>$model){
                $myvpoucher = 'Voucher';
                $total_pro = $model->point_admin + $model->shipping_point;
                $totl_f = $model->quantity * $total_pro;
                $unit = $model->point_admin;
                $mytotal2 += $totl_f / 2;
                
                $product_temp2 = array(                    
                    'Company Name' => $model->order->company->company_name,
                    'Code' => $model->order->customer->clients_ref_no,
                    'Order No.' => $model->order->invoice_prefix,
                    'Order Date' => date("d-m-Y", strtotime($model->order->created_datetime)),
                    'Invoice Number' => $model->invoiceno->invoice_no,
                    'Product Code' => $model->model,
                    'Product Description' => $model->name,
                    'Qty' => $model->quantity,
                    'Unit/pts' => $unit,
                    'Shipping/pts' => $model->shipping_point,
                    'Total Pts' => $totl_f,
                    'Unit/RM' => $total_pro / 2,
                    'Total RM' => $totl_f / 2,
                    'Status' => $model->order->orderStatus->name,
                    'Product Type' => $myvpoucher,
                    
                );
                $dataProducts[] = $product_temp2;
            }
        }
        
        $dataProducts[] = array(                    
                    'Company Name' => '',
                    'Code' => '',
                    'Order No.' => '',
                    'Order Date' => '',
                    'Invoice Number' => '',
                    'Product Code' => '',
                    'Product Description' => '',
                    'Qty' => '',
                    'Unit/pts' => '',
                    'Shipping/pts' => '',
                    'Total Pts' => $mytotal2,
                    'Unit/RM' => '',
                    'Total RM' => '',
                    'Status' => '',
                    
                );
        
        $dataProducts[] = array(                    
                    'Company Name' => '',
                    'Code' => '',
                    'Order No.' => '',
                    'Order Date' => '',
                    'Invoice Number' => '',
                    'Product Code' => '',
                    'Product Description' => '',
                    'Qty' => '',
                    'Unit/pts' => '',
                    'Shipping/pts' => '',
                    'Total Pts' => $mytotal + $mytotal2,
                    'Unit/RM' => '',
                    'Total RM' => '',
                    'Status' => '',
                    
                );

        $excel_data = ExportExcelUtil::excelDataFormat($dataProducts); 
        $excel_title = $excel_data['excel_title'];
        $excel_ceils = $excel_data['excel_ceils'];
        $excel_content = array(
            //sheet 1
            array(
                'sheet_name' => 'Invoice',
                'sheet_title' => $excel_title,
                'ceils' => $excel_ceils,
                //'freezePane' => 'B2',
                'headerColor' => ExportExcelUtil::getCssClass("header"),
                //'headerColumnCssClass' => array(
                    //'id' => ExportExcelUtil::getCssClass('blue'),
                    //'Status_Description' => ExportExcelUtil::getCssClass('grey'),
                //), //define each column's cssClass for header line only.  You can set as blank.
                //'oddCssClass' => ExportExcelUtil::getCssClass("odd"),
                //'evenCssClass' => ExportExcelUtil::getCssClass("even"),
                //'hightLight' => ExportExcelUtil::getCssClass("highlight"),
            )
            //sheet 2,....            
        );
        $excel_file = "Invoice";
        $this->export2excel($excel_content,$excel_file);
    }
    
    public function actionDownloadPdf($invoiceitemid){

        $invoiceitem = \common\models\InvoiceItem::find()
                        ->where('invoice_item_id = :invoice_item_id', [':invoice_item_id' => $invoiceitemid])
                        ->one();
        
        if(!empty($invoiceitem->invoice_pono)) {
            $invoice_pono = $invoiceitem->invoice_pono;
        }else {
            $invoice_pono = 'N/A';
        }
        
        $session = Yii::$app->session;
        $orderproducts = \common\models\VIPOrderProduct::find()->joinWith([
                'order',
                'order.customer' => function($que) {
                    $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
                },
                'order.company' => function($que) {
                    $que->select(['user_id','company_name']);
                },
                'masterProduct' => function($que) {
                    $que->select(['product_id','voucher']);
                },        
            ])
           ->where(['vip_order.clientID' => $session['currentclientID'], 'vip_order_product.invoice_item_id' => $invoiceitemid])
            //->andwhere(['vip_order.invoice_id' => $id])
            //->andwhere(['vip_product.voucher' => 'Y'])             
            //->andwhere(['!=', 'vip_product.voucher', 'Y'])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->all();
        $allItems[] = ''; 
        
        $html = '';
        $html = '<html><body>';
        $html .= '
                    <div id="project">
                        <h4>'.$invoiceitem->invoice->bill_from_name.'</h4>
                        <p>'.nl2br($invoiceitem->invoice->bill_from_add).'</p>
                    </div>
                    <div id="company">
                        <img src="'.Yii::$app->request->hostInfo.'/themes/adminLTE/custom/css/BB_logo.png" width="200"/>
                    </div>
                    <div class="clear"></div>';
        
        $html .= '<div id="invoice-title-number"><div id="title" class="ibcl_invoice_title" style="text-align: center;">INVOICE</div></div>';
        
        $html .= '<div id="client-info">
          <span style="color: #415472; font-size: 15px; font-weight: bold;">'.$invoiceitem->invoice->bill_to_name.'</span>
          <div class="client-name">
            <span>'.nl2br($invoiceitem->invoice->bill_to_add).'</span>
          </div>
        </div>';
        
        $mytbls = '<div id="items"><table cellpadding="0" cellspacing="0">
                    <thead>
                      <tr style= "background-color: #252223; color:#ffffff;">
                        <th>#</th>  
                        <th style="text-align: center;">Dealer Name</th>
                        <th style="text-align: center;">Dealer ID</th>
                        <th style="text-align: center;">Order No.</th>
                        <th style="text-align: center;">Order Date</th>
                        <th style="text-align: center;">Product Description</th>
                        <th style="text-align: center;">Quantity</th>
                        <th style="text-align: center;">Per Unit, RM</th>
                        <th style="text-align: center;">Amount, RM</th>
                      </tr></thead>
                    <tbody>';
        
        $html .= '<div class="clearfix" style="margin-bottom:20px;"></div>';
        $html .= '<div id="project">
                        <p><span>INVOICE NO. :</span> <label style="color: #b11116;">'.$invoiceitem->invoice_no.'</label></p>
                        <p><span>INVOICE DATE :</span> '.date('d-m-Y', strtotime($invoiceitem->invoice_date)).'</p>
                    </div>
                    <div id="company">
                        <p><span>P.O NO. :</span> '.$invoice_pono.'</p>
                        <p><span>PAYMENT DUE :</span> '.date('d-m-Y', strtotime($invoiceitem->invoice_due_date)).'</p>
                    </div>
                    <div class="clear" style="margin-bottom:10px;"></div>';
        
        $html .= $mytbls;
        
        if(!empty($orderproducts)){
            $dataProducts = array(); $product_temp1 = array();
            $mytotal = 0; $total_pro = 0; $totl_f = 0; $perunit = 0; $amount = 0;
            $n = 1;
            foreach($orderproducts as $key=>$model){
                if($model->masterProduct->voucher == 'Y') {
                    $myvpoucher = 'Voucher';
                    $total_pro = $model->point - $model->point_admin + $model->shipping_point;
                    $totl_f = $model->quantity * $total_pro;
                    $unit = $model->point - $model->point_admin;
                }else {
                    $myvpoucher = 'Items';
                    $total_pro = $model->point + $model->shipping_point;
                    $totl_f = $model->quantity * $total_pro;
                    $unit = $model->point;
                }

                $mytotal += $totl_f / 2;
                $perunit = $total_pro / 2;
                $amount = $totl_f / 2;
                $html .= '<tr>
                            <td>' . $n . '</td>
                            <td valign="middle" style="text-align: left;">' . $model->order->company->company_name . '</td>
                            <td valign="middle" style="text-align: center;">' . $model->order->customer->clients_ref_no. '</td>
                            <td valign="middle" style="text-align: center;">' . $model->order->invoice_prefix. '</td>
                            <td valign="middle" style="text-align: center;">' . date("d-m-Y", strtotime($model->order->created_datetime)) . '</td>
                            <td valign="middle" class="text-left">' . $model->name . '</td>       
                            <td valign="middle" style="text-align: center;">' . $model->quantity . '</td>
                            <td valign="middle" style="text-align: right;">' . Yii::$app->formatter->asDecimal($perunit) . '</td> 
                            <td valign="middle" style="text-align: right;">' . Yii::$app->formatter->asDecimal($amount) . '</td>     
                          </tr>';
                $n++;
            }
            
        }
        
        $html .= '</tbody></table></div>';
        
            if($invoiceitem->invoice_type == 'V') {
                $html .= '<h4 style="font-size: 12px;">Admin Fee</h4>';
                $html .= $mytbls;
                $orderproductsv = \common\models\VIPOrderProduct::find()->joinWith([
                'order',
                'order.customer' => function($que) {
                    $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
                },
                'order.company' => function($que) {
                    $que->select(['user_id','company_name']);
                },
                'masterProduct' => function($que) {
                    $que->select(['product_id','voucher']);
                },        
            ])
            ->where(['vip_order.clientID' => $session['currentclientID'], 'vip_order_product.invoice_item_id' => $invoiceitemid])
            //->andwhere(['vip_order.invoice_id' => $id])
            //->andwhere(['vip_product.voucher' => 'Y'])             
            //->andwhere(['!=', 'vip_product.voucher', 'Y'])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->all();
                
                if(!empty($orderproductsv)){
                    $product_temp2 = array();
                    $mytotal2 = 0; $perunit = 0; $amount = 0;
                    $n = 1;
                    foreach($orderproductsv as $key=>$modelvoucher){
                        $total_pro = $modelvoucher->point_admin + $modelvoucher->shipping_point;
                        $totl_f = $modelvoucher->quantity * $total_pro;
                        $unit = $modelvoucher->point_admin;
                        $mytotal2 += $totl_f / 2;

                        $perunit = $total_pro / 2;
                        $amount = $totl_f / 2;
                        $html .= '<tr>
                                    <td>' . $n . '</td>
                                    <td valign="middle" style="text-align: left;">' . $modelvoucher->order->company->company_name . '</td>
                                    <td valign="middle" style="text-align: center;">' . $modelvoucher->order->customer->clients_ref_no. '</td>
                                    <td valign="middle" style="text-align: center;">' . $modelvoucher->order->invoice_prefix. '</td>
                                    <td valign="middle" style="text-align: center;">' . date("d-m-Y", strtotime($modelvoucher->order->created_datetime)) . '</td>
                                    <td valign="middle" class="text-left">' . $modelvoucher->name . '</td>       
                                    <td valign="middle" style="text-align: center;">' . $modelvoucher->quantity . '</td>
                                    <td valign="middle" style="text-align: right;">' . Yii::$app->formatter->asDecimal($perunit) . '</td> 
                                    <td valign="middle" style="text-align: right;">' . Yii::$app->formatter->asDecimal($amount) . '</td>     
                                  </tr>';
                        $n++;

                    }
                    $mytotal = $mytotal + $mytotal2;
                }
                $html .= '</tbody></table></div>';
            }
        //$html .= '<div class="clear"></div>';
        
        $footer = '
        
        <table cellpadding="0" cellspacing="0" width="100%" style="margin-top:20px;">
  <tr>
    <td rowspan="3" style="font-size: 10px;" width="75%" valign="top">
    '.$invoiceitem->invoice->terms_conditions.'
    
    </td>
    <td style="font-weight: bold; font-size: 11px;" width="12%">SUB TOTAL</td>
    <td style="font-weight: bold; font-size: 11px; padding-right: 10px;" width="5%">:RM</td>
    <td style="text-align: right; font-weight: bold; font-size: 11px;" width="8%">'.Yii::$app->formatter->asDecimal($mytotal).'</td>
  </tr>
  <tr>
    <td style="font-weight: bold; font-size: 11px;">SST @ 0%</td>
    <td style="font-weight: bold; font-size: 11px; margin-right: 10px;">:RM</td>
    <td style="text-align: right; font-weight: bold; font-size: 11px;">0.00</td>
  </tr>
  <tr>
    <td style="font-weight: bold; font-size: 11px;">NET AMOUNT</td>
    <td style="font-weight: bold; font-size: 11px; margin-right: 10px;">:RM</td>
    <td style="text-align: right; font-weight: bold; font-size: 11px;">'.Yii::$app->formatter->asDecimal($mytotal).'</td>
  </tr>
</table>';
        $footer .= '<p style="text-align: center; font-size: 9px;"><b>**This invoice is computer generated and no signature is required**</b><br>Thank you for your prompt payment</p>';
        
       $footer .= "<table name='footer' width=\"1000\">
           <tr>
             <td style='font-size: 18px; padding-bottom: 20px;' align=\"right\">Page {PAGENO} of {nb}</td>
           </tr>
         </table>"; 
       
       $allfooter = "<table name='footer' width=\"1000\">
           <tr>
             <td style='font-size: 18px; padding-bottom: 20px;' align=\"right\">Page {PAGENO} of {nb}</td>
           </tr>
         </table>"; 
        $pdf = new Pdf(); // or new Pdf();
                $mpdf = $pdf->api;
                $stylesheet = file_get_contents(Yii::$app->request->hostInfo.'/themes/adminLTE/custom/css/print.css'); // external css
                //$mpdf->SetHTMLHeader('<img src="http://dev.agnichakra.com/images/club_logo.png"/>');
                $mpdf->SetFooter($allfooter);
                //$mpdf->SetHTMLHeader('<div style="width: 100%; text-align:right;"><img src="'.Yii::$app->request->hostInfo.'/images/pdf_header_dulux.jpg"/></div>');
                $mpdf->SetTitle($invoiceitem->invoice_no);
                $mpdf->AddPage('', // L - landscape, P - portrait 
                        '', '', '', '', 15, // margin_left
                        15, // margin right
                        15, // margin top
                        35, // margin bottom
                        15, // margin header
                        0); // margin footer
                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->WriteHtml($html);
                $mpdf->SetFooter($footer);
                //$mpdf->Output();
                $mpdf->Output($invoiceitem->invoice_no.'.pdf', 'I');
                //$mpdf->Output($membershipno.'.pdf', 'D');
                exit;
    }
    
    public function actionDownloadInternelInvoice($invoiceid){
        
        $invoiceitem = \common\models\InternalInvoicing::find()
                        ->where('internal_invoicing_id = :internal_invoicing_id', [':internal_invoicing_id' => $invoiceid])
                        ->one();
        

        $session = Yii::$app->session;
        $orderproducts = \common\models\InternalInvoicingItems::find()
            ->where('internal_invoicing_id = :internal_invoicing_id', [':internal_invoicing_id' => $invoiceid])
            ->orderBy(['internal_invoicing_items_id' => SORT_ASC])->all();
        $allItems[] = ''; 
        
        $html = '';
        $html = '<html><body>';
        $html .= '
                    <div id="project">
                        <h4>'.$invoiceitem->bill_from_name.'</h4>
                        <p>'.nl2br($invoiceitem->bill_from_add).'</p>
                    </div>
                    <div id="company">
                        <img src="'.Yii::$app->request->hostInfo.'/themes/adminLTE/custom/css/BB_logo.png" width="200"/>
                    </div>
                    <div class="clear"></div>';
        
        $html .= '<div id="invoice-title-number"><div id="title" class="ibcl_invoice_title" style="text-align: center;">INVOICE</div></div>';
        
        $html .= '<div id="client-info">
          <span style="color: #415472; font-size: 15px; font-weight: bold;">'.$invoiceitem->bill_to_name.'</span>
          <div class="client-name">
            <span>'.nl2br($invoiceitem->bill_to_add).'</span>
          </div>
        </div>';
        
        $mytbls = '<div id="items"><table cellpadding="0" cellspacing="0">
                    <thead>
                      <tr style= "background-color: #252223; color:#ffffff;">
                        <th>#</th>  
                        <th style="text-align: center;">Description</th>
                        <th style="text-align: center;">Quantity</th>
                        <th style="text-align: center;">Per Unit, RM</th>
                        <th style="text-align: center;">Amount, RM</th>
                      </tr>
                      </thead>
                    <tbody>';
        
        $html .= '<div class="clearfix" style="margin-bottom:20px;"></div>';
        $html .= '<div id="project">
                        <p><span>INVOICE NO. :</span> <label style="color: #b11116;">'.$invoiceitem->invoice_no.'</label></p>
                        <p><span>INVOICE DATE :</span> '.date('d-m-Y', strtotime($invoiceitem->invoice_date)).'</p>
                    </div>
                    <div id="company">
                        <p><span>PAYMENT DUE :</span> '.date('d-m-Y', strtotime($invoiceitem->due_date)).'</p>
                    </div>
                    <div class="clear" style="margin-bottom:10px;"></div>';
        
        $html .= $mytbls;
        
        if(!empty($orderproducts)){
            $n = 1;
            foreach($orderproducts as $key=>$model){

                $html .= '<tr>
                            <td>' . $n . '</td>
                            <td valign="middle" class="text-left" width="70%">' . $model->description . '</td>       
                            <td valign="middle" style="text-align: center;">' . $model->quantity . '</td>
                            <td valign="middle" style="text-align: right;">' . Yii::$app->formatter->asDecimal($model->per_unit) . '</td> 
                            <td valign="middle" style="text-align: right;">' . Yii::$app->formatter->asDecimal($model->amount) . '</td>     
                          </tr>';
                $n++;
            }
            
        }
        
        $html .= '</tbody></table></div>';
        
            
        
        $html .= '
        
        <table cellpadding="0" cellspacing="0" width="100%" style="margin-top:20px;">
  <tr>
    <td rowspan="3" style="font-size: 10px;" width="75%" valign="top">
    '.$invoiceitem->terms_conditions.'
    
    </td>
    <td style="font-weight: bold; font-size: 11px;" width="12%">SUB TOTAL</td>
    <td style="font-weight: bold; font-size: 11px; padding-right: 10px;" width="5%">:RM</td>
    <td style="text-align: right; font-weight: bold; font-size: 11px;" width="8%">'.Yii::$app->formatter->asDecimal($invoiceitem->sub_total).'</td>
  </tr>
  <tr>
    <td style="font-weight: bold; font-size: 11px;">SST @ 0%</td>
    <td style="font-weight: bold; font-size: 11px; margin-right: 10px;">:RM</td>
    <td style="text-align: right; font-weight: bold; font-size: 11px;">'.Yii::$app->formatter->asDecimal($invoiceitem->adjustment).'</td>
  </tr>
  <tr>
    <td style="font-weight: bold; font-size: 11px;">NET AMOUNT</td>
    <td style="font-weight: bold; font-size: 11px; margin-right: 10px;">:RM</td>
    <td style="text-align: right; font-weight: bold; font-size: 11px;">'.Yii::$app->formatter->asDecimal($invoiceitem->total).'</td>
  </tr>
</table>';

        $filename = $invoiceitem->invoice_no.".xls";
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=$filename");  
header("Pragma: no-cache"); 
header("Expires: 0");
        echo $html;
        exit;
    }
    
    public function actionDownloadInternelPdf($invoiceid){

        $invoiceitem = \common\models\InternalInvoicing::find()
                        ->where('internal_invoicing_id = :internal_invoicing_id', [':internal_invoicing_id' => $invoiceid])
                        ->one();
        

        $session = Yii::$app->session;
        $orderproducts = \common\models\InternalInvoicingItems::find()
            ->where('internal_invoicing_id = :internal_invoicing_id', [':internal_invoicing_id' => $invoiceid])
            ->orderBy(['internal_invoicing_items_id' => SORT_ASC])->all();
        $allItems[] = ''; 
        
        $html = '';
        $html = '<html><body>';
        $html .= '
                    <div id="project">
                        <h4>'.$invoiceitem->bill_from_name.'</h4>
                        <p>'.nl2br($invoiceitem->bill_from_add).'</p>
                    </div>
                    <div id="company">
                        <img src="'.Yii::$app->request->hostInfo.'/themes/adminLTE/custom/css/BB_logo.png" width="200"/>
                    </div>
                    <div class="clear"></div>';
        
        $html .= '<div id="invoice-title-number"><div id="title" class="ibcl_invoice_title" style="text-align: center;">INVOICE</div></div>';
        
        $html .= '<div id="client-info">
          <span style="color: #415472; font-size: 15px; font-weight: bold;">'.$invoiceitem->bill_to_name.'</span>
          <div class="client-name">
            <span>'.nl2br($invoiceitem->bill_to_add).'</span>
          </div>
        </div>';
        
        $mytbls = '<div id="items"><table cellpadding="0" cellspacing="0">
                    <thead>
                      <tr style= "background-color: #252223; color:#ffffff;">
                        <th>#</th>  
                        <th style="text-align: center;">Description</th>
                        <th style="text-align: center;">Quantity</th>
                        <th style="text-align: center;">Per Unit, RM</th>
                        <th style="text-align: center;">Amount, RM</th>
                      </tr>
                      </thead>
                    <tbody>';
        
        $html .= '<div class="clearfix" style="margin-bottom:20px;"></div>';
        $html .= '<div id="project">
                        <p><span>INVOICE NO. :</span> <label style="color: #b11116;">'.$invoiceitem->invoice_no.'</label></p>
                        <p><span>INVOICE DATE :</span> '.date('d-m-Y', strtotime($invoiceitem->invoice_date)).'</p>
                    </div>
                    <div id="company">
                        <p><span>PAYMENT DUE :</span> '.date('d-m-Y', strtotime($invoiceitem->due_date)).'</p>
                    </div>
                    <div class="clear" style="margin-bottom:10px;"></div>';
        
        $html .= $mytbls;
        
        if(!empty($orderproducts)){
            $n = 1;
            foreach($orderproducts as $key=>$model){

                $html .= '<tr>
                            <td>' . $n . '</td>
                            <td valign="middle" class="text-left" width="70%">' . nl2br($model->description) . '</td>       
                            <td valign="middle" style="text-align: center;">' . $model->quantity . '</td>
                            <td valign="middle" style="text-align: right;">' . Yii::$app->formatter->asDecimal($model->per_unit) . '</td> 
                            <td valign="middle" style="text-align: right;">' . Yii::$app->formatter->asDecimal($model->amount) . '</td>     
                          </tr>';
                $n++;
            }
            
        }
        
        $html .= '</tbody></table></div>';
        
            
        
        $footer = '
        
        <table cellpadding="0" cellspacing="0" width="100%" style="margin-top:20px;">
  <tr>
    <td rowspan="3" style="font-size: 10px;" width="75%" valign="top">
    '.$invoiceitem->terms_conditions.'
    
    </td>
    <td style="font-weight: bold; font-size: 11px;" width="12%">SUB TOTAL</td>
    <td style="font-weight: bold; font-size: 11px; padding-right: 10px;" width="5%">:RM</td>
    <td style="text-align: right; font-weight: bold; font-size: 11px;" width="8%">'.Yii::$app->formatter->asDecimal($invoiceitem->sub_total).'</td>
  </tr>
  <tr>
    <td style="font-weight: bold; font-size: 11px;">SST @ 0%</td>
    <td style="font-weight: bold; font-size: 11px; margin-right: 10px;">:RM</td>
    <td style="text-align: right; font-weight: bold; font-size: 11px;">'.Yii::$app->formatter->asDecimal($invoiceitem->adjustment).'</td>
  </tr>
  <tr>
    <td style="font-weight: bold; font-size: 11px;">NET AMOUNT</td>
    <td style="font-weight: bold; font-size: 11px; margin-right: 10px;">:RM</td>
    <td style="text-align: right; font-weight: bold; font-size: 11px;">'.Yii::$app->formatter->asDecimal($invoiceitem->total).'</td>
  </tr>
</table>';
        $footer .= '<p style="text-align: center; font-size: 9px;"><b>**This invoice is computer generated and no signature is required**</b><br>Thank you for your prompt payment</p>';
        
       $footer .= "<table name='footer' width=\"1000\">
           <tr>
             <td style='font-size: 18px; padding-bottom: 20px;' align=\"right\">Page {PAGENO} of {nb}</td>
           </tr>
         </table>"; 
       
       $allfooter = "<table name='footer' width=\"1000\">
           <tr>
             <td style='font-size: 18px; padding-bottom: 20px;' align=\"right\">Page {PAGENO} of {nb}</td>
           </tr>
         </table>"; 
        $pdf = new Pdf(); // or new Pdf();
                $mpdf = $pdf->api;
                $stylesheet = file_get_contents(Yii::$app->request->hostInfo.'/themes/adminLTE/custom/css/print.css'); // external css
                //$mpdf->SetHTMLHeader('<img src="http://dev.agnichakra.com/images/club_logo.png"/>');
                $mpdf->SetFooter($allfooter);
                //$mpdf->SetHTMLHeader('<div style="width: 100%; text-align:right;"><img src="'.Yii::$app->request->hostInfo.'/images/pdf_header_dulux.jpg"/></div>');
                $mpdf->SetTitle($invoiceitem->invoice_no);
                $mpdf->AddPage('', // L - landscape, P - portrait 
                        '', '', '', '', 15, // margin_left
                        15, // margin right
                        15, // margin top
                        25, // margin bottom
                        15, // margin header
                        0); // margin footer
                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->WriteHtml($html);
                $mpdf->SetFooter($footer);
                $mpdf->Output($invoiceitem->invoice_no.'.pdf', 'I');
                //$mpdf->Output($membershipno.'.pdf', 'D');
                exit;
    }
    
    public function actionDownloadFlashDeal(){
        
        $products = \common\models\VIPOrder::find()
                ->where(['>','vip_order.flash_deals_id',0])
                ->andwhere(['in', 'order_status_id', [10,20]])
                ->asArray()->all();
        $productUnit = [];
        foreach ($products as $product) {
            $productUnit[] = $product['order_id'];
        }

        
        $flashdealproducts = \common\models\VIPOrderProduct::find()
                ->select(['*'])
                ->where(['in','vip_order_product.order_id',$productUnit])
                //->andWhere(['between','redemption_status_date',$start_time, $end_time ])
                //->andwhere(['in', 'account_no_verification', $status])            
                //->andWhere(['sign_off' => 'O'])
                //->andWhere(['>=','vms_visits.updated_datetime',date($date_from)])
                //->andWhere(['<=','vms_visits.updated_datetime',date($date_to)])
                //->orderBy(['vms_visits.updated_datetime' => SORT_DESC])

                //->asArray()
                ->all();
        $dataProducts = array();
        if(!empty($flashdealproducts)){
            $n = 1;
            foreach($flashdealproducts as $key=>$product){
                $totalpts = $product['point_total'];
                $shipingpoint = $product['quantity'] * $product['shipping_point'];
                $totalpro = $totalpts + $shipingpoint;
                
                
                /*$product_temp = array(
                    'Order ID' => $product['order']['invoice_prefix'],
                    'User Code' => $product['order']['customer']['clients_ref_no'],
                    'Company Name' => $product['order']['shipping_company'],
                    'Shipping Add' => $product['order']['shipping_address_1'].' '.$product['order']['shipping_address_2'].','.$product['order']['shipping_city'].','.$product['order']['shipping_postcode'].','.$product['order']['shipping_zone'].'('.$product['order']['shipping_zone_id'].')',                   
                    'Product Code' => $product['model'],
                    'Product Name' => $product['name'],
                    'Qty' => $product['quantity'],
                    'Total Pts' => $totalpro
                    
                );*/
                $product_temp = array(
                    $n,
                    $product['order']['invoice_prefix'],
                    date('d-m-Y', strtotime($product['order']['created_datetime'])),
                    $product['order']['shipping_firstname'].' '.$product['order']['shipping_lastname'],
                    $product['order']['customer']['clients_ref_no'],
                    $product['order']['shipping_company'],
                    $product['order']['shipping_address_1'].' '.$product['order']['shipping_address_2'].','.$product['order']['shipping_city'].','.$product['order']['shipping_postcode'].','.$product['order']['shipping_zone'].'('.$product['order']['shipping_zone_id'].')',                   
                    $product['model'],
                    $product['name'],
                    $product['quantity'],
                    $totalpro,
                    $product['order']['flash_deals_rm']
                    
                );
                $dataProducts[] = $product_temp;
                $n++;
            }
        }

        $data = $dataProducts;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $sheet->setCellValue("A1", "No");
        $sheet->setCellValue("B1", "Order No");
        $sheet->setCellValue("C1", "Order Date");
        $sheet->setCellValue("D1", "Name");
        $sheet->setCellValue("E1", "Code");
        $sheet->setCellValue("F1", "Company Name");
        $sheet->setCellValue("G1", "Shipping Add");
        $sheet->setCellValue("H1", "Model");
        $sheet->setCellValue("I1", "Product");
        $sheet->setCellValue("J1", "Qty");
        //$sheet->setCellValue("J1", "Unit Points");
        $sheet->setCellValue("K1", "Total pts");
        $sheet->setCellValue("L1", "Cash Top Up RM");

        $row = 2;
        $startRow = -1;
        $previousKey = '';
        foreach($data as $index => $value){
            if($startRow == -1){
                $startRow = $row;
                $previousKey = $value[1];
            }
            $sheet->setCellValue('A' . $row, $value[0]);
            $sheet->setCellValue('B' . $row, $value[1]);
            $sheet->setCellValue('C' . $row, $value[2]);
            $sheet->setCellValue('D' . $row, $value[3]);
            $sheet->setCellValue('E' . $row, $value[4]);
            $sheet->setCellValue('F' . $row, $value[5]);
            $sheet->setCellValue('G' . $row, $value[6]);
            $sheet->setCellValue('H' . $row, $value[7]);
            $sheet->setCellValue('I' . $row, $value[8]);
            $sheet->setCellValue('J' . $row, $value[9]);
            $sheet->setCellValue('K' . $row, $value[10]);
            $sheet->setCellValue('L' . $row, $value[11]);
            $nextKey = isset($data[$index+1]) ? $data[$index+1][1] : null;

            if($row >= $startRow && (($previousKey <> $nextKey) || ($nextKey == null))){
                $cellToMerge = 'L'.$startRow.':L'. ''.$row;
                $spreadsheet->getActiveSheet()->mergeCells($cellToMerge);
                $startRow = -1;
            }

            $row++;
        }
        $filename = 'Flash_Deal_Report_'.date('Ymd-His');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type

        header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache 
        ob_end_clean();
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit();
//$baseRoot = Yii::getAlias('@webroot') . "/upload/temp/";
//$writer = new Xlsx($spreadsheet);
//$writer->save($baseRoot.'test-xls-merge.xlsx');
        
    }
    
    public function actionDownload($file_name, $file_type = 'excel', $deleteAfterDownload = false) {
        if (empty($file_name)) {
            return 0;
        }
        $baseRoot = Yii::getAlias('@webroot') . "/upload/";
        $file_name = $baseRoot . $file_name;
        if (!file_exists($file_name)) {
            return 0;
        }
        $fp = fopen($file_name, "r");
        $file_size = filesize($file_name);

        if ($file_type == 'excel') {
            ob_end_clean();
            header('Pragma: public');
            header('Expires: 0');
            header('Content-Encoding: none');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: public');
            header('Content-Type: application/vnd.ms-excel');
            //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Description: File Transfer');
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            header('Content-Transfer-Encoding: binary');
            Header("Content-Length:" . $file_size);
            
            /*header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;filename=". basename($file_name));
            header("Cache-Control: max-age=0");*/
        } else if ($file_type == 'picture') { //pictures
            Header("Content-Type:image/jpeg");
            Header("Accept-Ranges: bytes");
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            Header("Content-Length:" . $file_size);
        } else { //other files
            Header("Content-type: application/octet-stream");
            Header("Accept-Ranges: bytes");
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            Header("Content-Length:" . $file_size);
        }
 
        $buffer = 1024;
        $file_count = 0;

        while (!feof($fp) && $file_count < $file_size) {
            $file_con = fread($fp, $buffer);
            $file_count+=$buffer;
            echo $file_con;
        }
        //echo fread($fp, $file_size);
        fclose($fp);
        if ($deleteAfterDownload) {
            unlink($file_name);
        }
        return 1;
    }
    
    public function actionTrackingSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new TrackingForm();
        
        $date_from = Yii::$app->getRequest()->getQueryParam('date_from');
        $date_to = Yii::$app->getRequest()->getQueryParam('date_to');

        if(!empty($date_from) && !empty($date_to)){
            $datawhererange = ['between', 'vip_order.created_datetime', $date_from, $date_to ];
            $modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));
        }else {
            $datawhererange = '';
        }
        
        
        
        $order_no = Yii::$app->getRequest()->getQueryParam('order_no');
        $company_name = Yii::$app->getRequest()->getQueryParam('company_name');
        
        if(!empty($order_no) && !empty($company_name)){
           $rname = '&order_no='.$order_no;
           $gindirect = '&company_name='.$company_name;
           $datawhere = ['vip_order.invoice_prefix' => $order_no, 'vip_order.shipping_company' => $company_name];
        }elseif(!empty($order_no)){
           $rname = '&order_no='.$order_no;
           $datawhere = ['vip_order.invoice_prefix' => $order_no];
        }elseif(!empty($company_name)){
           $gindirect = '&company_name='.$company_name;
           $datawhere = ['vip_order.shipping_company' => $company_name];
        }else {
            $rname = '';
            $datawhere = '';
        }
        
        //$modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));
        $modelForm->order_no = $order_no;
        $modelForm->company_name = $company_name;

        if($modelForm->load(Yii::$app->request->post())){
            if(!empty($modelForm->date_range)) {
                $date_range = str_replace(',', '', $modelForm->date_range);
                $daterange = explode( 'to', $date_range);
                $date_from_arr = explode(' ', ltrim($daterange[0]));
                $date_to_arr = explode(' ', ltrim($daterange[1]));

                if(!empty($date_from_arr)){
                    $dateParse = date_parse($date_from_arr[1]);
                    $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                    $date_from = new DateTime();
                    $date_from->setDate((int)$date_from_arr[2], $monthNum, (int)$date_from_arr[0]);
                }

                if(!empty($date_to_arr)){
                    $dateParse = date_parse($date_to_arr[1]);
                    $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                    $date_to = new DateTime();
                    $date_to->setDate((int)$date_to_arr[2], $monthNum, (int)$date_to_arr[0]);
                }
                $daterange = 'date_from='.$date_from->format('Y-m-d').' 00:00:00'.'&date_to='.$date_to->format('Y-m-d').' 23:59:59';
                
            }else {
                $daterange = '';
            }
            
            if(!empty($modelForm->order_no)) {
                $order_no = '&order_no='.$modelForm->order_no;
            }else {
                $order_no = '';
            }
            
            if(!empty($modelForm->company_name)) {
                $company_name = '&company_name='.$modelForm->company_name;
            }else {
                $company_name = '';
            }

            $filter_string = '/reports/report/tracking-summary?'.$daterange.$order_no.$company_name;
            return $this->redirect([$filter_string]);          
        }
        
        $data = \common\models\VIPOrderProduct::find()->joinWith([
                'order',
                'order.customer' => function($que) {
                    $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
                },
                'order.company' => function($que) {
                    $que->select(['user_id','company_name']);
                },
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere($datawhererange) 
            ->andwhere($datawhere)
            //->andwhere(['IN', 'user.type', $type])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC]);

            $dataProvider = new ActiveDataProvider([
                'query' => $data,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('tracking_summary', [
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionDownloadExpiry(){
        $session = Yii::$app->session;
        $created_datetime = date('Y-m-d H:i:s');
        $type = array(1, 2);
        $status = array("P", "A", "X", "D", "N");

        $result = \common\models\VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                'user' => function($que) {
                    $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                },
                        'companyInfo' => function($que) {
                    $que->select(['user_id', 'company_name']);
                },
            ])
            ->where(['clientID' => $session['currentclientID']])
            ->andwhere(['IN', 'user.type', $type])
            ->andwhere(['IN', 'user.status', $status])            
            //->andWhere(['not', ['mobile_no' => null]])            
            //->andwhere(['IN', 'user.status', 'A'])
            //->andwhere(['!=', 'company_information.company_name', 'kansai_reserve'])
            ->orderBy(['user.created_datetime' => SORT_DESC])->all();
                
        $dataProducts = array(); $product_temp = array();
        foreach ($result as $model) {
            $balance = $model['totalAdd'] - $model['totalMinus'] - $model['expired'] + $model['adjustment'];
            $goingtoexpire = $model['goingtoExpire'] - $model['currentYearRedeemed'];

            if ($goingtoexpire > 0) {
                $goingtoexpire = $goingtoexpire;
            }else {
                $goingtoexpire = 0;
            }
                if (!empty($model['full_name'])) {
                    $name = $model['companyInfo']['company_name'];
                } else {
                    $name = $model['full_name'];
                }

                if (!empty($model['companyInfo']['company_name'])) {
                    $company_name = $model['companyInfo']['company_name'];
                } else {
                    $company_name = 'NA';
                }

                $product_temp = array(
                            'Code' => $model['clients_ref_no'],
                            'name' => $name,
                            'Email' => $model['user']['email'],
                            'Mobile' => $model['mobile_no'],
                            'Company Name' => $company_name,
                            'Going to Expire' => $goingtoexpire,
                            //'expired_date' => '31-12-' . date('y'),
                            //'today_date' => date('d-m-y'),
                            'Balance Point' => $balance
                );
                
                $dataProducts[] = $product_temp;
            //}
        }

        $excel_data = ExportExcelUtil::excelDataFormat($dataProducts); 
        $excel_title = $excel_data['excel_title'];
        $excel_ceils = $excel_data['excel_ceils'];
        $excel_content = array(
            //sheet 1
            array(
                'sheet_name' => '31-12-20',
                'sheet_title' => $excel_title,
                'ceils' => $excel_ceils,
                //'freezePane' => 'B2',
                'headerColor' => ExportExcelUtil::getCssClass("header"),
                //'headerColumnCssClass' => array(
                    //'id' => ExportExcelUtil::getCssClass('blue'),
                    //'Status_Description' => ExportExcelUtil::getCssClass('grey'),
                //), //define each column's cssClass for header line only.  You can set as blank.
                //'oddCssClass' => ExportExcelUtil::getCssClass("odd"),
                //'evenCssClass' => ExportExcelUtil::getCssClass("even"),
                //'hightLight' => ExportExcelUtil::getCssClass("highlight"),
            )
            //sheet 2,....            
        );
        $excel_file = "Going to Expiry 31-12-20";
        $this->export2excel($excel_content,$excel_file);
    }
    
    public function actionDownloadExpiryFinal(){
        $session = Yii::$app->session;
        $created_datetime = date('Y-m-d H:i:s');
        $type = array(1, 2);
        $status = array("P", "A", "X", "D", "N");

        $result = \common\models\VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                'user' => function($que) {
                    $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                },
                        'companyInfo' => function($que) {
                    $que->select(['user_id', 'company_name']);
                },
            ])
            ->where(['clientID' => $session['currentclientID']])
            ->andwhere(['IN', 'user.type', $type])
            ->andwhere(['IN', 'user.status', $status])            
            ->orderBy(['user.created_datetime' => SORT_DESC])->all();
                
        $dataProducts = array(); $product_temp = array(); $mydata = array();
        foreach ($result as $model) {
            $balance = $model['totalAdd'] - $model['totalMinus'] - $model['expired'] + $model['adjustment'];
            $goingtoexpire = $model['goingtoExpire'] - $model['currentYearRedeemed'];

            if ($goingtoexpire > 0) {
                $goingtoexpire = '-'.$goingtoexpire;
            }else {
                $goingtoexpire = 0;
            }
            
            $mydata[] = [$session['currentclientID'], $model['user']['id'], 0, 'Points Expired on 31-12-20', 'E', $goingtoexpire, 0, '2020-12-31 23:59:00', '2020-12-31 23:59:00', '2020-12-31 23:59:00', 1, 1, 'Y6'];
        }

        Yii::$app->db
            ->createCommand()
            ->batchInsert('vip_customer_reward', ['clientID', 'customer_id', 'point_upload_summary_id', 'description', 'bb_type', 'points', 'points_void', 'date_added', 'created_datetime', 'updated_datetime', 'created_by', 'updated_by', 'expired_cycle'], $mydata)
            ->execute();
        
        echo 'Done';
    }
    
    public function actionDownloadExpiryIndirect(){
        $session = Yii::$app->session;
        $created_datetime = date('Y-m-d H:i:s');
        $type = array(5);

        $result = \common\models\VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                'user' => function($que) {
                    $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                },
                        'companyInfo' => function($que) {
                    $que->select(['user_id', 'company_name']);
                },
            ])
            ->where(['clientID' => $session['currentclientID']])
            ->andwhere(['IN', 'user.type', $type])
            ->andwhere(['user.status' => 'A'])            
            //->andWhere(['not', ['mobile_no' => null]])            
            //->andwhere(['IN', 'user.status', 'A'])
            //->andwhere(['!=', 'company_information.company_name', 'kansai_reserve'])
            ->orderBy(['user.created_datetime' => SORT_DESC])->all();
                
        $dataProducts = array(); $product_temp = array();
        foreach ($result as $model) {
            $balance = $model['totalAdd'] - $model['totalMinus'] - $model['expired'] + $model['adjustment'];
            $goingtoexpire = $model['goingtoExpire'] - $model['currentYearRedeemed'];

            if ($goingtoexpire > 0) {
                $goingtoexpire = $goingtoexpire;
            }else {
                $goingtoexpire = 0;
            }
                if (!empty($model['full_name'])) {
                    $name = $model['companyInfo']['company_name'];
                } else {
                    $name = $model['full_name'];
                }

                if (!empty($model['companyInfo']['company_name'])) {
                    $company_name = $model['companyInfo']['company_name'];
                } else {
                    $company_name = 'NA';
                }

                $product_temp = array(
                            'Code' => $model['clients_ref_no'],
                            'name' => $name,
                            'Email' => $model['user']['email'],
                            'Mobile' => $model['mobile_no'],
                            'Company Name' => $company_name,
                            'Going to Expire' => $goingtoexpire,
                            //'expired_date' => '31-12-' . date('y'),
                            //'today_date' => date('d-m-y'),
                            'Balance Point' => $balance
                );
                
                $dataProducts[] = $product_temp;
        }

        $excel_data = ExportExcelUtil::excelDataFormat($dataProducts); 
        $excel_title = $excel_data['excel_title'];
        $excel_ceils = $excel_data['excel_ceils'];
        $excel_content = array(
            //sheet 1
            array(
                'sheet_name' => '31-12-20',
                'sheet_title' => $excel_title,
                'ceils' => $excel_ceils,
                //'freezePane' => 'B2',
                'headerColor' => ExportExcelUtil::getCssClass("header"),
                //'headerColumnCssClass' => array(
                    //'id' => ExportExcelUtil::getCssClass('blue'),
                    //'Status_Description' => ExportExcelUtil::getCssClass('grey'),
                //), //define each column's cssClass for header line only.  You can set as blank.
                //'oddCssClass' => ExportExcelUtil::getCssClass("odd"),
                //'evenCssClass' => ExportExcelUtil::getCssClass("even"),
                //'hightLight' => ExportExcelUtil::getCssClass("highlight"),
            )
            //sheet 2,....            
        );
        $excel_file = "Going to Expiry 31-12-20";
        $this->export2excel($excel_content,$excel_file);
    }
    
    public function actionDownloadExpiryIndirectFinal(){
        $session = Yii::$app->session;
        $created_datetime = date('Y-m-d H:i:s');
        $type = array(5);

        $result = \common\models\VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                'user' => function($que) {
                    $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                },
                        'companyInfo' => function($que) {
                    $que->select(['user_id', 'company_name']);
                },
            ])
            ->where(['clientID' => $session['currentclientID']])
            ->andwhere(['IN', 'user.type', $type])
            ->andwhere(['user.status' => 'A'])            
            //->andWhere(['not', ['mobile_no' => null]])            
            //->andwhere(['IN', 'user.status', 'A'])
            //->andwhere(['!=', 'company_information.company_name', 'kansai_reserve'])
            ->orderBy(['user.created_datetime' => SORT_DESC])->all();
                
        $dataProducts = array(); $product_temp = array(); $mydata = array();
        foreach ($result as $model) {
            $balance = $model['totalAdd'] - $model['totalMinus'] - $model['expired'] + $model['adjustment'];
            $goingtoexpire = $model['goingtoExpire'] - $model['currentYearRedeemed'];

            if ($goingtoexpire > 0) {
                $goingtoexpire = '-'.$goingtoexpire;
            }else {
                $goingtoexpire = 0;
            }
            
            $mydata[] = [$session['currentclientID'], $model['user']['id'], 0, 'Points Expired on 31-12-20', 'E', $goingtoexpire, 0, '2020-12-31 23:59:00', '2020-12-31 23:59:00', '2020-12-31 23:59:00', 1, 1, 'Y6'];
                
        }

        Yii::$app->db
            ->createCommand()
            ->batchInsert('vip_customer_reward', ['clientID', 'customer_id', 'point_upload_summary_id', 'description', 'bb_type', 'points', 'points_void', 'date_added', 'created_datetime', 'updated_datetime', 'created_by', 'updated_by', 'expired_cycle'], $mydata)
            ->execute();
        
        echo 'Done';
    }
}
