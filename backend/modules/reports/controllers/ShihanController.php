<?php

namespace app\modules\reports\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use DateTime;
use kartik\mpdf\Pdf;


use common\models\VIPCustomer;
use common\models\ReportForm;
use common\models\ReportFormCustomer;
use common\models\ReportOrderForm;
use common\models\ReceiptsForm;
use common\models\TrackingForm;
use common\models\VIPReceipt;
use common\models\TypeName;
use common\models\SMSQueue;

use app\components\ExportBehavior;
use app\components\ExportExcelUtil;



class ShihanController extends \yii\web\Controller
{

    public function actionIndex()
    {
        //410 497
        $userID = 416;
        $y1bal = $this->getY1($userID)['y1A'] - $this->getY1($userID)['y1V'];
        $y1ba2 = $this->getY2($userID)['y2A'] + $y1bal - $this->getY2($userID)['y2V'];
        $y1ba3 = $this->getY3($userID)['y3A'] + $y1ba2 - $this->getY3($userID)['y3V'];
        echo '<table>
<thead>
  <tr>
    <th></th>
    <th>Y1</th>
    <th>Y2</th>
    <th>Y3</th>
    <th>Y4</th>
    <th>Y5</th>
    <th></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Total Awarded</td>
    <td>'.$this->getY1($userID)['y1A'].'</td>
    <td>'.$this->getY2($userID)['y2A'].'</td>
    <td>'.$this->getY3($userID)['y3A'].'</td>
    <td>'.$this->getY4($userID)['y4A'].'</td>
    <td>'.$this->getY5($userID)['y5A'].'</td>
    <td></td>
  </tr>
  <tr>
    <td>Balance b/f</td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Total Redeemed</td>
    <td>'.$this->getY1($userID)['y1V'].'</td>
    <td>'.$this->getY2($userID)['y2V'].'</td>
    <td>'.$this->getY3($userID)['y3V'].'</td>
    <td>'.$this->getY4($userID)['y4V'].'</td>
    <td>'.$this->getY5($userID)['y5V'].'</td>
    <td></td>
  </tr>
  <tr>
    <td>Expired Pts</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Balance</td>
    <td>'.$y1bal.'</td>
    <td>'.$y1ba2.'</td>
    <td>'.$y1ba3.'</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</tbody>
</table>';
        /*echo '<p>Y1 Balance = '.$this->getY1($userID).'</p>';
        echo '<p>Y2 Balance = '.$this->getY2($userID).'</p>';
        echo '<p>Y3 Balance = '.$this->getY3($userID).'</p>';
        echo '<p>Y4 Balance = '.$this->getY4($userID).'</p>';
        echo '<p>Y5 Balance = '.$this->getY5($userID).'</p>';
        echo '<hr>';
        echo $this->getGoingtoExpiry4($userID);
        echo '<br>';
        echo $this->getGoingtoExpiry5($userID);*/
    }
    
    public function getY1($userID) {
        $y1_total = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1_total = $y1A + $y1V;
        return ['y1A' => $y1A, 'y1V' => $y1V];

    }
    
    public function getY2($userID) {
        $y2A = 0;
        $y2V = 0;       

        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        

        $y2_total = $y2A + $y2V;
        return ['y2A' => $y2A, 'y2V' => $y2V];

    }
    
    public function getY3($userID) {
        $y3A = 0;
        $y3V = 0;

        $y3A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y3V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y3_total = $y3A + $y3V;
        //return $y3_total;
        return ['y3A' => $y3A, 'y3V' => $y3V];

    }
    
    public function getY4($userID) {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;


        
        $y4A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-12-31 23:59:59'])        
        ->sum('points');
        
        $y4V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-04-04 23:59:59'])        
        ->sum('points');

        /*$adj = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'description' => 'Point Value Adjustment (0.36 to 0.5) - 2017'])       
        ->sum('points');*/
        
        $y4_total = $y4A + $y4V;
        //return $y4_total;
        return ['y4A' => $y4A, 'y4V' => $y4V];

    }
    
    public function getY5($userID) {
        $y5A = 0;
        $y5V = 0;
        
        /*$adj = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'description' => 'Point Value Adjustment (0.36 to 0.5) - 2017'])       
        ->sum('points');*/
        
        //$tot = $y3B - $this->getY2() - $this->getY3() + $adj;
        
        $y5A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2019-01-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
        
        $y5V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2019-04-05 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
        
        $y5_total = $y5A + $y5V;
        //return $y5_total;
        return ['y5A' => $y5A, 'y5V' => $y5V];

    }
    
    public function getGoingtoExpiry4($userID) {
        $total = 0;
        $totaladd = 0;
        $totaladdr = 0;
        
        $totaladda = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $totaladdr = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
       
        

        $totaladd1 = $totaladda + $totaladdr;
        
        $totaladd = $totaladd1;
        
        if(empty($totaladd)){
            $totaladd = 0; 
        }
         
        return $totaladd;

    }
    
    public function getGoingtoExpiry5($userID) {
        $total = 0;
        $totaladd = 0;
        $totaladdr = 0;
        
        $totaladda = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2019-01-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
        
        $totaladdr = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>14, 'customer_id'=>$userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2019-04-05 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
       
        

        $totaladd1 = $totaladda + $totaladdr;
        
        $totaladd = $totaladd1;
        
        if(empty($totaladd)){
            $totaladd = 0; 
        }
         
        return $totaladd;

    }
    
    
}
