<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;

$session = Yii::$app->session;
$this->title = 'Report';
//$this->params['breadcrumbs'][] = $this->title;
//$this->title = $model->full_name;
//$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;

$actyps = common\models\TypeName::find()
            ->where([
                'clientID' => $session['currentclientID'],
                //'reg_form' => '1',                
        ])->all();

$actypslist = ArrayHelper::map($actyps, 'tier_id', 'name');
?>
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php $form = ActiveForm::begin(['id' => 'report-form','action' =>['/reports/report/customer-account']]); ?>
        <div class="box-body">
            <div>
                <?php
                $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;
                ?>
                <?=
            $form->field($model, 'date_range', [
                'addon'=>['prepend'=>['content'=>'<i class="glyphicon glyphicon-calendar"></i>']],
                'options'=>['placeholder' => 'kkkkk','class'=>'drp-container form-group', ]   ,
            ])->widget(DateRangePicker::classname(), [
                'useWithAddon' => true,
                'readonly' => true,
                //'todayHighlight' => true,
                /*'options' => [
                    'class'=>'drp-container form-group',
                    'placeholder' => 'kkkkk'
                ],*/
                
                'pluginOptions' => [ 
                    'showDropdowns'=>true,
                    'convertFormat' => true,
                    'format' => 'DD MMM, YYYY',
                    'locale' => ['format' => 'DD MMM, YYYY', 'applyLabel' => 'Apply',
                        'cancelLabel' => 'Cancel', 'separator' => ' to ',
                    ],
                    'ranges' => [
                        "Today" => ["moment().startOf('day')", "moment()"],
                        "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                        //"Last Week" => ["moment().subtract(1, 'weeks').startOf('isoWeek')", "moment().subtract(1, 'weeks').endOf('isoWeek')"],
                        "Last Week" => ["moment().subtract(1, 'weeks').startOf('week')", "moment().subtract(1, 'weeks').endOf('week')"],
                        "Last Month" => ["moment().subtract(1, 'months').startOf('month')", "moment().subtract(1, 'months').endOf('month')"],
                        "Last 7 Days" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                        "Last 30 Days" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                    ],
                ]
            ]);
            ?>
            </div>
            <?php
            echo $form->field($model, 'type')->widget(Select2::classname(), [
                'data' => $actypslist,
                'language' => 'en',
                'options' => ['placeholder' => 'Select a type ...','multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true,                    
                ],
            ]);
            ?>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton('Search...', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>