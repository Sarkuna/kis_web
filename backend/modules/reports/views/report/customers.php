<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Account Summary';
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => Yii::$app->request->referrer];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <?=
    $this->render('form_Customer', ['modelForm' => $modelForm, 'actypslist' => $actypslist]);
    ?>
</div>


    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-12 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index">
                <?php
                if (! empty($dataProvider)) {      
                 $gridColumns = [
                        ['class' => 'kartik\grid\SerialColumn'],
                        //'clients_ref_no',
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '160'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],
                        [
                            'attribute' => 'company_name1',
                            'label' => 'Company Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->companyInfo->company_name;
                            },
                        ],
                        [
                            'attribute' => 'company_address1',
                            'label' => 'Company Address',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(empty($model->companyInfo->mailing_address)){
                                    return 'N/A';
                                }else {
                                    return $model->companyInfo->mailing_address;
                                }
                            },
                        ],             
                        [
                            'attribute' => 'mobile1',
                            'label' => 'Full Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->full_name;
                            },
                        ],
                        [
                            'attribute' => 'email1',
                            'label' => 'Email',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->email;
                            },
                        ],
                        [
                            'attribute' => 'email1',
                            'label' => 'Email Verified',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                $email_status = $model->user->email_verification;
                                if($email_status == 'Y' || $email_status == 'y') {
                                    $verify = 'Yes';
                                }else {
                                    $verify = 'No';
                                }
                                return $verify;
                            },
                        ],
                        [
                            'attribute' => 'mobile1',
                            'label' => 'Mobile Number',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->mobile_no;
                            },
                        ],            
                        [
                            'attribute' => 'type1',
                            'label' => 'Type',
                            'format' => 'html',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->user->typeName->name;
                            },
                        ],            
                        [
                            'attribute' => 'status1',
                            'label' => 'Status',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->getStatustext();
                            },
                        ],            
                        [
                            'attribute' => 'created_datetime1',
                            'label' => 'Date Added',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return date('d-m-Y', strtotime($model->user->created_datetime));
                            },
                        ]
                                
                    ]           
                
                ?>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                    ],
                    'filename' => Html::encode($this->title).Date('YmdGis'),
                    //'showColumnSelector'=> true,
                    //'fontAwesome' => true,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]);
                            
                            
                echo GridView::widget([
                    'tableOptions' => ['id' => 'pointstbl'],
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    //'pjax'=>true, 
                    'columns' => $gridColumns,
                    
                ]);
                
                }
                ?>
            </div>
        </div>
    </div>



<?php
    $script = <<<EOD
                
    $(function () {
        var chkall = $("input[name='export_columns_toggle']");   
        chkall.click(function () {
            if($(this).prop("checked") == true){
                var table = $("table tr");
                table.find("th, td").css('display', '');
            }
            else if($(this).prop("checked") == false){
                var table = $("table tr");
                table.find("th, td").toggle();
            }
        });    
        var chk = $("#w0-cols-list input:checkbox"); 
        var tbl = $("#pointstbl");
        var tblhead = $("#pointstbl th");

        chk.prop('checked', true);
        chk.click(function () {
            var cbox_val = $(this).data('key');
            $("table tr").find("th:eq("+cbox_val+")").toggle();  
            $("table tr").find("td:eq("+cbox_val+")").toggle();
        });   
    });      

EOD;
$this->registerJs($script);
    ?>

