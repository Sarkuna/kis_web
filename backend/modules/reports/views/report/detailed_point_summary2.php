<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detailed Points Summary';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-12 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index">
                <?php
                       
                 $gridColumns = [
                        ['class' => 'kartik\grid\SerialColumn'],
                        [
                            'attribute' => 'clients_ref_no',
                            'label' => 'Dealer Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],
           
                        
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Ttl Y1 - Y4',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->VIPglobal->PointsExpireAwared($model->userID);
                            },
                        ],
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Red Y1 - Y5',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->VIPglobal->PointsExpireVoide($model->userID);
                            },
                        ],
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Points Aadjust',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->VIPglobal->PointsExpireAadjust($model->userID);
                            },
                        ],            
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Exp Y1 - Y4',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->VIPglobal->PointsExpirePast($model->userID);
                            },
                        ],
                                    
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Total',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->VIPglobal->PointsExpireAwared($model->userID) + Yii::$app->VIPglobal->PointsExpireVoide($model->userID) + Yii::$app->VIPglobal->PointsExpirePast($model->userID) - Yii::$app->VIPglobal->PointsExpireAadjust($model->userID);
                            },
                        ],
                                      
                              
                    ]           
                ?>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    //'showColumnSelector'=> true,
                    //'fontAwesome' => true,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]);
                            
                echo GridView::widget([
                    'tableOptions' => ['id' => 'pointstbl'],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    //'pjax'=>true, 
                    'columns' => $gridColumns,
                    
                ]); 
                ?>
            </div>
        </div>
    </div>



<?php
    $script = <<<EOD
                
    $(function () {
        var chkall = $("input[name='export_columns_toggle']");   
        chkall.click(function () {
            if($(this).prop("checked") == true){
                var table = $("table tr");
                table.find("th, td").css('display', '');
            }
            else if($(this).prop("checked") == false){
                var table = $("table tr");
                table.find("th, td").toggle();
            }
        });    
        var chk = $("#w0-cols-list input:checkbox"); 
        var tbl = $("#pointstbl");
        var tblhead = $("#pointstbl th");

        chk.prop('checked', true);
        chk.click(function () {
            var cbox_val = $(this).data('key');
            $("table tr").find("th:eq("+cbox_val+")").toggle();  
            $("table tr").find("td:eq("+cbox_val+")").toggle();
        });   
    });      

EOD;
$this->registerJs($script);
    ?>

