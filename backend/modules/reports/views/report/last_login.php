<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Date Last Login';
$this->params['breadcrumbs'][] = ['label' => 'Reports'];
//$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-12 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index">
                <?php
   
                 $gridColumns = [
                        ['class' => 'kartik\grid\SerialColumn'],
                        //'clients_ref_no',
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '160'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],
                        [
                            'attribute' => 'company_name1',
                            'label' => 'Company Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                //return $model->user->id;
                                return $model->companyInfo->company_name;
                            },
                        ],
            
                        [
                            'attribute' => 'mobile1',
                            'label' => 'Full Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->full_name;
                            },
                        ],
           
                        [
                            'attribute' => 'type1',
                            'label' => 'Type',
                            'format' => 'html',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                /*if($model->user->type == 5) {
                                    $name = $model->user->typeName->name.'<br>'.$model->user->getMyDistubuter($model->userID);
                                }else{
                                    $name = $model->user->typeName->name;
                                }*/
                                $name = $model->user->typeName->name;
                                return $name;
                            },
                        ],
                        
                        [
                            'attribute' => 'mobile1',
                            'label' => 'Last Login',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return date('d-m-Y H:i:s', strtotime($model->user->date_last_login));
                            },
                        ],
                                   
                        [
                            'attribute' => 'balance',
                            'label' => 'Balance',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                //$tot = $model->getTotalAdd() - $model->getTotalMinus() - $model->getExpired() - $model->getEy3() - $model->getEy4();
				$tot = $model->getTotalAdd() - $model->getTotalMinus() - $model->getExpired() + $model->getAdjustment();

                                return $tot;
                                //return Yii::$app->formatter->asInteger($model->getY5());
                            },
                        ],
                        
                        [
                            'attribute' => 'balance',
                            'label' => 'Going to Expire',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                //$tot = $model->getTotalAdd() - $model->getTotalMinus() - $model->getExpired() - $model->getEy3() - $model->getEy4();
				$tot = $model->getGoingtoExpire() - $model->getCurrentYearRedeemed();
                                if($tot > 0) {
                                    $tot = $tot;
                                }else {
                                    $tot = 0;
                                }

                                return $tot;
                                //return Yii::$app->formatter->asInteger($model->getY5());
                            },
                        ],             
                                    
                                
                    ]           
                
                ?>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                    ],
                    'filename' => Html::encode($this->title).Date('YmdGis'),
                    //'showColumnSelector'=> true,
                    //'fontAwesome' => true,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]);
                            
                            
                echo GridView::widget([
                    'tableOptions' => ['id' => 'pointstbl'],
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    //'pjax'=>true, 
                    'columns' => $gridColumns,
                    
                ]);

                ?>
            </div>
        </div>
    </div>



<?php
    $script = <<<EOD
                
    $(function () {
        var chkall = $("input[name='export_columns_toggle']");   
        chkall.click(function () {
            if($(this).prop("checked") == true){
                var table = $("table tr");
                table.find("th, td").css('display', '');
            }
            else if($(this).prop("checked") == false){
                var table = $("table tr");
                table.find("th, td").toggle();
            }
        });    
        var chk = $("#w0-cols-list input:checkbox"); 
        var tbl = $("#pointstbl");
        var tblhead = $("#pointstbl th");

        chk.prop('checked', true);
        chk.click(function () {
            var cbox_val = $(this).data('key');
            $("table tr").find("th:eq("+cbox_val+")").toggle();  
            $("table tr").find("td:eq("+cbox_val+")").toggle();
        });   
    });      

EOD;
$this->registerJs($script);
    ?>

