<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detail Points Summary Account 2 ('.$code.')';
$this->params['breadcrumbs'][] = ['label' => 'Reports'];
$this->params['breadcrumbs'][] = ['label' => 'Points Summary Account 2', 'url' => 'points-summary-acc2'];
//$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-12 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index">
                <?php
                if (! empty($dataProvider)) {      
                 $gridColumns = [
                        ['class' => 'kartik\grid\SerialColumn'],
                        //'clients_ref_no',
                        [
                            'attribute' => 'mobile1',
                            'label' => 'Date Added',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return date('d-m-Y', strtotime($model->date_added));
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Code',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '160'],
                            'value' => function ($model) {
                                if($model->indirect_id > 0) {
                                    return $model->indirectUser->clients_ref_no;
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],            
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '160'],
                            'value' => function ($model) {
                                if($model->indirect_id > 0) {
                                    return $model->indirectUser->full_name;
                                }else {
                                    return 'N/A';
                                }
                                
                            },
                        ],
                          
                        [
                            'attribute' => 'company_name1',
                            'label' => 'Type',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->bb_type;
                            },
                        ],            
                        [
                            'attribute' => 'company_name1',
                            'label' => 'Description',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->description;
                            },
                        ],
            
                        [
                            'attribute' => 'mobile1',
                            'label' => 'Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->points_in;
                            },
                        ],
              
                    ]           
                
                ?>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                    ],
                    'filename' => $code.' Account 2 Summary of '.Date('d-m-Y'),
                    //'showColumnSelector'=> true,
                    //'fontAwesome' => true,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]);
                            
                            
                echo GridView::widget([
                    'tableOptions' => ['id' => 'pointstbl'],
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    //'pjax'=>true, 
                    'columns' => $gridColumns,
                    
                ]);
                
                }
                ?>
            </div>
        </div>
    </div>



<?php
    $script = <<<EOD
                
    $(function () {
        var chkall = $("input[name='export_columns_toggle']");   
        chkall.click(function () {
            if($(this).prop("checked") == true){
                var table = $("table tr");
                table.find("th, td").css('display', '');
            }
            else if($(this).prop("checked") == false){
                var table = $("table tr");
                table.find("th, td").toggle();
            }
        });    
        var chk = $("#w0-cols-list input:checkbox"); 
        var tbl = $("#pointstbl");
        var tblhead = $("#pointstbl th");

        chk.prop('checked', true);
        chk.click(function () {
            var cbox_val = $(this).data('key');
            $("table tr").find("th:eq("+cbox_val+")").toggle();  
            $("table tr").find("td:eq("+cbox_val+")").toggle();
        });   
    });      

EOD;
$this->registerJs($script);
    ?>

