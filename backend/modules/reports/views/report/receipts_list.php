<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receipts List Summary';
$this->params['breadcrumbs'][] = ['label' => 'Reports'];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <?=
    $this->render('form_Orders', ['modelForm' => $modelForm]);
    ?>
</div>


    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-12 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index">
                <?php
                if (! empty($dataProvider)) {      
                 $gridColumns = [
                        ['class' => 'kartik\grid\SerialColumn'],
                        //'clients_ref_no',
                        [
                            'attribute' => 'company_name1',
                            'label' => 'Company Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(empty($model->customer->companyInfo->company_name)) {
                                    return 'N/A';
                                }else {
                                    return $model->customer->companyInfo->company_name;
                                }
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Code',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->customer->clients_ref_no;
                            },
                        ],
                        [
                            'attribute' => 'mobile_no',
                            'label' => 'Mobile No',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(empty($model->customer->mobile_no)) {
                                    return 'N/A';
                                }else {
                                    return $model->customer->mobile_no;
                                }
                            },
                        ],            
                        [
                            'attribute' => 'order_num',
                            'label' => 'Order Num',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->order_num;
                            },
                        ],
                        [
                            'attribute' => 'date_of_receipt',
                            'format' => 'raw',
                            'headerOptions' => ['width' => '120'],
                            'value' => function ($model) {
                                if(empty($model->date_of_receipt) || $model->date_of_receipt == '1970-01-01'){
                                    return 'N/A';
                                }else {
                                    return Yii::$app->formatter->asDatetime($model->date_of_receipt, "php:d-m-Y");
                                }
                            }
                        ], 
                        [
                            'attribute' => 'invoice_no',
                            'label' => 'Invoice No',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->invoice_no;
                            },
                        ],            
                        [
                            'attribute' => 'company_name',
                            'label' => 'Distributor',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                            //'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                //return $model->company->company_name;
                                if(empty($model->company->company_name)) {
                                    return 'N/A';
                                }else {
                                    return $model->company->company_name;
                                }
                            },
                        ],

                        //'userID',
                        //'clientID',
                        [
                            'attribute' => 'total_items',
                            //'label' => 'Total Transactions',
                            'format' => 'html',
                            'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getTotalItems();
                            },
                        ],
                        [
                            'attribute' => 'order_total_point',
                            'label' => 'Total Point',
                            'format' => 'html',
                            'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->getTotalPoints();
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Status',
                            'format' => 'html',
                            'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                            //'contentOptions' =>['class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->getStatustext();
                            },
                        ],           
                        //'date_of_receipt',
                         [
                            'attribute' => 'created_datetime',
                            'format' => 'raw',
                            'label' => 'Created Date',
                            'headerOptions' => ['width' => '120'],
                            'value' => function ($model) {
                                if(empty($model->created_datetime) || $model->created_datetime == '1970-01-01'){
                                    return 'N/A';
                                }else {
                                    return date('d-m-Y', strtotime($model->created_datetime));
                                    //return Yii::$app->formatter->asDatetime($model->created_datetime, "php:d-m-Y");
                                }
                            }
                        ],           
                                
                    ]           
                
                ?>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                    ],
                    'filename' => Html::encode($this->title).Date('YmdGis'),
                    //'showColumnSelector'=> true,
                    //'fontAwesome' => true,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]);
                            
                            
                echo GridView::widget([
                    'tableOptions' => ['id' => 'pointstbl'],
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    //'pjax'=>true, 
                    'columns' => $gridColumns,
                    
                ]);
                
                }
                ?>
            </div>
        </div>
    </div>



<?php
    $script = <<<EOD
                
    $(function () {
        var chkall = $("input[name='export_columns_toggle']");   
        chkall.click(function () {
            if($(this).prop("checked") == true){
                var table = $("table tr");
                table.find("th, td").css('display', '');
            }
            else if($(this).prop("checked") == false){
                var table = $("table tr");
                table.find("th, td").toggle();
            }
        });    
        var chk = $("#w0-cols-list input:checkbox"); 
        var tbl = $("#pointstbl");
        var tblhead = $("#pointstbl th");

        chk.prop('checked', true);
        chk.click(function () {
            var cbox_val = $(this).data('key');
            $("table tr").find("th:eq("+cbox_val+")").toggle();  
            $("table tr").find("td:eq("+cbox_val+")").toggle();
        });   
    });      

EOD;
$this->registerJs($script);
    ?>

