<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
$session = Yii::$app->session;
$this->title = 'Report';
//$this->params['breadcrumbs'][] = $this->title;
//$this->title = $model->full_name;
//$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;

$actyps = common\models\TypeName::find()
            ->where([
                'clientID' => $session['currentclientID'],
                //'reg_form' => '1',                
        ])->all();

$actypslist = ArrayHelper::map($actyps, 'tier_id', 'name');
?>
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php $form = ActiveForm::begin(['id' => 'report-form']); ?>
        <div class="box-body">

            <?=
            $form->field($model, 'type', [
                'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
            ])->dropDownList(
                $actypslist
            );
            ?>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton('Download', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>