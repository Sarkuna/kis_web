<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tracking Summary';
$this->params['breadcrumbs'][] = ['label' => 'Reports'];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <?=
    $this->render('form_Tracking', ['modelForm' => $modelForm]);
    ?>
</div>


    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-12 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index">
                <?php
                if (! empty($dataProvider)) {      
                 $gridColumns = [
                        ['class' => 'kartik\grid\SerialColumn'],
                        //'clients_ref_no',
                        [
                            'attribute' => 'order_id1',
                            'label' => 'Order ID',
                            'value' => function($model){
                                return $model->order->invoice_prefix;
                            },       
                            //'group' => true,        
                        ],
                        [
                            'header' => 'Invoice #',
                            'value' => function($model){
                                if(!empty($model->invoice_item_id)) {
                                    return $model->invoiceitem->invoice_no;
                                }else {
                                    return 'N/A';
                                }
                            },
                            //'group' => true,        
                        ],
                        [
                            //'attribute' => 'full_name',
                            'label' => 'Name',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->shipping_firstname .' '.$model->order->shipping_firstname;
                            },
                        ],

                        [
                            'attribute' => 'company_name1',
                            'label' => 'Company Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->order->shipping_company;
                            },
                        ],            
                        [
                            //'attribute' => 'company_name',
                            'label' => 'Shipping Details',
                            'format' => 'html',
                            'headerOptions' => ['width' => '380'],
                            'value' => function ($model) {
                                $address = $model->order->shipping_address_1.' '.$model->order->shipping_address_2.' '.$model->order->shipping_city.' '.$model->order->shipping_postcode.' '.$model->order->shipping_zone.' '.$model->order->shipping_zone_id;
                                return $address;
                            },        
                        ],
                        [
                            //'attribute' => 'company_name',
                            'label' => 'Mobile',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                //return $model->order->customer_id;
                                return $model->order->customer->mobile_no;
                            },
                        ],

                        [
                            //'attribute' => 'product',
                            'label' => 'Product',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                //return $model->order->customer_id;
                                return $model->name;
                            },
                        ],
                        [
                            //'attribute' => 'company_name',
                            'label' => 'Qty',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                //return $model->order->customer_id;
                                return $model->quantity;
                            },
                        ],
          
                        [
                            //'attribute' => 'company_name',
                            'label' => 'Shipped Date',
                            'format' => 'raw',
                            'contentOptions' => ['style' => 'width:500px;'],
                            'value' => function ($model) {
                                //return $model->order->customer_id;
                                if(!empty($model->shipped_date)) {
                                   return date('d-m-Y', strtotime($model->shipped_date)); 
                                }else {
                                   return 'N/A'; 
                                }

                            },
                        ],
                        [
                            //'attribute' => 'company_name',
                            'label' => 'Tracking No.',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                //return $model->order->customer_id;
                                return $model->tracking_no;
                            },       
                        ],
                        [
                            //'attribute' => 'company_name',
                            'label' => 'Tracking Link',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                //return $model->order->customer_id;
                                return $model->tracking_link;
                            },
                        ],            
                                
                    ]           
                
                ?>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                    ],
                    'filename' => Html::encode($this->title).Date('YmdGis'),
                    //'showColumnSelector'=> true,
                    //'fontAwesome' => true,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]);
                            
                            
                echo GridView::widget([
                    'tableOptions' => ['id' => 'pointstbl'],
                    'dataProvider' => $dataProvider,
                    'tableOptions'=>['class'=>'table-striped table-bordered table-condensed'],
  'options'=>['style' => 'white-space:nowrap;'],
                    //'filterModel' => $searchModel,
                    //'pjax'=>true, 
                    'columns' => $gridColumns,
                    
                ]);
                
                }
                ?>
            </div>
        </div>
    </div>



<?php
    $script = <<<EOD
                
    $(function () {
        var chkall = $("input[name='export_columns_toggle']");   
        chkall.click(function () {
            if($(this).prop("checked") == true){
                var table = $("table tr");
                table.find("th, td").css('display', '');
            }
            else if($(this).prop("checked") == false){
                var table = $("table tr");
                table.find("th, td").toggle();
            }
        });    
        var chk = $("#w0-cols-list input:checkbox"); 
        var tbl = $("#pointstbl");
        var tblhead = $("#pointstbl th");

        chk.prop('checked', true);
        chk.click(function () {
            var cbox_val = $(this).data('key');
            $("table tr").find("th:eq("+cbox_val+")").toggle();  
            $("table tr").find("td:eq("+cbox_val+")").toggle();
        });   
    });      

EOD;
$this->registerJs($script);
    ?>

