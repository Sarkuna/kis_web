<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAddress */

$this->title = 'Update Vipcustomer Address: ' . $model->vip_customer_address_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipcustomer Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vip_customer_address_id, 'url' => ['view', 'id' => $model->vip_customer_address_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vipcustomer-address-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
