<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerBankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vipcustomer Banks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipcustomer-bank-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Vipcustomer Bank', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'vip_customer_bank_id',
            'userID',
            'clientID',
            'bank_name_id',
            'account_name',
            // 'account_number',
            // 'nric_passport',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
