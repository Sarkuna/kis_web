<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerBank */

$this->title = 'Update Vipcustomer Bank: ' . $model->vip_customer_bank_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipcustomer Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vip_customer_bank_id, 'url' => ['view', 'id' => $model->vip_customer_bank_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vipcustomer-bank-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
