<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DatePicker;
use borales\extensions\phoneInput\PhoneInput;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */
/* @var $form yii\widgets\ActiveForm */
use common\models\Salutation;
use common\models\VipCountry;
use common\models\VIPZone;

$session = Yii::$app->session;
$clientID = $session['currentclientID'];
        
$countrylists = VipCountry::find()->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');


$regionlists = VIPZone::find()->orderBy([
    'name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'zone_id', 'name');

$typename = \common\models\TypeName::find()
           ->select('tier_id, name')
           ->where(['clientID' => $clientID])
           ->asArray()->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');

$raceslists = common\models\Races::find()->orderBy([
    'sort_order' => SORT_ASC,
])->all();
$races = ArrayHelper::map($raceslists, 'races_id', 'name');
?>


<div class="vipcustomer-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box-success box view-item col-xs-12 col-lg-12">
        <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Personal Information'); ?></h4>
                </div>
            
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-lg-7 no-padding">
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model, 'clients_ref_no', [
                        //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                      ])->textInput(array('placeholder' => ''));  ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?=
                            $form->field($model, 'type', [
                                    //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                            ])->dropDownList($typenames, ['prompt' => '-- Select --', 'disabled' => $model->isNewRecord ? false : true]);
                            ?>
                        </div>
                    </div>
                    
                <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                    <div class="col-xs-12 col-sm-3 col-lg-3">
                        <?php
                        $salutations = Salutation::find()->all();
                        $salutationsData = ArrayHelper::map($salutations, 'salutation_id', 'name');
                        //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
                        echo $form->field($model, 'salutation_id', [
                                    //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                                ])
                                ->dropDownList(
                                        $salutationsData, ['prompt' => '--', 'id' => 'salutation']
                        );
                        ?>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-lg-9">
                        <?= $form->field($model, 'full_name', [
                            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                          ])->textInput(array('placeholder' => ''));  ?>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-lg-7">
                        <?= $form->field($model, 'email', [
                        //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                      ])->textInput(array('placeholder' => ''));  ?>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-lg-5">
                        <?=
                        $form->field($model, 'gender', [
                            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->radioList(['M' => 'Male', 'F' => 'Female'], ['itemOptions' => ['class' => 'flat', 'id' => 'client_SMS']]);
                        ?>
                        
                    </div>                    
                </div>
                    
                <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                    <div class="col-xs-12 col-sm-6 col-lg-6">                        
                        <?=
                            $form->field($model, 'mobile_no', [
                                //'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                            ])->widget(PhoneInput::className(), [
                                    'jsOptions' => [
                                        'allowExtensions' => false,
                                        'onlyCountries' => ['my'],

                                    ],
                                    'options' => ['placeholder' => '+60XXXXXXXXX', 'class' => 'form-control'],
                                ]);
                        ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model, 'telephone_no', [
                                    //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                                ])->textInput(array('placeholder' => ''));  
                            ?>
                    </div>
                </div>    
                
                <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                    
                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model, 'date_of_Birth', [
                        //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                      ])->widget(DatePicker::classname(), [
                          'options' => ['readOnly' => true, 'placeholder' => 'From date ...'],
                          'removeButton' => false,
                          'pluginOptions' => [
                              'autoclose' => true,
                              'todayHighlight' => false,
                              'todayBtn' => false,
                              'format' => 'yyyy-mm-dd',
                          //'startDate' => $startDate,
                          ]]) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model, 'nric_passport_no', [
                            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                          ])->textInput(array('placeholder' => ''));  ?>
                    </div>
                </div>

                </div>
                
                <div class="col-xs-12 col-sm-12 col-lg-5 no-padding">
                    <div class="col-xs-12 col-sm-12 col-lg-12">
                            <?= $form->field($model, 'address_1', [
                                //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                              ])->textInput(array('placeholder' => ''));  ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12">
                            <?= $form->field($model, 'address_2', [
                                //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                              ])->textInput(array('placeholder' => ''));  ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($model, 'city', [
                                //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                              ])->textInput(array('placeholder' => ''));  ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($model, 'postcode', [
                                //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                              ])->textInput(array('placeholder' => ''));  ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($model, 'Race', [
                                //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                              ])->widget(Select2::classname(), [
                                              'data' => $races,
                                              //'language' => 'de',
                                              'options' => [
                                                  'placeholder' => 'Select ...',
                                              ],
                                              'pluginOptions' => [
                                                  'allowClear' => true
                                              ],               

                                          ]);  ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($model, 'nationality_id', [
                                //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                              ])->widget(Select2::classname(), [
                                              'data' => $country,
                                              //'language' => 'de',
                                              'options' => [
                                                  'placeholder' => 'Select ...',
                                              ],
                                              'pluginOptions' => [
                                                  'allowClear' => true
                                              ],               

                                          ]);  ?>
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($model, 'country_id', [
                      //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->widget(Select2::classname(), [
                                    'data' => $country,
                                    //'language' => 'de',
                                    'options' => [
                                        'placeholder' => 'Select ...',
                                        'onchange' => '$.get( "' . Url::toRoute('/ajax/states') . '", {id: $(this).val() } )
                                            .done(function( data ) {
                                               $("#vipcustomer-region").html(data);
                                            }
                                        );'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],               

                                ]);  ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($model, 'Region', [
  //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->widget(Select2::classname(), [
                    'data' => $regionlist,
                    //'language' => 'de',
                    'options' => [
                        'placeholder' => '-- Select --',
                     ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);  ?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                            <div class="col-xs-12 col-sm-6 col-lg-6">
                                <?=
                        $form->field($model, 'newsletter', [
                            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->dropDownList(['E' => 'Enabled', 'D' => 'Disabled']);
                        ?>
                            </div>    
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            
                        </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        <div class="box-footer">
            <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
