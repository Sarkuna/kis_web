<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */
$session = Yii::$app->session;
$clientID = $session['currentclientID'];

$total = 0;
$totalpoint = \common\models\VIPCustomerAccount2::find()
->where(['>', 'points_in', 0])
->andWhere(['clientID'=>$clientID, 'customer_id'=>$model->userID])
->sum('points_in');

$redeemed_point = 0;
$redeemed_point = \common\models\VIPCustomerAccount2::find()
->where(['<', 'points_in', 0])
->andWhere(['clientID'=>$clientID, 'customer_id'=>$model->userID])
->sum('points_in');

$account2lists = \common\models\VIPCustomerAccount2::find()
           ->select('account2_id, description, points_in, date_added')
           ->where(['clientID'=>$clientID, 'customer_id' => $model->userID])
           ->andWhere(['BETWEEN', 'created_datetime',
                new \yii\db\Expression('(NOW() - INTERVAL 1 MONTH)'),
                new \yii\db\Expression('NOW()')
            ])
           ->orderBy(['created_datetime'=>SORT_DESC])
           //->limit(20)
           ->asArray()
           ->all();
$account2countall = \common\models\VIPCustomerAccount2::find()
           ->where(['clientID'=>$clientID, 'customer_id' => $model->userID])
           ->count();

$redeemed_point = str_replace('-', '', $redeemed_point);
?>
<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Account 2'); ?> - Last 30 days
	<div class="pull-right">
            <?php if($model->user->status != 'X') { ?>
                <?= Html::a('Add Reward Points Acc2', ['add-reward-point2', 'id' => $model->vip_customer_id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?php } ?>
            <?php if($account2countall > 0) { ?>
		<?= Html::a('<i class="fa fa-pencil-square-o"></i> '.Yii::t('app', 'All Records'), ['/reports/report/points-summary-acc2-detail', 'code' => $model->clients_ref_no], ['class' => 'btn btn-primary btn-sm']) ?>
            <?php } ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>


<div class="box-body">
    <table class="table table-bordered" id="myTable">
                <tbody id= "data_table">
                    <tr>
                        <th width="5%">#</th>
                        <th width="15%">Added Date</th>
                        <th width="65%">Description</th>
                        <th width="10%">Points</th>
                    </tr>               
                    <?php
                    $n = 1;
                    $book_id = '';
                    
                    if(count($account2lists) > 0) {
                        foreach($account2lists as $account2list){
                            $book_id = $account2list['account2_id'];
                            echo '<tr>
                                <td>'.$n.'</td>
                                <td>'.date('d-m-y', strtotime($account2list['date_added'])).'</td>                            
                                <td>'.$account2list['description'].'</td>
                                <td class="text-right">'.$account2list['points_in'].'</td>   
                                </tr>';
                            $n++;
                        }
                    }else {
                        echo '<tr><td class="text-center" colspan="4">No record found!</td></tr>';
                    }
                    ?>
            
            </tbody>
            </table>
</div>

<?php
    $tst = Url::to(['account-two-data']);
    $userID = $model->userID;
    $script = <<<EOD
            $(document).ready(function(){
	$(document).on('click', '#load', function(){
		var last_row = $(this).data('id');
                var rowCount = $('#myTable >tbody >tr').length - 2;
		$.ajax({
			url: '$tst',
			type: 'POST',
			data: {'last_row': last_row, 'userid': $userID, 'rowCount': rowCount},
                        cache: false,
			success: function(data){
				if(data != ""){
					$('#remove_row').html('<td class="text-center" colspan="4">Loading...</td>');
					setTimeout(function(){
						$('#remove_row').remove();
						$('#data_table').append(data);
					}, 3000);
				}else{
					$('#load').html('No Data');
				}
			}
		});
		
	});
});
EOD;
$this->registerJs($script);
    ?>   