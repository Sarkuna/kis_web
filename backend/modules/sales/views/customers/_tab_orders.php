<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\VIPOrderStatus;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */

?>

<?= GridView::widget([
        'dataProvider' => $dataProviderOrder,
        //'filterModel' => $searchOrder,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'order_id',
            //'invoice_no',
            [
                'attribute' => 'created_datetime',
                'label' => 'Order Date',
                'format' => 'html',
                'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->created_datetime));
                },
            ],
            [
                'attribute' => 'invoice_prefix',
                'label' => 'Order ID',
                'format' => 'html',
                'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->invoice_prefix;
                },
            ],
            [
                'attribute' => 'No_of_Items',
                'label' => 'No of Items',
                'format' => 'html',
                'headerOptions' => ['width' => '120'],
                'contentOptions' =>['class' => 'text-center'],
                'value' => function ($model) {
                    return $model->getTotalTransactions();
                },
            ],
            [
                'attribute' => 'total_point',
                'label' => 'Total Point',
                'format' => 'html',
                'headerOptions' => ['width' => '130'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getTotalPoints();
                },
            ],            
            [
                'attribute' => 'order_status_id',
                'label' => 'Status',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    $actionlabel = '<span class="label label-'.$model->orderStatus->labelbg.'">'.$model->orderStatus->name.'</span>';
                    return $actionlabel;
                },
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],
                'filter' => ArrayHelper::map(VIPOrderStatus ::find()->all(), 'order_status_id', 'name'),        
                //'filter' => ["P" => "Pending", "C" => "Comply ", "N" => "Unfit for review"],        
            ],
            [
                'attribute' => 'updated_datetime',
                'label' => 'Date Modified',
                'format' => 'html',
                'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->updated_datetime));
                },
            ],             
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', //{view} {delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        $url = '/sales/orders/view?id='.$model->order_id;
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    }
                ],
            ],             
        ],
    ]); ?>