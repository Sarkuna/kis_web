<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Profile Information'); ?>
	<div class="pull-right">
	<?php if($model->user->status != 'X') { ?>
		<?= Html::a('<i class="fa fa-pencil-square-o"></i> '.Yii::t('app', 'Edit'), ['update', 'id' => $model->vip_customer_id, 'tab' => 'personal'], ['class' => 'btn btn-primary btn-sm', 'id' => 'update-data']) ?>
	<?php } ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('full_name') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?=  $model->full_name ? $model->salutations->name.'. '.$model->full_name : Yii::t("app", "N/A") ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label"><?= $model->user->getAttributeLabel('email') ?></div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?=  $model->user->email ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('date_of_Birth') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text">
                <?php
                //jS F Y
                //echo 'uuiou'.$model->date_of_Birth;
                if(!empty($model->date_of_Birth)){
                    echo date('d-m-Y', strtotime($model->date_of_Birth));
                }else {
                    echo 'N/A';
                }
                ?>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('gender') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text">
                <?php
                    if($model->gender == 'M') {
                        echo 'Male';
                    }else if($model->gender == 'F') {
                        echo 'Female';
                    }else {
                        echo 'N/A';
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('telephone_no') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->telephone_no ? $model->telephone_no : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('mobile_no') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->mobile_no ? $model->mobile_no : Yii::t("app", "N/A") ?></div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('nric_passport_no') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->nric_passport_no ? $model->nric_passport_no : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('Race') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->Race ? $model->races->name : Yii::t("app", "N/A") ?></div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label">Address</div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= ($model->address_1) ? $model->address_1 : Yii::t("app", "N/A") ?> <?= ($model->address_1) ? $model->address_1 : Yii::t("app", "") ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('city') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->city ? $model->city : 'N/A' ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('postcode') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->postcode ? $model->postcode : 'N/A' ?></div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('nationality') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->nationality ? $model->nationality->name : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('country_id') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->country_id ? $model->country->name : Yii::t("app", "N/A") ?></div>
        </div>
    </div>
    
</div>

<?php if(!empty($model->assign_dealer_id)) { ?>
<?= $this->render('_tab_dealer_info', ['model' => $model]) ?>
<?php } ?>
