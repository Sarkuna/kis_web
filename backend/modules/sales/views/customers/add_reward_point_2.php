<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */

$this->title = 'Reward Points Account 2';
$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="vipcustomer-create">
            <?=
            $this->render('_form_add_reward_point_2', [
                'model' => $model,
            ])
            ?>

        </div>
    </div>

