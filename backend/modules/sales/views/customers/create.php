<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */

$this->title = 'New Customer';
$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="vipcustomer-create">
            <?=
            $this->render('_form_new', [
                'model' => $model,
                'companyInformation' => $companyInformation,
                'clientID' => $clientID,
                
            ])
            ?>

        </div>

