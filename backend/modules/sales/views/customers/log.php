<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\components\Customers_Status_Widget;

$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Login Activity';
$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => ['approve']];
$this->params['breadcrumbs'][] = $this->title;

?>



    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="col-lg-2 text-right">

        
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index table-responsive">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Date</th>
                            <!--<th>Browser</th>-->
                            <th>IP</th>                            
                        </tr>
                        <?php
                        if(count($totallog) > 0) {
                            $n = 1;
                            foreach ($totallog as $log) {
                                echo '<tr>';
                                echo '<td>'.$n++.'</td>';
                                echo '<td>'.date('d-m-Y H:i:s', strtotime($log->time)).'</td>';
                                //echo '<td>'.$log->user_remote.'</td>';
                                echo '<td>'.$log->user_remote.'</td>';
                                echo '</tr>';
                            }
                        }else {
                            echo '<tr><td colspan="4">No Record</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>


