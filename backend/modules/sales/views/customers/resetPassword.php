<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
//$this->params['breadcrumbs'][] = $this->title;
//$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
        <div class="box-body">
            <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" readonly="" class="form-control" id="exampleInputEmail1" value="<?= $model->email ?>">
                </div>
            <?= $form->field($password, 'password')->passwordInput(['autofocus' => true]) ?>
            <?= $form->field($password, 'confirm_password')->passwordInput(['autofocus' => true]) ?>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>