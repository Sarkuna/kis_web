<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$totaladd = \common\models\VIPCustomerReward::find()
        ->where(['>', 'points', 0])
        ->andWhere(['clientID'=>$model->clientID, 'customer_id'=>$model->userID])
        ->sum('points');
$totalminus = \common\models\VIPCustomerReward::find()
        ->where(['<', 'points', 0])
        ->andWhere(['clientID'=>$model->clientID, 'customer_id'=>$model->userID])
        ->sum('points');
$total = $totaladd + $totalminus;

if($totalminus == 0) {
    $totalminus = 0;
}
?>
<div class="vipcustomer-view">
    <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="../../images/avatar_image.jpg" alt="User profile picture">

              <h3 class="profile-username text-center"><?= Html::encode($model->full_name) ?></h3>
              <p class="text-muted text-center">Clients Ref No: <?= Html::encode($model->clients_ref_no) ?></p>

              <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                  <b>Total Awarded Points</b> <a class="pull-right"><?= $totalminus ?></a>
                </li>
                <li class="list-group-item">
                  <b>Total Redeemed Point</b> <a class="pull-right"><?= $totalminus ?></a>
                </li>
                <li class="list-group-item">
                  <b>Total Available Point</b> <a class="pull-right"><?= $total ?></a>
                </li>
                <li class="list-group-item">
                  <b>Expired Points</b> <a class="pull-right">0</a>
                </li>
                
              </ul>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9  profile-data">
            <ul class="nav nav-tabs responsive" id = "profileTab">
                <li class="active" id = "personal-tab"><a href="#personal" data-toggle="tab"><i class="fa fa-street-view"></i> <?php echo Yii::t('app', 'Personal Details'); ?></a></li>
                <li id = "family-tab"><a href="#company" data-toggle="tab"><i class="fa fa-bank"></i> <?php echo Yii::t('app', 'Company Information'); ?></a></li>
                <li id = "fieldofstudy-tab"><a href="#bank" data-toggle="tab"><i class="fa fa-credit-card"></i> <?php echo Yii::t('app', 'Banking Information'); ?></a></li>
                <li id = "document-tab"><a href="#document" data-toggle="tab"><i class="fa fa-files-o"></i> <?php echo Yii::t('app', 'Documents'); ?></a></li>

            </ul>
            <div id='content' class="tab-content responsive">
                <div class="tab-pane active" id="personal">
                    <?= $this->render('_tab_personal', ['model' => $model]) ?>	
                </div>
            </div> 
          <div class="nav-tabs-custom">
                 
            <!--<ul class="nav nav-tabs">
              <li class="active"><a href="#general" data-toggle="tab">Personal Info</a></li>
              <li><a href="#reward_points" data-toggle="tab">Reward Points</a></li>
              <li><a href="#orders" data-toggle="tab">Orders</a></li>
            </ul>-->
            <div class="tab-content">
                <div class="active tab-pane" id="general">
                    <div class="box-group" id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        <div class="panel box box-primary">
                            <div class="box-header with-border">
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class="">
                                        Profile Info
                                    </a>                                    
                                </h4>
                                <p class="pull-right" style="margin-bottom: 0px">
                                        <?= Html::a('Update', ['update', 'id' => $model->vip_customer_id], ['class' => 'btn btn-primary btn-sm']) ?>
                                    </p>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                <div class="box-body">
                                    <div class="col-lg-6">
                                        <p>
                                            <strong><?= $model->getAttributeLabel('full_name') ?></strong>: <?= Html::encode($model->full_name) ?>
                                        </p>
                                        <p>
                                            <strong><?= $model->getAttributeLabel('gender') ?></strong>: <?= $model->gender == 'M' ? 'Male' : 'Female' ?>
                                        </p>
                                        <p>
                                            <strong><?= $model->getAttributeLabel('date_of_Birth') ?></strong>: <?= date('d-m-Y', strtotime($model->date_of_Birth)) ?>
                                        </p>
                                        <p>
                                            <strong><?= $model->getAttributeLabel('nric_passport_no') ?></strong>: <?= Html::encode($model->nric_passport_no) ?>
                                        </p>
                                        <p>
                                            <strong><?= $model->getAttributeLabel('telephone_no') ?></strong>: <?= Html::encode($model->telephone_no) ?>
                                        </p>
                                        <p>
                                            <strong><?= $model->getAttributeLabel('mobile_no') ?></strong>: <?= Html::encode($model->mobile_no) ?>
                                        </p>
                                        <p>
                                            <strong>Email</strong>: <?= Html::encode($model->user->email) ?>
                                        </p>
                                        <p>
                                            <strong><?= $model->getAttributeLabel('Race') ?></strong>: <?= Html::encode($model->Race) ?>
                                        </p>
                                    </div>
                                    
                                    <div class="col-lg-6">
                                        <p>
                                            <strong><?= $companyInformation->getAttributeLabel('company_name') ?></strong>: <?= Html::encode($companyInformation->company_name ? $companyInformation->company_name : 'N/A') ?>
                                        </p>
                                        <p>
                                            <strong><?= $companyInformation->getAttributeLabel('tel') ?></strong>: <?= Html::encode($companyInformation->tel) ?>
                                        </p>
                                        <p>
                                            <strong><?= $companyInformation->getAttributeLabel('mailing_address') ?></strong>: <?= Html::encode($companyInformation->mailing_address) ?>
                                        </p>
                                        <p>
                                            <strong><?= $companyInformation->getAttributeLabel('no_painters') ?></strong>: <?= Html::encode($companyInformation->no_painters) ?>
                                        </p>
                                        <p>
                                            <strong><?= $companyInformation->getAttributeLabel('painter_sites') ?></strong>: <?= Html::encode($companyInformation->painter_sites) ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Shipping Address -->
                        <?=
                        $this->render('_tab_shpping_view', [
                            'model' => $model,
                            'address' => $address,
                        ])
                        ?>
                        
                        <!-- Bank Info -->
                        <?=
                        $this->render('_tab_bank_view', [
                            'model' => $model,
                            'bankslisit' => $bankslisit,
                        ])
                        ?>
                    </div>
              </div>
              <!-- /.tab-pane -->
              
              <div class="tab-pane" id="reward_points">
                  <?=
                  $this->render('_tab_reward_points', [
                      'model' => $model,
                      'searchrewardModel' => $searchrewardModel,
                      'datarewardProvider' => $datarewardProvider,
                  ])
                  ?>
                  
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="orders">
                  <?=
                  $this->render('_tab_orders', [
                      'model' => $model,
                      'searchOrder' => $searchOrder,
                      'dataProviderOrder' => $dataProviderOrder,
                  ])
                  ?>
                  
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
</div>
