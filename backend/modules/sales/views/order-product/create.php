<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPOrderProduct */

$this->title = 'Create Viporder Product';
$this->params['breadcrumbs'][] = ['label' => 'Viporder Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="viporder-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
