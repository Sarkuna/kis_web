<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPOrderProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Viporder Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="viporder-product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Viporder Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'order_product_id',
            'order_id',
            'product_id',
            'name',
            'model',
            // 'quantity',
            // 'point',
            // 'point_total',
            // 'price',
            // 'total',
            // 'product_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
