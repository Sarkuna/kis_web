<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\MembershipPack */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Order Action';
$totalpoint = $model->totalPoints + $model->shippingPointTotal;
$balancepoint = Yii::$app->VIPglobal->customersAvailablePoint($model->customer_id);

$msg = '';
if($model->last_point_balance >= 0) {
    $OrderStatus = common\models\VIPOrderStatus::find()
        ->orderBy(['order_status_id' => SORT_ASC,])
        ->all();    
}else {
    $OrderStatus = common\models\VIPOrderStatus::find()
        ->where(['IN', 'order_status_id', [50,60]])
        ->orderBy(['order_status_id' => SORT_ASC,])
        ->all();
    $msg = '<div class="alert alert-warning"><strong>Sorry!</strong> Point balance is insufficient to make this order.</div>';
}

$OrderStatuslist = ArrayHelper::map($OrderStatus, 'order_status_id', 'name');
$actionform->order_status_id = $model->order_status_id;
?>
<div class="redemption-form">
            <?php
            $actionform->notify = 1;
        $form = ActiveForm::begin([
                'id' => 'stu-master-update',
                'options' => ['enctype' => 'multipart/form-data'],
                //'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'fieldConfig' => [
                    'template' => "{label}{input}{error}",
                ],
            ]);
        ?>

            <?php echo $form->field($actionform, 'order_status_id')->dropDownList($OrderStatuslist, ['prompt' => 'Select...']); ?>
            <?= $form->field($actionform, 'bb_invoice_no')->textInput() ?>
            <?php
                if($model->flash_deals_id > 0) {
                    echo $form->field($actionform, 'file', [
                                /*'template' => "<label for='basicInputFile'>{label} <sub>You may upload JPG, GIF, BITMAP or PNG images up to 6MB in size.</sub></label>{input}{hint}{error}",
                                'options' => [
                                    'class' => 'file form-group',
                                ],*/
                        ])->fileInput();
                }
            ?>
            <?= $form->field($actionform, 'comment')->textarea(['rows' => '6']) ?>
            <?= $form->field($actionform, 'notify')->checkbox(); ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <?= $msg ?>
        </div>

<?php
$script = <<< JS
    $("#orderactionform-order_status_id").change(function(){
        if($(this).val()=="50" || $(this).val()=="60"){
            $(".field-orderactionform-bb_invoice_no").hide();
        }else{
            $(".field-orderactionform-bb_invoice_no").show();
        }
    });
JS;
$this->registerJs($script);
?>