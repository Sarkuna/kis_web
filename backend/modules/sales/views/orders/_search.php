<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="viporder-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'order_id') ?>

    <?= $form->field($model, 'invoice_no') ?>

    <?= $form->field($model, 'invoice_prefix') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?php // echo $form->field($model, 'customer_group_id') ?>

    <?php // echo $form->field($model, 'shipping_firstname') ?>

    <?php // echo $form->field($model, 'shipping_lastname') ?>

    <?php // echo $form->field($model, 'shipping_company') ?>

    <?php // echo $form->field($model, 'shipping_address_1') ?>

    <?php // echo $form->field($model, 'shipping_address_2') ?>

    <?php // echo $form->field($model, 'shipping_city') ?>

    <?php // echo $form->field($model, 'shipping_postcode') ?>

    <?php // echo $form->field($model, 'shipping_country_id') ?>

    <?php // echo $form->field($model, 'shipping_zone') ?>

    <?php // echo $form->field($model, 'shipping_zone_id') ?>

    <?php // echo $form->field($model, 'shipping_method') ?>

    <?php // echo $form->field($model, 'shipping_code') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'order_status_id') ?>

    <?php // echo $form->field($model, 'language_id') ?>

    <?php // echo $form->field($model, 'ip') ?>

    <?php // echo $form->field($model, 'user_agent') ?>

    <?php // echo $form->field($model, 'accept_language') ?>

    <?php // echo $form->field($model, 'created_datetime') ?>

    <?php // echo $form->field($model, 'updated_datetime') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
