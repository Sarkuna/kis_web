<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PointUploadSummaryReport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="point-upload-summary-report-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'point_upload_summary_id')->textInput() ?>

    <?= $form->field($model, 'dealer_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'points')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
