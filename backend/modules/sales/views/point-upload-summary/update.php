<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PointUploadSummaryReport */

$this->title = 'Update Point Upload Summary Report: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Point Upload Summary Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="point-upload-summary-report-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
