<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PointUploadSummarySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="point-upload-summary-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'point_upload_summary_id') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'effective_month') ?>

    <?= $form->field($model, 'no_of_accounts') ?>

    <?php // echo $form->field($model, 'total_points') ?>

    <?php // echo $form->field($model, 'expiry_date') ?>

    <?php // echo $form->field($model, 'created_datetime') ?>

    <?php // echo $form->field($model, 'updated_datetime') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
