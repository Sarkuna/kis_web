<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerReward */

$this->title = 'Create Vipcustomer Reward';
$this->params['breadcrumbs'][] = ['label' => 'Vipcustomer Rewards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipcustomer-reward-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
