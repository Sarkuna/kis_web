<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerRewardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vipcustomer Rewards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipcustomer-reward-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Vipcustomer Reward', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'customer_reward_id',
            'clientID',
            'customer_id',
            'order_id',
            'description:ntext',
            // 'points',
            // 'date_added',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
