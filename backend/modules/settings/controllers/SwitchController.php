<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use yii\web\Controller;
//use yii\web\NotFoundHttpException;

class SwitchController extends Controller {

    public function actionIndex($clientID) { //$id is user_role_id
        $session = Yii::$app->session;
        $session['currentclientID'] = $clientID;
        /*$userRole = \common\models\UserRole::findOne($id);
        $session['currentUserRoleId'] = $userRole->id;
        $session['currentRoleId'] = $userRole->role_id;
        $session['currentRole'] = $userRole->role->name;
        $session['currentCondoId'] = ($userRole->condo_id == null ? null : $userRole->condo_id);
        $session['currentCondoUnitId'] = ($userRole->condo_unit_id == null ? null : $userRole->condo_unit_id);
        Yii::$app->residenz->addtolog('success', 'Switch Unit', Yii::$app->user->id);*/
        return $this->redirect(['/site/index']);
    }
    
    public function actionVip() { //$id is user_role_id
        unset(Yii::$app->session['currentclientID']);
        //$session = Yii::$app->session;
        //$session['currentclientID'] = $clientID;
        /*$userRole = \common\models\UserRole::findOne($id);
        $session['currentUserRoleId'] = $userRole->id;
        $session['currentRoleId'] = $userRole->role_id;
        $session['currentRole'] = $userRole->role->name;
        $session['currentCondoId'] = ($userRole->condo_id == null ? null : $userRole->condo_id);
        $session['currentCondoUnitId'] = ($userRole->condo_unit_id == null ? null : $userRole->condo_unit_id);
        Yii::$app->residenz->addtolog('success', 'Switch Unit', Yii::$app->user->id);*/
        return $this->redirect(['/site/index']);
    }

}
