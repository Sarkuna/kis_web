<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- Smart Wizard -->
<div class="box-body">
        <br />

        <?php $form = ActiveForm::begin(['options' => ['id' => 'myForm', 'role' => 'form', 'data-toggle' => 'validator', 'class' => 'form-horizontal form-label-left', 'enctype' => 'multipart/form-data']]); ?>            
        <!-- SmartWizard html -->
        <div id="smartwizard" class="form_wizard wizard_horizontal">
            <ul id="mybc" class="wizard_steps">
                <li>
                    <a href="#step-1">
                        <span class="step_no">1</span>
                        <span class="step_descr">
                            Step 1<br />
                            <small>Step 1 General</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-2">
                        <span class="step_no">2</span>
                        <span class="step_descr">
                            Step 2<br />
                            <small>Step 2 Billing Information</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-3">
                        <span class="step_no">3</span>
                        <span class="step_descr">
                            Step 3<br />
                            <small>Step 3 Point Setup</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-4">
                        <span class="step_no">4</span>
                        <span class="step_descr">
                            Step 4<br />
                            <small>Step 4 Option</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-5">
                        <span class="step_no">5</span>
                        <span class="step_descr">
                            Step 5<br />
                            <small>Step 5 Domain Setup</small>
                        </span>
                    </a>
                </li>
            </ul>
            
            <div>
                <div id="step-1">
                    <h3 class="text-center">General</h3>
                    <div id="form-step-0" role="form" data-toggle="validator">

                        <?=
                        $form->field($addressmodel, 'company', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(['placeholder' => 'Company Name', 'required' => true]);
                        ?>

                        <?=
                        $form->field($addressmodel, 'name', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(array('placeholder' => 'Name'));
                        ?>

                        <?=
                        $form->field($addressmodel, 'address_1', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(['required' => true]);
                        ?>

                        <?=
                        $form->field($addressmodel, 'address_2', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(['required' => true]);
                        ?>

                        <?=
                        $form->field($addressmodel, 'postcode', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(['required' => true]);
                        ?>

                        <?=
                        $form->field($addressmodel, 'city', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(['required' => true]);
                        ?>

                        <?=
                        $form->field($addressmodel, 'state', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?php
                        $gcountrys = common\models\VipCountry::find()->all();
                        $gcountryData = ArrayHelper::map($gcountrys, 'country_id', 'name');
                        //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
                        echo $form->field($addressmodel, 'country_id', [
                                    'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6'>{input}</div>\n{hint}\n{error}"
                                ])
                                ->dropDownList(
                                        $gcountryData, ['prompt' => '--', 'id' => 'clientaddress-country_id', 'required' => true]
                        );
                        ?>

                        <?=
                        $form->field($addressmodel, 'email_address', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($addressmodel, 'telephone', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($addressmodel, 'fax_no', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($addressmodel, 'mobile_no', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>
                    </div>
                </div>
                
                <div id="step-2">
                    <h3 class="text-center">Billing Information</h3>
                    <div id="form-step-1" role="form" data-toggle="validator">
                        <?=
                        $form->field($billinginformation, 'name', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(['required' => true]);
                        ?>

                        <?=
                        $form->field($billinginformation, 'address1', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($billinginformation, 'address2', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($billinginformation, 'postcode', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($billinginformation, 'city', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($billinginformation, 'state', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?php
                        $bcountrys = common\models\VipCountry::find()->all();
                        $bcountryData = ArrayHelper::map($bcountrys, 'country_id', 'name');
                        //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
                        echo $form->field($billinginformation, 'country_id', [
                                    'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6'>{input}</div>\n{hint}\n{error}"
                                ])
                                ->dropDownList(
                                        $bcountryData, ['prompt' => '--', 'id' => 'billinginformation-country_id']
                        );
                        ?>

                        <?=
                        $form->field($billinginformation, 'email_address', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($billinginformation, 'telephone_no', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($billinginformation, 'fax_no', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($billinginformation, 'mobile_no', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>
                    </div>
                </div>
                
                <div id="step-3">
                    <h3 class="text-center">Point Setup</h3>
                    <div id="form-step-2" role="form" data-toggle="validator">
                        <?=
                        $form->field($clientpointoption, 'point_value', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(['required' => true]);
                        ?>
                    </div>
                </div>
                
                <div id="step-4">
                    <h3 class="text-center">Option</h3>

                    <div id="form-step-3" role="form" data-toggle="validator">
                        <?=
                        $form->field($clientoption, 'invoice_prefix', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(['required' => true]);
                        ?>
                    </div>
                    
                    
                </div>
                
                <div id="step-5" class="">
                    <h3 class="text-center">Domain Setup</h3>

                    <div id="form-step-4" role="form" data-toggle="validator">
                        <?=
                        $form->field($model, 'programme_title', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(array('placeholder' => '','required' => true));
                        ?>
                        
                        <?=
                        $form->field($model, 'fimage', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div><br>\n{hint}\n{error}"
                        ])->fileInput();
                        ?>
                        
                        <?=
                        $form->field($model, 'cart_domain', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(array('placeholder' => 'http://abc.com.my','required' => true));
                        ?>

                        <?=
                        $form->field($model, 'admin_domain', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(array('placeholder' => 'http://adminabc.com.my', 'required' => true));
                        ?>
                        <?=
                        $form->field($model, 'support_email', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(array('placeholder' => 'Support Email'));
                        ?>
                        <?=
                        $form->field($model, 'email', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput(array('placeholder' => 'BCC Email'));
                        ?>
                        <?php
                        $themes = common\models\Theme::find()->all();
                        $themesData = ArrayHelper::map($themes, 'id', 'name');
                        //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
                        echo $form->field($model, 'theme_id', [
                                    'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6'>{input}</div>\n{hint}\n{error}"
                                ])
                                ->dropDownList(
                                        $themesData, ['prompt' => '--', 'id' => 'theme_id']
                        );
                        ?>

                        <?=
                        $form->field($model, 'client_SMS', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->radioList(['N' => 'No', 'Y' => 'Yes'], ['itemOptions' => ['class' => 'flat', 'id' => 'client_SMS', 'required' => true]]);
                        ?>

                        <?=
                        $form->field($model, 'sms_username', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>
                        <?=
                        $form->field($model, 'sms_password', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($model, 'client_email', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->radioList(['N' => 'No', 'Y' => 'Yes'], ['itemOptions' => ['class' => 'flat', 'id' => 'client_SMS', 'required' => true]]);
                        ?>


                        <?=
                        $form->field($model, 'mailgun_username', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>
                        <?=
                        $form->field($model, 'mailgun_password', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textInput();
                        ?>

                        <?=
                        $form->field($model, 'membership', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->radioList(['N' => 'No', 'Y' => 'Yes'], ['itemOptions' => ['class' => 'flat', 'id' => 'client_SMS']]);
                        ?>
                        
                        <?=
                        $form->field($model, 'footer1', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textarea(['rows' => '6']);
                        ?>
                        <?=
                        $form->field($model, 'footer2', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textarea(['rows' => '6']);
                        ?>
                        
                        <?=
                        $form->field($model, 'terms', [
                            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->textarea(['rows' => '6']);
                        ?>
                    </div>
                    
                    
                </div>
            </div>
        </div>
        
      <?php ActiveForm::end(); ?>
        
    </div>