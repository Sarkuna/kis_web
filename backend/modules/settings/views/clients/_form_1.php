<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- Smart Wizard -->
<p>This is a basic form wizard example that inherits the colors from the selected scheme.</p>
<?php $form = ActiveForm::begin(['options' => ['id' => '','class' => 'form-horizontal form-label-left', 'data-toggle' => "validator", 'enctype' => 'multipart/form-data']]); ?>
<div class="box-body">
<div id="wizard" class="form_wizard wizard_horizontal">
    <ul class="wizard_steps">
        <li>
            <a href="#step-1">
                <span class="step_no">1</span>
                <span class="step_descr">
                    Step 1<br />
                    <small>Step 1 General</small>
                </span>
            </a>
        </li>
        <li>
            <a href="#step-2">
                <span class="step_no">2</span>
                <span class="step_descr">
                    Step 2<br />
                    <small>Step 2 Billing Information</small>
                </span>
            </a>
        </li>
        <li>
            <a href="#step-3">
                <span class="step_no">3</span>
                <span class="step_descr">
                    Step 3<br />
                    <small>Step 3 Point Setup</small>
                </span>
            </a>
        </li>
        <li>
            <a href="#step-4">
                <span class="step_no">4</span>
                <span class="step_descr">
                    Step 4<br />
                    <small>Step 4 Option</small>
                </span>
            </a>
        </li>
        <li>
            <a href="#step-5">
                <span class="step_no">5</span>
                <span class="step_descr">
                    Step 5<br />
                    <small>Step 5 Domain Setup</small>
                </span>
            </a>
        </li>
    </ul>
    
    
    <div id="step-1">
        <?=
        $form->field($addressmodel, 'company', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput(array('placeholder' => 'Company Name'));
        ?>

        <?=
        $form->field($addressmodel, 'name', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput(array('placeholder' => 'Name'));
        ?>

        <?=
        $form->field($addressmodel, 'address_1', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($addressmodel, 'address_2', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($addressmodel, 'postcode', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($addressmodel, 'city', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($addressmodel, 'state', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?php
        $gcountrys = common\models\VipCountry::find()->all();
        $gcountryData = ArrayHelper::map($gcountrys, 'country_id', 'name');
        //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
        echo $form->field($addressmodel, 'country_id', [
                    'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6'>{input}</div>\n{hint}\n{error}"
                ])
                ->dropDownList(
                        $gcountryData, ['prompt' => '--', 'id' => 'clientaddress-country_id']
        );
        ?>

        <?=
        $form->field($addressmodel, 'email_address', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($addressmodel, 'telephone', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($addressmodel, 'fax_no', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($addressmodel, 'mobile_no', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>
    </div>
    
    <div id="step-2">
        <?=
        $form->field($billinginformation, 'name', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($billinginformation, 'address1', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($billinginformation, 'address2', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($billinginformation, 'postcode', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($billinginformation, 'city', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($billinginformation, 'state', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?php
        $bcountrys = common\models\VipCountry::find()->all();
        $bcountryData = ArrayHelper::map($bcountrys, 'country_id', 'name');
        //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
        echo $form->field($billinginformation, 'country_id', [
                    'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6'>{input}</div>\n{hint}\n{error}"
                ])
                ->dropDownList(
                        $bcountryData, ['prompt' => '--', 'id' => 'billinginformation-country_id']
        );
        ?>

        <?=
        $form->field($billinginformation, 'email_address', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($billinginformation, 'telephone_no', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($billinginformation, 'fax_no', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($billinginformation, 'mobile_no', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>
    </div>
    
    <div id="step-3">

    </div>
    
    <div id="step-4">
        <?=
        $form->field($clientoption, 'invoice_prefix', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>
    </div>
    
    <div id="step-5">
        <?=
        $form->field($model, 'cart_domain', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput(array('placeholder' => 'http://abc.com.my'));
        ?>

        <?=
        $form->field($model, 'admin_domain', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput(array('placeholder' => 'http://adminabc.com.my'));
        ?>

        <?php
        $themes = common\models\Theme::find()->all();
        $themesData = ArrayHelper::map($themes, 'id', 'name');
        //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
        echo $form->field($model, 'theme_id', [
                    'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6'>{input}</div>\n{hint}\n{error}"
                ])
                ->dropDownList(
                        $themesData, ['prompt' => '--', 'id' => 'theme_id']
        );
        ?>

        <?=
        $form->field($model, 'client_SMS', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->radioList(['N' => 'No', 'Y' => 'Yes'], ['itemOptions' => ['class' => 'flat', 'id' => 'client_SMS']]);
        ?>

        <?=
        $form->field($model, 'sms_username', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>
        <?=
        $form->field($model, 'sms_password', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($model, 'client_email', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->radioList(['N' => 'No', 'Y' => 'Yes'], ['itemOptions' => ['class' => 'flat', 'id' => 'client_SMS']]);
        ?>


        <?=
        $form->field($model, 'mailgun_username', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>
        <?=
        $form->field($model, 'mailgun_password', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>

        <?=
        $form->field($model, 'membership', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->radioList(['N' => 'No', 'Y' => 'Yes'], ['itemOptions' => ['class' => 'flat', 'id' => 'client_SMS']]);
        ?>
    </div>
</div>
<!-- End SmartWizard Content -->
<?php ActiveForm::end(); ?>
</div>