<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-xs-12">
        <h2 class="page-header">	
            <i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Address'); ?>
            <div class="pull-right">

            </div>
        </h2>
    </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label">Contact Person</div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= $address->name ? $address->name : Yii::t("app", "N/A") ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label">Address</div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= $address->address_1 ? $address->address_1 : Yii::t("app", "N/A") ?><?= $address->address_2 ? $address->address_2 : Yii::t("app", "N/A") ?></div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $address->getAttributeLabel('city') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $address->city ? $address->city : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $address->getAttributeLabel('state') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $address->state ? $address->state : Yii::t("app", "N/A") ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $address->getAttributeLabel('postcode') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $address->postcode ? $address->postcode : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $address->getAttributeLabel('country_id') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $address->country_id ? $address->country->name : Yii::t("app", "N/A") ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $address->getAttributeLabel('email_address') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $address->email_address ? $address->email_address : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $address->getAttributeLabel('mobile_no') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $address->mobile_no ? $address->mobile_no : Yii::t("app", "N/A") ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $address->getAttributeLabel('telephone') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $address->telephone ? $address->telephone : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $address->getAttributeLabel('mobile_no') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $address->fax_no ? $address->fax_no : Yii::t("app", "N/A") ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h2 class="page-header">	
            <i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Billing Address'); ?>
            <div class="pull-right">

            </div>
        </h2>
    </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label">Contact Person</div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= $billing_address->name ? $billing_address->name : Yii::t("app", "N/A") ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label">Address</div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= $billing_address->address1 ? $billing_address->address1 : Yii::t("app", "N/A") ?><?= $billing_address->address2 ? $billing_address->address2 : Yii::t("app", "N/A") ?></div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $billing_address->getAttributeLabel('city') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $billing_address->city ? $billing_address->city : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $billing_address->getAttributeLabel('state') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $billing_address->state ? $billing_address->state : Yii::t("app", "N/A") ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $billing_address->getAttributeLabel('postcode') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $billing_address->postcode ? $billing_address->postcode : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $billing_address->getAttributeLabel('country_id') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $billing_address->country_id ? $billing_address->country->name : Yii::t("app", "N/A") ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $billing_address->getAttributeLabel('email_address') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $billing_address->email_address ? $billing_address->email_address : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $billing_address->getAttributeLabel('mobile_no') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $billing_address->mobile_no ? $billing_address->mobile_no : Yii::t("app", "N/A") ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $billing_address->getAttributeLabel('telephone_no') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $billing_address->telephone_no ? $billing_address->telephone_no : Yii::t("app", "N/A") ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $billing_address->getAttributeLabel('mobile_no') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $billing_address->fax_no ? $billing_address->fax_no : Yii::t("app", "N/A") ?></div>
        </div>
    </div>
</div>