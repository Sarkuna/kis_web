<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-xs-12">	
	<div class="pull-right">
            <?php
            echo '<button class="btn btn-sm btn-primary pull-right" onclick="editSetting('.$clientoption->option_ID.');return false;"><i class="fa fa-edit"></i> Edit</button>';
            ?>
	</div>
  </div><!-- /.col -->
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label">Invoice Prefix</div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= $clientoption->invoice_prefix ? $clientoption->invoice_prefix : Yii::t("app", "N/A") ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label">Receipts Prefix</div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= $clientoption->receipts_prefix ? $clientoption->receipts_prefix : Yii::t("app", "N/A") ?></div>
    </div>
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label">Point Value</div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= $clientoption->point_value ? $clientoption->point_value : Yii::t("app", "N/A") ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label">BB Percent</div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= $clientoption->bb_percent ? $clientoption->bb_percent : Yii::t("app", "N/A") ?>%</div>
    </div>

    

    
</div>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'EditSettingModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Edit Site Info </h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>
<script>
/***Start Update Gardian Jquery***/
function editSetting(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["/admin/clients/edit-setting"]) ?>',
	  data: { id : id},
	  success: function(data)
            {
                $(".modal-content").addClass("row");
                $('.modal-body').html(data);
                $('#EditSettingModal').modal();
            }
	});
}
</script>  