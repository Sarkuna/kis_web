<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = 'Create Client';
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary client-create">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="vipcustomer-create">
        <?=
        $this->render('_form', [
            'model' => $model,
            'addressmodel' => $addressmodel,
            'billinginformation' => $billinginformation,
            'clientoption' => $clientoption,
            'clientpointoption' => $clientpointoption
        ])
        ?>

    </div>
</div>


