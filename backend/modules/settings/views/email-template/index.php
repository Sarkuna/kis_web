<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EmailTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Email Templates';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-12 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
           
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="email-template-index table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'code',
            //'clientID',
            'name',
            'subject',
            // 'template:ntext',
            // 'sms_text',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

            //['class' => 'yii\grid\ActionColumn'],
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}', //{view} {delete}
            'buttons' => [
                'update' => function ($url, $model) {
                    return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                }, /* ,
                      'view' => function ($url, $model) {
                      return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                      },
                      'delete' => function ($url, $model) {
                      return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                      } */
                    ],
                //'visible' => $visible,
                ],
            ],
    ]); ?>
            </div>
        </div>
    </div>
