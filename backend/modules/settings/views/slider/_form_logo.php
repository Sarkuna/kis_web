<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

$this->title = 'Slider images';
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
?>
<style>
    .krajee-default.file-preview-frame .kv-file-content {
    width: 213px;
    height: 160px;
}
</style>
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12 client-logo-index">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>            
        </div><!-- /.box-header -->
        <div class="box-body">  
            <div class="col-lg-12">
                <div class="callout callout-info">
                <h4>Note</h4>

                <p>File limit: Minimum 2 & Maximum images 5.<br>
                Dimension: 1280 pixels wide X 430 pixels heighten<br>
                File format : ONLY jpg, jpe or png extension
                </p>
                
              </div>

                <?php
                $max = 5;
                $totalmax = $max - count($initialPreview);
                ?>

<?=
                    FileInput::widget([
                        'name' => 'upload_ajax[]',
                        'options' => ['multiple' => true, 'accept' => 'image/*'], //'accept' => 'image/*' หากต้องเฉพาะ image
                        'pluginOptions' => [
                            'showPreview' => true,
                            'overwriteInitial' => false,
                            'initialPreviewShowDelete' => true,
                            'allowedFileExtensions' => ['jpg','png','jpeg','bmp'],
                            'initialPreview' => $initialPreview,
                            'initialPreviewConfig' => $initialPreviewConfig,
                            'uploadUrl' => Url::to(['/admin/pages/uploadajax']),
                            /*'uploadExtraData' => [
                                'ref' => $model->ref,
                            ],*/
                            //'minFileCount' => 2,
                            'maxFileCount' => $totalmax
                        ]
                    ]);
                    ?>
            
            </div>
        </div>
    </div>
</div>
</div>