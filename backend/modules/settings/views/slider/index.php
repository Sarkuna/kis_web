<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
           <div class="col-lg-2 text-right" style="padding-left: 0px;">
                <p>
        <?= Html::a('Add New', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
            </div>
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="pages-index table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'page_id',
            //'clientID',
            'page_title',
            [
                'attribute' => 'created_datetime',
                'label' => 'Created Date',
                'format' => 'html',
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->created_datetime));
                },
            ],
            [
                'attribute' => 'updated_datetime',
                'label' => 'Modified Date',
                'format' => 'html',
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->updated_datetime));
                },
            ],
            [
                'attribute' => 'created_datetime',
                'label' => 'Created By',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'value' => function ($model) {
                    return $model->createdBy->first_name.' '.$model->createdBy->last_name;
                },
            ],            
            //'page_description:ntext',
            //'position',
            //'type',
            //Statustext
            // 'status',

            //['class' => 'yii\grid\ActionColumn'],
            [
            'class' => 'yii\grid\ActionColumn',
            'headerOptions' => ['width' => '60'],    
            'template' => '{update} {delete}', //{view} {delete}
            'buttons' => [
                'update' => function ($url, $model) {
                    return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                },
                'delete' => function ($url, $model) {
                    return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                }         
                        /* ,
                      'view' => function ($url, $model) {
                      return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                      },
                      'delete' => function ($url, $model) {
                      return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                      } */
                    ],
                //'visible' => $visible,
                ],
        ],
    ]); ?>
            </div>
        </div>
    </div>
