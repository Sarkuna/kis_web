<?php
use backend\assets\LoginAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

LoginAsset::register($this);
$session = Yii::$app->session;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="login-page">
    <?php $this->beginBody() ?>
    <div class="login-box">
        <div class="logo">
            <?php
            if(!empty($session['currentLogo'])){
                echo '<img src="/upload/client_logos/'.$session['currentLogo'].'" class="img-responsive avatar" width="100" style="margin-left: auto;margin-right: auto;">';                
            }else{
                echo '<img src="../images/logo.png" class="img-responsive avatar" width="100" style="margin-left: auto;margin-right: auto;">';
            }
            ?>
            
            <a href="javascript:void(0);"><?php echo $session['currentclientName'] ?></a>
        </div>
        <div class="card">
            <div class="body">
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>
    
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
