<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\LatestOrdersWidget;
use app\components\ReceiptsListWidget;
use app\components\RecentlyAddedProductsWidget;
use app\components\TopProductsWidget;
use common\models\VIPCustomer;
use app\components\IndirectWidget;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
/*'themes/adminLTE/dist/js/pages/dashboard2.js',*/
$session = Yii::$app->session;
$clientID = $session['currentclientID'];
$total_members = \common\models\User::find()
        ->where(['client_id' => $clientID, 'user_type' =>['C','D']])
        ->count();
$pending = \common\models\User::find()
        ->where(['client_id' => $clientID, 'user_type' =>['C','D'], 'status' => 'P'])
        ->count();
$approve = \common\models\User::find()
        ->where(['client_id' => $clientID, 'user_type' =>['C','D'], 'status' => 'A'])
        ->count();
$deactive = \common\models\User::find()
        ->where(['client_id' => $clientID, 'user_type' =>['C','D'], 'status' => 'D'])
        ->count();
?>

      <div class="row">
          <div class="col-lg-12">
              <div class="alert alert-info alert-dismissible">
                  <h4><i class="icon fa fa-info"></i> Info!</h4>
                  Coming soon
              </div>
          </div>

      </div>
