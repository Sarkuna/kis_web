<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', "Change Password"); 
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-xs-12">
  <div class="col-lg-4 col-sm-6 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title); ?></h3></div>
</div>

<div class="col-xs-6 col-lg-6">
  <div class="<?php echo 'box-info'; ?> box view-item col-xs-12 col-lg-12">
    <div class="form">
	<?php $form = ActiveForm::begin([
			'id' => 'change-password-form',
			'fieldConfig' => [
			    'template' => "{label}{input}{error}",
			],
    ]); ?>

	<?= $form->field($model, 'password')->passwordInput(['maxlength' => 60, 'placeholder' => $model->getAttributeLabel('new_pass')]) ?>

	<?= $form->field($model, 'repassword')->passwordInput(['maxlength' => 60, 'placeholder' => $model->getAttributeLabel('retype_pass')]) ?>

   <div class="form-group col-xs-12 col-sm-6 col-lg-4 no-padding">
            <?= Html::submitButton( Yii::t('app', 'Save'), ['class' =>'btn btn-info']) ?>
	<?= Html::a(Yii::t('app', 'Cancel'), ['/site/index'], ['class' => 'btn btn-default']) ?>
     </div>
 <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>