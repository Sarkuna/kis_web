<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

            <?php $form = ActiveForm::begin(['id' => 'sign_in']); ?>
            <div class="msg hide">Sign in to start your session</div>

                <?= $form->field($model, 'email', ['options' => [],
                    'template' => "<div class=\"input-group\"><span class='input-group-addon'><i class='material-icons'>person</i></span><div class='form-line'>{input}</div>\n{error}</div>"
                ])->textInput(['placeholder' => 'Username/Email'])->label(false)
                ?>

            <div class="row">

                <div class="col-xs-4">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-block bg-pink waves-effect', 'name' => 'login-button']) ?>
                </div>
            </div>
            <div class="row m-t-15 m-b--20">
                <div class="col-xs-6">
                </div>
                <div class="col-xs-6 align-right">
                    <a href="index">Back to Login</a>
                </div>
            </div>
            <?php ActiveForm::end(); ?>


