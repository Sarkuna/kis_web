$(document).ready(function(){
  $("#s").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#clientlist *").filter(function() {      
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      $(".iconclient").removeAttr('style');
    });
  });
});