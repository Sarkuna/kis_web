
var webpush_sub_url = window.location.protocol + '//' + window.location.hostname + '/webpush/register';

var OneSignal = window.OneSignal || [];

OneSignal.push(function() {
              
	OneSignal.init({
        appId: 'f67970b5-253a-4818-ace0-963953f62ebd',
	    notifyButton: {
	          enable: true,
	    },
	});

	OneSignal.on('subscriptionChange', function(isSubscribed) {
		//alert('subscriptionChange: ' + isSubscribed);
	       
		if (isSubscribed == true) {
			// The user is subscribed
			OneSignal.getUserId( function(userId) {
				// Make a POST call to your server with the user ID
				//alert(userId);
				mySubscription(userId, 'Y');
			}); // one signal get userid

		} else {
			// this function is not fired due to OneSignal SDK error
			OneSignal.getUserId( function(userId) {
				mySubscription(userId, 'N');
			}); // one signal get userid
		}
	});

         
	function mySubscription(player_id, sub_status) {
		$.ajax({
			method: "POST",
			url: webpush_sub_url,
			data: { user_id: player_id, status: sub_status }
		}).done(function( msg ) {
			console.log('subscription status: ' + sub_status);
		});
	}


////////// start custom function to fix onesignal sdk ///////////////////
	window.OneSignal.once('notifyButtonLauncherClick', function() {
         
         hookTreeModified();
    });

    function hookTreeModified() {
        // alert('1');
         if (getDialogBody()) {
           // alert('2');
           getDialogBody().addEventListener('DOMSubtreeModified', onDialogBodyModified);
           //onDialogBodyModified();
        }
    }

    function onDialogBodyModified() {
        //alert('OneSignal: Dialog body DOMSubtreeModified event.');
        if (getSubscribeButton()) {
            //alert('3');
            getSubscribeButton().removeEventListener('click', onSubscribeButtonClicked);
            getSubscribeButton().addEventListener('click', onSubscribeButtonClicked);
        }
        if (getUnsubscribeButton()) {
            //alert('4');
            getUnsubscribeButton().removeEventListener('click', onUnsubscribeButtonClicked);
            getUnsubscribeButton().addEventListener('click', onUnsubscribeButtonClicked);
        }
    }

    function onSubscribeButtonClicked() {
        //console.log('Subscribe button clicked.')
        // _satellite.track('one-signal-subscribe');
		OneSignal.getUserId( function(userId) {
			mySubscription(userId, 'Y');
		}); // one signal get userid
        OneSignal.event.trigger('notifyButtonSubscribeClick');
    }

    function onUnsubscribeButtonClicked() {
        //console.log('Unsubscribe button clicked.');
        // _satellite.track('one-signal-subscribe');

		OneSignal.getUserId( function(userId) {
			mySubscription(userId, 'N');
		}); // one signal get userid

        OneSignal.event.trigger('notifyButtonUnsubscribeClick');
    }

    function getDialogBody() {
        return document.querySelector('.onesignal-bell-launcher-dialog-body');
    }

    function getSubscribeButton() {
        return document.querySelector('#onesignal-bell-container .onesignal-bell-launcher #subscribe-button');
    }

    function getUnsubscribeButton() {
        return document.querySelector('#onesignal-bell-container .onesignal-bell-launcher #unsubscribe-button');
    }


});