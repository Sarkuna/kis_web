<?php
// common/components/MyOauth.php

// add use statement at the top of controller then u can use this file
// use common\components\MyOauth;

namespace common\components;


use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class WebPushNotification extends Component
{
	/**
	 * Oauth Token
	 */
	protected $app_id;

	/**
	 * This is the condo id
	 */
	protected $rest_api_key;

	public function __construct() {
		$this->app_id = Yii::$app->params['onesignal.appId'];
		$this->rest_api_key = Yii::$app->params['onesignal.apiKey'];
	}

	public function sendToGroup($group_name, $title, $message, $target_url = '') {
		$session = Yii::$app->session;
		$player_ids = array();
		switch($group_name) {
			case 'management':
				// get user id then select webpush player id
				$player_ids = array();
				
				$mgmt_staffs = \common\models\UserRole::find()
				->leftjoin('role', 'role.id = user_role.role_id')
				->where([
						'user_role.condo_id' => $session['currentCondoId'],
						'user_role.status' => 'A',
						'role.name' =>  Yii::$app->params['role.name.'.$group_name]
				])->all();

				foreach($mgmt_staffs as $s) {
					//$user_ids[] = $s->user_id;
					$webpushs = \common\models\WebPushUser::find()
									->where([
										'user_id' => $s->user_id,
										'status' => 'Y'
									])->all();
					if($webpushs) {
						foreach($webpushs as $webpush) {
							$player_ids[] = $webpush->player_id;
						}
					}
				}
				break;
			default:
				// currently no other group
		}

		//print_r($player_ids);
		if(!empty($player_ids)) {
			print_r($player_ids);
			return $this->send($player_ids, $title, $message, $target_url);
		} else {
			return false;
		}
	}

	public function send($player_ids, $title, $message, $target_url = '') {
		$url = "https://onesignal.com/api/v1/notifications";
		$headers = array(
			'Content-Type: application/json; charset=utf-8',
			'Authorization: Basic ' . $this->rest_api_key,
		);

		$fields = array(
			'app_id' => $this->app_id,
			'include_player_ids' => $player_ids,
			'contents' => $message,
			'headings' => $title,
                        'chrome_web_image' => 'https://dummyimage.com/300x180/a6623a/ffffff&text=iResidenz+Mgmt+Alert',
			//'chrome_web_image' => 'https://99designs-start-attachments.imgix.net/alchemy-pictures/2016%2F02%2F22%2F04%2F24%2F31%2Fb7bd820a-ecc0-4170-8f4e-3db2e73b0f4a%2F550250_artsigma.png?auto=format&ch=Width%2CDPR&w=250&h=250',
			///'chrome_web_image' => 'https://dummyimage.com/300x180/a6623a/ffffff&text=iResidenz+Mgmt+Alert',
			'url' => 'https://'. $target_url,
		);
		$fields = json_encode($fields);

		return $this->curl_call($url, 'POST', $fields, $headers);
	}


	/**
	 * Curl Call to qbo server
	 */
	public function curl_call($url, $method = 'GET', $param = null, $headers = array()) {

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		if($method == 'POST') {
			curl_setopt($ch, CURLOPT_POST, 1);
		} else {
			curl_setopt($ch, CURLOPT_POST, 0);
		}

		if(!empty($param)) {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
			// if(is_array($param)) {
			// 	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));
			// } else {
			// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
			// }
		}

		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$ret = curl_exec ($ch);
		//$info = curl_getinfo($ch);

		curl_close ($ch);

		return $ret;

	}

}
