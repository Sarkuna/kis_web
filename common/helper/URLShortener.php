<?php

namespace common\helper;

class URLShortener {
	
	const BITLY_ACCESS_TOKEN = "1573c6b982a351d813108cfab5f77699152ac7c3";
	const BITLY_DOMAIN = "bit.ly";
	
	public function bitly_url_shorten($long_url) {
		$url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' . self::BITLY_ACCESS_TOKEN. '&longUrl=' . urlencode($long_url) . '&domain=' . self::BITLY_DOMAIN;
		try {
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_TIMEOUT, 4);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$output = json_decode(curl_exec($ch));
		} catch (Exception $e) {
			return null;
		}
		if (isset($output)) {
			return isset($output->data->url) ? $output->data->url : null;
		}
	}
}
