<?php
namespace common\models;

use yii\base\Model;
use Yii;


class AddressSet extends Model
{
    public $add_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['add_id'], 'required'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'add_id' => 'Address',
        ];
    }

    
}
