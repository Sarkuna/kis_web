<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class Approve_Form extends Model
{
    public $status;
    public $remark;
    public $reasons_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'required'],
            [['status'], 'string', 'max' => 1],
            [['reasons_id'],
                'required',
                'when' => function($model) {
                        return $model->status == 'D';
                    },
                'whenClient' => "function (attribute, value) {
                return $('#approve_form-status').val() == 'D';
            }",], 
            [['remark'], 'safe'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'status' => 'Status',
            'remark' => 'Remark',
            'reasons_id' => 'Reasons'
        ];
    }

    
}
