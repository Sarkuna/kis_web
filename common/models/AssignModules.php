<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_assign_modules".
 *
 * @property integer $assign_id
 * @property integer $clientID
 * @property integer $module_id
 */
class AssignModules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_assign_modules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'module_id'], 'required'],
            [['clientID', 'module_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'assign_id' => 'Assign ID',
            'clientID' => 'Client ID',
            'module_id' => 'Module ID',
        ];
    }
}
