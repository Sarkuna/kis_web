<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "balance_carryforward".
 *
 * @property integer $balance_carryforward_id
 * @property integer $clientID
 * @property integer $customer_id
 * @property string $balance_bf_year
 * @property string $description
 * @property integer $balance_points
 * @property string $date_added
 */
class BalanceCarryforward extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balance_carryforward';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'balance_bf_year', 'description', 'date_added'], 'required'],
            [['clientID', 'customer_id', 'balance_points'], 'integer'],
            [['description'], 'string'],
            [['date_added'], 'safe'],
            [['balance_bf_year'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'balance_carryforward_id' => 'Balance Carryforward ID',
            'clientID' => 'Client ID',
            'customer_id' => 'Customer ID',
            'balance_bf_year' => 'Balance Bf Year',
            'description' => 'Description',
            'balance_points' => 'Balance Points',
            'date_added' => 'Date Added',
        ];
    }
}
