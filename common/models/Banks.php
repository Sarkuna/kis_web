<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banks".
 *
 * @property integer $id
 * @property string $bank_name
 * @property string $bank_code
 * @property string $status
 */
class Banks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_name', 'bank_code'], 'required'],
            [['bank_name'], 'string', 'max' => 100],
            [['bank_code'], 'string', 'max' => 3],
            [['status'], 'string', 'max' => 2],
            [['bank_name'], 'unique'],
            [['bank_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bank_name' => 'Bank Name',
            'bank_code' => 'Bank Code',
            'status' => 'Status',
        ];
    }
}
