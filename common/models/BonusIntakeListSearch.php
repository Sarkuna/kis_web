<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BonusIntakeList;

/**
 * BonusIntakeListSearch represents the model behind the search form about `common\models\BonusIntakeList`.
 */
class BonusIntakeListSearch extends BonusIntakeList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_intake_list_id', 'bonus_intake_id', 'clientID', 'list_target', 'created_by', 'updated_by'], 'integer'],
            [['month_start', 'month_end', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BonusIntakeList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bonus_intake_list_id' => $this->bonus_intake_list_id,
            'bonus_intake_id' => $this->bonus_intake_id,
            'clientID' => $this->clientID,
            'month_start' => $this->month_start,
            'month_end' => $this->month_end,
            'list_target' => $this->list_target,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
