<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BonusIntake;

/**
 * BonusIntakeSearch represents the model behind the search form about `common\models\BonusIntake`.
 */
class BonusIntakeSearch extends BonusIntake
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_intake_id', 'clientID', 'bonus_intake_year', 'bonus_intake_month', 'bonus_intake_type', 'bonus_intake_target', 'created_by', 'updated_by'], 'integer'],
            [['created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BonusIntake::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bonus_intake_id' => $this->bonus_intake_id,
            'clientID' => $this->clientID,
            'bonus_intake_year' => $this->bonus_intake_year,
            'bonus_intake_month' => $this->bonus_intake_month,
            'bonus_intake_type' => $this->bonus_intake_type,
            'bonus_intake_target' => $this->bonus_intake_target,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
