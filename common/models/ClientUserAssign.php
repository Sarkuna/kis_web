<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "client_user_assign".
 *
 * @property integer $id
 * @property integer $client_ID
 * @property integer $user_ID
 */
class ClientUserAssign extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 'X';
    const STATUS_ACTIVE = 'A';
    const STATUS_PENDING = 'P';
    
    const CLIENT_STATUS_ACTIVE = 'A';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_user_assign';
    }
    public $email;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_ID', 'user_ID'], 'required'],
            [['client_ID', 'user_ID'], 'integer'],
            [['email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_ID' => 'Client  ID',
            'user_ID' => 'User  ID',
        ];
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_ID']);
    }
    
    public function getClient() {
        return $this->hasOne(\common\models\Client::className(), ['clientID' => 'client_ID'])->
       andWhere(['client_status' => self::CLIENT_STATUS_ACTIVE]);
    }
}
