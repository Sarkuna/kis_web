<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_documents".
 *
 * @property integer $vip_documents_id
 * @property integer $clientID
 * @property integer $userID
 * @property string $file_name
 * @property string $new_file_name
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class Documents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'userID', 'created_by', 'updated_by'], 'integer'],
            [['file_name', 'new_file_name'], 'string'],
            [['created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_documents_id' => 'Vip Documents ID',
            'clientID' => 'Client ID',
            'userID' => 'User ID',
            'file_name' => 'File Name',
            'new_file_name' => 'New File Name',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
