<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "flash_deals".
 *
 * @property integer $flash_deals_id
 * @property string $name
 * @property string $startdate
 * @property string $enddate
 * @property string $status
 */
class FlashDeals extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flash_deals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'startdate', 'enddate', 'status'], 'required'],
            [['startdate', 'enddate'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'flash_deals_id' => 'Flash Deals ID',
            'name' => 'Name',
            'startdate' => 'Startdate',
            'enddate' => 'Enddate',
            'status' => 'Status',
        ];
    }
}
