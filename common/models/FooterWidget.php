<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "footer_widget".
 *
 * @property integer $id
 * @property integer $clientID
 * @property string $footer_description
 * @property integer $position
 */
class FooterWidget extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'footer_widget';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'footer_description'], 'required'],
            [['clientID', 'position'], 'integer'],
            [['footer_description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientID' => 'Client ID',
            'footer_description' => 'Footer Description',
            'position' => 'Position',
        ];
    }
}
