<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "import_customer_excel".
 *
 * @property integer $import_customer_excel_id
 * @property integer $import_customer_upload_summary_id
 * @property string $user_code
 * @property string $company_name
 * @property string $email
 * @property string $account_type
 * @property string $full_name
 * @property string $mobile
 * @property string $company_address
 * @property string $status
 * @property string $date_import
 */
class ImportCustomerExcel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'import_customer_excel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['import_customer_upload_summary_id', 'user_code', 'company_name', 'email', 'account_type', 'status', 'date_import'], 'required'],
            [['import_customer_upload_summary_id'], 'integer'],
            [['company_address'], 'string'],
            [['date_import','remark'], 'safe'],
            [['user_code', 'mobile'], 'string', 'max' => 50],
            [['company_name', 'full_name'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 200],
            [['account_type', 'status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'import_customer_excel_id' => 'Import Customer Excel ID',
            'import_customer_upload_summary_id' => 'Import Customer Upload Summary ID',
            'user_code' => 'User Code',
            'company_name' => 'Company Name',
            'email' => 'Email',
            'account_type' => 'Account Type',
            'full_name' => 'Full Name',
            'mobile' => 'Mobile',
            'company_address' => 'Company Address',
            'status' => 'Status',
            'date_import' => 'Date Import',
            'remark' => 'Remark'
        ];
    }
    
    public function getStatus() {
        $returnValue = "";
        if ($this->status == "P") {
            $returnValue = '<span class="label label-warning">Pending</span>';
        } else if ($this->status == "S") {
            $returnValue = '<span class="label label-success">Success</span>';
        } else if ($this->status == "U") {
            $returnValue = '<span class="label label-success">Update</span>';
        } else if ($this->status == "X") {
            $returnValue = '<span class="label label-danger">Delete</span>';
        }
        return $returnValue;
        
    }
    
    
}
