<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ImportCustomerExcel;

/**
 * PointUploadSummaryReportSearch represents the model behind the search form about `common\models\PointUploadSummaryReport`.
 */
class ImportCustomerExcelSearch extends ImportCustomerExcel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['import_customer_upload_summary_id', 'user_code', 'company_name', 'email', 'account_type', 'status', 'date_import'], 'required'],
            [['import_customer_upload_summary_id'], 'integer'],
            [['company_address'], 'string'],
            [['date_import','remark'], 'safe'],
            [['user_code', 'mobile'], 'string', 'max' => 50],
            [['company_name', 'full_name'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 200],
            [['account_type', 'status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImportCustomerExcel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'import_customer_upload_summary_id' => $this->import_customer_upload_summary_id,
        ]);

        $query->andFilterWhere(['like', 'user_code', $this->user_code])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
