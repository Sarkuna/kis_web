<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "internal_invoicing".
 *
 * @property integer $internal_invoicing_id
 * @property string $invoice_no
 * @property string $subject
 * @property string $bill_from_name
 * @property string $bill_from_add
 * @property string $bill_to_name
 * @property string $bill_to_add
 * @property string $invoice_date
 * @property string $due_date
 * @property string $sub_total
 * @property string $adjustment
 * @property string $total
 * @property string $customer_notes
 * @property string $terms_conditions
 * @property string $status
 */
class InternalInvoicing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'internal_invoicing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_no', 'invoice_type', 'subject', 'bill_from_name', 'bill_from_add', 'bill_to_name', 'bill_to_add', 'invoice_date', 'due_date', 'sub_total', 'total'], 'required'],
            [['bill_from_add', 'bill_to_add'], 'string'],
            [['adjustment', 'invoice_date', 'due_date'], 'safe'],
            [['sub_total', 'adjustment', 'total'], 'number'],
            [['invoice_no'], 'string', 'max' => 50],
            [['internal_invoicing_status'], 'string', 'max' => 10],
            [['subject'], 'string', 'max' => 200],
            [['bill_from_name', 'bill_to_name'], 'string', 'max' => 100],
            [['terms_conditions'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'internal_invoicing_id' => 'Internel Invoice ID',
            'invoice_no' => 'Invoice No',
            'subject' => 'Name',
            'bill_from_name' => 'Bill From Name',
            'bill_from_add' => 'Bill From Add',
            'bill_to_name' => 'Bill To Name',
            'bill_to_add' => 'Bill To Add',
            'invoice_date' => 'Invoice Date',
            'due_date' => 'Due Date',
            'sub_total' => 'Sub Total',
            'adjustment' => 'Adjustment',
            'total' => 'Total',
            'terms_conditions' => 'Terms Conditions',
            'internal_invoicing_status' => 'Status',
        ];
    }
    
    public function getActions() {        
        $returnValue = "";

        //$returnValue = $returnValue . ' <a href="' . Url::to(['/reports/report/download-internel-invoice', 'invoiceid' => $this->internal_invoicing_id]) . '" title="Download Excel"><i class="fa fa-file-excel fa-lg text-success"></i></a> ';
        $returnValue = $returnValue . ' <a href="' . Url::to(['/reports/report/download-internel-pdf', 'invoiceid' => $this->internal_invoicing_id]) . '" title="Download Invoice" target="_blank"><i class="fa fa-file-pdf fa-lg text-warning"></i></a> ';

        return $returnValue;
    }
}
