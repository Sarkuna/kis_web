<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InvoiceItem;

/**
 * InvoiceItemSearch represents the model behind the search form about `common\models\InvoiceItem`.
 */
class InvoiceItemSearch extends InvoiceItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_item_id', 'invoice_id'], 'integer'],
            [['invoice_type', 'invoice_no', 'invoice_date', 'invoice_pono', 'invoice_due_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvoiceItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'invoice_item_id' => $this->invoice_item_id,
            'invoice_id' => $this->invoice_id,
            'invoice_date' => $this->invoice_date,
            'invoice_due_date' => $this->invoice_due_date,
        ]);

        $query->andFilterWhere(['like', 'invoice_type', $this->invoice_type])
            ->andFilterWhere(['like', 'invoice_no', $this->invoice_no])
            ->andFilterWhere(['like', 'invoice_pono', $this->invoice_pono]);

        return $dataProvider;
    }
}
