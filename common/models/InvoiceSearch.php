<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Invoice;

/**
 * InvoiceSearch represents the model behind the search form about `common\models\Invoice`.
 */
class InvoiceSearch extends Invoice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id'], 'integer'],
            [['invoice_name', 'bill_from_name', 'bill_from_add', 'bill_to_name', 'bill_to_add', 'bill_create_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'invoice_id' => $this->invoice_id,
            'bill_create_date' => $this->bill_create_date,
        ]);

        $query->andFilterWhere(['like', 'invoice_name', $this->invoice_name])
            ->andFilterWhere(['like', 'bill_from_name', $this->bill_from_name])
            ->andFilterWhere(['like', 'bill_from_add', $this->bill_from_add])
            ->andFilterWhere(['like', 'bill_to_name', $this->bill_to_name])
            ->andFilterWhere(['like', 'bill_to_add', $this->bill_to_add]);

        return $dataProvider;
    }
}
