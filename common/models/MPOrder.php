<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "mp_order".
 *
 * @property integer $mp_order_id
 * @property string $invoice_no
 * @property string $invoice_prefix
 * @property integer $customer_id
 * @property string $shipping_firstname
 * @property string $shipping_lastname
 * @property string $shipping_company
 * @property string $shipping_address_1
 * @property string $shipping_address_2
 * @property string $shipping_city
 * @property string $shipping_postcode
 * @property string $shipping_country_id
 * @property string $shipping_zone
 * @property string $shipping_zone_id
 * @property string $comment
 * @property string $order_status
 * @property string $ip
 * @property string $user_agent
 * @property string $accept_language
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $invoice_id
 */
class MPOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_order';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => 'mdm\autonumber\Behavior',
                'attribute' => 'invoice_prefix', // required
                //'group' => $this->clientID, // optional
                'value' => 'MP-?' , // format auto number. '?' will be replaced with generated number
                'digit' => 6 // optional, default to null. 
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip', 'user_agent'], 'required'],
            [['customer_id', 'created_by', 'updated_by'], 'integer'],
            [['order_status'], 'string'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            //[['invoice_no'], 'string', 'max' => 5],
            [['invoice_prefix'], 'string', 'max' => 26],
            [['ip'], 'string', 'max' => 40],
            [['user_agent'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mp_order_id' => 'Mp Order ID',
            'invoice_prefix' => 'Invoice Prefix',
            'customer_id' => 'Customer ID',
            'order_status' => 'Order Status',
            'ip' => 'Ip',
            'user_agent' => 'User Agent',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'invoice_id' => 'Invoice ID',
        ];
    }
    
    public function getOrderProducts()
    {
        return $this->hasMany(MPOrderItem::className(), ['mp_order_id' => 'mp_order_id']);
    }
    
    public function getTotalTransactions(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return MPOrderItem::find()->where(['mp_order_id' => $this->mp_order_id])->count();
    }
    
    public function getTotalPoints()
    {
        //return $this->hasMany(MPOrderItem::className(), ['mp_order_id' => 'mp_order_id'])->sum('point_total');
        $shipping_point_total = 0;
        $shipingpoint = 0;
        $myshippings = MPOrderItem::find()->where(['mp_order_id' => $this->mp_order_id])->all();
        foreach($myshippings as $product) {
            $qty = $product->quantity;
            $admin_fee = $product->admin_fee;
            $total_fee = $product->price + $admin_fee;
            $point = $total_fee * 2;
            $shipingpoint = round($point) * $qty;
            $shipping_point_total += $shipingpoint;
        }
        return $shipping_point_total;
    }
    
    
    public function getStatusbg() {
        if($this->order_status == 'Processing') {
            $status = 'info';
        }else if($this->order_status == 'Approves') {
            $status = 'success';
        }else if($this->order_status == 'Decline') {
            $status = 'danger';
        }else {
            $status = 'warning';
        }
        return $status;
    }
    
    public function getCustomer() {
        return $this->hasOne(\common\models\VIPCustomer::className(), ['userID' => 'customer_id']);
    }
    
    public function getCustomeremail() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'customer_id']);
    }
    
    public function getCompany() {
        return $this->hasOne(CompanyInformation::className(), ['user_id' => 'customer_id']);
    }
    
}
