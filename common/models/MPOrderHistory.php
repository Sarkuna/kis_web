<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "mp_order_history".
 *
 * @property integer $mp_order_history_id
 * @property integer $mp_order_id
 * @property string $order_status
 * @property integer $notify
 * @property integer $type
 * @property string $comment
 * @property string $date_added
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class MPOrderHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_order_history';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mp_order_id', 'order_status', 'comment', 'date_added'], 'required'],
            [['mp_order_id', 'type', 'created_by', 'updated_by'], 'integer'],
            [['order_status', 'comment'], 'string'],
            [['date_added', 'created_datetime', 'updated_datetime'], 'safe'],
            [['notify'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mp_order_history_id' => 'Mp Order History ID',
            'mp_order_id' => 'Mp Order ID',
            'order_status' => 'Order Status',
            'notify' => 'Notify',
            'type' => 'Type',
            'comment' => 'Comment',
            'date_added' => 'Date Added',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getBy()
    {
        return $this->hasOne(UserProfile::className(), ['userID' => 'created_by']);
    }
    
    public function getStatusbg() {
        if($this->order_status == 'Processing') {
            $status = 'info';
        }else if($this->order_status == 'Approves') {
            $status = 'success';
        }else if($this->order_status == 'Decline') {
            $status = 'danger';
        }else {
            $status = 'warning';
        }
        return $status;
    }
}
