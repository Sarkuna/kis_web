<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mp_order_item".
 *
 * @property integer $mp_order_item_id
 * @property integer $mp_order_id
 * @property string $product_name
 * @property string $product_type
 * @property string $product_colour
 * @property string $product_pack_size
 * @property integer $quantity
 * @property string $price
 * @property string $admin_fee
 * @property string $product_status
 */
class MPOrderItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_order_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mp_order_id', 'product_name', 'product_type', 'product_pack_size', 'quantity', 'admin_fee'], 'required'],
            [['mp_order_id', 'quantity'], 'integer'],
            [['price', 'admin_fee'], 'number'],
            [['product_status'], 'string'],
            [['product_name'], 'string', 'max' => 255],
            [['product_type', 'product_colour'], 'string', 'max' => 100],
            [['product_pack_size'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mp_order_item_id' => 'Mp Order Item ID',
            'mp_order_id' => 'Mp Order ID',
            'product_name' => 'Product Name',
            'product_type' => 'Product Type',
            'product_colour' => 'Product Colour',
            'product_pack_size' => 'Product Pack Size',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'admin_fee' => 'Admin Fee',
            'product_status' => 'Product Status',
        ];
    }
    
    public function getOrder()
    {
        return $this->hasOne(MPOrder::className(), ['mp_order_id' => 'mp_order_id']);
    }
}
