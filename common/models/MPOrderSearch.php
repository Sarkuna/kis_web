<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MPOrder;

/**
 * MPOrderSearch represents the model behind the search form about `common\models\MPOrder`.
 */
class MPOrderSearch extends MPOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mp_order_id', 'customer_id', 'created_by', 'updated_by', 'invoice_id'], 'integer'],
            [['invoice_prefix', 'order_status', 'ip', 'user_agent', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MPOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'mp_order_id' => $this->mp_order_id,
            'customer_id' => $this->customer_id,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'invoice_id' => $this->invoice_id,
        ]);

        $query->andFilterWhere(['like', 'invoice_prefix', $this->invoice_prefix])
            ->andFilterWhere(['like', 'order_status', $this->order_status])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'user_agent', $this->user_agent]);

        return $dataProvider;
    }
}
