<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mp_product_colour".
 *
 * @property integer $product_colour_id
 * @property string $product_colour_name
 * @property string $product_colour_status
 */
class MPProductColour extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_product_colour';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_colour_name'], 'required'],
            [['product_colour_name'], 'string', 'max' => 300],
            [['product_colour_status'], 'string', 'max' => 20],
            [['product_colour_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_colour_id' => 'Product Colour ID',
            'product_colour_name' => 'Product Colour Name',
            'product_colour_status' => 'Product Colour Status',
        ];
    }
}
