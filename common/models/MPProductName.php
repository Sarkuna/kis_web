<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mp_product_name".
 *
 * @property integer $product_name_id
 * @property string $product_name
 * @property string $product_name_status
 */
class MPProductName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_product_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name'], 'required'],
            [['product_name'], 'string', 'max' => 300],
            [['product_name_status'], 'string', 'max' => 20],
            [['product_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_name_id' => 'Product Name ID',
            'product_name' => 'Product Name',
            'product_name_status' => 'Product Name Status',
        ];
    }
}
