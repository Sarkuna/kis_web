<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mp_product_size".
 *
 * @property integer $product_size_id
 * @property string $product_size_name
 * @property string $product_size_status
 */
class MPProductSize extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_product_size';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_size_name'], 'required'],
            [['product_size_name'], 'string', 'max' => 300],
            [['product_size_status'], 'string', 'max' => 20],
            [['product_size_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_size_id' => 'Product Size ID',
            'product_size_name' => 'Product Size Name',
            'product_size_status' => 'Product Size Status',
        ];
    }
}
