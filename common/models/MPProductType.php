<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mp_product_type".
 *
 * @property integer $product_type_id
 * @property string $product_type_name
 * @property string $product_type_status
 */
class MPProductType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_product_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_type_name'], 'required'],
            [['product_type_name'], 'string', 'max' => 300],
            [['product_type_status'], 'string', 'max' => 20],
            [['product_type_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_type_id' => 'Product Type ID',
            'product_type_name' => 'Product Type Name',
            'product_type_status' => 'Product Type Status',
        ];
    }
}
