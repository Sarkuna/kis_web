<?php

namespace common\models;

use Yii;
use common\models\MPProductName;
use common\models\MPProductType;
use common\models\MPProductColour;
use common\models\MPProductSize;
/**
 * This is the model class for table "mp_products".
 *
 * @property integer $mp_products_id
 * @property integer $product_name_id
 * @property integer $product_type_id
 * @property integer $product_colour_id
 * @property integer $product_size_id
 * @property string $products_price
 * @property string $products_status
 */
class MPProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name_id', 'product_size_id', 'products_price'], 'required'],
            [['product_name_id', 'product_type_id', 'product_colour_id', 'product_size_id'], 'integer'],
            [['products_price'], 'number'],
            [['products_status','type'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mp_products_id' => 'Mp Products ID',
            'type' => 'Type',
            'product_name_id' => 'Product Name',
            'product_type_id' => 'Product Type',
            'product_colour_id' => 'Product Colour',
            'product_size_id' => 'Product Size',
            'products_price' => 'Products Price',
            'products_status' => 'Status',
        ];
    }
    
    public function getProductName()
    {
        return $this->hasOne(MPProductName::className(), ['product_name_id' => 'product_name_id']);
    }
    
    public function getProductType()
    {
        return $this->hasOne(MPProductType::className(), ['product_type_id' => 'product_type_id']);
    }
    
    public function getProductColour()
    {
        return $this->hasOne(MPProductColour::className(), ['product_colour_id' => 'product_colour_id']);
    }
    
    public function getProductSize()
    {
        return $this->hasOne(MPProductSize::className(), ['product_size_id' => 'product_size_id']);
    }
}
