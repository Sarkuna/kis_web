<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MPProducts;

/**
 * MPProductsSearch represents the model behind the search form about `common\models\MPProducts`.
 */
class MPProductsSearch extends MPProducts
{
    /**
     * @inheritdoc
     */
    public $product_name;
    public $product_type;
    public $product_colour;
    public $product_size;
    
    public function rules()
    {
        return [
            //[['mp_products_id', 'product_name_id', 'product_type_id', 'product_colour_id', 'product_size_id'], 'integer'],
            [['type', 'product_name', 'product_type', 'product_colour', 'product_size', 'products_status'], 'safe'],
            [['products_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MPProducts::find();
        $query->joinWith(['productName','productType','productColour','productSize']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['!=', 'products_status', 'delete']);
        $query->andFilterWhere([
            'mp_products_id' => $this->mp_products_id,
            //'product_name_id' => $this->product_name_id,
            //'product_type_id' => $this->product_type_id,
            //'product_colour_id' => $this->product_colour_id,
            //'product_size_id' => $this->product_size_id,
            'products_price' => $this->products_price,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'mp_product_name.product_name', $this->product_name])
            ->andFilterWhere(['like', 'mp_product_type.product_type_name', $this->product_type])
            ->andFilterWhere(['like', 'mp_product_colour.product_colour_name', $this->product_colour])
            ->andFilterWhere(['like', 'mp_product_size.product_size_name', $this->product_size])    
            ->andFilterWhere(['like', 'products_status', $this->products_status]);

        return $dataProvider;
    }
}
