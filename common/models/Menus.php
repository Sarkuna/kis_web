<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menus".
 *
 * @property integer $id
 * @property integer $clientID
 * @property string $name
 * @property string $display_location
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Menus extends \yii\db\ActiveRecord
{
    public $menu_nodes;
    public $external_node_title;
    public $external_node_url;
    public $external_node_target;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'name'], 'required'],
            [['clientID'], 'integer'],
            [['created_at', 'updated_at', 'menu_nodes', 'display_location'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientID' => 'Client ID',
            'name' => 'Menu Name',
            'display_location' => 'Display Location',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'menu_nodes' => 'nestable-output',
            'external_node_title' => 'Title',
            'external_node_url' => 'URL',
            'external_node_target' => 'Target',
        ];
    }
    
    public function getMenuitems()
    {
        return $this->hasMany(MenuItems::className(), ['menu_id' => 'id']);
    }
    
    public function getDisplaymenuitems()
    {
        return $this->hasMany(MenuItems::className(), ['menu_id' => 'id']);
    }
    
    public function getMenus()
    {
        return $this->hasOne(MenuItems::className(), ['menu_id' => 'id']);
    }
}
