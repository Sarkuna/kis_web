<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "vip_cms_pages".
 *
 * @property integer $page_id
 * @property integer $clientID
 * @property string $page_title
 * @property string $slug
 * @property string $page_url
 * @property string $page_description
 * @property string $type
 * @property string $status
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }
    
    public function behaviors() {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'page_title',
                //'slugAttribute' => 'page_url',//default name slug
                //'immutable' => true,
                'ensureUnique'=>true,
                
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'page_title', 'page_description'], 'required'],
            [['clientID', 'created_by', 'updated_by'], 'integer'],
            [['page_url', 'page_description'], 'string'],
            [['created_datetime', 'updated_datetime', 'created_by', 'updated_by', 'slug'], 'safe'],
            [['page_title'], 'string', 'max' => 300],
            [['slug'], 'string', 'max' => 255],
            [['type', 'status'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'page_id' => 'Page ID',
            'clientID' => 'Client ID',
            'page_title' => 'Page Title',
            'slug' => 'Slug',
            'page_url' => 'Page Url',
            'page_description' => 'Page Description',
            'type' => 'Type',
            'status' => 'Status',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getCreatedBy() {
        return $this->hasOne(UserProfile::className(), ['profile_ID' => 'created_by']);
    }
    
    public function getUpdatedBy() {
        return $this->hasOne(UserProfile::className(), ['profile_ID' => 'updated_by']);
    }
}
