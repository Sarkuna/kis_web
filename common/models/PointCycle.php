<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "point_cycle".
 *
 * @property integer $point_cycle_id
 * @property string $point_cycle_name
 * @property string $awarded_start
 * @property string $awarded_end
 * @property string $redemption_start
 * @property string $redemption_end
 * @property string $status
 */
class PointCycle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'point_cycle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_cycle_name', 'awarded_start', 'awarded_end', 'redemption_start', 'redemption_end', 'status'], 'required'],
            [['awarded_start', 'awarded_end', 'redemption_start', 'redemption_end'], 'safe'],
            [['point_cycle_name'], 'string', 'max' => 2],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'point_cycle_id' => 'Point Cycle ID',
            'point_cycle_name' => 'Point Cycle Name',
            'awarded_start' => 'Awarded Start',
            'awarded_end' => 'Awarded End',
            'redemption_start' => 'Redemption Start',
            'redemption_end' => 'Redemption End',
            'status' => 'Status',
        ];
    }
}
