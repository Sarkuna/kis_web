<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "point_upload_summary_report".
 *
 * @property integer $id
 * @property integer $point_upload_summary_id
 * @property string $dealer_id
 * @property integer $points
 * @property string $status
 */
class PointUploadSummaryReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'point_upload_summary_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_upload_summary_id', 'dealer_id', 'points'], 'required'],
            [['clientID','point_upload_summary_id', 'points'], 'integer'],
            [['dealer_id'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_upload_summary_id' => 'Point Upload Summary ID',
            'dealer_id' => 'Dealer ID',
            'points' => 'Points',
            'status' => 'Status',
        ];
    }
    
    public function getPointupload()
    {
        return $this->hasOne(PointUploadSummary::className(), ['point_upload_summary_id' => 'point_upload_summary_id']);
    }
    
    public function getStatus() {
        $returnValue = "";
        if ($this->status == "N") {
            $returnValue = '<span class="label label-warning">Nothing</span>';
        } else if ($this->status == "S") {
            $returnValue = '<span class="label label-success">Success</span>';
        } else if ($this->status == "F") {
            $returnValue = '<span class="label label-danger">Fail</span>';
        }
        return $returnValue;
        
    }
    
    public function getRemark() {
        $session = Yii::$app->session;
        $checkcustomer = \common\models\VIPCustomer::find()->where([
                'clientID' => $session['currentclientID'],
                'clients_ref_no' => $this->dealer_id,
            ])->count();
        
        if($checkcustomer > 0) {
            $returnValue = 'Ready to Syc';
        }else {
            $returnValue = 'Unable to Sync';
        }
        return $returnValue;
        
    }
}
