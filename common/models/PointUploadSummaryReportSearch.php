<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PointUploadSummaryReport;

/**
 * PointUploadSummaryReportSearch represents the model behind the search form about `common\models\PointUploadSummaryReport`.
 */
class PointUploadSummaryReportSearch extends PointUploadSummaryReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'point_upload_summary_id', 'points'], 'integer'],
            [['dealer_id', 'status_remark', 'status','clientID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PointUploadSummaryReport::find();
        $query->joinWith(['pointupload']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'point_upload_summary_id' => $this->point_upload_summary_id,
            'points' => $this->points,
        ]);

        $query->andFilterWhere(['like', 'dealer_id', $this->dealer_id])
            ->andFilterWhere(['like', 'status_remark', $this->status_remark])
            ->andFilterWhere(['like', 'point_upload_summary_report.clientID', $this->clientID])    
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
    
    public function searchview($params)
    {
        $query = PointUploadSummaryReport::find();
        //$query->joinWith(['pointupload']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'point_upload_summary_id' => $this->point_upload_summary_id,
            'points' => $this->points,
        ]);

        $query->andFilterWhere(['like', 'dealer_id', $this->dealer_id])
            ->andFilterWhere(['like', 'status_remark', $this->status_remark])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
