<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PointUploadSummary;

/**
 * PointUploadSummarySearch represents the model behind the search form about `common\models\PointUploadSummary`.
 */
class PointUploadSummarySearch extends PointUploadSummary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_upload_summary_id', 'clientID', 'created_by', 'updated_by'], 'integer'],
            [['description', 'effective_month', 'expiry_date', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PointUploadSummary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_datetime'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'point_upload_summary_id' => $this->point_upload_summary_id,
            'clientID' => $this->clientID,
            'effective_month' => $this->effective_month,
            'expiry_date' => $this->expiry_date,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
