<?php

namespace common\models;

use Yii;
/**
 * This is the model class for table "product_list".
 *
 * @property integer $product_list_id
 * @property integer $clientID
 * @property integer $product_name_id
 * @property string $description
 * @property string $pack_size
 * @property integer $total_points_awarded
 */
class ProductList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'product_name_id', 'description', 'pack_size', 'total_points_awarded'], 'required'],
            [['clientID', 'product_name_id', 'total_points_awarded'], 'integer'],
            [['description'], 'string'],
            [['pack_size'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_list_id' => 'Product List ID',
            'clientID' => 'Client ID',
            'product_name_id' => 'Product Name',
            'description' => 'Product Items',
            'pack_size' => 'Pack Size',
            'total_points_awarded' => 'Total Points Awarded',
        ];
    }
    
    public function getProductName()
    {
        return $this->hasOne(ProductName::className(), ['product_name_id' => 'product_name_id']);
    }
    
    public function getProductNameTotal()
    {
        $totaladdr = ProductName::find()
        ->where(['clientID'=>$this->clientID])       
        ->count();        
        return $totaladdr;
    }
    
    public function getProductItemTotal()
    {
        $totaladdr = ProductList::find()
        ->where(['clientID'=>$this->clientID, 'del'=>'A'])       
        ->count();        
        return $totaladdr;
    }
    
    public function getProductItemTotalDel()
    {
        $totaladdr = ProductList::find()
        ->where(['clientID'=>$this->clientID, 'del'=>'X'])       
        ->count();        
        return $totaladdr;
    }
    
    public function getValuerm()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $rmvalues = \common\models\ClientPointOption::find()->where(['clientID' => $clientID])->one();
        $rmvalue = $rmvalues->point_value;
        return $rmvalue;
    }
    
    
    
        
    
}
