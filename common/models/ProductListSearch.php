<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProductList;

/**
 * ProductListSearch represents the model behind the search form about `common\models\ProductList`.
 */
class ProductListSearch extends ProductList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_list_id', 'clientID', 'product_name_id', 'total_points_awarded'], 'integer'],
            [['description', 'del'], 'safe'],
            [['pack_size'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_list_id' => $this->product_list_id,
            'clientID' => $this->clientID,
            'product_name_id' => $this->product_name_id,
            'pack_size' => $this->pack_size,
            'total_points_awarded' => $this->total_points_awarded,
            'del' => $this->del,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
