<?php

namespace common\models;

use Yii;

use common\models\ProductList;

/**
 * This is the model class for table "product_name".
 *
 * @property integer $product_name_id
 * @property integer $clientID
 * @property string $name
 */
class ProductName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'name'], 'required'],
            [['clientID'], 'integer'],
            [['name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_name_id' => 'Product Name ID',
            'clientID' => 'Client ID',
            'name' => 'Name',
        ];
    }
    
    public function getProductItemTotal()
    {
        return $this->hasMany(ProductList::className(), ['product_name_id' => 'product_name_id'])->count();
    }
}
