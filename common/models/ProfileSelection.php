<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile_selection".
 *
 * @property integer $profile_selection_id
 * @property integer $clientID
 * @property string $name
 * @property string $prefix
 * @property integer $prefix_value
 * @property integer $sort_order
 * @property integer $status
 */
class ProfileSelection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_selection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'name', 'prefix', 'prefix_value', 'status'], 'required'],
            [['clientID', 'prefix_value', 'sort_order', 'status'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['prefix'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'profile_selection_id' => 'Profile Selection ID',
            'clientID' => 'Client ID',
            'name' => 'Name',
            'prefix' => 'Prefix',
            'prefix_value' => 'Prefix Value',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
        ];
    }
}
