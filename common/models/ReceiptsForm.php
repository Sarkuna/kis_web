<?php
namespace common\models;

use yii\base\Model;
use Yii;


class ReceiptsForm extends Model
{
    public $date_range;
    public $reseller_name;
    public $indirect;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['date_range', 'required'],
            [['reseller_name','indirect'], 'safe'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'date_range' => 'Year',
            'reseller_name' => 'Distributor Name',
            'indirect' => 'Indirect'
       ];
    }
    

    
}
