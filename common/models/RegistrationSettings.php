<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_registration_settings".
 *
 * @property integer $id
 * @property string $is_subscribed
 * @property string $should_notify_sms
 * @property string $should_notify_email
 * @property string $should_call
 * @property integer $clientID
 * @property string $sign_in_fields
 * @property string $additional_fields
 * @property string $other_settings
 * @property string $terms
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_datetime
 * @property string $updated_datetime
 */
class RegistrationSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_registration_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'created_by', 'updated_by'], 'integer'],
            [['sign_in_fields', 'additional_fields', 'other_settings', 'terms'], 'string'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['is_subscribed', 'should_notify_sms', 'should_notify_email', 'should_call'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_subscribed' => 'Is Subscribed',
            'should_notify_sms' => 'Should Notify Sms',
            'should_notify_email' => 'Should Notify Email',
            'should_call' => 'Should Call',
            'clientID' => 'Client ID',
            'sign_in_fields' => 'Sign In Fields',
            'additional_fields' => 'Additional Fields',
            'other_settings' => 'Other Settings',
            'terms' => 'Terms',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
        ];
    }
}
