<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "registration_type".
 *
 * @property integer $registration_type_id
 * @property string $name
 * @property integer $del
 */
class RegistrationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'registration_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['del'], 'integer'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'registration_type_id' => 'Registration Type ID',
            'name' => 'Name',
            'del' => 'Del',
        ];
    }
}
