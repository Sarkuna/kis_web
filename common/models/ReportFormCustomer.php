<?php
namespace common\models;

use yii\base\Model;
use Yii;


class ReportFormCustomer extends Model
{
    public $type;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'required'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'type' => 'Type'
       ];
    }
    

    
}
