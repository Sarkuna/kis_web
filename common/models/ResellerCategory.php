<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reseller_category".
 *
 * @property integer $reseller_category_id
 * @property integer $clientID
 * @property string $name
 * @property string $prefix
 * @property integer $prefix_value
 * @property integer $sort_order
 * @property integer $status
 */
class ResellerCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reseller_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'name', 'prefix', 'prefix_value', 'status'], 'required'],
            [['clientID', 'prefix_value', 'sort_order', 'status'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['prefix'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reseller_category_id' => 'Reseller Category ID',
            'clientID' => 'Client ID',
            'name' => 'Name',
            'prefix' => 'Prefix',
            'prefix_value' => 'Prefix Value',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
        ];
    }
}
