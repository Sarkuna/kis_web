<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reseller_list".
 *
 * @property integer $reseller_list_id
 * @property integer $clientID
 * @property string $reseller_name
 * @property integer $reseller_category_id
 * @property integer $state
 */
class ResellerList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reseller_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'reseller_name', 'reseller_category_id', 'state'], 'required'],
            [['clientID', 'reseller_category_id', 'state'], 'integer'],
            [['reseller_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reseller_list_id' => 'Reseller List ID',
            'clientID' => 'Client ID',
            'reseller_name' => 'Reseller Name',
            'reseller_category_id' => 'Reseller Category ID',
            'state' => 'State',
        ];
    }
}
