<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sms_OTP".
 *
 * @property integer $id
 * @property integer $userID
 * @property string $mobile
 * @property string $otp
 */
class SMSOTP extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_OTP';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'mobile', 'otp'], 'required'],
            [['userID'], 'integer'],
            [['mobile'], 'string', 'max' => 100],
            [['otp'], 'string', 'max' => 6],
            [['otp'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userID' => 'User ID',
            'mobile' => 'Mobile',
            'otp' => 'Otp',
        ];
    }
}
