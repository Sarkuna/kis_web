<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "salutation".
 *
 * @property integer $salutation_id
 * @property string $name
 * @property integer $position
 * @property string $status
 */
class Salutation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'salutation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position'], 'required'],
            [['position'], 'integer'],
            [['name'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'salutation_id' => 'Salutation ID',
            'name' => 'Name',
            'position' => 'Position',
            'status' => 'Status',
        ];
    }
}
