<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "statement_of_account".
 *
 * @property integer $sa_id
 * @property string $statement_date
 * @property string $particulars
 * @property string $amount
 * @property string $payment
 * @property string $payment_type
 * @property string $payment_status
 * @property integer $internal_invoicing_id
 * @property integer $invoice_item_id
 */
class StatementOfAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statement_of_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statement_date', 'payment_type'], 'required'],
            [['statement_date', 'payment_id'], 'safe'],
            [['particulars'], 'string'],
            [['amount', 'payment'], 'number'],
            [['internal_invoicing_id', 'invoice_item_id'], 'integer'],
            [['payment_type'], 'string', 'max' => 50],
            [['payment_status'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sa_id' => 'Sa ID',
            'statement_date' => 'Statement Date',
            'particulars' => 'Particulars',
            'amount' => 'Amount',
            'payment' => 'Payment',
            'payment_type' => 'Payment Type',
            'payment_status' => 'Payment Status',
            'internal_invoicing_id' => 'Internal Invoicing ID',
            'invoice_item_id' => 'Invoice Item ID',
        ];
    }
    
    public function getInternalInvoice()
    {
        return $this->hasOne(InternalInvoicing::className(), ['internal_invoicing_id' => 'internal_invoicing_id']);
    }
    
    public function getInvoiceItem()
    {
        return $this->hasOne(InvoiceItem::className(), ['invoice_item_id' => 'invoice_item_id']);
    }
    
    public function getBalance()
    {

        //$data = StatementOfAccount::find($this->sa_id);
        $total= 0; $cap_bal = 0; $int_bal = 0;
        if($this->amount != 0){ 
           $cap_bal = $cap_bal +($this->amount - $this->payment); 
        }

        if($this->payment != 0){  
           $int_bal = $int_bal + ($this->amount - $this->payment);  
        }

        $total = $cap_bal+$int_bal;

        return $total;
    }
}
