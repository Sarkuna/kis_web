<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "total_product_list".
 *
 * @property integer $total_product_list_id
 * @property integer $clientID
 * @property integer $distributor_code
 * @property string $brand
 * @property string $product_name
 * @property string $product_name_distirbutor
 * @property string $region
 * @property string $iws
 * @property string $mz
 * @property string $del
 */
class TotalProductList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'total_product_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'distributor_code', 'brand', 'product_name', 'region', 'iws', 'mz'], 'required'],
            [['clientID', 'distributor_code'], 'integer'],
            [['brand', 'region'], 'string', 'max' => 50],
            [['product_name', 'product_name_distirbutor'], 'string', 'max' => 300],
            [['iws', 'mz'], 'string', 'max' => 3],
            [['del'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'total_product_list_id' => 'Total Product List ID',
            'clientID' => 'Client ID',
            'distributor_code' => 'Distributor Code',
            'brand' => 'Brand',
            'product_name' => 'Product Name',
            'product_name_distirbutor' => 'Product Name Distirbutor',
            'region' => 'Region',
            'iws' => 'IWS',
            'mz' => 'MZ',
            'del' => 'Del',
        ];
    }
}
