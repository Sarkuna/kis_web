<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TotalProductList;

/**
 * TotalProductListSearch represents the model behind the search form about `common\models\TotalProductList`.
 */
class TotalProductListSearch extends TotalProductList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['total_product_list_id', 'clientID', 'distributor_code'], 'integer'],
            [['brand', 'product_name', 'product_name_distirbutor', 'region', 'iws', 'mz', 'del'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TotalProductList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'total_product_list_id' => $this->total_product_list_id,
            'clientID' => $this->clientID,
            'distributor_code' => $this->distributor_code,
        ]);

        $query->andFilterWhere(['like', 'brand', $this->brand])
            ->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'product_name_distirbutor', $this->product_name_distirbutor])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'iws', $this->iws])
            ->andFilterWhere(['like', 'mz', $this->mz])
            ->andFilterWhere(['like', 'del', $this->del]);

        return $dataProvider;
    }
}
