<?php
namespace common\models;

use yii\base\Model;
use Yii;


class TrackingForm extends Model
{
    public $date_range;
    public $order_no;
    public $company_name;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //['date_range', 'required'],
            [['date_range','order_no', 'company_name'], 'safe'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'date_range' => 'Date Range',
            'order_no' => 'Order ID'
       ];
    }
    

    
}
