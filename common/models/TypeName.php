<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "type_name".
 *
 * @property integer $type_name_id
 * @property integer $clientID
 * @property string $type_name
 * @property integer $tier_id
 */
class TypeName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'name', 'tier_id'], 'required'],
            [['clientID', 'tier_id', 'reg_form'], 'integer'],
            [['type_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_name_id' => 'Type Name ID',
            'clientID' => 'Client ID',
            'name' => 'Name',
            'tier_id' => 'Tier ID',
        ];
    }
}
