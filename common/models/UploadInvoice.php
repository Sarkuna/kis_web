<?php

namespace common\models;

use Yii;
use yii\base\Model;
use common\models\VIPReceipt;

/**
 * This is the model class for table "vip_upload_invoice".
 *
 * @property string $image
 */
class UploadInvoice extends Model
{
    public $image;
    public $invoice_no;
    public $date_of_receipt;
    public $reseller_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['image', 'required'],  
            //[['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg,pdf', 'maxFiles' => 2],
            ['invoice_no', 'required'],
            ['invoice_no', 'uniqueInvoiceno'],
            [['date_of_receipt', 'reseller_name'], 'safe'],
            [['image'], 'file', 'skipOnEmpty' => true, 'checkExtensionByMimeType' => false, 'extensions' => 'jpg,gif,png,bmp,jpeg','wrongExtension'=>'You can only upload following files: {extensions}','maxSize' => 5242880, 'tooBig' => 'Maximum file size cannot exceed 5MB', 'maxFiles' => 4],
            //array('images', 'file', 'allowEmpty' => true, 'types' => 'gif,jpg,jpeg,png', 'maxSize' => 5242880, 'maxFiles' => 5, ), 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => 'Receipt[s]',
        ];
    }
    
    public function uniqueInvoiceno($attribute, $params)
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        if(
            $user = VIPReceipt::find()
                ->where(['invoice_no'=>$this->invoice_no, 'clientID' => $client_id])
                ->andWhere(['!=', 'status', 'D'])
                ->exists()
            //$user = User::->exists('email=:email',array('email'=>$this->email))
        )
          $this->addError($attribute, 'Invoice No "'.$this->invoice_no.'" has already been taken.');
    }
}
