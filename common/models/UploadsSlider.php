<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "uploads_slider".
 *
 * @property integer $upload_id
 * @property integer $clientID
 * @property string $file_name
 * @property string $real_filename
 * @property string $create_date
 */
class UploadsSlider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uploads_slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'file_name', 'real_filename'], 'required'],
            [['clientID'], 'integer'],
            [['create_date'], 'safe'],
            [['file_name', 'real_filename'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'upload_id' => 'Upload ID',
            'clientID' => 'Client ID',
            'file_name' => 'File Name',
            'real_filename' => 'Real Filename',
            'create_date' => 'Create Date',
        ];
    }
}
