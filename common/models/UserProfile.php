<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $profile_ID
 * @property integer $userID
 * @property string $first_name
 * @property string $last_name
 * @property string $company
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $postcode
 * @property string $region
 * @property integer $country
 * @property resource $image
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }
    
    public $photo;
    public $email;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['userID', 'country'], 'integer'],
            [['image'], 'string'],
            [['first_name', 'last_name', 'company', 'address1', 'address2'], 'string', 'max' => 200],
            [['city', 'region'], 'string', 'max' => 100],
            [['postcode'], 'string', 'max' => 50],
            [['tel'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'profile_ID' => 'Profile  ID',
            'userID' => 'User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'company' => 'Company',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'city' => 'City',
            'postcode' => 'Postcode',
            'region' => 'Region',
            'country' => 'Country',
            'image' => 'Image',
            'tel' => 'Tel'
        ];
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userID']);
    }
    
    public function getCountrys()
    {
        return $this->hasOne(VipCountry::className(), ['country_id' => 'country']);
    }
    
}
