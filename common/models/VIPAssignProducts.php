<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_assign_products".
 *
 * @property integer $vip_assign_products_id
 * @property integer $clientID
 * @property integer $productID
 * @property string $assign_status
 * @property string $p_type
 * @property string $p_value
 * @property string $date_assign
 * @property string $msrp
 * @property string $partner_price
 * @property string $featured_status
 *
 * @property VipProduct $product
 */
class VIPAssignProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_assign_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'productID', 'date_assign', 'msrp'], 'required'],
            [['clientID', 'productID'], 'integer'],
            [['date_assign'], 'safe'],
            [['msrp', 'partner_price'], 'number'],
            [['assign_status', 'p_type', 'featured_status'], 'string', 'max' => 1],
            [['p_value'], 'string', 'max' => 20],
            [['productID'], 'exist', 'skipOnError' => true, 'targetClass' => VIPProduct::className(), 'targetAttribute' => ['productID' => 'product_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_assign_products_id' => 'Vip Assign Products ID',
            'clientID' => 'Client ID',
            'productID' => 'Product ID',
            'assign_status' => 'Assign Status',
            'p_type' => 'Type',
            'p_value' => 'Value',
            'date_assign' => 'Date Assign',
            'msrp' => 'MSRP',
            'partner_price' => 'Partner Price',
            'featured_status' => 'Featured Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(VIPProduct::className(), ['product_id' => 'productID']);
    }
}
