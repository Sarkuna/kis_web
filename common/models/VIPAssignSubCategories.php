<?php

namespace common\models;

use Yii;
use common\models\VIPSubCategories;
/**
 * This is the model class for table "vip_assign_sub_categories".
 *
 * @property integer $vip_assign_sub_categories_id
 * @property integer $clientID
 * @property integer $categories_id
 * @property integer $sub_categories_id
 * @property string $status
 */
class VIPAssignSubCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_assign_sub_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'categories_id', 'sub_categories_id'], 'required'],
            [['clientID', 'categories_id', 'sub_categories_id'], 'integer'],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_assign_sub_categories_id' => 'Vip Assign Sub Categories ID',
            'clientID' => 'Client ID',
            'categories_id' => 'Categories ID',
            'sub_categories_id' => 'Sub Categories ID',
            'status' => 'Status',
        ];
    }
    
    public function getSubcategories()
    {
        //return $this->hasOne(VIPCategories::className(), ['vip_categories_id' => 'categories_id']);
        return $this->hasOne(VIPSubCategories::className(), ['vip_sub_categories_id' => 'sub_categories_id']);
    }
}
