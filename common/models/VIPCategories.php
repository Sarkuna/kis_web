<?php

namespace common\models;

use Yii;

use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

use common\models\VIPAssignCategories;

/**
 * This is the model class for table "vip_categories".
 *
 * @property integer $vip_categories_id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $image
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $position
 * @property integer $is_active
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property VipSubCategories[] $vipSubCategories
 */
class VIPCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_categories';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['description'], 'string'],
            [['position'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 45],
            [['is_active'], 'string', 'max' => 1],
            [['image', 'meta_title'], 'string', 'max' => 80],
            [['meta_keywords'], 'string', 'max' => 150],
            [['meta_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_categories_id' => 'Vip Categories ID',
            'name' => 'Category Name',
            'slug' => 'Slug',
            'description' => 'Description',
            'image' => 'Image',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'position' => 'Sort Order',
            'is_active' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVipSubCategories()
    {
        return $this->hasMany(VipSubCategories::className(), ['parent_id' => 'vip_categories_id']);
    }
    
    public function getAssignCategories()
    {
        return $this->hasMany(VIPAssignCategories::className(), ['categories_id' => 'vip_categories_id']);
    }
}
