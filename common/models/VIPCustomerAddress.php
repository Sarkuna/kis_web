<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "vip_customer_address".
 *
 * @property integer $vip_customer_address_id
 * @property integer $userID
 * @property integer $clientID
 * @property string $firstname
 * @property string $lastname
 * @property string $company
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property string $postcode
 * @property integer $country_id
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class VIPCustomerAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_customer_address';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'address_1', 'city', 'country_id','city', 'state', 'postcode'], 'required'],
            [['userID', 'clientID', 'country_id', 'state','created_by', 'updated_by'], 'integer'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['firstname', 'lastname'], 'string', 'max' => 32],
            [['company'], 'string', 'max' => 40],
            [['address_1', 'address_2', 'city'], 'string', 'max' => 128],
            [['postcode'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_customer_address_id' => 'Vip Customer Address ID',
            'userID' => 'User ID',
            'clientID' => 'Client ID',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'company' => 'Company',
            'address_1' => 'Address 1',
            'address_2' => 'Address 2',
            'city' => 'City',
            'state' => 'Region / State',
            'postcode' => 'Postcode',
            'country_id' => 'Country',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getCountry() {
        return $this->hasOne(\common\models\VipCountry::className(), ['country_id' => 'country_id']);
    }
    
    public function getStates() {
        return $this->hasOne(\common\models\VIPStates::className(), ['states_id' => 'state']);
    }
}
