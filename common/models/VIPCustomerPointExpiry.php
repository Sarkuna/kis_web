<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_customer_point_expiry".
 *
 * @property integer $vip_customer_point_expiry_id
 * @property integer $clientID
 * @property integer $customer_id
 * @property integer $points
 * @property string $description
 * @property string $date_expiry
 * @property string $date_add
 */
class VIPCustomerPointExpiry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_customer_point_expiry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'customer_id', 'points'], 'required'],
            [['clientID', 'customer_id', 'points'], 'integer'],
            [['description'], 'string'],
            [['date_expiry', 'date_add'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_customer_point_expiry_id' => 'Vip Customer Point Expiry ID',
            'clientID' => 'Client ID',
            'customer_id' => 'Customer ID',
            'points' => 'Points',
            'description' => 'Description',
            'date_expiry' => 'Date Expiry',
            'date_add' => 'Date Add',
        ];
    }
}
