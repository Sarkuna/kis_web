<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

use common\models\VIPOrderStatus;
/**
 * This is the model class for table "vip_order_history".
 *
 * @property integer $order_history_id
 * @property integer $order_id
 * @property integer $order_status_id
 * @property integer $notify
 * @property string $comment
 * @property string $date_added
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class VIPOrderHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_order_history';
    }
    
    /**
     * @behaviors
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'order_status_id', 'comment', 'date_added'], 'required'],
            [['order_id', 'order_status_id', 'notify', 'created_by', 'updated_by'], 'integer'],
            [['comment'], 'string'],
            [['date_added', 'created_datetime', 'updated_datetime','type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_history_id' => 'Order History ID',
            'order_id' => 'Order ID',
            'order_status_id' => 'Order Status ID',
            'notify' => 'Notify',
            'comment' => 'Comment',
            'date_added' => 'Date Added',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getOrderstatus()
    {
        return $this->hasOne(VIPOrderStatus::className(), ['order_status_id' => 'order_status_id']);
    }
    
    public function getBy()
    {
        return $this->hasOne(UserProfile::className(), ['userID' => 'created_by']);
    }
}
