<?php

namespace common\models;

use Yii;
use common\models\VIPOrder;

/**
 * This is the model class for table "vip_order_product".
 *
 * @property integer $order_product_id
 * @property integer $order_id
 * @property integer $product_id
 * @property string $name
 * @property string $model
 * @property integer $quantity
 * @property integer $point
 * @property integer $point_admin
 * @property integer $point_total
 * @property string $price
 * @property string $total
 * @property string $shipping_price
 * @property string $shipping_price_total
 * @property integer $shipping_point
 * @property integer $shipping_point_total
 * @property integer $product_status
 * @property integer $invoice_item_id
 * @property string $tracking_type
 * @property string $shipped_date
 * @property string $tracking_no
 * @property string $tracking_link
 *
 * @property VipOrder $order
 */
class VIPOrderProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_order_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'name', 'model', 'quantity'], 'required'],
            [['order_id', 'product_id', 'quantity', 'point', 'point_admin', 'point_total', 'shipping_point', 'shipping_point_total', 'product_status', 'invoice_item_id'], 'integer'],
            [['price', 'total', 'shipping_price', 'shipping_price_total'], 'number'],
            [['shipped_date'], 'safe'],
            [['tracking_no', 'tracking_link'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['model'], 'string', 'max' => 64],
            [['tracking_type'], 'string', 'max' => 1],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => VipOrder::className(), 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_product_id' => 'Order Product ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'name' => 'Name',
            'model' => 'Model',
            'quantity' => 'Quantity',
            'point' => 'Point',
            'point_admin' => 'Point Admin',
            'point_total' => 'Point Total',
            'price' => 'Price',
            'total' => 'Total',
            'shipping_price' => 'Shipping Price',
            'shipping_price_total' => 'Shipping Price Total',
            'shipping_point' => 'Shipping Point',
            'shipping_point_total' => 'Shipping Point Total',
            'product_status' => 'Product Status',
            'invoice_item_id' => 'Invoice Item ID',
            'tracking_type' => 'Tracking Type',
            'shipped_date' => 'Shipped Date',
            'tracking_no' => 'Tracking No',
            'tracking_link' => 'Tracking Link',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(VipOrder::className(), ['order_id' => 'order_id']);
    }
    
    public function getInvoiceitem()
    {
        return $this->hasOne(InvoiceItem::className(), ['invoice_item_id' => 'invoice_item_id']);
    }
    
    public function getMasterProduct()
    {
        return $this->hasOne(VIPProduct::className(), ['product_id' => 'product_id']);
    }
    
    public function getInvoiceno()
    {
        return $this->hasOne(InvoiceItem::className(), ['invoice_item_id' => 'invoice_item_id']);
    }
}
