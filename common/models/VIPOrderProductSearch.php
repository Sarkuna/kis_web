<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPOrderProduct;

/**
 * VIPOrderProductSearch represents the model behind the search form about `common\models\VIPOrderProduct`.
 */
class VIPOrderProductSearch extends VIPOrderProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_product_id', 'order_id', 'product_id', 'quantity', 'point', 'point_admin', 'point_total', 'shipping_point', 'shipping_point_total', 'product_status', 'invoice_item_id'], 'integer'],
            [['name', 'model', 'tracking_type', 'shipped_date', 'tracking_no', 'tracking_link'], 'safe'],
            [['price', 'total', 'shipping_price', 'shipping_price_total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $session = Yii::$app->session;
        
        $query = VIPOrderProduct::find()->joinWith(['order']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        //$query->andFilterWhere(['!=', 'invoice_item_id', 0]);
        $query->andFilterWhere(['vip_order.clientID' => $session['currentclientID']]);
        $query->andFilterWhere(['vip_order.order_status_id' => 20]);
        $query->andFilterWhere(['vip_order.order_id' => $this->order_id]);
        // grid filtering conditions
        $query->andFilterWhere([
            'order_product_id' => $this->order_product_id,
            //'order_id' => $this->order_id,
            'product_id' => $this->product_id,
            'quantity' => $this->quantity,
            'point' => $this->point,
            'point_admin' => $this->point_admin,
            'point_total' => $this->point_total,
            'price' => $this->price,
            'total' => $this->total,
            'shipping_price' => $this->shipping_price,
            'shipping_price_total' => $this->shipping_price_total,
            'shipping_point' => $this->shipping_point,
            'shipping_point_total' => $this->shipping_point_total,
            'product_status' => $this->product_status,
            'invoice_item_id' => $this->invoice_item_id,
            'shipped_date' => $this->shipped_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'tracking_type', $this->tracking_type])
            ->andFilterWhere(['like', 'tracking_no', $this->tracking_no])
            ->andFilterWhere(['like', 'tracking_link', $this->tracking_link]);

        return $dataProvider;
    }
}
