<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_order_status".
 *
 * @property integer $order_status_id
 * @property integer $language_id
 * @property string $name
 * @property string $description
 * @property string $action_done_by
 * @property string $action
 * @property string $labelbg
 */
class VIPOrderStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_order_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'name'], 'required'],
            [['language_id'], 'integer'],
            [['name'], 'string', 'max' => 32],
            [['description'], 'string', 'max' => 255],
            [['action_done_by', 'action'], 'string', 'max' => 100],
            [['labelbg'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_status_id' => 'Order Status ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
            'description' => 'Description',
            'action_done_by' => 'Action Done By',
            'action' => 'Action',
            'labelbg' => 'Labelbg',
        ];
    }
}
