<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPProduct;

/**
 * VIPProductSearch represents the model behind the search form about `common\models\VIPProduct`.
 */
class VIPProductSearch extends VIPProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'manufacturer_id', 'category_id', 'sub_category_id', 'points_value', 'stock_status_id', 'minimum', 'quantity', 'created_by', 'updated_by'], 'integer'],
            [['product_name', 'product_description', 'meta_tag_title', 'main_image', 'product_code', 'status', 'created_datetime', 'updated_datetime'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['product_id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_id' => $this->product_id,
            'manufacturer_id' => $this->manufacturer_id,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'points_value' => $this->points_value,
            'stock_status_id' => $this->stock_status_id,
            'minimum' => $this->minimum,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'product_description', $this->product_description])
            ->andFilterWhere(['like', 'meta_tag_title', $this->meta_tag_title])
            ->andFilterWhere(['like', 'main_image', $this->main_image])
            ->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
    
    public function searchassign($params)
    {
        //print_r($this->category_id);
        //AND category_id = 8
        //die();
        //if(!empty($this->category_id))
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $sql = 'SELECT * FROM vip_product WHERE NOT EXISTS (SELECT * from vip_assign_products WHERE vip_product.product_id =vip_assign_products.productID AND clientID = '.$clientID.') AND category_id = '.$this->category_id.' AND sub_category_id = '.$this->sub_category_id.'';
        $query = VIPProduct::findBySql($sql);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_id' => $this->product_id,
            'manufacturer_id' => $this->manufacturer_id,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'points_value' => $this->points_value,
            'stock_status_id' => $this->stock_status_id,
            'minimum' => $this->minimum,
            'price' => $this->price,
            'quantity' => $this->quantity,
        ]);

        $query->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'product_description', $this->product_description])
            ->andFilterWhere(['like', 'meta_tag_title', $this->meta_tag_title])
            ->andFilterWhere(['like', 'main_image', $this->main_image])
            ->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
