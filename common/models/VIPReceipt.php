<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

use common\models\VIPReceiptList;
use common\models\ClientOption;

/**
 * This is the model class for table "vip_receipt".
 *
 * @property integer $vip_receipt_id
 * @property integer $userID
 * @property integer $clientID
 * @property string $order_num
 * @property string $date_of_receipt
 * @property integer $reseller_name
 * @property integer $state
 * @property string $invoice_no
 * @property integer $handwritten_receipt
 * @property string $status
 * @property string $order_IP
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class VIPReceipt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $product_name;
    public $product_description;
    public $vqty;
    public $price_total;
    public $price_per_product;
    public $total_items;
    public $remark;
    
    
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }
    
    public static function tableName()
    {
        return 'vip_receipt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'clientID', 'invoice_no'], 'required'],
            [['date_of_receipt', 'reseller_name'],
                'required',
                'when' => function($model) {
                        return $model->status != 'D';
                    },
                'whenClient' => "function (attribute, value) {
                return $('#vipreceipt-status').val() != 'D';
            }",],
            [['reasons_id'],
                'required',
                'when' => function($model) {
                        return $model->status == 'D';
                    },
                'whenClient' => "function (attribute, value) {
                return $('#vipreceipt-status').val() == 'D';
            }",],                
            [['userID', 'clientID', 'reseller_name', 'handwritten_receipt', 'created_by', 'updated_by', 'reasons_id'], 'integer'],
            [['date_of_receipt', 'created_datetime', 'updated_datetime', 'price_total', 'price_per_product','remark','selected_image','state','assign_name'], 'safe'],
            [['order_num'], 'string', 'max' => 50],
            [['invoice_no'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 1],
            [['order_IP'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_receipt_id' => 'Vip Receipt ID',
            'userID' => 'User ID',
            'clientID' => 'Client ID',
            'order_num' => 'Order Num',
            'date_of_receipt' => 'Date Of Receipt',
            'reseller_name' => 'Reseller Name',
            'state' => 'State',
            'invoice_no' => 'Invoice No',
            'handwritten_receipt' => 'Handwritten Receipt',
            'status' => 'Status',
            'vqty' => 'Qty',
            'price_total' => 'Price',
            'price_per_product' => 'Price per Product',
            'order_IP' => 'Order  Ip',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'assign_name' => 'Assign',
            'reasons_id' => 'Reasons'
        ];
    }
    
    public function getStatustext() {
        $returnValue = "";
        $client_status = $this->status;
        if ($client_status == "P") {
            $returnValue = '<span class="label label-warning">Pending</span>';
        } else if ($client_status == "N") {
            $returnValue = '<span class="label label-info">Processing</span>';
        } else if ($client_status == "A") {
            $returnValue = '<span class="label label-success">Approve</span>';
        } else if ($client_status == "D") {
            $returnValue = '<span class="label label-danger">Decline</span>';
        } else if ($client_status == "R") {
            $returnValue = '<span class="label label-default">Drafts</span>';
        } else if ($client_status == "M") {
            $returnValue = '<span class="label label-primary">Modify</span>';
        }
        return $returnValue; 
    }
    
    public function getStatustextuser() {
        $returnValue = "";
        $client_status = $this->status;
        if ($client_status == "P") {
            $returnValue = '<i class="fa fa-circle font-small-3 text-warning"></i> Pending';
        } else if ($client_status == "N") {
            $returnValue = '<i class="fa fa-circle font-small-3 text-info"></i> Processing';
        } else if ($client_status == "A") {
            $returnValue = '<i class="fa fa-circle font-small-3 text-success"></i> Approve';
        } else if ($client_status == "D") {
            $returnValue = '<i class="fa fa-circle font-small-3 text-danger"></i> Decline';
        } else if ($client_status == "R") {
            $returnValue = '<i class="fa fa-circle font-small-3 text-default"></i> Drafts';
        } else if ($client_status == "M") {
            $returnValue = '<i class="fa fa-circle font-small-3 text-primary"></i> Modify';
        }
        return $returnValue; 
    }
    
    public function getCustomer() {
        return $this->hasOne(\common\models\VIPCustomer::className(), ['userID' => 'userID']);
    }
    
    public function getReceiptList() {
        return $this->hasOne(\common\models\VIPReceiptList::className(), ['vip_receipt_id' => 'vip_receipt_id']);
    }
    
    public function getReseller() {
        return $this->hasOne(\common\models\CompanyInformation::className(), ['user_id' => 'reseller_name']);
    }
    
    
    public function getDistributor() {
        return $this->hasOne(\common\models\VIPCustomer::className(), ['vip_customer_id' => 'reseller_name']);
    }
    
    public function getTotalItems(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return VIPReceiptList::find()->where(['vip_receipt_id' => $this->vip_receipt_id])->count();
    }
    
    public function getReasons(){
        return $this->hasOne(\common\models\Reasons::className(), ['reasons_id' => 'reasons_id']);
        
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\VIPReceiptList::className(), ['vip_receipt_id' => 'vip_receipt_id']);
    }
    
    public function getTotalPoints(){
        //return Comment::find()->where(['post' => $this->id])->count();
        $rd = VIPReceiptList::find()->where(['vip_receipt_id' => $this->vip_receipt_id])->all();
        //echo count($rd);
        $sum = 0;
        $ab = 0;
        foreach($rd as $r){
            $ab = $r->product_per_value * $r->product_item_qty;
            $sum += $ab;
        }
        return $sum;
    }
    
    public function getTotalpoint()
    {
        //return 'test'.$this->point_order_id;
        //'userid' => [1001,1002,1003,1004,1005],

        $rd = VIPReceiptList::find()->where(['vip_receipt_id' => $this->vip_receipt_id])->all();
        $sum = 0;
        $ab = 0;
        foreach($rd as $r){
            $ab = $r->product_per_value * $r->product_item_qty * $r->user_selector * $r->reseller_selector * $r->profile_selector;
            $sum += $ab;
        }
        return $sum;
    }
    
    public function getClientoption()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $rd = ClientOption::find()->where(['clientID' => $clientID])->one();
        return $rd->receipts_prefix;

    }
    
    public function getCompany() {
        return $this->hasOne(CompanyInformation::className(), ['user_id' => 'reseller_name']);
    }
    
    public function getIndirectCompany() {
        return $this->hasOne(CompanyInformation::className(), ['user_id' => 'userID']);
    }
    
}