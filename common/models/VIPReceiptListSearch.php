<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPReceiptList;

/**
 * VIPReceiptListSearch represents the model behind the search form about `common\models\VIPReceiptList`.
 */
class VIPReceiptListSearch extends VIPReceiptList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vip_receipt_list_id', 'vip_receipt_id', 'userID', 'clientID', 'product_list_id', 'product_per_value', 'product_item_qty', 'user_selector', 'reseller_selector', 'profile_selector'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPReceiptList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vip_receipt_list_id' => $this->vip_receipt_list_id,
            'vip_receipt_id' => $this->vip_receipt_id,
            'userID' => $this->userID,
            'clientID' => $this->clientID,
            'product_list_id' => $this->product_list_id,
            'product_per_value' => $this->product_per_value,
            'product_item_qty' => $this->product_item_qty,
            'user_selector' => $this->user_selector,
            'reseller_selector' => $this->reseller_selector,
            'profile_selector' => $this->profile_selector,
        ]);

        return $dataProvider;
    }
}
