<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPReceipt;

/**
 * VIPReceiptSearch represents the model behind the search form about `common\models\VIPReceipt`.
 */
class VIPReceiptSearch extends VIPReceipt
{
    /**
     * @inheritdoc
     */
    public $full_name;
    public $mobile_no;
    public $company_name;
    public $company_name1;
    public $globalSearch;
    
    public function rules()
    {
        return [
            [['vip_receipt_id', 'userID', 'clientID', 'reseller_name', 'state', 'handwritten_receipt', 'created_by', 'updated_by'], 'integer'],
            [['order_num', 'date_of_receipt', 'invoice_no', 'status', 'order_IP', 'created_datetime', 'updated_datetime','full_name', 'mobile_no','company_name', 'company_name1', 'assign_name','globalSearch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPReceipt::find();
        $query->joinWith(['customer', 'reseller',
                'indirectCompany' => function ($q) {
                        $q->from('company_information uc');
                }
            ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_datetime'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vip_receipt_id' => $this->vip_receipt_id,
            'userID' => $this->userID,
            'vip_receipt.clientID' => $this->clientID,
            //'date_of_receipt' => date('Y-m-d', strtotime($this->date_of_receipt)),
            'reseller_name' => $this->reseller_name,
            'state' => $this->state,
            'handwritten_receipt' => $this->handwritten_receipt,
            //'vip_receipt.created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
        
        
        if(!empty($this->date_of_receipt)) {
            $date = date('Y-m-d', strtotime($this->date_of_receipt));
        }else {
            $date = $this->date_of_receipt;
        }
        
        $query->andFilterWhere(['like', 'order_num', $this->order_num])
            ->andFilterWhere(['like', 'invoice_no', $this->invoice_no])
            ->andFilterWhere(['like', 'vip_receipt.status', $this->status])
            ->andFilterWhere(['like', 'assign_name', $this->assign_name])
            ->andFilterWhere(['=', 'date_of_receipt', $date])
            //->andFilterWhere(['between', 'created_datetime', $start, $end])     
            ->andFilterWhere(['like', 'vip_customer.full_name', $this->full_name])
            ->andFilterWhere(['like', 'vip_customer.mobile_no', $this->mobile_no])
            ->andFilterWhere(['like', 'company_information.company_name', $this->company_name])
            ->andFilterWhere(['like', 'uc.company_name', $this->company_name1])     
            ->andFilterWhere(['like', 'order_IP', $this->order_IP]);
        
        if(!empty($this->created_datetime)) {
            $start = date('Y-m-d', strtotime($this->created_datetime));
            $end = date('Y-m-d', strtotime($this->created_datetime));
            
            $start1 = $start.' 00:00:00';
            $end1 = $end.' 23:59:59';
            /*$query->andFilterWhere(['>=', 'vip_receipt.created_datetime', $start1])
                    ->andFilterWhere(['<=', 'vip_receipt.created_datetime', $end1]);*/
            $query->andFilterWhere(['between', 'vip_receipt.created_datetime', $start1, $end1]);
        }

        return $dataProvider;
    }
    
    public function searchall($params)
    {
        $query = VIPReceipt::find();
        $query->joinWith(['customer', 'reseller',
                'indirectCompany' => function ($q) {
                        $q->from('company_information uc');
                }
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_datetime'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        // grid filtering conditions
        if(!empty($this->globalSearch)) {
        $query->orFilterWhere(['=', 'order_num', $this->globalSearch])
            ->orFilterWhere(['=', 'invoice_no', $this->globalSearch])    
            ->orFilterWhere(['like', 'vip_customer.full_name', $this->globalSearch])
            ->orFilterWhere(['like', 'vip_customer.mobile_no', $this->globalSearch])
            ->orFilterWhere(['like', 'company_information.company_name', $this->globalSearch]);
        }else {
            $query->andFilterWhere([
                'vip_receipt_id' => $this->vip_receipt_id,
                'userID' => $this->userID,
                'vip_receipt.clientID' => $this->clientID,
                //'date_of_receipt' => date('Y-m-d', strtotime($this->date_of_receipt)),
                'reseller_name' => $this->reseller_name,
                'state' => $this->state,
                'handwritten_receipt' => $this->handwritten_receipt,
                //'vip_receipt.created_datetime' => $this->created_datetime,
                'updated_datetime' => $this->updated_datetime,
                'created_by' => $this->created_by,
                'updated_by' => $this->updated_by,
            ]);
        }
        
        

        return $dataProvider;
    }
    
    public function searchuser($params)
    {
        $query = VIPReceipt::find();
        //$query->joinWith(['customer']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vip_receipt_id' => $this->vip_receipt_id,
            'userID' => $this->userID,
            'clientID' => $this->clientID,
            //'date_of_receipt' => date('Y-m-d', strtotime($this->date_of_receipt)),
            'reseller_name' => $this->reseller_name,
            'state' => $this->state,
            'handwritten_receipt' => $this->handwritten_receipt,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
        

        $query->andFilterWhere(['like', 'order_num', $this->order_num])
            ->andFilterWhere(['like', 'invoice_no', $this->invoice_no])
            ->andFilterWhere(['like', 'status', $this->status]);;
        


        return $dataProvider;
    }
}