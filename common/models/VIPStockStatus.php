<?php

namespace common\models;

use Yii;
use common\models\VIPProduct;
/**
 * This is the model class for table "vip_stock_status".
 *
 * @property integer $stock_status_id
 * @property string $name
 *
 * @property VipProduct $vipProduct
 */
class VIPStockStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_stock_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'stock_status_id' => 'Stock Status ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVipProduct()
    {
        return $this->hasOne(VIPProduct::className(), ['stock_status_id' => 'stock_status_id']);
    }
}
