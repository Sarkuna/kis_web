<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * 
 */
class PointsSummaryController extends Controller {

    public function actionIndex() {
        $created_datetime = date('Y-m-d H:i:s');
        $type = array(1, 2, 5);
        $result = \common\models\VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                'user' => function($que) {
                    $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                },
                        'companyInfo' => function($que) {
                    $que->select(['user_id', 'company_name']);
                },
            ])
            ->where(['clientID' => 14])
            ->where(['IN', 'user.type', $type])
            //->andWhere(['not', ['mobile_no' => null]])            
            ->andwhere(['IN', 'user.status', 'A'])
            ->andwhere(['!=', 'company_information.company_name', 'kansai_reserve'])
            ->orderBy(['user.created_datetime' => SORT_DESC])->all();

        $mydata = array();
        $mydataemail = array();
        foreach ($result as $model) {
            $balance = $model['totalAdd'] - $model['totalMinus'] - $model['expired'] + $model['adjustment'];
            $goingtoexpire = $model['goingtoExpire'] - $model['currentYearRedeemed'];

            if ($goingtoexpire > 0) {
                if (!empty($model['full_name'])) {
                    $name = $model['companyInfo']['company_name'];
                } else {
                    $name = $model['full_name'];
                }

                if (!empty($model['companyInfo']['company_name'])) {
                    $company_name = $model['companyInfo']['company_name'];
                } else {
                    $company_name = 'NA';
                }

                $jsonData = \yii\helpers\Json::encode(array(
                            'name' => $name,
                            'company_name' => $company_name,
                            'expired_point' => $goingtoexpire,
                            'expired_date' => '31-12-' . date('y'),
                            'today_date' => date('d-m-y'),
                            'balance_point' => $balance
                ));

                if (!empty($model['mobile_no'])) {
                    $mydata[] = [4, 'reminder', 14, $model['userID'], $model['mobile_no'], $jsonData, 'P', $created_datetime];
                }

                if (!empty($model['user']['email'])) {
                    $mydataemail[] = [109, 'R', 14, $model['userID'], $model['clients_ref_no'], $model['user']['email'], $jsonData, 'P', $created_datetime];
                }
            }
        }

        if ($mydata > 0) {
            Yii::$app->db
                    ->createCommand()
                    ->batchInsert('sms_queue', ['sms_template_id', 'sms_type', 'clientID', 'user_id', 'mobile', 'data', 'status', 'created_datetime'], $mydata)
                    ->execute();
        }

        if ($mydataemail > 0) {
            Yii::$app->db
                    ->createCommand()
                    ->batchInsert('email_queue', ['email_template_id', 'email_queue_type', 'clientID', 'user_id', 'name', 'email', 'data', 'status', 'created_datetime'], $mydataemail)
                    ->execute();
        }
        exit;
    }
}