<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * 
 */
class SendemailController extends Controller {

    public function actionIndex() {

        $siteUrl = Yii::$app->params['siteUrl'];
        
        foreach (\common\models\EmailQueue::find()->where("email_queue_type= 'R' AND status = 'P' AND email_template_id = '109' AND (email IS NOT NULL)")->limit(100)->all() as $emailQueue) {
            //Check date_to_send 
            $today = new \DateTime('now');

            $emailTemplate = \common\models\EmailTemplate::findOne($emailQueue->email_template_id);

            $emailBody = $emailTemplate->template;
            $emailSubject = $emailTemplate->subject;
            
            $userlisit = \common\models\VIPCustomer::find()     
                        ->where(['userID' => $emailQueue->user_id]) 
                        ->one();
            

            $data2 = \yii\helpers\Json::decode($emailQueue['data']);

            $name = isset($data2['name']) ? $data2['name'] : null;
            $company_name = isset($data2['company_name']) ? $data2['company_name'] : null;
            $expired_point = isset($data2['expired_point']) ? $data2['expired_point'] : null;
            $expired_date  = isset($data2['expired_date']) ? $data2['expired_date'] : null;
            $today_date  = isset($data2['today_date']) ? $data2['today_date'] : null;
            $balance_point = isset($data2['balance_point']) ? $data2['balance_point'] : null;
            
            if(empty($name)) {
                $name = $userlisit->clients_ref_no;
            }

            $emailBody = str_replace('{name}', $name, $emailBody);
            $emailBody = str_replace('{company_name}', $company_name, $emailBody);
            $emailBody = str_replace('{expired_point}', $expired_point, $emailBody);
            $emailBody = str_replace('{expired_date}', $expired_date, $emailBody);
            $emailBody = str_replace('{today_date}', $today_date, $emailBody);
            $emailBody = str_replace('{balance_point}', $balance_point, $emailBody);    
            $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
            
            $emailSubject = str_replace('{name}', $name, $emailSubject);




            if (!empty($emailQueue->email)) {
                Yii::$app->mailer->compose()
                    ->setTo($emailQueue->email)
                    ->setFrom([Yii::$app->params['supportEmail'] =>  'Mykawankansai'])
                    //->setBcc('shihanagni@gmail.com')
                    ->setSubject($emailSubject)
                    ->setHtmlBody($emailBody)
                    ->send();
                
                $emailQueueToUpdate = \common\models\EmailQueue::findOne($emailQueue->id);
                $emailQueueToUpdate->status = "C";
                $emailQueueToUpdate->date_to_send = date('Y-m-d');
                $emailQueueToUpdate->save();
            }

            
        }
        exit();
    }

}
