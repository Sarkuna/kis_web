<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class LoginUpdate extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        //<!-- Google Webfont -->
        '//fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900',
        '//fonts.googleapis.com/css?family=Lato:400,100,300,300italic,700,900',
        '//fonts.googleapis.com/css?family=Montserrat:400,700',

        //<!-- CSS -->
        'themes/register/bootstrap.min.css',
        'themes/register/AdminLTE.min.css',
    ];
    public $js = [
        '//code.jquery.com/ui/1.11.4/jquery-ui.min.js',
        //'themes/register/jquery.min.js',
        'themes/register/bootstrap.min.js'
        
    ];
    /*public $jsOptions = [
    	'position' => \yii\web\View::POS_HEAD
    ];*/
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
