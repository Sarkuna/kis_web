<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RegCreativeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css',
        '//fonts.googleapis.com/css?family=Montserrat:400,700,200',
        'themes/creative/assets/css/bootstrap1.min.css',
        'themes/creative/assets/css/now-ui-kit1.css?v=1.2.0',
        'themes/creative/assets/css/custom.css'
    ];   
    public $js = [
        '//code.jquery.com/ui/1.11.4/jquery-ui.min.js',
        //'themes/creative/assets/js/core/jquery.3.2.1.min.js',
        'themes/creative/assets/js/core/bootstrap.min.js',
        //'//demos.creative-tim.com/now-ui-kit-pro/assets/js/now-ui-kit.js?v=1.2.0',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
