<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class BreadcrumbsVuexy extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        return $this->render('breadcrumbsvuexy');
        
    }
}