<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class FeaturedProductsWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $query = \common\models\VIPProduct::find()
                ->joinWith('assignProduct')
                ->where(['status' => 'E', 'vip_assign_products.featured_status' => 'Y', 'vip_assign_products.clientID' => $clientID])->orderBy('vip_assign_products.date_assign DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
        ]);

        //return $this->render('products_bl_widget',array('bestsellerdataProvider'=>$bestsellerdataProvider,'lpdataProvider'=>$dataProvider2));      
        return $this->render('featuredproductswidget', ['products' => $dataProvider]);
        
    }
}