<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class H3HeaderWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $expired = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'E'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $expired = str_replace('-', '', $expired);
        if(empty($expired)){
            $expired = 0; 
         }

        $totaladd = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        
        if(empty($totaladd)){
            $totaladd = 0; 
         }
         
         $totalminus = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }

        
        $balance = $totaladd - $totalminus - $expired;
        
        
        $newDate = date('Y-m-d', strtotime("+6 months"));
        $pointsexpiring = 0;

        
        return $this->render('h3_header', [
            'totalavailablepoint' => $balance,
            'totalredeemedpoint' => $totalminus,
            'pointsexpiring' => $pointsexpiring,
            'newDate' => $newDate,
        ]);

        
    }
    
    function getY2() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
        
        
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y1E = $y1B + $y2V;
        $y2B = $y1B + $y2A + $y2V - $y1E;
        
        
        
        if(empty($y1B)){
            $y1B = 0; 
        }
        
        if(empty($y2B)){
            $y2B = 0; 
        }
        
        if($y1E < 0 ){
            $y1E = 0;
        }
         
        return $y1E;

    }
    
    function getY3() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
        
        
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y3A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y3V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y1E = $y1B + $y2V;
        if($y1E < 0){
            $y1E = 0;
        }
        $y2B = $y1B + $y2A + $y2V - $y1E;
        
        
        
        $y3B = $y2B + $y3V;
        
        
        if(empty($y1B)){
            $y1B = 0; 
        }
        
        if(empty($y2B)){
            $y2B = 0; 
        }
        
        if($y3B < 0 ){
            $y3B = 0;
        }
         
        return $y3B;

    }
    
    function getY5() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
        
        
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2B = $y2A + $y1B + $y2V;
        
        $y3A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y3V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y3B = $y3A + $y2B + $y3V;
        
        $tot = $y3B - $this->getY2() - $this->getY3();
        
        $adj = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'description' => 'Point Value Adjustment (0.36 to 0.5) - 2017'])       
        ->sum('points');
        
        //$tot = $y3B - $this->getY2() - $this->getY3() + $adj;
        $tot = $y3B - $this->getY2() - $this->getY3();
        
        $y4A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-12-31 23:59:59'])        
        ->sum('points');
        
        $y4V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-03-31 23:59:59'])        
        ->sum('points');
        
        $tot4 = $tot + $y4V;
        
        //$tot5 = $y4A + $tot + $y4V - $tot4;
        $tot5 = $y4A + $tot + $y4V - $tot4;
        //$tot $y4V
        $goingto = $tot + $y4V;
        
        if($goingto < 0) {
            $goingto = 0;
        }
        return $goingto;

    }
}