<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

use common\models\UploadsSlider;

class H3SliderWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $images = UploadsSlider::find()->where(['clientID'=>$session['currentclientID']])->all();
        return $this->render('h3_slider',
            [
                'images' => $images
            ]
        );
        
    }
}