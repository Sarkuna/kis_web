<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class H3TopBarWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        //$clientID = '11';
        $pages = \common\models\Menus::find()
                ->where(['clientID' => $clientID])
                ->andWhere(['like', 'display_location', 'header-menu'])
                ->one();

        return $this->render('h3_top_bar', [
            'pages' => $pages,
        ]);
        
    }
}