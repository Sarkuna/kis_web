<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use common\models\VIPAssignProducts;
//use yii\helpers\Html;

class ManufacturesAll extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        //$parent = $item['categories_id'];
        $parent = Yii::$app->getRequest()->getQueryParam('id');
        $sub_category_id = Yii::$app->getRequest()->getQueryParam('sid');

        $manufactureslisits = VIPAssignProducts::find()
        ->joinWith([
        'product' => function($que) {
            //$que->select(['product_id','category_id','sub_category_id','manufacturer_id']);
            $que->select(['product_id','category_id','sub_category_id','manufacturer_id'])->where(['not', ['manufacturer_id' => null]]);
        }])
        ->where(['=', 'vip_assign_products.clientID', $session['currentclientID']])         
        ->groupBy('vip_product.manufacturer_id')
        ->all();
        return $this->render('manufacturesall',['manufactureslisits' => $manufactureslisits]);
        
    }
}