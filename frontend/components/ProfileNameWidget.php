<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\data\ActiveDataProvider;
//use common\models\PointOrder;
//use app\modules\painter\models\PainterProfile;

class ProfileNameWidget extends Widget
{
    public $path;

    public function init()
    {
        
        parent::init();

    }

    public function run()
    {
        $session = Yii::$app->session;
        $userID = \Yii::$app->user->id;
        $profile = \common\models\VIPCustomer::find()
            ->where(['userID' => $userID])
            ->one();
        $firstname = $profile->full_name.' '.$profile->clients_ref_no;
        $session['distributors'] = $profile->kis_category_id;

        return $this->render('profilename',['firstname' => $firstname]);
        
    }
}