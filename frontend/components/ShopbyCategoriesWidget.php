<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use common\models\VIPAssignSubCategories;
//use yii\helpers\Html;

class ShopbyCategoriesWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        //$parent = $item['categories_id'];
        $parent = Yii::$app->getRequest()->getQueryParam('id');
        //$cname = $item->categories->name;
        $subcategorieslisits = VIPAssignSubCategories::find()
                ->joinWith(['subcategories'])
                ->where(['categories_id' => $parent, 'clientID' => $session['currentclientID']])
                ->orderBy([
                    'position' => SORT_ASC,
                ])
                ->all();
        return $this->render('shop_by_categories',['subcategorieslisits' => $subcategorieslisits]);
        
    }
}