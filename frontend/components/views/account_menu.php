<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$myurl = Yii::$app->request->url;
?>
<div class="col-md-3 col-sm-4 checkout-steps">
    <h6>My Account</h6>
    <div>
        <ul class="account-list">
            <li><a href="#"> <i class="fa fa-edit"></i> Account Information </a></li>
            <li class="<?= $myurl == '/accounts/my-account' ? 'active' : '' ?>"><a href="/accounts/my-account"> <i class="fa fa-edit"></i> My Account</a></li>                                        
            <li class="<?= $myurl == '/accounts/my-account/change-password' ? 'active' : '' ?>"><a href="/accounts/my-account/change-password"> <i class="fa fa-edit"></i> Change Password</a></li>
            <li class="<?= $myurl == '/accounts/my-account/address' ? 'active' : '' ?>"><a href="/accounts/my-account/address"> <i class="fa fa-edit"></i> Address Books</a></li>
            <li class="<?= $myurl == '/accounts/my-account/reward-points' ? 'active' : '' ?>"><a href="/accounts/my-account/reward-points"> <i class="fa fa-edit"></i> My Reward Points</a></li>
            <li><a href="order-history.html"> <i class="fa fa-edit"></i> Order History</a></li>
            <li><a href="return.html"> <i class="fa fa-edit"></i> Returns Requests</a></li>
            <li><a href="newsletter.html"> <i class="fa fa-edit"></i> Newsletter</a></li>
        </ul>                                
    </div>
</div>