<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

?>
<div class="row breadcrumbs-top">
    <div class="col-12">
        <?php
        if ( Url::toRoute( Url::home() ) == Url::toRoute( Yii::$app->controller->getRoute() ) ) {
            echo '<h2 class="content-header-title float-left mb-0">'.Html::encode($this->title).'</h2>';
        }
        ?>
        
        <div class="breadcrumb-wrapper col-12">
            <?php
            echo Breadcrumbs::widget([
                'tag' => 'ol',
                'homeLink' => ['label' => 'Home', 'url' => ['/index']],
                'activeItemTemplate' => "<li class='breadcrumb-item'>{link}</li>",
                'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>",
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                //'links' => '<li class="breadcrumb-item"><a href="{url}">{label}</a></li>',
                'encodeLabels' => false,
                //'delimiter'=>' / ',
                'options' => array('class' => 'breadcrumb'),
            ]);
            //<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i></li>
            ?>
        </div>
    </div>
</div>