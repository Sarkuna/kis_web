<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\ProfileNameWidget;
//$role = Yii::$app->session->get('currentRole');
$imgurl = $this->theme->basePath;
?>
<!-- FOOTER -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 widget-footer">
                <h5>My Account</h5>
                <ul class="list-unstyled">
                    <li><a href="/accounts/my-account">My Account</a></li>
                    <li><a href="/accounts/my-account/order">Order History</a></li>
                    <li><a href="/accounts/my-account/reward-points">Your Reward Points</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 widget-footer">
                <?= $footer1 ?>
            </div>
            <div class="col-md-3 col-sm-3 widget-footer">
                <h5>INFORMATION</h5>
                <ul>
                    <?php
                    if(!empty($footermenus->menuitems)) {
                    foreach($footermenus->menuitems as $page){
                        $title = Html::encode($page->title);
                        if ($page->related_id > 0) {
                            $link = '/' . $page->link;
                        } else {
                            $link = $page->link;
                        }
                        echo '<li><a href="'.$link.'" target="'.$page->target.'">'.$title.'</a></li>';
                    }
                    }
                    ?>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 widget-footer">
                <?= $footer2 ?>
            </div>

        </div>
    </div>
</footer>