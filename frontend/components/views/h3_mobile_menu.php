<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\ShoppingCartBagWidget;
use app\components\ProfileNameWidget;
//$role = Yii::$app->session->get('currentRole');
$imgurl = $this->theme->basePath;


$myclient = \common\models\Client::find()->where([
                'clientID' => $session['currentclientID'],
            ])->one();

$clientname = common\models\ClientAddress::find()->where([
    'clientID' => $session['currentclientID'],
])->one();

$session['currentclientID'] = $myclient->clientID;
$session['currentclientName'] = $myclient->company;
$session['currentLogo'] = $clientname->company_logo;
$session['adminURL'] = $myclient->admin_domain;
$session['membership'] = $myclient->membership;
$session['background'] = $myclient->background_img;
//$session['upload_receipt_module'] = $myclient->upload_receipt_module;


if(!empty($session['currentLogo'])){
    $img = $session['adminURL']."/upload/client_logos/".$session['currentLogo'];            
}else{
    $img = $imgurl.'images/basic/logo-lite.png';
}


?>
<div class="side-menu visible-xs">
    
    <div class="row no-padding">
        <div class="col-lg-12 col-xs-12 boxmobile">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?= Yii::$app->VIPglobal->myAvailablePoint() ?></h3>
                    <p>Total Available Point</p>
                </div>
                <a href="/accounts/my-account/reward-points" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-12 col-xs-12 boxmobile">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <?php
                        $exppoint = Yii::$app->VIPglobal->PointsExpireUser();
                    ?>
                    <h3><?= $exppoint ?></h3>

                    <p>Points Expiring 31-12-19</p>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <!-- ./col -->
        <div class="col-lg-12 col-xs-12 boxmobile">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= $totalredeemedpoint ?></h3>

                    <p>Total Redeemed Point</p>
                </div>   
                <a href="/accounts/my-account/order" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!--Welcome KANSAI DEALER<br>You have 3,697 points<br>Expiring on 31 March 2018: 0points-->
    </div>
    
    <?php
    if(Yii::$app->user->identity->type == 1){                                 
    ?>
    <div class="row no-padding">        
        <div class="col-lg-12 col-xs-12 boxmobile">
            <h3 style="color: #ffffff;">Account 2</h3>
            <!-- small box -->
            <div class="small-box bg-maroon" style="background-color: #12e1f3 !important;">
                <div class="inner">
                    <h3><?= $totalpoint_acc2 ?></h3>
                </div>   
                <a href="/accounts/my-account/account2" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <div id='cssmenu'>
        <ul>
            <li class='has-sub1'><a href="<?= Yii::$app->homeUrl ?>"><span>Home</span></a></li>

            <?php
            //$dfUrl = Yii::$app->params['dfUrl'];
            $dfUrl = '';
            $items = common\models\VIPAssignCategories::find()
                    ->joinWith(['categories'])
                    ->where(['clientID' => $session['currentclientID']])
                    ->orderBy([
                        'position' => SORT_ASC,
                            //'username' => SORT_DESC,
                    ])
                    ->all();
            //->all();
            $ur3 = Yii::$app->urlManager->createUrl(['' . $dfUrl . '/products/product/all']);
            echo '<li class="nav"><a href="'.$ur3.'">All Products</a></li>';
            $result = [];
            foreach ($items as $item) {
                $parent = $item['categories_id'];
                $cname = $item->categories->name;
                echo '<li class="has-sub"><a href="#"><span>' . $cname . '</span></a><ul>';
                $id = $item['categories_id'];
                $ur = Yii::$app->urlManager->createUrl(['' . $dfUrl . 'site/product', 'id' => $id]);
                //echo '<li><a href="'.$ur.'">'.$item['cat_name'].'</a></li>';
                $items2 = common\models\VIPAssignSubCategories::find()
                        ->joinWith(['subcategories'])
                        ->where(['categories_id' => $parent, 'clientID' => $session['currentclientID']])
                        ->orderBy([
                            'position' => SORT_ASC,
                                //'position' => SORT_DESC,
                        ])
                        //->orderBy('subname')
                        ->all();
                foreach ($items2 as $item2) {
                    //echo $item2['subname'];

                    $id = $item2['categories_id'];
                    $sid = $item2['sub_categories_id'];
                    $subcname = $item2['subcategories']['name'];
                    $ur2 = Yii::$app->urlManager->createUrl(['' . $dfUrl . '/products/product', 'id' => $id, 'sid' => $sid]);
                    echo '<li><a href="' . $ur2 . '">' . $subcname . '</a></li>';
                }
                echo '</ul></li>';
            }
            ?>
            <div class="space30"></div>
            <?php
            if(!empty($pages->menuitems)) {
                foreach ($pages->menuitems as $page) {
                    $title = Html::encode($page->title);
                    if ($page->related_id > 0) {
                        $link = '/' . $page->link;
                    } else {
                        $link = $page->link;
                    }
                    echo '<li><a href="' . $link . '" target="' . $page->target . '">' . $title . '</a></li>';
                }
            }
            ?>
        </ul>
    </div>
    
    
</div>
            