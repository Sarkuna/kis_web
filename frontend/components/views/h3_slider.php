<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$session = Yii::$app->session;
?>
<!-- SLIDER -->
<?php

if(count($images) > 1) {
    echo '<div class="container">
    <div class="row">
        <div class="slider-wrap">
            <div class="tp-banner-container">
                <div class="tp-banner slider-5">';
    echo '<ul>';
    foreach($images as $image){
        $img = Yii::getAlias('@back').'/upload/slider/'.$image->clientID.'/'.$image->real_filename;
        echo '<li data-transition="fade" data-slotamount="2" data-masterspeed="500" data-saveperformance="on"  data-title="Intro Slide">
            <img src="'.$img.'"  alt="slidebg1" data-lazyload="'.$img.'" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
        </li>';
    }
    
    echo '</ul>';

    echo '<div class="tp-bannertimer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>';
}
?>