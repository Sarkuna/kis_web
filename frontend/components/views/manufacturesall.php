<?php
use common\models\VIPAssignProducts;
$session = Yii::$app->session;

?>



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Manufactures
                </a>
            </h4>

        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <?php
                    echo '<ul class="cat-list">';
                    $manufacturescount = 0;
                    //echo '<li><a href="'.Yii::$app->urlManager->createUrl(['/products/product', 'id' => $parent, 'sid' => $sub_category_id]).'">All</a></li>';
                    foreach ($manufactureslisits as $lisit) {        
                        $id = $lisit->product->category_id;
                        $sid = $lisit->product->sub_category_id;
                        $subcname = $lisit->product->manufacturer->name;
                        //manufactureslisits
                        $manufacturescount = VIPAssignProducts::find()
                        ->joinWith([
                        'product' => function($que) {
                            //$que->select(['product_id','category_id','sub_category_id','manufacturer_id']);
                            $que->select(['product_id','category_id','sub_category_id','manufacturer_id'])->where(['not', ['manufacturer_id' => null]]);
                        }])
                        ->where(['=', 'vip_assign_products.clientID', $session['currentclientID']])
                        ->andWhere(['=', 'manufacturer_id', $lisit->product->manufacturer_id])         
                        ->count();
                        $ur2 = Yii::$app->urlManager->createUrl(['/products/product/all', 'manufacturer' => $lisit->product->manufacturer_id]);
                        echo '<li><a href="' . $ur2 . '" class="active">' . $subcname . '</a> ('.$manufacturescount.')</li>';
                    }
                    echo '</ul>';
                ?>
            </div>
        </div>
    </div>
</div>