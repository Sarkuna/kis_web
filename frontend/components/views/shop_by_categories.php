<?php
    echo '<h5>Categories</h5>';
    echo '<ul class="cat-list">';
    foreach ($subcategorieslisits as $lisit) {
        $id = $lisit['categories_id'];
        $sid = $lisit['sub_categories_id'];
        $subcname = $lisit['subcategories']['name'];
        $count = \common\models\VIPProduct::find()
                ->where(['category_id' => $id,'sub_category_id' => $sid])
                ->count();
        $ur2 = Yii::$app->urlManager->createUrl(['/products/product', 'id' => $id, 'sid' => $sid]);
        echo '<li><a href="' . $ur2 . '">' . $subcname . ' ('.$count.')</a></li>';
    }
    echo '</ul>';
?>