<?php

$carts = Yii::$app->session['cart'];
$itemcount = count($carts);

if ($itemcount > 0) {
    echo '<li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon feather icon-shopping-cart"></i><span class="badge badge-pill badge-primary badge-up">'.$itemcount.'</span></a>';
    echo '<ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">';
    $subtotal = 0;
    $total = 0;
    
    echo '<li class="dropdown-menu-header">
        <div class="dropdown-header m-0 p-2">
            <h3 class="white">'.$itemcount.' Items</h3><span class="notification-title">In Your Cart</span>
        </div>
    </li>';
    echo '<li class="scrollable-container media-list">';
    foreach ($carts as $key => $carlist) {
        if(!empty($carlist['main_image'])) {
            $thumbimage = $carlist['main_image'];
        }else {
            $thumbimage = 'no_image.png';
        }
        
        $totalpoint = $carlist['points_value'] * $carlist['product_qty'];
        echo '<div class="media d-flex align-items-start">
            <div class="media-left col-2" style="padding: 0px;">
                <img src="' . Yii::getAlias('@back') . '/upload/product_cover/thumbnail/' . $thumbimage . '" alt="item" class="img-thumbnail-cart">
            </div>
            <div class="media-body col-10" style="padding-right: 0px;">
                <h6 class="primary media-heading"><a href="/products/product/product-detail?id=' . $key . '">' . $carlist['product_name'] . '</a></h6>
                <small class="notification-text">' . $carlist['product_qty'] . ' X ' . $carlist['points_value'] . '</small>
                <small class="notification-text">' . $totalpoint . ' pts</small>
                <small class="notification-text pull-right ci-edit"><a class="deletebagitem edit" href="javascript::void(0)" onclick="deleteItem(' . $key . ');"><i class="fa fa-trash fa-2x text-danger" ></i></a></small>
            </div>
        </div>';

        $total += $totalpoint;
    }
    echo '</li>';
    echo '<li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center" href="/shopping/cart"><i class="feather icon-shopping-cart mr-50 font-medium-2"></i> Checkout</a></li>';
    echo '</ul>';
    echo '</li>';
} else {
    //echo '<div class="cart-info" style="height:auto;">Your shopping cart is empty!</div>';
    echo '<li class="nav-item"><a class="nav-link nav-link-label" href="/shopping/cart"><i class="ficon feather icon-shopping-cart"></i></a></li>';
}

?>
