<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use app\components\ProfileNameWidget;
//$role = Yii::$app->session->get('currentRole');
$imgurl = $this->theme->basePath;

$controller = Yii::$app->controller;
$default_controller = Yii::$app->defaultRoute;
$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;

if($isHome == true){
    echo '<div class="slider-wrap">
    <div class="tp-banner-container">
        <div class="tp-banner slider-4">
            <ul>
                <!-- SLIDE  -->
                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="2" data-masterspeed="500" data-thumb="homeslider_thumb1.jpg"  data-saveperformance="on"  data-title="Intro Slide">
                    <!-- MAIN IMAGE -->
                    <img src="'.$imgurl.'images/dummy.png"  alt="slidebg1" data-lazyload="'.$imgurl.'images/slides/watchbanner-1903x621.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <div class="tp-caption lft ss-color skewtoleftshort rs-parallaxlevel-9"
                         data-x="5"
                         data-y="220"
                         data-speed="1000"
                         data-start="1400"
                         data-easing="Power3.easeInOut"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-end="7300"
                         data-endspeed="1000"
                         style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;	font-family: Montserrat;
                                     font-size: 23px;
                                     font-weight: bold;
                                     text-transform: uppercase;
                                     color: #d6644a;
                         ">Smile lookbook 2104</div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9"
                         data-x="5"
                         data-y="250"
                         data-speed="1000"
                         data-start="1400"
                         data-easing="Power3.easeInOut"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-end="7300"
                         data-endspeed="1000"
                         style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;font-family: Montserrat;
                                     font-size: 46px;
                                     font-weight: bold;
                                     line-height:44px;
                                     text-transform: uppercase;
                                     color: #fff;">Modern & Classic<br>Outlet<br>Menswear</div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9"
                         data-x="5"
                         data-y="395"
                         data-speed="1000"
                         data-start="1800"
                         data-easing="Power3.easeInOut"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-end="7300"
                         data-endspeed="1000"
                         style="z-index: 3; max-width: 80px; max-height: 4px; width:100%;height:100%;background:#fff;"></div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9"
                         data-x="5"
                         data-y="410"
                         data-speed="1000"
                         data-start="2200"
                         data-easing="Power3.easeInOut"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-end="7300"
                         data-endspeed="1000"
                         style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;		font-family: Raleway;
                                     font-size: 18px;
                                     color: #fff;">
                        Clean & Elegant design with a modern style. This template includes<br>
                        all you need for a fashion & accessories store
                    </div>
                    <a href="#" class="tp-caption lft ss-bg-color skewtoleftshort rs-parallaxlevel-9"
                       data-x="5"
                       data-y="480"
                       data-speed="1000"
                       data-start="2300"
                       data-easing="Power3.easeInOut"
                       data-elementdelay="0.1"
                       data-endelementdelay="0.1"
                       data-end="7300"
                       data-endspeed="1000"
                       style="z-index: 3; height:43px;line-height:43px;color:#fff;font-family: Montserrat;
                                   font-size: 12px;
                                   font-weight: bold;
                                   text-transform:uppercase;padding:0 25px;background:#d6644a;">
                        Get the look !
                    </a>
                    <a href="#" class="tp-caption lft skewtoleftshort rs-parallaxlevel-9"
                       data-x="160"
                       data-y="480"
                       data-speed="1000"
                       data-start="2600"
                       data-easing="Power3.easeInOut"
                       data-elementdelay="0.1"
                       data-endelementdelay="0.1"
                       data-end="7300"
                       data-endspeed="1000"
                       style="z-index: 3; height:43px;line-height:43px;color:#fff;font-family: Montserrat;
                                   font-size: 12px;
                                   font-weight: bold;
                                   text-transform:uppercase;padding:0 25px;background:#000000;">
                        or find something u need
                    </a>
                </li>
                <li data-transition="fade" data-slotamount="2" data-masterspeed="500" data-thumb="homeslider_thumb1.jpg"  data-saveperformance="on"  data-title="Intro Slide">
                    <!-- MAIN IMAGE -->
                    <img src="'.$imgurl.'images/dummy.png"  alt="slidebg1" data-lazyload="'.$imgurl.'images/slides/hotelbanner-1903x621.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <div class="tp-caption lft ss-color skewtoleftshort rs-parallaxlevel-9"
                         data-x="5"
                         data-y="220"
                         data-speed="1000"
                         data-start="1400"
                         data-easing="Power3.easeInOut"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-end="7300"
                         data-endspeed="1000"
                         style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;	font-family: Montserrat;
                                     font-size: 23px;
                                     font-weight: bold;
                                     text-transform: uppercase;
                                     color: #d6644a;
                         ">Smile lookbook 2104</div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9"
                         data-x="5"
                         data-y="250"
                         data-speed="1000"
                         data-start="1400"
                         data-easing="Power3.easeInOut"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-end="7300"
                         data-endspeed="1000"
                         style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;font-family: Montserrat;
                                     font-size: 46px;
                                     font-weight: bold;
                                     line-height:44px;
                                     text-transform: uppercase;
                                     color: #fff;">Modern & Classic<br>Outlet<br>Menswear</div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9"
                         data-x="5"
                         data-y="395"
                         data-speed="1000"
                         data-start="1800"
                         data-easing="Power3.easeInOut"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-end="7300"
                         data-endspeed="1000"
                         style="z-index: 3; max-width: 80px; max-height: 4px; width:100%;height:100%;background:#fff;"></div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9"
                         data-x="5"
                         data-y="410"
                         data-speed="1000"
                         data-start="2200"
                         data-easing="Power3.easeInOut"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-end="7300"
                         data-endspeed="1000"
                         style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;		font-family: Raleway;
                                     font-size: 18px;
                                     color: #fff;">
                        Clean & Elegant design with a modern style. This template includes<br>
                        all you need for a fashion & accessories store
                    </div>
                    <a href="#" class="tp-caption lft ss-bg-color skewtoleftshort rs-parallaxlevel-9"
                       data-x="5"
                       data-y="480"
                       data-speed="1000"
                       data-start="2300"
                       data-easing="Power3.easeInOut"
                       data-elementdelay="0.1"
                       data-endelementdelay="0.1"
                       data-end="7300"
                       data-endspeed="1000"
                       style="z-index: 3; height:43px;line-height:43px;color:#fff;font-family: Montserrat;
                                   font-size: 12px;
                                   font-weight: bold;
                                   text-transform:uppercase;padding:0 25px;background:#d6644a;">
                        Get the look !
                    </a>
                    <a href="#" class="tp-caption lft skewtoleftshort rs-parallaxlevel-9"
                       data-x="160"
                       data-y="480"
                       data-speed="1000"
                       data-start="2600"
                       data-easing="Power3.easeInOut"
                       data-elementdelay="0.1"
                       data-endelementdelay="0.1"
                       data-end="7300"
                       data-endspeed="1000"
                       style="z-index: 3; height:43px;line-height:43px;color:#fff;font-family: Montserrat;
                                   font-size: 12px;
                                   font-weight: bold;
                                   text-transform:uppercase;padding:0 25px;background:#000000;">
                        or find something u need
                    </a>
                </li>
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</div>';
    echo '<div class="space10"></div>';
}else{
    echo '<div class="page_header">
                <div class="container">
                    <div class="page_header_info text-center">
                        <div class="page_header_info_inner">
                            <h2>New Arrivals</h2>
                            <p>Nunc tincidunt consequat elit vitae placerat. Sed id ex vel tortor ultrices accumsan. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                            <div class="ph_btn">
                                <a href="#">Shop Men</a>
                                <a href="#">Shop Women</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
}
?>
<!-- SLIDER -->


