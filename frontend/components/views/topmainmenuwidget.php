<?php
$session = Yii::$app->session;
use yii\helpers\Url;
//$role = Yii::$app->session->get('currentRole');
$imgurl = $this->theme->basePath;
?>
 <header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Logo -->
                    <?php
                    if(!empty($session['currentLogo'])){
                        $img = $session['adminURL']."/upload/client_logos/".$session['currentLogo'];            
                    }else{
                        $img = $imgurl.'images/basic/logo-lite.png';
                    }
                    ?>
                    <a class="navbar-brand" href="./index.html"><img src="<?= $img ?>" class="img-responsive" alt=""/></a>
                </div>
                <!-- Cart & Search -->
                <div class="header-xtra pull-right">
                    <div class="topcart">
                        <span><i class="fa fa-shopping-cart"></i></span>
                        <div class="cart-info">
                            <small>You have <em class="highlight">3 item(s)</em> in your shopping bag</small>
                            <div class="ci-item">
                                <img src="<?= $imgurl ?>images/products/fashion/8.jpg" width="80" alt=""/>
                                <div class="ci-item-info">
                                    <h5><a href="./single-product.html">Product fashion</a></h5>
                                    <p>2 x $250.00</p>
                                    <div class="ci-edit">
                                        <a href="#" class="edit fa fa-edit"></a>
                                        <a href="#" class="edit fa fa-trash"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="ci-item">
                                <img src="<?= $imgurl ?>images/products/fashion/15.jpg" width="80" alt=""/>
                                <div class="ci-item-info">
                                    <h5><a href="./single-product.html">Product fashion</a></h5>
                                    <p>2 x $250.00</p>
                                    <div class="ci-edit">
                                        <a href="#" class="edit fa fa-edit"></a>
                                        <a href="#" class="edit fa fa-trash"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="ci-total">Subtotal: $750.00</div>
                            <div class="cart-btn">
                                <a href="#">View Bag</a>
                                <a href="#">Checkout</a>
                            </div>
                        </div>
                    </div>
                    <div class="topsearch">
                        <span>
                            <i class="fa fa-search"></i>
                        </span>
                        <form class="searchtop">
                            <input type="text" placeholder="Search entire store here.">
                        </form>
                    </div>
                </div>
                <!-- Navmenu -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="./index.html" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-expanded="false">Home</a>
                            <ul class="dropdown-menu submenu" role="menu">
                                <li><a href="./index.html">Home - Style 1</a>
                            </ul>
                        </li>
                        
                        <?php
                            //$dfUrl = Yii::$app->params['dfUrl'];
                            $dfUrl = '';
                            $items = common\models\VIPAssignCategories::find()
                                    ->joinWith(['categories'])
                                    ->where(['clientID' => $session['currentclientID']])
                                    ->orderBy([
                                        'position' => SORT_ASC,
                                        //'username' => SORT_DESC,
                                    ])
                                    ->all();
                                    //->all();
                            $result = [];
                            foreach ($items as $item) {
                                    $parent = $item['categories_id'];
                                    $cname = $item->categories->name;
                                    echo '<li class="dropdown"><a href="./index.html" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-expanded="false">'.$cname.'</a><ul class="dropdown-menu submenu" role="menu">';
                                    //echo $item['id'];  
                                    //$ur = \Yii::app()->createUrl("fooController/someAction",array("int"=>"someNumber","cat"=>"someString","foo"=>"something"));
                                    //Yii::app()->createUrl("foo/foo",array("int"=>"1","cfm"=>"8"));
                                    //die();
                                    $id = $item['categories_id'];
                                    $ur = Yii::$app->urlManager->createUrl([''.$dfUrl.'site/product', 'id' => $id]);
                                    //echo '<li><a href="'.$ur.'">'.$item['cat_name'].'</a></li>';
                                    $items2 = common\models\VIPAssignSubCategories::find()
                                    ->joinWith(['subcategories'])        
                                    ->where(['categories_id' => $parent])
                                    ->orderBy([
                                        'position' => SORT_ASC,
                                        //'position' => SORT_DESC,
                                    ])        
                                    //->orderBy('subname')
                                    ->all();
                                    foreach ($items2 as $item2) {
                                            //echo $item2['subname'];
                                            $id = $item2['categories_id'];
                                            $sid = $item2['sub_categories_id'];
                                            $subcname = $item2->subcategories->name;
                                            //$subcname = 'sss';
                                            //$ur2 = Url::to(["product", 'id' => $id, 'sid' => $sid]);
                                            $ur2 = Yii::$app->urlManager->createUrl([''.$dfUrl.'/products/product', 'id' => $id, 'sid' => $sid]);
                                            echo '<li class="nav"><a href="'.$ur2.'">'.$subcname.'</a></li>';
                                    }
                                    echo '</ul></li>';
                            }
                        ?>

                    </ul>
                </div>
            </div>
        </nav>
    </header>