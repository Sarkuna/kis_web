<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\behaviors\BlameableBehavior;
use borales\extensions\phoneInput\PhoneInputValidator;
use borales\extensions\phoneInput\PhoneInputBehavior;


use common\models\User;
use common\models\VIPCustomer;



/**
 * Signup form
 */
class PasswordResetRequestMobileForm extends Model
{
    public $phone;
    public $mobile_no;
    
    public function behaviors() {
        return [
            BlameableBehavior::className(),
            'phoneInput' => PhoneInputBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            ['mobile_no', 'trim'],
            ['mobile_no', 'required'],
            ['mobile_no', 'uniqueMobile'],
            ['mobile_no', 'string', 'min' => 7, 'max' => 15],
            [['mobile_no'], PhoneInputValidator::className()],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */

    public function uniqueMobile($attribute, $params)
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        $mobile_no = VIPCustomer::find()->where(['mobile_no'=>$this->mobile_no, 'clientID' => $client_id])->count();
        if($mobile_no == 0){
            $this->addError($attribute, 'Mobile "'.$this->mobile_no.'" not registered.');
        }          
    }
}
