<?php
namespace frontend\models;

use yii\base\Model;
use Yii;


class ProductForm extends Model
{
    public $product_name;
    public $type_colour;
    public $pack_size;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name','pack_size'], 'required'],
            [['type_colour'], 'safe'],
        ];
    }

    
}
