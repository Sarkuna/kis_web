<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
use YoHang88\LetterAvatar\LetterAvatar;
use borales\extensions\phoneInput\PhoneInputBehavior;
use borales\extensions\phoneInput\PhoneInputValidator;

use common\models\User;
use common\models\VIPCustomer;
use common\models\AuthAssignment;
use common\models\CompanyInformation;

$session = Yii::$app->session;

/**
 * Signup form
 */
class SignupFormUpdate extends Model
{

    public $phone;
    public $salutation;
    public $full_name;
    public $mobile_no;
    public $company_name;
    public $company_address;
    

    //public $reCaptcha;

    public function behaviors() {
        return [
            BlameableBehavior::className(),
            'phoneInput' => PhoneInputBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['salutation', 'full_name', 'company_address','mobile_no'], 'required'],
            [['company_address'], 'string', 'max' => 300],
            ['mobile_no', 'trim'],
            ['mobile_no', 'required'],
            ['mobile_no', 'uniqueMobile'],
            ['mobile_no', 'string', 'min' => 12, 'max' => 14],
            [['mobile_no'], PhoneInputValidator::className(), 'region' => 'MY', 'message' => 'The format of the phone is invalid or system not supported your country.'],      
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        if ($this->validate()) {
            
            $model = VIPCustomer::find()->where(['userID'=>Yii::$app->user->id, 'clientID' => $client_id])->one();
            $mob = preg_replace('/(?<=\d)\s+(?=\d)/', '', $this->mobile_no);

            $model->salutation_id = $this->salutation;
            $model->full_name = $this->full_name;
            $model->mobile_no = $mob;
            
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {
                    $companyInformation = CompanyInformation::find()->where(['user_id'=>Yii::$app->user->id])->one();
                    $companyInformation->mailing_address = $this->company_address;
                    if (($flag = $companyInformation->save(false)) === false) {
                        $transaction->rollBack();
                        //break;
                    }
                }
                
                if ($flag) {
                    $transaction->commit();
                    return $model;
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }       
        }
        return null;
    }
    
    public function uniqueEmail($attribute, $params)
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        
        if(
            $user = User::find()->where(['email'=>$this->email, 'client_id' => $client_id, 'user_type' => 'D'])->exists()
            //$user = User::->exists('email=:email',array('email'=>$this->email))
        )
          $this->addError($attribute, 'Email "'.$this->email.'" has already been taken.');
    }
    
    public function uniqueMobile($attribute, $params)
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        
        if(
            $user = VIPCustomer::find()->where(['mobile_no'=>$this->mobile_no, 'clientID' => $client_id])->exists()
            //$user = User::->exists('email=:email',array('email'=>$this->email))
        )
          $this->addError($attribute, 'Mobile "'.$this->mobile_no.'" has already been taken.');
    }
    
    public function attributeLabels()
    {
        return [
            'mobile_no' => 'Mobile No +60',
        ];
    }
}