<?php

//namespace app\modules\support\controllers;
namespace app\modules\account\controllers;

use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\Box;

use common\models\VIPProduct;
use common\models\VIPCustomerAddress;
use common\models\VIPCustomerRewardSearch;
use common\models\VIPOrder;
use common\models\VIPOrderSearch;
use common\models\VIPCustomer;
use common\models\VIPCustomerSearch;
use common\models\VIPCustomerBank;
use common\models\ClientOption;
use common\models\ReceiptHistory;
use common\models\VIPCustomerAccount2Search;

/**
 * Default controller for the `support` module
 */



class MyAccountAppController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionChangePassword()
    {
        $user = \common\models\User::findOne(Yii::$app->user->id);
        $changepassword = new \frontend\models\ChangePasswordForm();
        if ($changepassword->load(Yii::$app->request->post()) && $changepassword->validate()) {
            $oldPassVal = $user->validatePassword($changepassword->oldPassword);
            if(!$oldPassVal){
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Fail', 'text' => 'Incorrect Current Password']);
                return $this->redirect(['index', '#' => "change_password"]);
            }else {
                $user->setPassword($changepassword->password);
                $user->save(false);
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Your password has been changed successfully']);
                return $this->redirect(['index', '#' => "change_password"]);
            }
        }else {
            return $this->render('change_password', ['changepassword' => $changepassword]);
        }
    }
    
    public function actionAddress()
    {
        $session = Yii::$app->session;
        $address = VIPCustomerAddress::find()->where(['userID' => Yii::$app->user->id, 'clientID' => $session['currentclientID']])->all();
        return $this->render('address', [
            'address' => $address,
        ]);
    }
    
    public function actionBanks()
    {
        $session = Yii::$app->session;
        $banks = VIPCustomerBank::find()->where(['userID' => Yii::$app->user->id, 'clientID' => $session['currentclientID']])->all();
        return $this->render('banks', [
            'banks' => $banks,
        ]);
    }
    
    public function actionRewardPoints()
    {
        $session = Yii::$app->session;
        $searchrewardModel = new VIPCustomerRewardSearch();
        $searchrewardModel->clientID = $session['currentclientID'];
        $searchrewardModel->customer_id = Yii::$app->user->id;
        $datarewardProvider = $searchrewardModel->search(Yii::$app->request->queryParams);
        return $this->render('reward_points', [
            'searchrewardModel' => $searchrewardModel,
            'datarewardProvider' => $datarewardProvider,
        ]);
    }
    
    public function actionAccount2()
    {
        $session = Yii::$app->session;
        $searchModelModel = new VIPCustomerAccount2Search();
        $searchModelModel->clientID = $session['currentclientID'];
        $searchModelModel->customer_id = Yii::$app->user->id;
        $dataProvider = $searchModelModel->search(Yii::$app->request->queryParams);
        
        $total = 0;
        $totalpoint = \common\models\VIPCustomerAccount2::find()
        ->where(['>', 'points_in', 0])
        ->andWhere(['clientID'=>$session['currentclientID'], 'customer_id'=>Yii::$app->user->id])
        ->sum('points_in');
        
        $redeemed_point = 0;
        $redeemed_point = \common\models\VIPCustomerAccount2::find()
        ->where(['<', 'points_in', 0])
        ->andWhere(['clientID'=>$session['currentclientID'], 'customer_id'=>Yii::$app->user->id])
        ->sum('points_in');
        
        return $this->render('account2_points', [
            'searchModel' => $searchModelModel,
            'dataProvider' => $dataProvider,
            'totalpoint' => $totalpoint,
            'redeemed_point' => $redeemed_point
        ]);
    }
    
    public function actionIndirect()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $customer = VIPCustomer::find()->where(['userID' => Yii::$app->user->id, 'clientID' => $clientID])->one();

        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $searchModel->distributors_ref = $customer->vip_customer_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('indirect', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionAddAddress()
    {
        $session = Yii::$app->session;
        $model = new VIPCustomerAddress();
        
        if ($model->load(Yii::$app->request->post())) {
            //return $this->redirect(['view', 'id' => $model->vip_customer_address_id]);
            $model->userID = Yii::$app->user->id;
            $model->clientID = $session['currentclientID'];
            if($model->save()){
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified shipping address!']);
                return $this->redirect(['/accounts/my-account/address']);
            }else {
                print_r($model->getErrors());
            }
        }  else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    
    public function actionAddBanks()
    {
        $session = Yii::$app->session;
        $model = new VIPCustomerBank();
        
        if ($model->load(Yii::$app->request->post())) {
            //return $this->redirect(['view', 'id' => $model->vip_customer_address_id]);
            $model->userID = Yii::$app->user->id;
            $model->clientID = $session['currentclientID'];
            if($model->save()){
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified bank info!']);
                return $this->redirect(['/accounts/my-account/banks']);
            }else {
                print_r($model->getErrors());
            }
        }  else {
            return $this->render('create_bank', [
                'model' => $model,
            ]);
        }
    }
    
    
    public function actionUpdateAddress($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            //$model->validate()
            if($model->save()){
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified shipping address!']);
                return $this->redirect(['/accounts/my-account/address']);
            }else {
                print_r($model->getErrors());
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdateBanks($id)
    {
        $session = Yii::$app->session;
        $model = VIPCustomerBank::find()->where(['vip_customer_bank_id' => $id,'userID' => Yii::$app->user->id, 'clientID' => $session['currentclientID']])->one();

        if ($model->load(Yii::$app->request->post())) {
            //$model->validate()
            if($model->save()){
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified shipping address!']);
                return $this->redirect(['/accounts/my-account/address']);
            }else {
                print_r($model->getErrors());
            }
        } else {
            return $this->render('update_bank', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdateProfile()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = VIPCustomer::find()
                ->where("clientID = '" . $clientID . "' AND userID = '" . Yii::$app->user->id . "'")
                ->one();

        $model->email = $model->user->email;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('updateprofile', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDeleteaddress($id)
    {

        $del = VIPCustomerAddress::deleteAll('vip_customer_address_id = :vip_customer_address_id AND userID = :userID', [':vip_customer_address_id' => $id, ':userID' => Yii::$app->user->id]);
        if($del){
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified shipping address!']);
            
        }
        return $this->redirect(['/accounts/my-account/address']);
    }
    
    public function actionOrder(){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPOrderSearch();
        $searchModel->clientID = $clientID;
        $searchModel->customer_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('order', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
    }
    
    public function actionView($id)
    {
        $session = Yii::$app->session;
        $Model = VIPOrder::find()->where(['order_id' => $id,'customer_id' => Yii::$app->user->id, 'clientID' => $session['currentclientID']])->one();
        if(count($Model) > 0){
            return $this->render('view', [
                'model' => $Model,
            ]);
        }else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionUploadReceipt()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = new \common\models\UploadInvoice(); 
        $filegroup = Yii::$app->security->generateRandomString();
        if ($model->load(Yii::$app->request->post())) {
            $path = Yii::getAlias('@backend/web') .'/upload/upload_invoice/';
            $paththumb = Yii::getAlias('@backend/web') .'/upload/upload_invoice/thumb/';
            $imagefiles = UploadedFile::getInstances($model, 'image');
            
            if (!is_null($imagefiles)) {
                foreach ($imagefiles as $file) {
                    $productimage = new \common\models\VIPUploadInvoice();
                    $new_name = $clientID.'_'.Yii::$app->user->id.'_'.Yii::$app->security->generateRandomString(). '.' . $file->extension;
                    
                    $productimage->userID = Yii::$app->user->id;
                    $productimage->clientID = $clientID;
                    $productimage->file_group = $filegroup;
                    $productimage->upload_file_name = $file->baseName . '.' . $file->extension;
                    $productimage->upload_file_new_name = $new_name;
                    $productimage->add_date = date('Y-m-d H:i:s');

                    if ($productimage->save()) {
                        $file->saveAs($path . $new_name);
                        $imagine = Image::getImagine();
                        $image = $imagine->open($path . $new_name);
                        $image->resize(new Box(200, 300))->save($paththumb . $new_name, ['quality' => 100]);
                    }
                }
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'The file[s] has been successfully uploaded!']);
            }
        }
        return $this->render('upload_receipt', [
                'model' => $model,
            ]);
    }
    
    public function actionUploadReceipt1()
    {

        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = new \common\models\UploadInvoice(); 
        $filegroup = Yii::$app->security->generateRandomString();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $path = Yii::getAlias('@backend/web') .'/upload/upload_invoice/';
            $paththumb = Yii::getAlias('@backend/web') .'/upload/upload_invoice/thumb/';
            $imagefiles = UploadedFile::getInstances($model, 'image');

            $VIPReceipt = new \common\models\VIPReceipt();
            
            $data = \common\models\VIPReceipt::find()->select('running_no')
                    ->where(['clientID' => $clientID])
                    ->orderBy(['running_no' => SORT_DESC])->one();
            if(!empty($data)){
                $invoice_no = str_pad(++$data->running_no,6,'0',STR_PAD_LEFT);
                $VIPReceipt->running_no = $invoice_no;
            }else {
                $VIPReceipt->running_no = '000001';
            }
            
            
            $VIPReceipt->invoice_no = $model->invoice_no;
            $VIPReceipt->clientID = $clientID;
            $VIPReceipt->userID = Yii::$app->user->id;
            $VIPReceipt->date_of_receipt = date('Y-m-d', strtotime($model->date_of_receipt));
            $VIPReceipt->order_num = $this->clientOption().'-'.$clientID.'-'.$VIPReceipt->running_no;

            if($VIPReceipt->save()) {
                $lastinsid = $VIPReceipt->vip_receipt_id;
                
                $receipthistory = new ReceiptHistory();
                $receipthistory->vip_receipt_id = $lastinsid;
                $receipthistory->vip_receipt_status = 'P';
                $receipthistory->notify = '0';
                $receipthistory->comment = 'new receipt created';
                $receipthistory->date_added = date('Y-m-d H:i:s');
                $receipthistory->save(false);
                
                if (!is_null($imagefiles)) {
                    foreach ($imagefiles as $file) {
                        $productimage = new \common\models\VIPUploadInvoice();
                        $new_name = $clientID.'_'.Yii::$app->user->id.'_'.Yii::$app->security->generateRandomString(). '.' . $file->extension;

                        $productimage->userID = Yii::$app->user->id;
                        $productimage->clientID = $clientID;
                        $productimage->file_group = $filegroup;
                        $productimage->upload_file_name = $file->baseName . '.' . $file->extension;
                        $productimage->upload_file_new_name = $new_name;
                        $productimage->add_date = date('Y-m-d H:i:s');
                        $productimage->vip_receipt_id = $lastinsid;

                        if ($productimage->save(false)) {
                            $file->saveAs($path . $new_name);
                            //$imagine = Image::getImagine();
                            //$image = $imagine->open($path . $new_name);
                            //$image->resize(new Box(200, 300))->save($paththumb . $new_name, ['quality' => 100]);
                        }else {
                            echo '<pre>';
                            print_r($productimage->getErrors());
                            die;
                        }
                    }                
                }               
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'The file[s] has been successfully uploaded!']);
                return $this->redirect(['/accounts/receipt']);
            }else {
                echo '<pre>';
                print_r($VIPReceipt->getErrors());
                die;
            }
            
        }
        return $this->render('upload_receipt1', [
                'model' => $model,
            ]);
    }


    
    protected function findModel($id)
    {
        if (($model = VIPCustomerAddress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function clientOption()
    {
        $clientID = Yii::$app->user->identity->client_id;

        $rd = ClientOption::find()->where(['clientID' => $clientID])->one();
        return $rd->receipts_prefix;

    }
}
