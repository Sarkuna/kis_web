<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;

use common\models\VipCountry;

$countrylists = VipCountry::find()
        ->where(['status' => '0'])
        ->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');

if(!($model->isNewRecord)) {
    $cid = $model->country_id;
}else {
    $cid = '129';
}
$regionlists = \common\models\VIPStates::find()
        ->where(['country_id' => $cid])
        ->orderBy([
    'state_name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'states_id', 'state_name');

?>

    <?php $form = ActiveForm::begin(['method' => 'post', 'id' => 'profile-form', 'options' => [
                'class' => 'form form-horizontal',
                'novalidate' => ''
             ]]); ?>
    <div class="form-body">
        <div class="row">
        <?=
        $form->field($model, 'company', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6 mr-5',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
        ])->textInput(array('placeholder' => ''));
        ?>
        <?=
        $form->field($model, 'firstname', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
        ])->textInput(array('placeholder' => ''));
        ?>
        <?=
        $form->field($model, 'lastname', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
        ])->textInput(array('placeholder' => ''));
        ?>
        
        
        <?=
        $form->field($model, 'address_1', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
        ])->textInput(array('placeholder' => ''));
        ?>
        
        <?=
        $form->field($model, 'address_2', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
        ])->textInput(array('placeholder' => ''));
        ?>
        
        <?=
        $form->field($model, 'postcode', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
        ])->textInput(array('placeholder' => ''));
        ?>
        
        <?=
        $form->field($model, 'city', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
        ])->textInput(array('placeholder' => ''));
        ?>
        
        <?=
        $form->field($model, 'country_id', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
        ])->dropDownList($country, [
            'prompt' => 'Select...',
            'onchange' => '$.get( "' . Url::toRoute('/accounts/my-account/region') . '", {id: $(this).val() } )
                .done(function( data ) {
                   $("#vipcustomeraddress-state").html(data);
                }
            );'
          ]);
        ?>
        
        <?=
        $form->field($model, 'state', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
        ])->dropDownList($regionlist, ['prompt' => 'Select...']);
        ?>       
    <div class="col-md-8 offset-md-4">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) ?>
                <?= Html::a('Back ', ['/addressbook'], ['class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) ?>
            </div>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>
