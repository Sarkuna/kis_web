<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
//use kartik\select2\Select2;

$typename = \common\models\RegistrationType::find()
           ->select('registration_type_id, name')
           ->where(['del' => '0'])
           ->asArray()->all();

$typenames = ArrayHelper::map($typename, 'registration_type_id', 'name');

?>

<?php
$form = ActiveForm::begin(['method' => 'post', 'id' => 'company-form', 'options' => [
                'class' => 'form form-horizontal',
                'novalidate' => ''
        ]]);
?>
<div class="form-body">
    <div class="row">

        <?=
        $form->field($model, 'company_name', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
            'options' => [
                'class' => 'col-md-6 pull-md-6',
            //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
            ],
        ])->textInput(array('placeholder' => ''));
        ?>
        <?=
        $form->field($model, 'mailing_address', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
            'options' => [
                'class' => 'col-12 col-md-6',
            //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
            ],
        ])->textarea(array('rows' => 5, 'cols' => 5, 'style' => 'text-transform:uppercase'));
        ?>
        
        

        <?=
        $form->field($model, 'tel', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
            'options' => [
                'class' => 'col-12 col-md-6',
            //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
            ],
        ])->textInput(array('placeholder' => ''));
        ?>
        
        <?=
        $form->field($model, 'fax', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
            'options' => [
                'class' => 'col-12 col-md-6',
            //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
            ],
        ])->textInput(array('placeholder' => ''));
        ?>
        

        <?php
        echo $form->field($model, 'company_reg_type', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
            'options' => [
                'class' => 'col-12 col-md-6',
            ],
        ])->dropDownList($typenames, ['prompt' => '-- Select --']);
        ?>

        <?php
        echo $form->field($model, 'company_registration_number', [
            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
            'options' => [
                'class' => 'col-12 col-md-6',
            ],
        ])->textInput(array('placeholder' => ''));
        ?>


        <div class="col-md-8 offset-md-4">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) ?>
            <?= Html::a('Back ', ['index'], ['class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) ?>
        </div>
    </div>
</div>
 <?php ActiveForm::end(); ?>