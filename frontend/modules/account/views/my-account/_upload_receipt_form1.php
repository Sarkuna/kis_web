<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\FileInput;
use kartik\date\DatePicker;


?>


<div class="account-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-6">
            <?=
        $form->field($model, 'invoice_no', [
            //'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
        ])->textInput(array('placeholder' => ''));
        ?>
         <?php
        echo $form->field($model, 'date_of_receipt')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Date of Receipt'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd-M-yyyy',
                'startDate'=> '01-01-2019',
                'endDate' => date('d-m-Y'),
                'maxDate' => date('d-m-Y'),
            ],
            'pluginEvents' => [
                'changeDate' => 'function(ev) {
 
                                }'
            ]
        ]);
        ?>   
            <label class="control-label" for="uploadinvoice-image">Receipt[s] <sub>You may upload JPG, GIF, BITMAP or PNG images up to 6MB in size.</sub></label>
            <?php
            echo $form->field($model, 'image[]')->widget(FileInput::classname(), [
                'options' => ['multiple' => true, 'accept' => 'image/*'],
                'pluginOptions' => [
                    'maxFileCount' => 5,
                    'maxFileSize'=>6120,
                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp', 'jpeg'],
                    'initialPreview' => [
                    //Html::img("/images/moon.jpg", ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon'])
                    //$ClientAddress->company_logo ? Html::img('/upload/client_logos/'.$ClientAddress->company_logo, ['class'=>'thumbnail', 'alt'=>'Logo', 'title'=>'Logo']) : null, // checks the models to display the preview
                    ],
                ]
            ])->label(false);
            ?>
            

        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>