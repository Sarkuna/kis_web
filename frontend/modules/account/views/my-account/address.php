<?php

use yii\helpers\Html;
use app\components\AccountMenuWidget;
use yii\helpers\Url;
$this->title = 'Address Book';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$cart = Yii::$app->session['cart'];
?>

<!-- Basic Tables start -->
<div class="row" id="basic-table">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Manage Shipping Address</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <?= Html::a('<span><i class="feather icon-plus"></i> Add New</span>', ['/addressbook/add-address'], ['class' => 'btn btn-outline-primary']) ?>
                    <?php
                        if(!empty($cart)) {
                            echo Html::a('<span><i class="feather icon-arrow-left"></i> Back</span>', ['/shopping/cart'], ['class' => 'btn btn-outline-warning']);
                        }
                    ?>
                    <!-- Table with outer spacing -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Default Address</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Postcode</th>                                    
                                    <th>Country</th>                                    
                                    <th style="width: 85px"></th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                               

                                <?php
                                if (count($address) > 0) {
                                    $n = 1;
                                    foreach ($address as $myaddress) {
                                        $edit = '<a href="' . Url::to(['/accounts/my-account/update-address', 'id' => $myaddress->vip_customer_address_id]) . '" title="Edit"><span class="action-edit"><i class="feather icon-edit"></i></span></a>';
                                        if($myaddress->default_ID == 1) { $checked = "checked"; $label = "My Default Address";}else {$checked = ''; $label = "";}
                                        /* $del = Html::a('<i class="fa fa-trash"></i>', ['/accounts/my-account/deleteaddress', 'id' => $myaddress->vip_customer_address_id], [
                                          'class' => 'btn btn-danger btn-sm',
                                          'data' => [
                                          'confirm' => 'Are you sure you want to delete this?',
                                          'method' => 'post',
                                          ],
                                          ]); */
                                        echo '<tr>
                        <td><fieldset>
                            <div class="vs-radio-con vs-radio-success">
                              <input type="radio" name="defaultaddress" value="'.$myaddress->vip_customer_address_id.'" '.$checked.'>
                              <span class="vs-radio">
                                <span class="vs-radio--border"></span>
                                <span class="vs-radio--circle"></span>
                              </span>
                              <span class="">'.$label.'</span>
                            </div>
                          </fieldset></td>
                        <td>' . $myaddress->firstname . ' ' . $myaddress->lastname . '</td>
                        <td>' . $myaddress->address_1 . ' ' . $myaddress->address_2 . '<br>' . $myaddress->city . '</td>
                        <td>' . $myaddress->postcode . '<br>' . $myaddress->states->state_name . '</td>
                        <td>' . $myaddress->country->name . '</td>
                        
                        <td>' . $edit . '</td>
                    </tr>';
                                        $n++;
                                    }
                                } else {
                                    echo '<tr><td colspan="8" class="text-center">No record!</td></tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic Tables end -->
<?php
$script = <<< JS
    $(function() {
	$(document).on('change', '[name="defaultaddress"]' , function(){
  	var val = $('[name="defaultaddress"]:checked').val();
        addresdefault(val);
  }); 

});
JS;
$this->registerJs($script);
?>
