<?php

use yii\helpers\Html;
use app\components\AccountMenuWidget;
use yii\helpers\Url;
$this->title = 'Bank Info';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Basic Tables start -->
<div class="row" id="basic-table">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Manage Bank Info</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Table with outer spacing -->
                    <div class="table-responsive">
                        <?= Html::a('<span><i class="feather icon-plus"></i> Add New</span>', ['/bank-info/add-bank'], ['class' => 'btn btn-outline-primary']) ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Bank Name</th>
                                    <th>Account Name</th>
                                    <th>Account Number</th>
                                    <th>NRIC/Passport</th>
                                    <th>Verify</th>                        
                                    <th style="width: 85px"></th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                               

                                <?php
                                if (count($banks) > 0) {
                                    $n = 1;
                                    foreach ($banks as $mybanks) {
                                        $edit = '<a href="' . Url::to(['/accounts/my-account/update-banks', 'id' => $mybanks->vip_customer_bank_id]) . '" title="Edit"><i class="feather icon-edit"></i></a>';

                                        $del = Html::a('<i class="fa fa-trash"></i>', ['/accounts/my-account/deletebanks', 'id' => $mybanks->vip_customer_bank_id], [
                                                    'class' => '',
                                                    'data' => [
                                                        'confirm' => 'Are you sure you want to delete this?',
                                                        'method' => 'post',
                                                    ],
                                        ]);
                                        echo '<tr>
                                            <td>' . $n . '</td>
                                            <td>' . $mybanks->bank->bank_name . '</td>
                                            <td>' . $mybanks->account_name . '</td>
                                            <td>' . $mybanks->account_number . '</td>
                                            <td>' . $mybanks->nric_passport . '</td>
                                            <td>' . $mybanks->status . '</td>
                                            <td>' . $edit . ' ' . $del . '</td>
                                        </tr>';
                                        $n++;
                                    }
                                } else {
                                    echo '<tr><td colspan="8" class="text-center">No record!</td></tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic Tables end -->   