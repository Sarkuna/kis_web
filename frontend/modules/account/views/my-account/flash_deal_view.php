<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = $model->invoice_prefix;
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Flash Deal History', 'url' => ['/flash-deal-history']];
$this->params['breadcrumbs'][] = $this->title;

$clientsetup = Yii::$app->VIPglobal->clientSetup();
$pointpersent = $clientsetup['point_value'];
$mark_up = $clientsetup['pervalue'];
?>

<section class="card invoice-page">
    <div id="invoice-template" class="card-body">
        <!-- Invoice Company Details -->
        <div id="invoice-company-details" class="row">
            <div class="col-md-6 col-sm-12 text-left pt-1">
                <div class="media pt-1">
                </div>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                
                <?php echo '<div class="badge badge-'.$model->orderStatus->labelbg.' badge-lg mr-1 mb-1">' . $model->orderStatus->name . '</div>'; ?>
                
            </div>
        </div>
        <!--/ Invoice Company Details -->

        <!-- Invoice Recipient Details -->
        <div id="invoice-customer-details" class="row pt-2">
            <div class="col-md-6 col-sm-12 text-left">
                <div class="invoice-details mt-2">
                    <h6>ORDER NO.</h6>
                    <p><?= $model->invoice_prefix ?></p>
                    <h6 class="mt-2">ORDER DATE</h6>
                    <p><?= date('d/m/Y', strtotime($model->created_datetime)) ?></p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                <h5>Delivery Address</h5>
                <div class="company-info my-2">
                    <p><?= $model->shipping_firstname ?> <?= $model->shipping_lastname ?></p>
                    <p><?= $model->shipping_address_1 ?> <?= $model->shipping_address_1 ?></p>
                    <p><?= $model->shipping_city ?> <?= $model->shipping_postcode ?></p>
                    <p><?= $model->shipping_zone ?> - <?= $model->shipping_zone_id ?></p>
                    <p><?= $model->shipping_country_id ?></p>
                </div>
            </div>
        </div>
        <!--/ Invoice Recipient Details -->

        <!-- Invoice Items Details -->
        <div id="invoice-items-details" class="pt-1 invoice-items-table">
            <div class="row">
                <div class="table-responsive col-sm-12">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th>PRODUCT NAME</th>
                                <th>MODEL</th>
                                <th class="text-right">QUANTITY</th>
                                <th class="text-right">UNIT POINTS</th>
                                <th class="text-right">POINTS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                    $total = '';
                                    $subtotal = 0; $total = 0; $shipping_point_total = 0;
                                    foreach ($model->orderProducts as $subproduct) {
                                        //echo $subproduct->name.'<br>';
                                        $optlisit = '';
                                        $OrderOption = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
                                        if ($OrderOption > 0) {
                                            $OrderOptionlisits = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                                            foreach ($OrderOptionlisits as $OrderOptionlisit) {
                                                    $optlisit .= '<br>&nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                                                }
                                            }
                                            $productpoint = $subproduct->point + $subproduct->shipping_point;
                                            //$totalproductpoint = $subproduct->point_total + $subproduct->shipping_point_total;
                                            $totalproductpoint = $subproduct->point_total + $subproduct->shipping_point_total;
                                            
                                            $subtotal += $subproduct->point_total;
                                            $shipingpoint = $subproduct->quantity * $subproduct->shipping_point;
                                            //$shipping_point_total += $subproduct->shipping_point_total;
                                            $shipping_point_total += $shipingpoint;
                                            
                                            echo '<tr>
                                              <td>' . $subproduct->name . '
                                                  ' . $optlisit . '
                                              </td>
                                              <td>' . $subproduct->model . '</td>
                                              <td class="text-right">' . $subproduct->quantity . '</td>
                                              <td class="text-right">' .$subproduct->point. 'pts</td>
                                              <td class="text-right">' . Yii::$app->formatter->asInteger($subproduct->point_total) . 'pts</td>
                                            </tr>';
                                            //$total += $totalproductpoint;
                                            $total = $subtotal + $shipping_point_total;
                                    }
                                    ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div id="invoice-total-details" class="invoice-total-table">
            <div class="row">
                
                <div class="col-5 offset-7">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>Sub Total</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($subtotal) ?> pts</td>
                                </tr>
                                <tr>
                                    <th>Delivery Charges</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($shipping_point_total) ?> pts</td>
                                </tr>
                                <tr>
                                    <th>Total</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($total) ?> pts</td>
                                </tr>
                                <tr class="text-danger">
                                    <th>Point Used</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($model->orderAmount->points) ?> pts</td>
                                </tr>
                                <tr class="text-success">
                                    <th>Top up Point</th>
                                    <td class="text-right">
                                            <?php
                                            $top_up_point = $model->flash_deals_rm / $pointpersent;
                                            
                                            ?>
                                            <?= Yii::$app->formatter->asInteger($top_up_point) ?> pts
                                    </td>
                                </tr>
                                <tr class="text-success">
                                    <th>Top up Cash</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($model->flash_deals_rm) ?> RM</td>
                                </tr>
                                <tr class="text-info">
                                    <th>Payment Receipt</th>
                                    <td class="text-right">
                                        <?php
                                        if(!empty($model->payment_receipt)) {
                                            $path = Yii::getAlias('@back') .'/upload/payment_receipt/';
                                            $img = $path . $model->payment_receipt;
                                            echo '<a data-toggle="modal" data-target="#exampleModalScrollable">View</a>';
                                        }else {
                                            $img = '';
                                            echo 'N/A';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>

<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Payment Receipt</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img class="img-responsive" src="<?= $img ?>">
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>