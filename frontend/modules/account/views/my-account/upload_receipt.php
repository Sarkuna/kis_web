<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAddress */

//$this->title = 'Update Vipcustomer Address: ' . $model->vip_customer_address_id;
$this->title = 'Upload Receipt';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'Manage Bank Info', 'url' => ['banks']];
//$this->params['breadcrumbs'][] = ['label' => $model->vip_customer_address_id, 'url' => ['view', 'id' => $model->vip_customer_address_id]];
$this->params['breadcrumbs'][] = 'Edit Address';
?>

<div class="account-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="account-title pull-left"><span class="fa fa-chevron-right"></span><?= $this->title ?></h4>
                <h4 class="account-title pull-right"><?= Html::a('Back ', ['banks'], ['class' => 'btn btn-default btn-sm']) ?></h4>
                <div class="clear"></div>
            </div>
            <div class="col-md-12">
                <div id="account-id">                    
                         <?=
                            $this->render('_upload_receipt_form', [
                                'model' => $model,
                            ])
                        ?>
                </div>
            </div>

            <!--- Account Menu Left Side -->
        </div>
    </div>
</div>