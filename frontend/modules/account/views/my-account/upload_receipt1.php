<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAddress */

//$this->title = 'Update Vipcustomer Address: ' . $model->vip_customer_address_id;
$this->title = 'Upload Receipt';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Receipts List', 'url' => ['/accounts/receipt']];
//$this->params['breadcrumbs'][] = ['label' => $model->vip_customer_address_id, 'url' => ['view', 'id' => $model->vip_customer_address_id]];
$this->params['breadcrumbs'][] = 'Upload Receipt';
?>

<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?=
                            $this->render('_upload_receipt_form', [
                                'model' => $model,
                            ])
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>