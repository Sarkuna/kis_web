<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\Projects */
/* @var $form yii\widgets\ActiveForm */

?>
<style>
    .krajee-default.file-preview-frame .kv-file-content {
    overflow: hidden;
}
    .file-caption-info {margin-top: 10px !important;}
</style>
<div class="account-form">

     <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'ref')->hiddenInput(['maxlength' => 50])->label(false); ?>
    <?=
    FileInput::widget([
        'name' => 'upload_ajax[]',
        'options' => ['multiple' => true, 'accept' => 'image/*'], //'accept' => 'image/*' หากต้องเฉพาะ image
        'pluginOptions' => [
            'showPreview' => true,
            'overwriteInitial' => false,
            'initialPreviewShowDelete' => true,
            'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'bmp'],
            'initialPreview' => $initialPreview,
            //'initialPreview'=> $initialPreview[$model->ref],
            //'initialPreviewConfig'=>$model->initialPreview($model->docs,'docs','config'),
            'initialPreviewConfig' => $initialPreviewConfig,
            'uploadUrl' => Url::to(['/accounts/projects/uploadajax']),
            'uploadExtraData' => [
                'ref' => $model->ref,
            ],
            'maxFileCount' => 10
        ]
    ]);
    ?>

    <?php ActiveForm::end(); ?>

</div>
