<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My Projects';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Basic Tables start -->
<div class="row" id="basic-table">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <?=
                        GridView::widget([
                            'layout' => "{items}\n{summary}\n<nav aria-label='Page navigation example'>{pager}</nav>",
                            'tableOptions' => ['class' => 'table'],
                            'options' => ['class' => ''],
                            'pager' => [
                                'hideOnSinglePage' => true,
                                //'firstPageLabel' => '<i class="feather icon-chevron-left"></i>',
                                //'lastPageLabel' => '<li class="page-item next-item"><a class="page-link" href="#"><i class="feather icon-chevron-right"></i></a></li>',
                                'nextPageLabel' => '<i class="feather icon-chevron-right"></i>',
                                'prevPageLabel' => '<i class="feather icon-chevron-left"></i>',
                                'maxButtonCount' => 10,
                                'options' => [
                                    //'tag' => 'div',
                                    'class' => 'pagination justify-content-center mt-2',
                                //'id' => 'pager-container',
                                ],
                                'pageCssClass' => ['class' => 'page-item'],
                                'linkOptions' => ['class' => 'page-link'],
                                'activePageCssClass' => 'active',
                                'prevPageCssClass' => 'page-item prev-item',
                                'nextPageCssClass' => 'page-item next-item',
                            //'internalPageCssClass' => 'btn btn-info btn-sm',
                            //'disabledPageCssClass' => 'mydisable'
                            ],
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                /* [
                                  'attribute' => 'userID',
                                  'label' => 'ID Name',
                                  'format' => 'html',
                                  //'headerOptions' => ['width' => '180'],
                                  'value' => function ($model) {
                                  return $model->customer->full_name;
                                  },
                                  ], */
                                [
                                    'attribute' => 'projects_date',
                                    'label' => 'Date',
                                    //'format' => 'html',
                                    'format' => ['date', 'php:d/m/Y'],
                                    'headerOptions' => ['width' => '120'],
                                    'value' => function ($model) {
                                return $model->projects_date;
                            },
                                ],
                                [
                                    'attribute' => 'projects_ref',
                                    'label' => 'Project No.',
                                    'format' => 'html',
                                    'headerOptions' => ['width' => '100'],
                                    'value' => function ($model) {
                                return $model->projects_ref;
                            },
                                ],
                                [
                                    'attribute' => 'project_name',
                                    'label' => 'Project Name',
                                    'format' => 'html',
                                    //'headerOptions' => ['width' => '180'],
                                    'value' => function ($model) {
                                        return $model->project_name;
                                    },
                                ],
                                [
                                    'attribute' => 'project_status',
                                    'label' => 'Status',
                                    'format' => 'html',
                                    'headerOptions' => ['width' => '120'],
                                    'value' => function ($model) {
                                return $model->getStatustext();
                            },
                                ],
                                [
                                    'attribute' => 'project_nominated_points',
                                    'label' => 'Nominated Points',
                                    'format' => 'html',
                                    //'headerOptions' => ['width' => '180'],
                                    'value' => function ($model) {
                                        return $model->project_nominated_points;
                                    },
                                ],
                                [
                                    'attribute' => 'project_approved_points',
                                    'label' => 'Approved Points',
                                    'format' => 'html',
                                    //'headerOptions' => ['width' => '180'],
                                    'value' => function ($model) {
                                        return $model->project_approved_points ? $model->project_approved_points : '0';
                                    },
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'headerOptions' => ['width' => '120'],
                                    'template' => '{update} {view}', //{view} {delete}
                                    'buttons' => [
                                        'update' => function ($url, $model) {
                                            return (Html::a('<i class="feather icon-image"></i>', $url, ['title' => Yii::t('app', 'Upload Images'), 'class' => 'btn btn-icon btn-primary mb-1 waves-light']));
                                        },
                                                'view' => function ($url, $model) {
                                            return (Html::a('<i class="feather icon-eye"></i>', $url, ['title' => Yii::t('app', 'View'), 'class' => 'btn btn-icon btn-success mb-1 waves-effect waves-light']));
                                        },
                                            ],
                                        //'visible' => $visible,
                                        ],
                                    ],
                                ]);
                                ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic Tables end -->