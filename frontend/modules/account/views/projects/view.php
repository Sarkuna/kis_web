<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */

$this->title = $model->project_name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section id="profile-info">
    <div class="row">
        
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4><?= Html::encode($this->title) ?></h4>
                </div>
                <div class="card-body">
                    <div class="row text-center mx-0">
                        <div class="col-6 border-top border-bottom border-right d-flex align-items-between flex-column py-1">
                            <p class="mb-50">Nominated Points</p>
                            <p class="font-large-1 text-bold-700 text-warning"><?= Yii::$app->formatter->asInteger($model->project_nominated_points) ?></p>
                        </div>
                        <div class="col-6 border-top border-bottom d-flex align-items-between flex-column py-1">
                            <p class="mb-50">Approved Points</p>
                            <?php
                            if($model->admin_status == 'A') {
                                echo '<p class="font-large-1 text-bold-700 text-success">'.Yii::$app->formatter->asInteger($model->project_nominated_points).'</p>';
                            }
                            ?>
                        </div>
                    </div>
                    
                    <div class="row pt-2">
                        <div class="col-4">
                            <div class="mt-1">
                                <h6 class="mb-0">Ref:</h6>
                                <p><?= $model->projects_ref ?></p>
                            </div>
                            <div class="mt-1">
                                <h6 class="mb-0">Date:</h6>
                                <p><?= date('d-m-y', strtotime($model->projects_date)) ?></p>
                            </div>
                            <div class="mt-1">
                                <h6 class="mb-0">Client Name:</h6>
                                <p><?= $model->project_client_name ?></p>
                            </div>
                            <div class="mt-1">
                                <h6 class="mb-0">Client Email:</h6>
                                <p><a href="mailto:<?= $model->project_client_email ?>"><?= $model->project_client_email ?></a></p>
                            </div>
                            <div class="mt-1">
                                <h6 class="mb-0">Client Tel:</h6>
                                <p><?= $model->project_client_tel ?></p>
                            </div>
                        </div>

                        <div class="col-8">
                            <p class="text-justify"><?= $model->project_details ?></p>
                            <?php
                            if(!empty($model->remark)) {
                                
                                echo '<p><b>Remark</b></p>
                                <p>'.$model->remark.'</p>';
                            }
                            ?> 
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>