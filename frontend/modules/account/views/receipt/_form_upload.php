<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Upload Receipt';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Receipt History', 'url' => ['/receipts-history']];
//$this->params['breadcrumbs'][] = ['label' => $model->vip_customer_address_id, 'url' => ['view', 'id' => $model->vip_customer_address_id]];
$this->params['breadcrumbs'][] = $this->title;

$max = date('Y,m,d', strtotime('-1 MONTH'));
$min = date('Y,m', strtotime('-6 MONTH'));
?>

<style>
    .has-error .help-block {
    color: red;
}

    .file-actions {
    display: none;
}
.picker__table {
    table-layout: auto;
}
</style>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<section>
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <label class="card-title"><?= $this->title ?></label>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="alert alert-warning mb-2" role="alert">
                            <?= Yii::$app->params['msg.upload.receipt'] ?>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <?=
                                    $form->field($model, 'invoice_no', [
                                        //'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
                                    ])->textInput(array('placeholder' => ''));
                                ?>
                            </div>
                            <div class="col-lg-6 col-md-12">

                                
                             <?=
                                    $form->field($model, 'date_of_receipt', [
                                        'template' => "<fieldset class='form-group'>{label}{input}</fieldset>\n{hint}\n{error}"
                                        //'template' => "<fieldset class='form-group'><label for='basicInputFile'>{label} <sub>You may upload JPG, GIF, BITMAP or PNG images up to 6MB in size.</sub></label><div class='custom-file'>{input}<label class='custom-file-label' for='inputGroupFile01'>Choose file</label>{hint}</div>{error}</fieldset>",
                                    ])->textInput(array('placeholder' => '', 'class' => 'form-control pickadate-limits',));
                                ?>   
                            </div>
                        </div>
                            
                        <?php
                            echo $form->field($model, 'image[]', [
                                        'template' => "<label for='basicInputFile'>{label} <sub>You may upload JPG, GIF, BITMAP or PNG images up to 6MB in size.</sub></label>{input}{hint}{error}",
                                        'options' => [
                                            'class' => 'file form-group',
                                        ],
                                ])->fileInput(['multiple'=>true, 'id' => 'inputGroupFile01']
                            );
                        ?>
                        

                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) ?>
                        <?= Html::a('Back ', ['/receipts-history'], ['class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<?php ActiveForm::end(); ?>
<script>
$(document).ready(function() {
    $("#inputGroupFile01").fileinput({
        maxFileCount: 4,
        showDrag: false,
        showRemove: true,
        showClose: false,
        showUpload: false,
        showZoom: false,
        allowedFileExtensions: ["jpeg", "jpg", "gif", "png", "pdf"],
        layoutTemplates: {
            main1: "{preview}\n" +
            "<div class=\'input-group {class}\'>\n" +
            "   <div class=\'input-group-btn\ input-group-prepend'>\n" +
            "       {upload}\n" +
            "       {browse}\n" +            
            "       {remove}\n" +
            "   </div>\n" +
            "   {caption}\n" +
            "</div>"
        }
    });
});
</script>

<?php
$script = <<< JS
  $('.pickadate-limits').pickadate({
      format: 'dd-mm-yyyy',  
      locale: {
        format: 'DD/MM/YYYY'
      },
      min: new Date($min),
      max: new Date($max),
  });
JS;
$this->registerJs($script);
?>