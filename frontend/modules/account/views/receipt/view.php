<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Expression;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Redemption */

$this->title = 'Receipt Invoice #'. $model->invoice_no;
$this->params['breadcrumbs'][] = ['label' => 'Receipts History', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>
<section class="card invoice-page">
    <div id="invoice-template" class="card-body">
        <!-- Invoice Company Details -->
        <div id="invoice-company-details" class="row">
            <div class="col-md-6 col-sm-12 text-left pt-1">
                <div class="media pt-1">
                </div>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                
                <?php echo $model->getStatustextuser(); ?>
                
            </div>
        </div>
        <!--/ Invoice Company Details -->

        <!-- Invoice Recipient Details -->
        <div id="invoice-customer-details" class="row pt-2">
            <div class="col-md-6 col-sm-12 text-left">
                <div class="invoice-details mt-2">
                    <h6>ORDER NO.</h6>
                    <p><?= $model->order_num ?></p>
                    <h6 class="mt-2">Date Added</h6>
                    <p><?= date('d/m/Y', strtotime($model->created_datetime)) ?></p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                <div class="invoice-details mt-2">
                    <h6>Receipt Invoice#</h6>
                    <p><?= $model->invoice_no ?></p>
                    <h6>Date Of Receipt</h6>
                    <p><?= date('d/m/Y', strtotime($model->date_of_receipt)) ?></p>
                    <h6 class="mt-2">Distributor</h6>
                    <p>
                        <?php 
                        if(!empty($model->reseller_name)) {
                            echo $model->company->company_name;
                        }else {
                            echo 'N/A';
                        }
                        ?>
                    </p>
                </div>
            </div>
        </div>
        <!--/ Invoice Recipient Details -->

        <!-- Invoice Items Details -->
        <div id="invoice-items-details" class="pt-1 invoice-items-table">
            <div class="row">
                <div class="table-responsive col-sm-12">
                    
                    <table class="table table-borderless">
                        <thead>
                            <tr class="text-uppercase">
                                <th width="5%">#</th>
                                <th width="65%">Product Description</th>
                                <th width="5%"  class="text-right">Qty</th>
                                <th width="10%"  class="text-right">Point</th>
                                <th width="15%"  class="text-right">Total Point</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php
                                $redemptionitems = common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $model->vip_receipt_id])->all();
                                $n = 1;
                                $totalpoints = 0;
                                if(count($redemptionitems) > 0) {
                                    foreach ($redemptionitems as $redemptionitem) {
                                        $product_list_id = $redemptionitem->product_list_id;
                                        //$product_list = \common\models\BBPoints::find()->where(['bb_points_id' => $product_list_id])->one();
                                        $product_list_id = $redemptionitem->product_list_id;
                                        $product_list = \common\models\ProductList::find()->where(['product_list_id' => $product_list_id])->one();

                                        $totalpoint = $redemptionitem->product_item_qty * $redemptionitem->product_per_value;
                                        echo '<tr>
                                            <td>' . $n . '</td>
                                            <td>'.$product_list->description.'</td> 
                                            <td class="text-right">'.$product_list->total_points_awarded.'</td>     
                                            <td class="text-right">'.$redemptionitem->product_item_qty.'</td>                               
                                            <td class="text-right">'.$redemptionitem->qty_points_awarded.'</td>   
                                          </tr>';
                                        $totalpoints += $totalpoint;
                                        $n++;
                                    }
                                }else {
                                    echo '<tr><td colspan="5" class="text-center">No products found</td></tr>';
                                }
                                ?>
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div id="invoice-total-details" class="invoice-total-table">
            <div class="row">
                <div class="col-5 offset-7">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                                <tr>
                                    <th><h3>Total Awarded Points</h3></th>
                                    <td  class="text-right text-bold-800"><h3><?= Yii::$app->formatter->asInteger($totalpoints) ?></h3></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>