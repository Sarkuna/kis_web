<?php

//namespace app\modules\support\controllers;
namespace app\modules\merchandise\controllers;

use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;



use common\libs\Cart;
use yii\web\Session;

use common\models\MPOrder;
use common\models\MPOrderSearch; 
use common\models\MPOrderHistory;
use common\models\MPProductName;
use common\models\MPProductType;
use common\models\MPProductColour;
use common\models\MPProductSize;
use common\models\VIPCustomerReward;
use common\models\VIPCustomerAddress;


/**
 * Default controller for the `support` module
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        $this->layout = '@app/themes/vuexy/layouts/merchandise';
        $session = Yii::$app->session;
        $formproduct = new \frontend\models\ProductForm();

        return $this->render('index', [
            'formproduct' => $formproduct,
            //'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionOrder(){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new MPOrderSearch();
        //$searchModel->clientID = $clientID;
        $searchModel->customer_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->query->andWhere(['flash_deals_id' => 0]);

        return $this->render('order', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
    }
    
    public function actionView($id)
    {
        $session = Yii::$app->session;
        $Model = MPOrder::find()->where(['mp_order_id' => $id,'customer_id' => Yii::$app->user->id])->one();
        if(count($Model) > 0){
            return $this->render('view', [
                'model' => $Model,
            ]);
        }else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionRedeem(){
        Yii::$app->cache->flush();
        $session = Yii::$app->session;
        //$data=Yii::$app->request->post('action');
        $data = Yii::$app->request->post();

        $mp_products_id = $data['mp_products_id'];
        $totqty = $data['qty'];
        $keys = array_combine($mp_products_id, $totqty);
        if (!empty($mp_products_id)) {
            $userAgent = \xj\ua\UserAgent::model();
            $platform = $userAgent->platform;
            $browser = $userAgent->browser;
            $version = $userAgent->version;
            
            $model = new MPOrder();
            $model->customer_id = Yii::$app->user->id;
            $model->ip = $_SERVER['REMOTE_ADDR'];
            $model->user_agent = $platform.'-'.$browser.'-'.$version;
            
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {
                    if ($flag) {
                        $data = array();
                        $pointtotal = 0;$sum = 0;
                        foreach ($keys as $key => $value) {
                            $product_id = $key;
                            $qty = $value;
                            $product = \common\models\MPProducts::find()
                                    ->where(['mp_products_id' => $product_id])
                                    ->one();

                            if (!empty($product->product_type_id)) {
                                $type = $product->productType->product_type_name;
                            } else {
                                $type = null;
                            }

                            if (!empty($product->product_colour_id)) {
                                $colour = $product->productColour->product_colour_name;
                            } else {
                                $colour = null;
                            }
                            $admin_fee = $product->products_price * 0.05;
                            $total_fee = $product->products_price + $admin_fee;
                            $point = $total_fee * 2;
                            $pointtotal = round($point) * $qty;
                            $sum+= $pointtotal;

                            $data[] = [$model->mp_order_id,$product_id,$product->productName->product_name, $type, $colour, $product->productSize->product_size_name, $qty, $product->products_price, $admin_fee];
                            //echo $product->productName->product_name.'<br>';
                        }

                        $customerReward = new VIPCustomerReward();
                        $customerReward->clientID = $session['currentclientID'];
                        $customerReward->customer_id = Yii::$app->user->id;        
                        $customerReward->order_id = $model->mp_order_id;
                        $customerReward->description = $model->invoice_prefix;
                        $customerReward->points = '-'.$sum;
                        $customerReward->bb_type = 'V';
                        $customerReward->date_added = date('Y-m-d');
                        $customerReward->mp_order_id = 1;
                        if (($flag = $customerReward->save(false)) === false) {        
                            $transaction->rollBack();
                        }
                        
                        $history = new MPOrderHistory();
                        $history->mp_order_id = $model->mp_order_id;
                        $history->order_status = 'Accepted';
                        $history->comment = 'New Order - '.$model->invoice_prefix;
                        $history->date_added = date('Y-m-d');
                        $history->type = 2;
                        if (($flag = $history->save(false)) === false) {        
                            $transaction->rollBack();
                        }
                    }                    
                }
                
                if ($flag) {
                    $transaction->commit();
                    Yii::$app->db
                    ->createCommand()
                    ->batchInsert('mp_order_item', ['mp_order_id','product_id','product_name', 'product_type','product_colour','product_pack_size','quantity','price','admin_fee'],$data)
                    ->execute();
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Thank you', 'text' => 'Thank you for order.']);
                    $data = '';
                    
                    $code = 'mp_order.status_accepted';
                    $returnedValue = Yii::$app->VIPglobal->sendEmailMPOrder($model->mp_order_id, Yii::$app->params[$code], $data);
                    //return $this->redirect([$rpage.'/view', 'id' => $model->order_id]);
                    //return $this->redirect($rpage . '/view/' . $model->order_id);
                    return $this->redirect(['index']);
                } else {
                    \Yii::$app->getSession()->setFlash('error', ['title' => 'Sorry', 'text' => 'Thank you for order.']);
                    return $this->redirect(['index']);
                }
            } catch (Exception $ex) {
                $transaction->rollBack();
            }
            
            
            
        }
    }
    
    public function actionProductName($id){
        $names = \common\models\MPProducts::find()
                ->where(['type' => $id, 'products_status' => 'active'])
                ->groupby('product_name_id')
                ->all();
        
        if ($names) {
            $opt = "<option value=''>-</option>";
            foreach ($names as $name) {
                $opt .= "<option value='" . $name->product_name_id . "'>" . $name->productName->product_name . "</option>";
            }
            
            echo $opt;
        } else {
            echo "<option value=''>-</option>";
        }
    }
    
    public function actionProductSize($type,$productname,$type_colour){
        if($type == 'Bases') {
            $products = \common\models\MPProducts::find()
                ->where(['product_name_id' => $productname, 'type' => $type, 'product_type_id' => $type_colour])
                //->andWhere(['not', ['product_type_id' => $type_colour]])
                //->groupby('product_size_id')
                ->all();
        }else if($type == 'Colour') {
            $products = \common\models\MPProducts::find()
                ->where(['product_name_id' => $productname, 'type' => $type, 'product_colour_id' => $type_colour])
                //->andWhere(['not', ['product_type_id' => $type_colour]])
                //->groupby('product_size_id')
                ->all();
        }else {
            $products = \common\models\MPProducts::find()
                ->where(['product_name_id' => $productname, 'type' => $type])
                //->andWhere(['not', ['product_type_id' => $type_colour]])
                //->groupby('product_size_id')
                ->all();
        }

        
        if ($products) {
            $opt = "<option value=''>-</option>";
            foreach ($products as $product) {
                $opt .= "<option value='" . $product->product_size_id . "'>" . $product->productSize->product_size_name . "</option>";
            }
            
            echo $opt;
        } else {
            echo "<option value=''>-</option>";
        }
    }
    
    public function actionProductPoint($type,$productname,$typecolour,$packsize){
        if(!empty($typecolour)) {
            if($type == 'Bases') {
                $products = \common\models\MPProducts::find()
                    ->where(['product_name_id' => $productname, 'type' => $type, 'product_type_id' => $typecolour, 'product_size_id' => $packsize])
                    ->one();
            }else if($type == 'Colour') {
                $products = \common\models\MPProducts::find()
                    ->where(['product_name_id' => $productname, 'type' => $type, 'product_colour_id' => $typecolour, 'product_size_id' => $packsize])
                    ->one();
            }
        }else {
            $products = \common\models\MPProducts::find()
                    ->where(['product_name_id' => $productname, 'type' => $type, 'product_size_id' => $packsize])
                    ->one();
        }
        
        $admin_fee = $products->products_price * 0.05;
        $total_fee = $products->products_price + $admin_fee;
        $point = $total_fee * 2;
        
        //echo $admin_fee.'+'.$total_fee;
        return json_encode(['point' => round($point), 'mp_products_id' => $products->mp_products_id, 'price' => $products->products_price, 'admin_fee' => $admin_fee]);
        //echo round($point);
    }
    
    public function actionProducts($id,$type){
        if($type == 'Bases') {
            $products = \common\models\MPProducts::find()
                ->where(['product_name_id' => $id, 'type' => $type])
                ->andWhere(['not', ['product_type_id' => null]])
                ->groupby('product_type_id')
                ->all();
            
            $type_arr = array();
            if (!empty($products)) {
                foreach ($products as $product) {
                    $type_arr[] = array("id" => $product->product_type_id, "name" => $product->productType->product_type_name);
                }

            } else {
                $type_arr = "";
            }
        }else if($type == 'Colour') {
            $products = \common\models\MPProducts::find()
                ->where(['product_name_id' => $id, 'type' => $type])
                ->andWhere(['not', ['product_colour_id' => null]])
                ->groupby('product_colour_id')
                ->all();
            $type_arr = array();
            if (!empty($products)) {
                foreach ($products as $product) {
                    $type_arr[] = array("id" => $product->product_colour_id, "name" => $product->productColour->product_colour_name);
                }

            } else {
                $type_arr = "";
            }
            
        }
        
        
        $sizes = \common\models\MPProducts::find()
                ->where(['product_name_id' => $id])
                ->andWhere(['not', ['product_size_id' => null]])
                ->groupby('product_size_id')
                ->all();

        $size_arr = array();
        if (!empty($sizes)) {
            foreach ($sizes as $size) {
                $productsize = MPProductSize::find()->where(['product_size_id' => $size->product_size_id])->one();
                $size_arr[] = array("id" => $size->product_size_id, "name" => $productsize->product_size_name);
            }
            
        } else {
            $size_arr = "";
        }
        
        echo json_encode(array('ptype' => $type_arr, 'size' => $size_arr));
    }
}