<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\MPProductName;
?>
<style>
    th.dt-checkboxes-cell.dt-checkboxes-select-all.sorting_disabled {display: none;}
    td.dt-checkboxes-cell {display: none;}
    .action-filters {display: none;}
    tr.odd {display: none;}
    #place-order-summery {display: none;}
    #nocreadit{display: none;}
</style>
<section id="data-list-view" class="data-list-view-header">
    
<?=Html::beginForm(['redeem'],'post',['id' => 'form1']);?>
    <!-- DataTable starts -->
    <div class="table-responsive">
        <table class="table data-list-view" id="retc">
            <thead>
                <tr>
                    <th></th>
                    <th>Product Name</th>
                    <th>Qty</th>
                    <th>Unit pts</th>
                    <th>Total pts</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                

            </tbody>
        </table>
    </div>
    
    <div class="row" id="place-order-summery">
        <div class="col-lg-6 col-sm-12 data-field-col">
            <div class="checkout-options">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="detail">
                                <div class="details-title detail-top">
                                    Your Current Balance
                                </div>

                                <div class="detail-amt total-amt discount-amt text-danger"><span id="balance"><?= Yii::$app->formatter->asInteger(Yii::$app->VIPglobal->myAvailablePoint()) ?></span> pts</div>
                            </div>
                            
                            <div class="detail">
                                <div class="details-title detail-top">
                                    Sub Total
                                </div>
                                <div class="detail-amt">
                                    <strong><span id="sub_total">0</span> pts</strong>
                                </div>
                            </div>
                            
                            <div class="detail">
                                <div class="details-title detail-top">
                                    Delivery Charges
                                </div>
                                <div class="detail-amt">
                                    0 pts
                                </div>
                            </div>
                            <hr>
                            <div class="detail detail-bot">
                                <div class="details-title">
                                    Total
                                </div>
                                <div class="detail-amt total-amt"><span id="total">0</span> pts</div>
                            </div>
                            <div class="detail detail-bot">
                                <div class="details-title">
                                    New Balance
                                </div>
                                <div class="detail-amt total-amt text-success"><span id="new-balance"><?= Yii::$app->formatter->asInteger(Yii::$app->VIPglobal->myAvailablePoint()) ?></span> pts</div>
                            </div>
                            <?=
                            Html::submitButton('PLACE ORDER', ['class' => 'btn btn-primary btn-block place-order waves-effect waves-light', 'id' => 'place_order', 'data' => [
                                    //'confirm' => 'Are you sure you want to redeem selected items?',
                                    //'method' => 'post',                        
                                ],]);
                            ?>
                            <div class="alert alert-info" style="color: #ffffff;" id="nocreadit">
                                <strong>Sorry!</strong> Your point balance is insufficient to make this order. Please reduce the quantity or change your products.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
<?= Html::endForm() ?> 
    <!-- DataTable ends -->

    <!-- add new sidebar starts -->
    <div class="add-new-data-sidebar">
        <div class="overlay-bg"></div>
        <div class="add-new-data">
            <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
                <div>
                    <h4>ADD NEW</h4>
                </div>
                <div class="hide-data-sidebar">
                    <i class="feather icon-x"></i>
                </div>
            </div>
            <div class="data-items pb-3">
                <div class="data-fields px-2 mt-1">
                    <div class="row">
                        <div class="col-sm-12 data-field-col">
                            <label for="data-name">Product Type</label>
                            <?= Html::dropDownList('product_type', null,['Bases' => 'Bases', 'Colour' => 'Colour'],
                            ['prompt' => '--- select ---', 
                                'class' => 'form-control', 
                                'id' => 'product-type',
                                'onchange' => '$.get( "' . Url::toRoute('/merchandise/product/product-name') . '", {id: $(this).val() } )
                                 .done(function( data ) {
                                    $("#data-name").html(data);
                                    $("#data-point, #data-qty, #data-total-point").val("");
                                    $("#data-name, #data-type, #data-pack-size").prop("selectedIndex", 0);
                                 }
                                );'
                            ]) ?>
                        </div>
                        
                        <div class="col-sm-12 data-field-col">
                            <label for="data-name">Product Name</label>
                            <?= Html::dropDownList('product_name_id', null,
      ArrayHelper::map(MPProductName::find()->orderBy(['product_name' => SORT_DESC])->all(), 'product_name_id', 'product_name'),
                            ['prompt' => '--- select ---', 
                                'class' => 'form-control', 
                                'id' => 'data-name',
                                'onchange' => '$.get( "' . Url::toRoute('/merchandise/product/products') . '", {id: $(this).val(),type: $("#product-type").val() } )
                                 .done(function( data ) {
                                    var parse_data = $.parseJSON(data); //parse encoded data
                                    if (parse_data.ptype.length > 0) {
                                        var html="<option value>--- select ---</option>";

                                        for (var i = 0; i < parse_data.ptype.length; i++) {
                                            var id = parse_data.ptype[i][\'id\'];
                                            var name = parse_data.ptype[i][\'name\'];
                                            html=html+\'<option value="\'+id+\'">\'+name+\'</option>\';                             
                                        };
                                        $(\'#data-type\').html(html);
                                    }                                    
                                    
                                    if (parse_data.size.length > 0) {
                                        var html="<option value>--- select ---</option>";

                                        for (var i = 0; i < parse_data.size.length; i++) {
                                            var id = parse_data.size[i][\'id\'];
                                            var name = parse_data.size[i][\'name\'];
                                            html=html+\'<option value="\'+id+\'">\'+name+\'</option>\';
                                        };
                                        $(\'#data-pack-size\').html(html);
                                    }
                                    $("#data-point, #data-qty, #data-total-point").val("");
                                    $("#data-type, #data-pack-size").prop("selectedIndex", 0);
                                 }
                                );'
                            ]) ?>
                        </div>
                        
                        <div class="col-sm-12 data-field-col">
                            <label for="data-colour">Type/Colour</label>
                            <?= Html::dropDownList('product_type_colour', null,[],
                            ['prompt' => '--- select ---', 
                                'class' => 'form-control', 
                                'id' => 'data-type',
                                'onchange' => '$.get( "' . Url::toRoute('/merchandise/product/product-size') . '", {type: $("#product-type").val(),productname: $("#data-name").val(),type_colour: $(this).val(), } )
                                 .done(function( data ) {
                                    $("#data-pack-size").html(data);
                                    $("#data-point, #data-qty, #data-total-point").val("");
                                    $("#data-pack-size").prop("selectedIndex", 0);
                                 }
                                );'
                            ]) ?>
                        </div>
                        
                        <div class="col-sm-12 data-field-col">
                            <label for="data-pack-size">Pack Size</label>
                            <?= Html::dropDownList('product_size_id', null,[],
                            ['prompt' => '--- select ---', 
                                'class' => 'form-control', 
                                'id' => 'data-pack-size',
                                'onchange' => '$.get( "' . Url::toRoute('/merchandise/product/product-point') . '", {type: $("#product-type").val(),productname: $("#data-name").val(), typecolour: $("#data-type").val(), packsize: $(this).val(), } )
                                 .done(function( data ) {
                                    $("#data-qty, #data-total-point").val("");
                                    var parse_data = $.parseJSON(data);
                                    $("#data-point").val(parse_data.point);
                                    $("#mp-products-id").val(parse_data.mp_products_id);
                                    $("#mp-price").val(parse_data.price);
                                    $("#mp-admin-fee").val(parse_data.admin_fee);
                                 }
                                );'
                            ]) ?>
                        </div>
                        <div class="col-sm-4 data-field-col">
                            <label for="data-point">Point</label>
                            <input type="number" class="form-control text-right" id="data-point" readonly="">
                            <input type="hidden" id="mp-products-id">
                            <input type="hidden" id="mp-price">
                            <input type="hidden" id="mp-admin-fee">
                        </div>
                        <div class="col-sm-4 data-field-col">
                            <label for="data-qty">Qty</label>
                            <input type="number" class="form-control text-right" id="data-qty">
                        </div>
                        <div class="col-sm-4 data-field-col">
                            <label for="data-total-point">Total Point</label>
                            <input type="number" class="form-control text-right" id="data-total-point" readonly="">
                        </div>
                    </div>
                    <hr>
                    <div class="checkout-options mt-2">
                        <div class="detail">
                            <div class="details-title detail-top">
                                Your Current Balance
                            </div>

                            <div class="detail-amt total-amt discount-amt text-danger"><span id="balance"><?= Yii::$app->formatter->asInteger(Yii::$app->VIPglobal->myAvailablePoint()) ?></span> pts</div>
                        </div>

                        <div class="detail detail-bot">
                            <div class="details-title">
                                New Balance
                            </div>
                            <div class="detail-amt total-amt text-success"><span id="new-balance2">0</span> pts</div>
                        </div>
                    </div>
                </div>
                
                
            </div>
            <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
                <div class="add-data-btn">
                    <button id="add-row" class="add-row btn btn-primary">Add Product</button>
                </div>
                <div class="cancel-data-btn">
                    <button class="btn btn-outline-danger">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- add new sidebar ends -->
</section>

<?php
    $script = <<<EOD
      $("#data-qty").on("change", function() {
        var ret = parseInt($("#data-point").val()) * parseInt($("#data-qty").val())
        $("#data-total-point").val(ret);            
        var balance = $("#balance").text();
        balance = balance.replace(/,/g, "");
        var total = $("#total").text();    
        var sum = 0;
        if(total > 0) {
            sum = balance - total - ret; 
        }else {
            sum = balance - ret;
        }
        $('#new-balance2').text(sum);       
      })
          
    $('#place_order').click(function(e) {
        e.preventDefault();    
        var balance = $("#balance").text();
        var total = $("#total").text();
        balance = balance.replace(/,/g, "");    
        if(total == '0') {
            alert('Total can not be Zero');
        }else {
            if(parseInt(total) <= parseInt(balance)) {
                $("#form1").submit();  
             }else {
                alert('Your point balance is insufficient to make this order. Please reduce the quantity or change your products.'); 
             }
        }
    });
            
    $(".add-row").click(function(){
            if($("#product-type").val() == '')
            {
                alert('Select Product Type');
                return false;	
            }
            if($("#data-name").val() == '')
            {
                alert('Select Product Name');
                return false;	
            }

            if($("#data-pack-size").val() == '')
            {
                alert('Select Pack Size');
                return false;	
            }
            if($("#data-qty").val() == '')
            {
                alert('QTY can not be empty');
                return false;	
            }
            if($("#data-total-point").val() == 0)
            {
                alert('Total Point can not be 0');
                return false;	
            }
            $("#place-order-summery").show();
            var name = $("#data-name option:selected").html();
            var type_colour = $("#data-type option:selected").html();
            var type_size = $("#data-pack-size option:selected").html();
            var point = $("#data-point").val();
            var qty = $("#data-qty").val();
            var total_point = $("#data-total-point").val();
            var mp_products_id = $("#mp-products-id").val();
            var mp_price = $("#mp-price").val();
            var mp_admin_fee = $("#mp-admin-fee").val();
            
            
            var markup = "<tr><td><input type='hidden' name='mp_products_id[]' value='" + mp_products_id + "'><input type='hidden' name='qty[]' value='" + qty + "'>" + name + "<br>" + type_colour + "<br>" + type_size + "</td><td>" + qty + "</td><td>" + point + "</td><td class='total'>" + total_point + "</td><td class='text-center'><a href='javascript::;' class='btnDelete' ><i class='fa fa-trash'></i></a></td></tr>";
            $("table tbody").append(markup);
            $(".add-new-data").removeClass("show");
            $(".overlay-bg").removeClass("show");
            $("#data-point, #data-qty, #data-total-point").val("");
            $("#product-type, #data-name, #data-type, #data-pack-size").prop("selectedIndex", 0);
            calc_total();
            new_balance();
            checkbal();
        });
    $("#retc").on('click','.btnDelete',function(){
                //$(this).closest('tr').remove();
                var tableRow = $(this).closest('tr');
                
                tableRow.find('td').fadeOut('fast', 
                    function(){ 
                        tableRow.remove();
                        var count = $('#retc tbody').children('tr').length;
                        var balance = $("#new-balance").text();
                        $('#new-balance2').text(balance);
                        calc_total();
                        new_balance();
                        checkbal();
                    }
                );
            });
                
            function calc_total(){
                var sum = 0;
                $('.total').each(function() {
                    sum += parseInt($(this).text());
                });
                $('#sub_total').text(sum);
                $('#total').text(sum);
                
            }
            
            function new_balance(){
                var balance = $("#balance").text();
                var total = $("#total").text();
                balance = balance.replace(/,/g, "");
                var sum = 0;
                sum = balance - total;
                $('#new-balance').text(sum);    
            }
            
            function checkbal(){
                var balance = $("#balance").text();
                var total = $("#total").text();
                balance = balance.replace(/,/g, "");    
                if(parseInt(total) <= parseInt(balance)) {
                    $("#place_order").show();
                    $("#nocreadit").hide();
                }else {
                    $("#place_order").hide();
                    $("#nocreadit").show();                        
                }
            }
            
EOD;
$this->registerJs($script);
    ?>