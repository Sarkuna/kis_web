<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = $model->invoice_prefix;
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Order History', 'url' => ['/merchandise/orders']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="card invoice-page">
    <div id="invoice-template" class="card-body">
        <!-- Invoice Company Details -->
        <div id="invoice-company-details" class="row">
            <div class="col-md-6 col-sm-12 text-left pt-1">
                <div class="media pt-1">
                </div>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                
                <?php
                $bg = $model->getStatusbg();
                echo '<div class="badge badge-'.$bg.' badge-lg mr-1 mb-1">' . $model->order_status . '</div>'; ?>
                
            </div>
        </div>
        <!--/ Invoice Company Details -->

        <!-- Invoice Recipient Details -->
        <div id="invoice-customer-details" class="row pt-2">
            <div class="col-md-6 col-sm-12 text-left">
                <div class="invoice-details mt-2">
                    <h6>ORDER NO.</h6>
                    <p><?= $model->invoice_prefix ?></p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                <h6 class="mt-2">ORDER DATE</h6>
                    <p><?= date('d/m/Y', strtotime($model->created_datetime)) ?></p>
            </div>
        </div>
        <!--/ Invoice Recipient Details -->

        <!-- Invoice Items Details -->
        <div id="invoice-items-details" class="pt-1 invoice-items-table">
            <div class="row">
                <div class="table-responsive col-sm-12">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Type/Colour</th>
                                <th>Pack Size</th>
                                <th>Qty</th>
                                <th>Unit pts</th>
                                <th>Total pts</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                    $total = ''; $productpoint = 0;
                                    $subtotal = 0; $total = 0; $shipping_point_total = 0;
                                    foreach ($model->orderProducts as $product) {
                                        $qty = $product->quantity;
                                        $admin_fee = $product->admin_fee;
                                        $total_fee = $product->price + $admin_fee;
                                        $point = $total_fee * 2;
                                        $pointtotal = round($point) * $qty;
                                            
                                            echo '<tr>
                                              <td>' . $product->product_name . '</td>
                                              <td>' . $product->product_type . $product->product_colour. '</td>
                                              <td>' . $product->product_pack_size.'</td>
                                              <td class="text-right">' . $qty . '</td>
                                              <td class="text-right">' .round($point). 'pts</td>
                                              <td class="text-right">' . Yii::$app->formatter->asInteger($pointtotal) . 'pts</td>
                                            </tr>';
                                            $total += $pointtotal;
                                    }
                                    ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div id="invoice-total-details" class="invoice-total-table">
            <div class="row">
                <div class="col-5 offset-7">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th><h3>Total</h3></th>
                                    <td  class="text-right text-bold-800"><h3><?= Yii::$app->formatter->asInteger($total) ?>pts</h3></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>