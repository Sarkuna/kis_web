<?php

use yii\helpers\Html;
use yii\helpers\Url;
//$productdescription = strip_tags($model->product_description);
$session = Yii::$app->session;
$cart = Yii::$app->session['cart'];
$point = Yii::$app->VIPglobal->myPoint($model->product->product_id);
$stockcount = Yii::$app->VIPglobal->stock($model->product->product_id);

if(!empty($cart)) {
    if(array_key_exists($model->product->product_id, $cart)){
        $currentqty = $cart[$model->product->product_id]['product_qty'];
        $stockcount = $stockcount - $currentqty;
    }
}

if(!empty($model->product->main_image)) {
    $imagename = $model->product->main_image;
}else {
    $imagename = 'no_image.png';
}
?>
<div class="card ecommerce-card">
    <div class="card-content">
        <div class="item-img text-center">
            <img class="img-fluid" src="<?= Yii::getAlias('@back') ?>/upload/product_cover/thumbnail/<?= $imagename ?>" alt="img-placeholder">
        </div>
        <div class="card-body">
            <div class="item-wrapper">
                <div class="item-rating">
                    <span><?= $model->product->product_code ?></span>
                </div>
                <div class="item-cost">
                    <h6 class="item-price">
                        <?= $point ?> <em>- Pts</em>
                    </h6>
                </div>
            </div>
            <div class="item-name">
                <span>
                <?= Html::a($model->product->product_name, ['/products/product/product-detail', 'id' => $model->product->product_id,])?>
                </span>    
            </div>
            <div>

            </div>
        </div>
        <div class="item-options text-center">
            <div class="item-wrapper">
                <div class="item-rating">
                    <span><?= $model->product->product_code ?></span>
                </div>
                <div class="item-cost">
                    <h6 class="item-price">
                        <?= $point ?> <em>- Pts</em>
                    </h6>
                </div>
            </div>

            <?php
            if($stockcount > 0) {
                echo '<div class="cart cartlist" onclick="addCart('.$model->product->product_id.',1);">
                    <i class="feather icon-shopping-cart mr-25"></i> <span class="add-to-cart">Add to cart</span>
                </div>';
            }else {
                echo '<div class="cart cartlistnoitem">
                    <i class="feather icon-shopping-cart mr-25"></i> Out of Stock
                </div>';
            }
            ?>
            
        </div>
    </div>
</div>
                        

                        
                        