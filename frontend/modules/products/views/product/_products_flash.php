<?php

use yii\helpers\Html;
use yii\helpers\Url;
//$productdescription = strip_tags($model->product_description);
$session = Yii::$app->session;
$cart = Yii::$app->session['cart'];
$point = Yii::$app->VIPglobal->myPoint($model->product->product_id);
$stockcount = Yii::$app->VIPglobal->stock($model->product->product_id);

if(!empty($cart)) {
    if(array_key_exists($model->product->product_id, $cart)){
        $currentqty = $cart[$model->product->product_id]['product_qty'];
        $stockcount = $stockcount - $currentqty;
    }
}

if(!empty($model->product->main_image)) {
    $imagename = $model->product->main_image;
}else {
    $imagename = 'no_image.png';
}
?>
<div class="card ecommerce-card">
    <div class="card-content">
        <div class="item-img text-center">
            <img class="img-fluid" src="<?= Yii::getAlias('@back') ?>/upload/product_cover/thumbnail/<?= $imagename ?>" alt="img-placeholder">
        </div>
        <div class="card-body">
            <div class="item-wrapper">
                <div class="item-rating">
                    <span><?= $model->product->product_code ?></span>
                </div>
                <div class="item-cost">
                    <h6 class="item-price">
                        <?= $point ?> <em>- Pts</em>
                    </h6>
                </div>
            </div>
            <div class="item-name">
                <span>
                <?= Html::a($model->product->product_name, ['/products/product/product-detail', 'id' => $model->product->product_id,])?>
                </span>    
            </div>
            <div>

            </div>
        </div>
        <div class="item-options text-center">
            <div class="item-wrapper">
                <div class="item-rating">
                    <span><?= $model->product->product_code ?></span>
                </div>
                <div class="item-cost">
                    <h6 class="item-price">
                        <?= $point ?> <em>- Pts</em>
                    </h6>
                </div>
            </div>
            <?php
            $nostock = '<div class="cart cartlistnoitem">
                    <i class="feather icon-shopping-cart mr-25"></i>Out of Stock
                </div>';
            if($model->product->categories->slug == 'alcoholic_beverages') {
               if($stockcount > 0) {
                echo '<div class="cart cartlist" data-toggle="modal" data-target="#example'.$model->product->product_id.'">
                    <i class="feather icon-shopping-cart mr-25"></i> <span class="add-to-cart">Add to cart</span> <a href="#" class="view-in-cart d-none">View In Cart</a>
                </div>';
               }else {
                   echo $nostock;
               }
            }else {
                if($stockcount > 0) {
                    echo '<div class="cart cartlist" onclick="addCart('.$model->product->product_id.',1);">
                        <i class="feather icon-shopping-cart mr-25"></i> <span class="add-to-cart">Add to cart</span> <a href="#" class="view-in-cart d-none">View In Cart</a>
                    </div>';
                }else {
                   echo $nostock;
                }
            }
            ?>
            
        </div>
    </div>
</div>
                        



<div class="modal fade" id="example<?= $model->product->product_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle"><?= $model->product->product_name ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p>Alcohol abuse is dangerous for your health.<br><br>
                    Alcohol is prohibited to Muslims and anyone below 21 years old. </p>

                <hr>
                <p>Penyalahgunaan alkohol berbahaya untuk kesihatan anda.<br><br>

Alkohol dilarang untuk orang Islam dan sesiapa yang berumur di bawah 21 tahun</p>

                


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="addCart(<?= $model->product->product_id ?>,1);">Add to cart F</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">close</button>
            </div>
        </div>
    </div>
</div>
                        