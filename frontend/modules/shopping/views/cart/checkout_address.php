<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = ['label' => 'Cart', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//$cid = Yii::$app->request->get('id');
//$sid = Yii::$app->request->get('sid');


?>

<div class="account-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
               <a href="/accounts/my-account/address" class="btn btn-primary">Manage Address</a> 
            </div>

            <div class="col-md-12" style="margin-top: 20px;">
            
                <div class="list-group">
                    <?php
                        $i = 0;
                        foreach($address as $key=>$addresslisit){
                            $fulladdress = $addresslisit->address_1.', '.$addresslisit->address_2.', '.$addresslisit->city.', '.$addresslisit->states->state_name.', '.$addresslisit->postcode.', '.$addresslisit->country->name;
                            echo '<a href="/shopping/cart/myaddress?id='.$addresslisit->vip_customer_address_id.'" class="list-group-item">  
                                <!--Heading with class .list-group-item-heading-->  
                                <h3 class="list-group-item-heading">'.$addresslisit->firstname.' '.$addresslisit->lastname.'</h4>  
                                <!--Paragraph with class .list-group-item-text -->  
                                <h4 class="list-group-item-text">'.$addresslisit->company.'</h4>
                                <p class="list-group-item-text">'.$fulladdress.'</p> 
                            </a>';
                            $i++;
                        }

                    ?>
                    
                </div>    
            </div>

        </div>
    </div>
</div>