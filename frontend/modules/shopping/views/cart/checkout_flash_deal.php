<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = ['label' => 'Cart', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//$cid = Yii::$app->request->get('id');
//$sid = Yii::$app->request->get('sid');
$carlists = $lisitcart;
$total = 0;
$subtotal = 0;
$cartlistscount = count($carlists);
$session = Yii::$app->session;
use common\models\VipCountry;
use common\models\VIPZone;

$countrylists = VipCountry::find()
        ->where(['country_id' => '129'])
        ->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');

$regionlists = \common\models\VIPStates::find()
        ->where(['country_id' => 129])
        ->orderBy([
    'state_name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'states_id', 'state_name');

$clientsetup = Yii::$app->VIPglobal->clientSetup();
$pointpersent = $clientsetup['point_value'];


?>
<?php
    $shipping_point = 0;
    $shipping_point_total = 0;
    $shipping_Fee = 0;
    $qtys = 0;
    $stateid = $myaddress->states->zone_id;
    foreach($carlists as $key => $carlist){                            

        $shipingcost = \common\models\VIPProductDeliveryZone::find()
            ->where(['product_id' => $key, 'clientID' => $session['currentclientID'], 'zone_id' => $stateid])
            ->count();
        if($shipingcost > 0) {
            $shipingcost = \common\models\VIPProductDeliveryZone::find()
            ->where(['product_id' => $key, 'clientID' => $session['currentclientID'], 'zone_id' => $stateid])
            ->one();

            $shipping_point = $shipingcost->price / $pointpersent;
            $shipping_point_total = $shipping_point * $carlist['product_qty'];
            $shipping_Fee += $shipping_point_total;

            //$shipping_point = $shipingcost->points / $pointpersent;
            //$shipping_point_total = $shipping_point * $carlist['product_qty'];

        }
        $points_value = $carlist['points_value'] + $shipping_point;
        $total_points = $carlist['points_value']* $carlist['product_qty'];

        $subtotal += $carlist['points_value'];
        $total += $total_points;
        $qtys += $carlist['product_qty'];
        $finalprice = $total + $shipping_Fee;
    }
?>
<style>
    .help-block {
    color: red;
}
.detail-top {
    color: #b8c2cc;
}
.detail-bot {
    color: #626262;
    font-weight: 600;
}
.detail1 {margin-bottom: 20px;}
.text-danger {font-weight: 600;font-size: 18px;}
</style>
<?php $form = ActiveForm::begin(); ?>
<section id="checkout-payment" class="list-view product-checkout">
    
    <div class="amount-payable checkout-options">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Order Summary</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="detail">
                        <div class="details-title detail-top">
                            Your Current Balance
                        </div>

                        <div class="detail-amt total-amt discount-amt text-danger"><?= Yii::$app->formatter->asInteger(Yii::$app->VIPglobal->myAvailablePoint()) ?> pts</div>
                    </div>
                    <div class="detail">
                        <div class="details-title detail-top">
                            Sub Total
                        </div>
                        <div class="detail-amt">
                            <strong><?= Yii::$app->formatter->asInteger($total) ?> pts</strong>
                        </div>
                    </div>
                    <div class="detail">
                        <div class="details-title detail-top">
                            Delivery Charges
                        </div>
                        <div class="detail-amt">
                            <?= Yii::$app->formatter->asInteger($shipping_Fee) ?> pts
                        </div>
                    </div>
                    <hr>
                    <div class="detail detail-bot">
                        <div class="details-title">
                            Total
                        </div>
                        <div class="detail-amt total-amt"><?= Yii::$app->formatter->asInteger($finalprice) ?> pts</div>
                    </div>
                    <div class="detail detail-bot">
                        <div class="details-title">
                            New Balance
                        </div>
                        <?php
                        $newbalance = Yii::$app->VIPglobal->myAvailablePoint() - ($finalprice);
                        $newbalance2 = str_replace('-', '', $newbalance);
                        if($newbalance > 0) {
                            $newbalance2 = $newbalance;
                        }else {
                            $newbalance2 = 0;
                        } 
                        ?>
                        <div class="detail-amt total-amt "><?= Yii::$app->formatter->asInteger($newbalance2) ?> pts</div>
                    </div>
                    <?php
                        if($newbalance < 0){
                            $newbalance = str_replace('-', '', $newbalance);
                            $clientsetup = Yii::$app->VIPglobal->clientSetup();
                            $pointpersent = $clientsetup['point_value'];
                            $amountrm = $newbalance * $pointpersent;
                            $amountpoint = $amountrm / $pointpersent;
                        }else {
                            $amountrm = 0.00;
                            $amountpoint = 0;
                        }
                    ?>
                    <div class="detail detail-bot">
                        <div class="details-title">
                            Top up Point
                        </div>                        
                        <div class="detail-amt total-amt discount-amt text-danger"><?= Yii::$app->formatter->asInteger($amountpoint) ?> pts</div>
                    </div>
                    <div class="detail detail-bot">
                        <div class="details-title">
                            Top up Cash
                        </div>                        
                        <div class="detail-amt total-amt discount-amt text-danger"><?= Yii::$app->formatter->asInteger($amountrm) ?> RM</div>
                    </div>
                    <hr>
                    <?php
                    //if(Yii::$app->VIPglobal->flashDeal() > 0 ){
                        echo $form->field($agreeform, 'agree', [
                                'template' => "<fieldset><div id='abc' class=\"vs-checkbox-con vs-checkbox-primary\">{input}<span class=\"vs-checkbox\"><span class=\"vs-checkbox--check\"><i class=\"vs-icon feather icon-check\"></i></span></span><span class=''> I have read and agree to the Terms & Conditions</span></div> \n{hint}\n{error}</fieldset>",
                            ])->checkbox(['data-toggle' => 'modal', 'data-target' => '#exampleModalScrollable'], false);

                        echo '<button class="btn btn-primary place-order" type="submit">CONFIRM ORDER</button>';
                    
                    //}
                    ?>
                    
                </div>
            </div>
        </div>
        
        
    </div>
    
    <div class="amount-payable checkout-options">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Bank Details</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <p>Below the title Bank Info, add this statement :</p>
                    <hr>
                    <div class="detail1">
                        <div class="details-title">
                            Bank Name
                        </div>
                        <div class="detail-amt">
                            <strong>Malayan Banking Berhad</strong>
                        </div>
                    </div>
                    <div class="detail1">
                        <div class="details-title">
                            Account Name
                        </div>
                        <div class="detail-amt">
                            <strong>Rewards Solution Sdn. Bhd</strong>
                        </div>
                    </div>
                    <div class="detail1">
                        <div class="details-title">
                            Account No
                        </div>
                        <div class="detail-amt">
                            <strong>5123 4362 7558</strong>
                        </div>
                    </div>
                    <hr>
                    Please email your payment receipt to support@businessboosters.com.my. Do indicate your Order ID in the receipt.<br>Thank you.
                </div>
            </div>
        </div>
        
        
    </div>
</section>

<?php ActiveForm::end(); ?>

<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Terms & Conditions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= Yii::$app->VIPglobal->terms() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>