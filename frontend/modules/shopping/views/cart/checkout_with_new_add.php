<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = ['label' => 'Cart', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//$cid = Yii::$app->request->get('id');
//$sid = Yii::$app->request->get('sid');
$carlists = $lisitcart;
$total = 0;
$subtotal = 0;
$cartlistscount = count($carlists);
$session = Yii::$app->session;
use common\models\VipCountry;
use common\models\VIPZone;
use common\models\VIPOptionValueDescription;

$countrylists = VipCountry::find()
        ->where(['status' => '0'])
        ->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');
$cid =  Yii::$app->getRequest()->getQueryParam('cid');

//die;
$df1 = '';
$df ='';
if(empty($cid)) {
    $cid = '129';
    $df1= 'default';
}else {
    $df= 'default';
}
$model->shipping_country_id = $cid;
$regionlists = \common\models\VIPStates::find()
        ->where(['country_id' => $cid])
        ->orderBy([
    'state_name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'states_id', 'state_name');

$options = VIPOptionValueDescription::find()
        ->where(['option_id' => '2'])
        ->orderBy([
    'name' => SORT_ASC,
])->all();
$optionslist = ArrayHelper::map($options, 'option_value_id', 'name');



?>

<div class="account-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-8">
                <!-- HTML -->
                <?php $form = ActiveForm::begin(); ?>
                <div id="accordion">
                    <h4 class="accordion-toggle"><span>01</span>Shipping Country</h4>
                    <div class="accordion-content <?= $df1 ?>">
                        <div class="row">
                            <ul class="form-list">
                                <?=
                            $form->field($model, 'shipping_country_id', [
                                'template' => "<li class='col-md-4 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
                            ])->dropDownList($country, ['prompt' => 'Select...']);
                            ?>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <h4 class="accordion-toggle"><span>02</span>Shipping information</h4>
                    <div class="accordion-content <?= $df ?>"> 
                        <div class="row">
                        <ul class="form-list"> 
                            <div class="shipping-new-address">
                            <?=
                                $form->field($model, 'shipping_firstname', [
                                    'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
                                ])->textInput(array('placeholder' => ''));
                            ?>
                            <?=
                                $form->field($model, 'shipping_lastname', [
                                    'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
                                ])->textInput(array('placeholder' => ''));
                            ?>
                            <?=
                            $form->field($model, 'shipping_company', [
                                'template' => "<li class='col-md-12 col-sm-12'>{label}{input}\n{hint}\n{error}</li>"
                            ])->textInput(array('placeholder' => ''));
                            ?>

                            <?=
                            $form->field($model, 'shipping_address_1', [
                                'template' => "<li class='col-md-12 col-sm-12'>{label}{input}\n{hint}\n{error}</li>"
                            ])->textInput(array('placeholder' => ''));
                            ?>

                            <?=
                            $form->field($model, 'shipping_address_2', [
                                'template' => "<li class='col-md-12 col-sm-12'>{label}{input}\n{hint}\n{error}</li>"
                            ])->textInput(array('placeholder' => ''));
                            ?>

                            <?=
                            $form->field($model, 'shipping_postcode', [
                                'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
                            ])->textInput(array('placeholder' => ''));
                            ?>

                            <?=
                            $form->field($model, 'shipping_city', [
                                'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
                            ])->textInput(array('placeholder' => ''));
                            ?>

                            <?=
                            $form->field($model, 'shipping_country_id', [
                                'template' => "<li class='col-md-4 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
                            ])->dropDownList($country);
                            ?>

                            <?=
                            $form->field($model, 'shipping_zone', [
                                'template' => "<li class='col-md-4 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
                            ])->dropDownList($regionlist, ['prompt' => 'Select...']);
                            ?>

                            </div>
                        </ul> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <h4 class="accordion-toggle"><span>03</span>Comment</h4>
                    <div class="accordion-content">
                        <div class="row">
                            <?=
                                $form->field($model, 'comment', [
                                    'template' => "<div class='col-md-12 col-sm-6'>{label}{input}\n{hint}\n{error}</div>"
                                ])->textarea(['rows' => '6']);
                            ?>
                            <?= $form->field($model, 'agree', [
                                'template' => "<div class='col-md-12 col-sm-6'>{input} I have read and agree to the Terms & Conditions \n{hint}\n{error}</div>",
                            ])->checkbox([],false) ?>
                        </div>
                    </div>
                    <h4 class="accordion-toggle"><span>04</span>Confirm Order</h4>
                    <div class="accordion-content">   
                        <div class="cart-infovip">
                        <?php                    
                    
                        echo '<table class="table table-bordered" id="cart-table"><tr>
                            <th>Product Image</th>
                            <th class="text-left">Product Name</th>
                            <th class="text-center">Product Code</th>
                            <th width="10%" class="text-center">Quantity</th>
                            <th class="text-center">Unit Point</th>
                            <th class="text-center">Total Points</th>
                        </tr>';
                        foreach($carlists as $key => $carlist){
                            
                            echo '<tr id="remove_'.$key.'">
                                <td><img src="'.Yii::getAlias('@back').'/upload/product_cover/thumbnail/'.$carlist['main_image'].'" class="img-responsive" alt="" style="width: 75px;"/></td>
                                <td style="text-align: left;">
                                    <h4><a href="/products/product/product-detail?id='.$key.'">'.$carlist['product_name'].'</a></h4>
                                    '.$carlist['opttionlisititem'].'
                                </td>
                                <td class="text-center">'.$carlist['product_code'].'</td>
                                <td class="text-center">'.$carlist['product_qty'].'</td>    
                                <td class="text-right">'.$carlist['points_value'].' pts</td>
                                <td class="text-right">'.$carlist['points_value']* $carlist['product_qty'].' pts</td>
                            </tr>';
                            $subtotal += $carlist['points_value'];
                            $total += $carlist['points_value']* $carlist['product_qty'];
                        }
                        echo '<tr><td colspan="5"></td>
                            <td class="a-right text-right">'.$total.' pts</td></tr>';
                        echo '</table>';                                             
                    
                    ?>
                       <div class="details-box">
                            <button class="btn-black" type="submit">Confirm Order</button>
                        </div> 
                        </div>
                    </div>                               
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs(' 
    $(document).ready(function(){
    $("#viporder-shipping_country_id").change(function(e) {
        var text = $("#viporder-shipping_country_id option:selected").val();
        window.location = "?cid="+text;
    });     
});', \yii\web\View::POS_READY);
?>