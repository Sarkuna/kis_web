<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shopping Cart';
$this->params['breadcrumbs'][] = $this->title;
//$cid = Yii::$app->request->get('id');
//$sid = Yii::$app->request->get('sid');
?>
<div class="shop-single shopping-cart" id="shoppingcart">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8">
                <div class="items box box-info">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6">2 ITEMS</div>
                            <div class="col-md-2 text-center">QUANTITY</div>
                            <div class="col-md-2">PRICE</div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                    <div class="box-body">
                    <?php                    
                    $carlists = $lisitcart;
                    $total = 0;
                    $subtotal = 0;
                    $cartlistscount = count($carlists);
                    if($cartlistscount > 0) {
                        foreach($carlists as $key => $carlist){
                            echo '<div class="product">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="'.Yii::getAlias('@back').'/upload/product_cover/thumbnail/'.$carlist['main_image'].'" class="img-fluid mx-auto d-block image img-responsive" alt=""/>
                            </div>
                            <div class="col-md-10">
                                <div class="info">
                                    <div class="row">
                                        <div class="col-md-6 product-name">
                                            <div class="product-name">
                                                <a href="/products/product/product-detail?id='.$key.'">'.$carlist['product_name'].'</a>
                                                <div class="product-info1">
                                                    <div>Product Code: <span class="value">'.$carlist['product_code'].'</span></div>
                                                    <div>'.$carlist['opttionlisititem'].'</div>
                                                    <div>Delivery: <span class="value">0</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1 price">
                                            <span>'.$carlist['product_qty'].'</span>
                                        </div>
                                        <div class="col-md-4 price">
                                            <span>'.$carlist['points_value'].' pts</span>
                                        </div>
                                        <div class="col-md-1 price">
                                            <span><a class="deletebagitem btn btn-danger btn-sm" href="javascript::void(0)" onclick="deleteItem('.$key.');"><i class="fa fa-trash-o"></i></a></span>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
                        }
                    }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="summary">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Place Order</button>
                    <h3>Shipping & Billing</h3>
                    <div class="address-title-container">
                        <span class="address-title">Ship to</span>
                        <a href="###" class="address-edit automation-address-edit pull-right">EDIT</a>
                    </div>
                    <div class="address-name">Mohamed</div>
                    <div class="address-info-item address-value">22 1/2 Main Road</div>
                    <div class="address-info-item address-postcode">Obesekarapura, Colombo - Suburbs, Western</div>
                    <div class="address-info-item address-mobile">765499596</div>
                    <h3>Order Summary</h3>
                    <div class="summary-item"><span class="text">Subtotal (2 Items)</span><span class="price">$360</span></div>
                    <div class="summary-item"><span class="text">Shipping Fee</span><span class="price">$0</span></div>
                    <div class="summary-item"><span class="text">Total</span><span class="price">$360</span></div>
                    <button type="button" class="btn btn-primary btn-lg btn-block">Place Order</button>
                </div>
            </div>
        </div> 
        
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="contact-info space50">
                    <?php                    
                    $carlists = $lisitcart;
                    $total = 0;
                    $subtotal = 0;
                    $cartlistscount = count($carlists);
                    if($cartlistscount > 0) {
                        echo '<table class="cart-table" id="cart-table"><tr>
                            <th>Remove</th>
                            <th>Product Image</th>
                            <th>Product Name</th>
                            <th>Product Code</th>
                            <th>Quantity</th>
                            <th>Unit Point</th>
                            <th>Total Points</th>
                        </tr>';
                        foreach($carlists as $key => $carlist){
                            
                            echo '<tr id="remove_'.$key.'">
                                <td><a class="deletebagitem btn btn-danger" href="javascript::void(0)" onclick="deleteItem('.$key.');"><i class="fa fa-trash-o"></i></a></td>
                                <td><img src="'.Yii::getAlias('@back').'/upload/product_cover/thumbnail/'.$carlist['main_image'].'" class="img-responsive" alt=""/></td>
                                <td>
                                    <h4><a href="/products/product/product-detail?id='.$key.'">'.$carlist['product_name'].'</a></h4>
                                    '.$carlist['opttionlisititem'].'
                                </td>
                                <td>'.$carlist['product_code'].'</td>
                                <td>
                                    <div class="cart_quantity_button">
                                            <span><input name="quantity_('.$key.')" class="btn" type="text" value="'.$carlist['product_qty'].'" id="quantity_'.$key.'" autocomplete="off" size="2"></span>    
                                            <a class="cart_quantity_down btn btn-primary" href="javascript::void(0)" onclick="itemQty('.$key.');"><i class="fa fa-refresh"></i></a>
                                    </div>
                                </td>
                                <td>
                                    '.$carlist['points_value'].' pts
                                </td>
                                <td>
                                    '.$carlist['points_value']* $carlist['product_qty'].' pts
                                </td>
                            </tr>';
                            $subtotal += $carlist['points_value'];
                            $total += $carlist['points_value']* $carlist['product_qty'];
                        }
                        echo '</table>';
                        echo '<div class="table-btn">
                            <a href="'.Yii::$app->homeUrl.'" class="btn-black pull-left">Continue Shopping</a>
                        </div>';
                        echo '<div class="clearfix space20"></div>';
                        echo '<div class="row shipping-info-wrap">
                            <div class="col-md-4 col-md-offset-8 col-sm-4 col-sm-offset-8 col-xs-12">
                                
                                    <ul class="checkout-types">
                                        <li class="space10">
                                        '.Html::a('Proceed to checkout', ['/shopping/cart/update-order-info'], ['class' => 'btn btn-color']).'
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>';                        
                    }else {
                        echo '<p>Your shopping cart is empty!</p>';
                    }
                    ?> 
                </div>
            </div>
            
            <div class="col-md-3 col-sm-4">
                <div id="account-id">
                    <?php
                echo '<div class="totals">
                                    <table id="shopping-cart-totals-table">
                                        <tfoot>
                                            <tr>
                                                <td class="a-right" colspan="1">
                                                    <strong>Grand Total: </strong>
                                                </td>
                                                <td class="a-right">
                                                    <strong><span class="price"> '.$total.' pts</span></strong>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <tr>
                                                <td class="a-right" colspan="1">
                                                    Sub Total:    
                                                </td>
                                                <td class="a-right">
                                                    <span class="price"> '.$subtotal.' pts</span>    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="a-right" colspan="1">
                                                    Shipping Cost:    
                                                </td>
                                                <td class="a-right">
                                                    <span class="price"> 0 pts</span>    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>';
                ?>
                </div>
                
            </div>

        </div>
    </div>
</div>
<div class="clearfix space20"></div>