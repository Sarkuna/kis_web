<?php
use frontend\assets\CreativeAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

CreativeAsset::register($this);
$session = Yii::$app->session;
if(!empty($session['currentclientID'])){
   $client = \common\models\Client::findOne($session['currentclientID']);
   $favicon = $client->favicon;
   $programme_title = $client->programme_title;
}else {
   $favicon = 'faviconvip.ico';
   $programme_title = 'VIP';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= Yii::getAlias('@back')?>/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title>123<?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        .page-header {
    padding-bottom: 9px;
    margin: 0px 0 0px;
    border-bottom: 1px solid #eee;
}
.si {width: 150px;}
    </style>
</head>
<body class="login-page sidebar-collapse">
    <?php $this->beginBody() ?>
    <?= Alert::widget() ?>
    <div class="page-header" filter-color="blue">
        <?php
        if (!empty($session['background'])) {
            $bgimg = Yii::getAlias('@back') . '/upload/client_bg/' . $session['background'];
        } else {
            $bgimg = '/images/hddclogin.jpg';
        }
        ?>
        <div class="page-header-image" style="background-image:url(<?= $bgimg ?>)"></div>

        <div class="container">
            <?= $content ?>
            
            <div class="col-md-4 content-left logo-container text-center">
            <div id="warning" style="display:none; margin-top: 10px;">
                    <p style="font-weight: bold;font-style: italic; color: #f44336;">Best viewed in Google Chrome and Mozilla Firefox version 3.0 and above (with resolution 1024x768)</p>
                </div>
            </div>
        </div>



        <footer class="footer">
            <div class="container">

                
                <div class="copyright card-login">
                    <?php
                    if ($session['mobileapp'] == 1) {
                        echo '<span>
                        <a href="https://itunes.apple.com/my/app/kansai-paint-dealer-rewards-point/id1015991312?mt=8" target="_blank"><img class="si" src="/images/app_store1.png"></a>
                        <a href="https://play.google.com/store/apps/details?id=com.kansai.madcat&hl=en" target="_blank"><img  class="si" src="/images/google_play1.png"></a>
                    </span>';
                    }
                    ?>
                    <div>
                        Copyright &copy; 2015-<script>document.write(new Date().getFullYear());</script> <a href="//businessboosters.com.my" target="_blank">Rewards Solution Sdn Bhd.</a> All Rights Reserved.
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <script type='text/javascript'>//<![CDATA[
window.onload=function(){
function GetIEVersion() {
  var sAgent = window.navigator.userAgent;
  var Idx = sAgent.indexOf("MSIE");

  // If IE, return version number.
  if (Idx > 0) 
    return parseInt(sAgent.substring(Idx+ 5, sAgent.indexOf(".", Idx)));

  // If IE 11 then look for Updated user agent string.
  else if (!!navigator.userAgent.match(/Trident\/7\./)) 
    return 11;

  else
    return 0; //It is not IE
}

if (GetIEVersion() > 0)
    $('#warning').show();
   //alert("Your Browser Is Not Supported " + GetIEVersion());
}
////]]> 

    </script>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
