<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\SmileAsset;
use common\widgets\Alert;

use app\components\TopMenuWidget;
use app\components\TopMainMenuWidget;
use app\components\SliderWidget;
use app\components\FooterWidget;

SmileAsset::register($this);
$controller = Yii::$app->controller;
$default_controller = Yii::$app->defaultRoute;
$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;

if($isHome == true){
    $home= 'header4';
}else{
    $home= '';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    <?php $this->head() ?>
</head>
<body id="home4">
<?php $this->beginBody() ?>
    <div id="loader"></div>
    <div class="body">
        <div id="<?= $home ?>">
            <?= TopMenuWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
            <?= TopMainMenuWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        </div>
        <?= SliderWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        
        <div class="bcrumbs">
            <div class="container">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'options' => ['class' => 'breadcrumb123'],
                ]) ?>
            </div>
        </div>
        <?= Alert::widget() ?>
        <?= $content ?>
        <!-- FOOTER -->
        <?= FooterWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
