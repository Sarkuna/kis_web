<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

//$this->layout = "loginlayout";        
$this->title = $name;
$session = Yii::$app->session;
$myclient = \common\models\Client::find()->where([
                'clientID' => $session['currentclientID'],
            ])->one();
?>
<?php if (Yii::$app->user->isGuest) { 
    //Url::to(['/site/login']);
    echo '<div class="container">
        <div class="col-md-4 content-left">
            <div class="card card-login card-plain" style="top: 20%;position: fixed;left: 5%;">
                <div class="header header-primary text-center">
                    <div class="logo-container">';
                        if (!empty($session['currentLogo'])) {
                            echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '">';
                        } else {
                            echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="200">';
                        }
                    echo '</div>                        
                </div>
                
                <div class="container" style="color: #000000;padding: 20px;">'.nl2br(Html::encode($message)).'</div>
                    <p><a href="'.Url::to(['/site/login']).'" class="btn btn-lg btn-theme-inverse btn-block">Go to login page</a></p>
            </div>
        </div>
    </div>';
}else {
    echo '<div class="container">
    <div class="col-main col-lg-12 text-center">';
                $exception = Yii::$app->errorHandler->exception;
                if ($exception instanceof \yii\web\NotFoundHttpException) {
                    echo '<i class="fa fa-exclamation-triangle text-warning" style="font-size: 4em;"></i>';
                    echo '<h2>Page not found</h2>
                    <div class="space30"></div>
                    <p>We\'re sorry, the page you requested could not be found. Please go back to homepage or contact us at <a href="mailto:'.$myclient->support_email.'">'.$myclient->support_email.'</a></p>
                    <a class="btn btn-primary" href="'.Url::to(['/site/index']).'">Visit Homepage</a>'; 
                } else {
                  echo '<i class="fa fa-bug text-danger" style="font-size: 4em;"></i>';
                  echo '<h2>Sorry, we have encountered a problem</h2>
                    <div class="space30"></div>
                    <p>An error had occurred while the web server was processing your request. Please email us at <a href="mailto:help@businessboosters.com.my">help@businessboosters.com.my</a> or contact our support team at +603-74909139 and tell us what you were trying to do. Thank you.</p>
                    <div class="space30"></div>
                    <p>Code: '.Html::encode($this->title).'</p>
                    <p>Message: '.Html::encode($message).'</p>     
                    <div class="space30"></div>
                    <a class="btn btn-primary" href="'.Url::to(['/site/index']).'">Visit Homepage</a>'; 
                }
                   
            echo '</div>
            
</div><div class="space30"></div>';
}
?>



