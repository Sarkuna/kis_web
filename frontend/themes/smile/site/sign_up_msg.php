<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Thank you';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>

<div class="col-md-4 content-left">
    <div class="card card-login card-plain" style="margin-top: 20%; left: 5%;">

        <div class="header header-primary text-center">
            <div class="logo-container">
                <?php
                //echo '<pre>'.print_r($session).die();
                if (!empty($session['currentLogo'])) {
                    echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '">';
                } else {
                    echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="200">';
                }
                ?>
            </div>
        </div>

        <div class="content"> 
            <?= $title ?>
            <p><?= $msg ?></p>
            <a class="btn white outline sm" href="/site/login">Log in</a>
        </div>


        


        <div class="clearfix"></div>




    </div>
</div>
