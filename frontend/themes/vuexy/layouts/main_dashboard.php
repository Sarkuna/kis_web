<?php

/* @var $this \yii\web\View */
/* @var $content string */
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use app\components\BreadcrumbsVuexy;
use frontend\assets\VuexyDashboardAsset;
use common\widgets\Alert;

use app\components\HeaderVuexy;
use app\components\MainMenuVuexy;
use app\components\CopyrightVuexy;

VuexyDashboardAsset::register($this);

if(!empty($session['currentclientID'])){
   $client = \common\models\Client::findOne($session['currentclientID']);
   $favicon = $client->favicon;
   $programme_title = $client->programme_title;
}else {
   $favicon = 'faviconvip.ico';
   $programme_title = 'VIP';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= Yii::getAlias('@back')?>/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= $programme_title ?> - <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">  
<?php $this->beginBody() ?>
    <!-- BEGIN: Header-->
        <?= HeaderVuexy::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
        <?= MainMenuVuexy::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <?= $content ?>
                <!-- Dashboard Analytics Start -->
                    
                <!-- Dashboard Analytics end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <?= CopyrightVuexy::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
    <!-- END: Footer-->
        
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
