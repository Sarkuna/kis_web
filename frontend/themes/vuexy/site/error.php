<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$exception = Yii::$app->errorHandler->exception;
if ($exception !== null) {
        $statusCode = $exception->statusCode;
        $name = $exception->getName();
        $message = $exception->getMessage();
        
        if($statusCode == '404') {
            $img = $this->theme->baseUrl.'/app-assets/images/pages/404.png';
        }else if($statusCode == '504') {
            $img = $this->theme->baseUrl.'/app-assets/images/pages/500.png';
        }else {
            $img = $this->theme->baseUrl.'/app-assets/images/pages/not-authorized.png';
        }
        
        echo '<img src="'.$img.'" alt="branding logo">
            <h1 class="font-large-2 my-1">'.$statusCode.' - '.$name.'</h1>
            <p class="p-2">'.$message.'</p>';
}
?>
