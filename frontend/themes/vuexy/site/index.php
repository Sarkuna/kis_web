<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use app\components\DashboardVuexy;


$this->title = 'Dashboard';
$imgurl = $this->theme->basePath;

//die();
?>

<?php if(Yii::$app->user->identity->type == 5){ ?>
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Message</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><?= Yii::$app->params['msg.upload.receipt'] ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php } ?>
<?= DashboardVuexy::widget(['path' => Yii::$app->request->getPathInfo()]) ?>

<?php
$script = <<< JS
    //$(window).on('load',function(){
        //if ($.cookie('pop') == null) {
            $('#default').modal('show');
            //$.cookie('pop', '1');
        //}
    //});
JS;
$this->registerJs($script);
?>
