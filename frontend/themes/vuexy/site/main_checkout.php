<?php

/* @var $this \yii\web\View */
/* @var $content string */
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\VuexyCheckoutAsset;
use common\widgets\Alert;

use app\components\HeaderVuexy;
use app\components\MainMenuVuexy;

/*use app\components\FooterWidget;

use app\components\H3TopBarWidget;
use app\components\H3HeaderWidget;
use app\components\H3SliderWidget;
use app\components\H3MobileMenu;
use app\components\ProfileNameWidget;
use app\components\ShoppingCartBagWidget;*/

VuexyCheckoutAsset::register($this); 
//$controller = Yii::$app->controller;
//$default_controller = Yii::$app->defaultRoute;
//$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;

$imgurl = $this->theme->basePath;

$myclient = \common\models\Client::find()->where([
                'clientID' => $session['currentclientID'],
            ])->one();

$clientname = common\models\ClientAddress::find()->where([
    'clientID' => $session['currentclientID'],
])->one();

$session['currentclientID'] = $myclient->clientID;
$session['currentclientName'] = $myclient->company;
//$session['currentLogo'] = $clientname->company_logo;
$session['adminURL'] = $myclient->admin_domain;
//$session['membership'] = $myclient->membership;
$session['background'] = $myclient->background_img;
//$session['upload_receipt_module'] = $myclient->upload_receipt_module;

if(!empty($session['currentLogo'])){
    $img = $session['adminURL']."/upload/client_logos/".$session['currentLogo'];            
}else{
    $img = $imgurl.'images/basic/logo-lite.png';
}

if(!empty($session['currentclientID'])){
   $client = \common\models\Client::findOne($session['currentclientID']);
   $favicon = $client->favicon;
   $programme_title = $client->programme_title;
}else {
   $favicon = 'faviconvip.ico';
   $programme_title = 'VIP';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= Yii::getAlias('@back')?>/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= $programme_title ?> - <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="vertical-layout vertical-menu-modern content-detached-left-sidebar ecommerce-application navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="content-detached-left-sidebar">
<?php $this->beginBody() ?>
    <!-- BEGIN: Header-->
        <?= HeaderVuexy::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
        <?= MainMenuVuexy::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0"><?= Html::encode($this->title) ?></h2>
                            <div class="breadcrumb-wrapper col-12">
                                <?php
                                echo Breadcrumbs::widget([
                                    'tag' => 'ol',
                                    'homeLink' => ['label' => 'Home', 'url' => ['/index']],
                                    'activeItemTemplate' => "<li class='breadcrumb-item'>{link}</li>",
                                    'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>",
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                    //'links' => '<li class="breadcrumb-item"><a href="{url}">{label}</a></li>',
                                    'encodeLabels' => false,
                                    //'delimiter'=>' / ',
                                    'options'=> array('class'=>'breadcrumb'),
                                ]);
                                //<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i></li>
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <?= Alert::widget() ?>
                <?= $content ?>
                <!-- Dashboard Analytics Start -->
                    
                <!-- Dashboard Analytics end -->


        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix blue-grey lighten-2 mb-0">
            <span class="float-md-left d-block d-md-inline-block mt-25">Copyright &copy; 2015-<script>document.write(new Date().getFullYear());</script>
                <a class="text-bold-800 grey darken-2" href="businessboosters.com.my" target="_blank">Rewards Solution Sdn Bhd,</a>
                All rights Reserved
            </span>
        </p>
    </footer>
    <!-- END: Footer-->
        
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>