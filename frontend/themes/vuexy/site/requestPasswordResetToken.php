<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>

<div class="row m-0">
            <div class="col-lg-6 d-lg-block d-none text-center align-self-center">
                <img src="<?php echo $this->theme->baseUrl ?>/app-assets/images/pages/forgot-password.png" alt="branding logo">
            </div>
            <div class="col-lg-6 col-12 p-0">
                <div class="card rounded-0 mb-0 px-2 py-1">
                    <div class="card-header pb-1">
                        <div class="card-title">
                            <h4 class="mb-0">Recover your password</h4>
                        </div>
                    </div>
                    <p class="px-2 mb-0">Please enter your email address and we'll send you instructions on how to reset your password.</p>
                    <div class="card-content">
                        <div class="card-body">
                            <?php $form = ActiveForm::begin(['id' => 'form']); ?>
                            <?= $form->field($model, 'email', [
                                //'template' => "<div class='input-group form-group-no-border input-lg'><span class='input-group-addon'><i class='now-ui-icons users_circle-08'></i></span>{input}</div>\n{hint}\n{error}"
                                ])->textInput(array('placeholder' => 'Email'))->label(false);
                            ?>
                            <div class="float-md-left d-block mb-1">
                                <a href="<?= Yii::$app->homeUrl; ?>" class="btn btn-outline-primary btn-block px-75">Back to Login</a>
                            </div>
                            <div class="float-md-right d-block mb-1">
                                <?= Html::submitButton('Recover Password', ['class' => 'btn btn-primary btn-block px-75', 'name' => 'login-button']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php
$script = <<< JS
    $(document).ready(function (e) {
    $('#retrieve').change(function () {
        if ($(this).val() == 'E') {
            $('.field-passwordresetrequestform-email').show();
            $('.field-passwordresetrequestform-mobile').hide();
        } else {
            $('.field-passwordresetrequestform-email').hide();
            $('.field-passwordresetrequestform-mobile').show();
        }
    });
});
JS;
$this->registerJs($script);
?>