<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Reset Password';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>

<div class="col-lg-6 d-lg-block d-none text-center align-self-center p-0">
    <img src="<?php echo $this->theme->baseUrl ?>/app-assets/images/pages/reset-password.png" alt="branding logo">
</div>

<div class="col-lg-6 col-12 p-0">
    <div class="card rounded-0 mb-0 px-2">
        <div class="card-header pb-1">
            <div class="card-title">
                <h4 class="mb-0">Reset Password</h4>
            </div>
        </div>
        <p class="px-2">Please enter your new password.</p>
        <div class="card-content">
            <div class="card-body pt-1">
                <?php $form = ActiveForm::begin(['id' => 'form', 'options' => ['novalidate' => '']]); ?>
                            <?=
                    $form->field($model, 'password', [
                        'template' => "<fieldset class='form-label-group'>{input}<label for='user-password'>Password</label>{error}</fieldset>"
                    ])->passwordInput(array('placeholder' => 'Password'))->label(false);
                    ?>
                         <?=
                    $form->field($model, 'password_repeat', [
                        'template' => "<fieldset class='form-label-group'>{input}<label for='user-confirm-password'>Confirm Password</label>{error}</fieldset>"
                    ])->passwordInput(array('placeholder' => 'Confirm Password'))->label(false);
                    ?>   


                    <div class="row pt-2">
                        <div class="col-12 col-md-6 mb-1">
                            <a href="<?= Yii::$app->homeUrl; ?>" class="btn btn-outline-primary btn-block px-0">Go Back to Login</a>
                        </div>
                        <div class="col-12 col-md-6 mb-1">
                            <button type="submit" class="btn btn-primary btn-block px-0">Reset</button>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
