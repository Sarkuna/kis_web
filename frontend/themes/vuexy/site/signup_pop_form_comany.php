<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use borales\extensions\phoneInput\PhoneInput;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use common\models\Salutation;
use common\models\EndUserLevel;
use common\models\VIPZone;
use common\models\VIPCustomer;


$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;



?>

<div class="col-lg-12 col-12 p-0">
    <div class="card rounded-0 mb-0 p-2">
        <div class="card-header pt-50 pb-1">
            <div class="card-title">
                <h4 class="mb-0">Account</h4>
            </div>
        </div>
        <p class="px-2">Finish setting up your account.</p>
        <div class="card-content">
            <div class="card-body pt-0">
                <?php $form = ActiveForm::begin(['id' => 'form', 'options' => ['novalidate' => '']]); ?>
                
                    <?=
                    $form->field($model, 'company_name', [
                        //'template' => "<div class='col-md-4 col-sm-4 col-xs-12 text-right'>{label}</div>\n<div class='col-md-8 col-sm-8 col-xs-12'>{input}\n{hint}\n{error}</div>"
                    ])->textInput()->label();
                    ?>

                    <?=
                    $form->field($model, 'company_address', [
                        //'template' => "<div class='col-md-4 col-sm-4 col-xs-12 text-right'>{label}</div>\n<div class='col-md-8 col-sm-8 col-xs-12'>{input}\n{hint}\n{error}</div>"
                    ])->textarea(array('rows'=>4,'cols'=>5));
                    ?>
                    
                    <?=
                    $form->field($model, 'ssm_number', [
                        //'template' => "<div class='col-md-4 col-sm-4 col-xs-12 text-right'>{label}</div>\n<div class='col-md-8 col-sm-8 col-xs-12'>{input}\n{hint}\n{error}</div>"
                    ])->textInput()->label();
                    ?>
                
                    <?=
                    $form->field($model, 'income_tax_no', [
                        //'template' => "<div class='col-md-4 col-sm-4 col-xs-12 text-right'>{label}</div>\n<div class='col-md-8 col-sm-8 col-xs-12'>{input}\n{hint}\n{error}</div>"
                    ])->textInput()->label();
                    ?>
                    <a href="<?php echo Url::to(['/site/logout']); ?>" data-method="post" class="btn btn-outline-primary float-left btn-inline mb-50">Logout</a>
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary float-right btn-inline mb-50', 'name' => 'login-button']) ?>    
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>