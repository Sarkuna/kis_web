<?php

use yii\helpers\Html;
//use yii\helpers\HtmlPurifier;
//use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */

$this->title = $pages->page_title;
//$this->params['breadcrumbs'][] = ' <i class="fa fa-angle-right"></i>';
$this->params['breadcrumbs'][] = $this->title;
?>

<section id="grid-options" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <?= $pages->page_description ?>
                </div>
            </div>
        </div>
    </div>
</section>